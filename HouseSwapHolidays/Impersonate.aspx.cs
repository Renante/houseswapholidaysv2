﻿using HouseSwapHolidays.Core.Attributes;
using HouseSwapHolidays.Core.Models.Utilities;
using HouseSwapHolidays.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HouseSwapHolidays
{
    public partial class Impersonate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var isAdmin = MemberService.Instance.isAdmin();
            var memberid = int.Parse(Request.QueryString["id"]);
            var email = Request.QueryString["email"];
            
            if (isAdmin)
            {
                var umbracoHelper = new Umbraco.Web.UmbracoHelper(Umbraco.Web.UmbracoContext.Current);
                FormsAuthentication.SignOut();
                FormsAuthentication.SetAuthCookie(string.Format("{0}_{1}_{2}", memberid, "admin", email), false);
                Response.Redirect(umbracoHelper.NiceUrl(Constants.NodeId.MyProfile));
            }
            else
            {
                Response.Redirect("/");
            }
        }
    }
}