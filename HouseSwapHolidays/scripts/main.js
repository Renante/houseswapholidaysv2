$(function () {

    $('body').on('click', '.send_enquiry', function () {
        var $parent = $(this).parent().parent();
        var msg = $parent.find('textarea').val();
        var memberId = $(this).data('memberid');
        var homeId = $(this).data('homeid');
        var error = $(this).parent().parent().find('.error-required');

        
        console.log(msg);

        if (msg == "") {
            error.show();
            console.log(error);
            return;
        }

        $.ajax({
            method: 'post',
            url: "/Umbraco/Api/HomeEnquiryApi/Send",
            data: {
                memberId: memberId,
                homeId: homeId,
                message: msg
            },
            success: function (result) {
                error.hide();
                $parent.find('.form-success').removeClass('hide');

                $('#html5-close').click();
                PopupSuccess('Success', 'Your Enquiry has been sent!');

                //setTimeout(function () { $('#html5-close').click() }, 1500);
            }
        });
    });

    $('.delete-favorite').click(function (e) {
        e.preventDefault();
        var id = $(this).data('id');

        $.ajax({
            method: 'get',
            url: "/Umbraco/Api/MemberShortListApi/Delete",
            data: {
                shortListId: id
            },
            success: function (result) {
                window.location.href = "/my-profile";
            }
        });
    });


    $('#homedetails-country').change(function () {
        if($(this).val() != ""){
            $.ajax({
                method: 'get',
                url: "/Umbraco/Api/UtilitiesApi/GetRegionsForCountry",
                data: {
                    countryCode: $(this).val()
                },
                success: function(result){
                    var str = "";
                
                    str = str + "<select id=\"homedetails-region-dd\" class=\"region\" name=\"region\" style=\"float:none;\">";
                    str = str + "<option value=\"00\">Please select...</option>";
                    $.each(result, function(key, value){
                        str = str + "<option value=\"" + value.RegionCode + "\">" + value.Name + "</option>";
                    });
                    str = str + "</select>";
                
                    $('#homedetails-region').html(str);
                    $('#homedetails-region-dd').msDropdown();
                }
            });
        }
    });


    $('#pop_up_enquiry input#enquire-now').click(function () {

        var $msg = $('#pop_up_enquiry').find('#enquiry_msg');
        
        var homeid = $('#homeid').val();
        var memberid = $('#memberid').val();
        var message = $('#message').val();

        console.log('homeid: ' + homeid);
        console.log('memberid: ' + memberid);
        console.log('message: ' + message);

        if (homeid == "" || memberid == "") {
            return;
        }

        if (message == "") {
            $msg.removeClass('hide');
            return;
        }
        else
            $msg.addClass('hide');

        $.ajax({
            method: 'post',
            url: "/Umbraco/Api/HomeEnquiryApi/Send",
            data: {
                memberId: memberid,
                homeId: homeid,
                message: message
            },
            success: function (result) {
                
                $('#pop_up_enquiry').trigger('close');
                PopupSuccess('Success', 'Your enquiry has been sent!');
            }
        });
        
    });

    $('#pop-inbox-msg-submit').click(function () {
        var $form = $('#pop-inbox-msg');
        var memberid = $form.find('#pop-msg-memberid').val();
        var homeid = $form.find('#pop-msg-homeid').val();
        var message = $form.find('#pop-msg-message').val();
        var msg = $form.find('.msg');

        if (message == "") {
            msg.addClass('error');
            msg.html('Message field is required!');
        }
        else {
            msg.html('');
            $.ajax({
                method: 'post',
                url: "/Umbraco/Api/HomeEnquiryApi/Send",
                data: {
                    memberId: memberid,
                    homeId: homeid,
                    message: message
                },
                success: function (result) {
                    var success = result.Success;
                    var message = result.Message;

                    if (success) {
                        $('#pop-inbox-msg').trigger('close');
                        PopupSuccess('Success','Your enquiry has been sent!');
                    }
                    else
                    {
                        msg.addClass('error')
                    }

                    msg.html(message);

                }
            });
        }

    });

    $('.show-pop-msg').click(function () {

        // clean up unwanted text
        $('#pop-inbox-msg .msg').html('');
        $('#pop-inbox-msg textarea').val('');

        var $this = $(this);
        var id = $this.data('id');
        var message = $this.data('message');
        var to = $this.data('to');
        var from = $this.data('from');
        var memberid = $this.data('memberid');
        var homeid = $this.data('homeid');
        var parent = $(this).parent().parent();
        var isnotreviewed = parent.hasClass('fred_smith');

        var $target = $('#pop-inbox-msg');
        $target.find('.msg-to-name').html(to);
        $target.find('.msg-from-name').html(from);
        $target.find('.msg-message').html(message);
        $target.find('#pop-msg-memberid').val(memberid);
        $target.find('#pop-msg-homeid').val(homeid);
        $target.find('#msg-view-property').prop('href', '/view-home/?id=' + homeid);
        $target.find('.msg-history').prop('href', '/my-profile/message-history/?id=' + memberid);

        $('#pop-inbox-msg').lightbox_me({
            centered: true
        });

        if(isnotreviewed) {
            $.ajax({
                method: 'get',
                url: "/Umbraco/Api/HomeEnquiryApi/SetHomeEnquiryToReviewed",
                data: {
                    homeEnquiryId: id
                },
                success: function (result) {
                    if (result) {
                        parent.removeClass('fred_smith');
                    }
                }
            });
        }


    });
    // auto open message
    if ((typeof global) !== 'undefined') {
        $("a.show-pop-msg[data-id='" + global.activeMessage + "']").click();
    }

    $('#save-to-favourites').click(function (e) {
        e.preventDefault();
        var homeid = $(this).data('id');
        $.ajax({
            method: 'get',
            url: "/Umbraco/Api/MemberShortListApi/Save",
            data: {
                homeID: homeid
            },
            success: function (result) {
                if (result) {
                    location.href = "/saved-to-favourites/";
                }
            }
        });
    });

    $('.add-to-favourites').click(function (e) {
        e.preventDefault();
        var $this = $(this);
        var homeid = $this.data('id');
        $.ajax({
            method: 'get',
            url: "/Umbraco/Api/MemberShortListApi/Save",
            data: {
                homeID: homeid
            },
            success: function (result) {
                if (result) {
                    $this.html("Done!");
                    $this.attr('href', '');
                    $this.attr('style', 'cursor:default;text-decoration:none');
                }
            }
        });
    });

    $('#pop-forgot-password-submit').click(function () {
        var email = $('#pop-forgot-password-email').val();
        var form = $('#pop-forgot-password');
        
        if (email == "") {
            form.find('.error-required').show();
        }
        console.log(email);
        $.ajax({
            method: 'get',
            url: "/Umbraco/Api/AccountApi/SendAndSavePasswordResetKey",
            data: {
                email: email
            },
            success: function (result) {
                var success = result.Success;
                var message = result.Message;
                if (success) {
                    $('#pop-forgot-password').trigger('close');
                    PopupSuccess('Thank you', 'A verification code was sent to your inbox.');
                }
                else {
                    $('#pop-forgot-password').trigger('close');
                    PopupSuccess('Error', message);
                }
            }
        });
    });

    // SEARCH PAGES

    $('#search-btn').click(function () {

        var qstr = $.query;
        qstr = qstr.remove('pg');
        
        var country = $('#search-country').val();
        var selectedCountry = $('#search-country').find('option:selected');
        var selectedCountryType = selectedCountry.data('type');
        var start = $('#search-from').val();
        var end = $('#search-to').val();
        var keyword = $('#search-text').val();
        var open = $('#search-opentolastmin').parent('[class*="icheckbox"]').hasClass("checked");
        
        if (country !== "-1")

            if(selectedCountryType === 'c') {
                qstr = qstr.set('c', country);
                qstr = qstr.remove('r');
            }
            else {
                qstr = qstr.set('c', "AU");
                qstr = qstr.set('r', country);
            }
            
        else {
            qstr = qstr.remove('c');
            qstr = qstr.remove('r');
        }

        if (start !== "-1")
            qstr = qstr.set('s', start);
        else
            qstr = qstr.remove('s');

        if (end !== "-1")
            qstr = qstr.set('e', end);
        else
            qstr = qstr.remove('e');
        
        if (keyword !== "") 
            qstr = qstr.set('k', keyword);
        else
            qstr = qstr.remove('k');
        
        if (open)
            qstr = qstr.set('lst', '1');
        else
            qstr = qstr.remove('lst');

        window.location.href = "/view-homes/" + qstr;

    });

    $('#search-relevance').change(function () {
        var val = $(this).val();
        var newqstr = $.query.set('sf', val);
        location.href = '/view-homes/' + newqstr;
    });

    $('#view-by-summary').click(function (e) {
        e.preventDefault();
        window.location.href = '/view-homes/' + $.query.remove('vm');
        
    });

    $('#view-by-detail').click(function (e) {
        e.preventDefault();
        window.location.href = '/view-homes/' + $.query.set('vm', 'd');
    });

    $("#view-by-map").click(function(e) {
        e.preventDefault();
        window.location.href = '/view-homes/' + $.query.set('vm', 'm');
    });

    $('#reverse-search').click(function (e) {
        $('#pop-reverse-search').lightbox_me({
            centered: true
        });
        e.preventDefault();
    });

    $('.popup-contact').click(function (e) {
        e.preventDefault();
        $('#pop-contactus').lightbox_me({
            centered:true
        });
    });
    
    

    $('#pop-rs-update').click(function () {
        var $form = $('#pop-reverse-search');
        var country = $form.find('#pop-rs-country');
        var city = $form.find('#pop-rs-city');
        
        var qstr = $.query;
        if (country.val() != "") {
            qstr = qstr.set('rc', country.val());
            var countrytext = country.find("option:selected").text();

            if(countrytext != "Select your country"){
                qstr = qstr.set('rcb', countrytext);
            }

            if (city.val() != "") {
                qstr = qstr.set('rk', city.val());
            }
        }
        else {
            qstr = qstr.set('rc', 0);
        }
        
        //console.log(qstr.toString());
        location.href = '/view-homes/' + qstr;

    });

    $('#reg-step1').click(function (e) {
        $('.error-required').hide();
        var error = false;

        var hasIntroduction = $('textarea[name=introduction]').val();
        if (!hasIntroduction) {
            $('#error-introduction').show();
            $('textarea[name=introduction]').focus();
            error = true;
        }

        if (error) {
            e.preventDefault();
            return;
        }
        else {
            $(this).submit();
        }
        

    });

    $('#reg-step3').click(function (e) {
        
        $('.error-required').hide();

        var error = false;

        var hasCountry = $('select[name=country]').val() != "";
        if (!hasCountry) {
            $('#error-country').show();
            $('select[name=country]').focus();
            error = true;
        }

        var hasCity = $('input[name=city]').val() != "";
        if (!hasCity) {
            $('#error-city').show();
            $('input[name=city]').focus();
            error = true;
        }

        var region = $('select[name=region]').val();
        var hasRegion = region != "" && region != undefined && region != "00";
        if (!hasRegion) {
            $('#error-region').show();
            $('select[name=region]').focus();
            error = true;
        }

        var hasBedroom = $('input[name=bedrooms]').val();
        if (!hasBedroom) {
            $('#error-bedrooms').show();
            $('input[name=bedrooms]').focus();
            error = true;
        }

        var hasBathroom = $('input[name=bathrooms]').val();
        if (!hasBathroom) {
            $('#error-bathrooms').show();
            $('input[name=bathrooms]').focus();
            error = true;
        }

        var hasSleeps = $('input[name=sleep').val();
        if (!hasSleeps) {
            $('#error-sleeps').show();
            $('input[name=sleep').focus();
            error = true;
        }

        var hasMaxPeople = $('input[name=maxpeople]').val();
        if (!hasMaxPeople) {
            $('#error-maxpeople').show();
            $('input[name=maxpeople]').focus();
            error = true;
        }
        
        var hasAirport = $('input[name=airport]').val();
        if (!hasAirport) {
            $("#error-airport").show();
            $('input[name=airport]').focus();
            error = true;
        }

        var hasHomehighlights = $('textarea[name=homehighlights]').val();
        if (!hasHomehighlights) {
            $('#error-homehighlights').show();
            $('textarea[name=homehighlights]').focus();
            error = true;
        }

        var hasHomeabout = $('textarea[name=homeabout]').val();
        if (!hasHomeabout) {
            $('#error-homeabout').show();
            $('textarea[name=homeabout]').focus();
            error = true;
        }

        var hasHomeloc = $('textarea[name=homeloc]').val();
        if (!hasHomeloc) {
            $('#error-homeloc').show();
            $('textarea[name=homeloc]').focus();
            error = true;
        }

        var hasHomeinfo = $('textarea[name=homeinfo]').val();
        if (!hasHomeinfo) {
            $('#error-homeinfo').show();
            $('textarea[name=homeinfo]').focus();
            error = true;
        }

        var facilities = $('#ul-facilities').find('input[type=checkbox]');
        var hasFacilityChecked = false;
        $.each(facilities, function (index, i) {
            var isChecked = $(i).parent().hasClass('checked');
            if (isChecked) {
                hasFacilityChecked = true;
            }
        });
        if (!hasFacilityChecked) {
            $('#error-facilities').show();
            $('#ul-facilities').focus();
            error = true;
        }
        
        var activities = $('#ul-activities').find('input[type=checkbox]');
        var hasActivityChecked = false;
        $.each(activities, function (index, i) {
            var isChecked = $(i).parent().hasClass('checked');
            if (isChecked) {
                hasActivityChecked = true;
            }
        });
        if (!hasActivityChecked) {
            $('#error-activities').show();
            $('#ul-activities').focus();
            error = true;
        }

        var availability = $('#ul-availability').find('input[type=checkbox]');
        var hasAvailabilityChecked = false;
        $.each(availability, function (index, i) {
            var isChecked = $(i).parent().hasClass('checked');
            if (isChecked) {
                hasAvailabilityChecked = true;
            }
        });
        if (!hasAvailabilityChecked) {
            $('#error-availability').show();
            $('#ul-availability').focus();
            error = true;
        }

        if (error) {
            e.preventDefault();
            return;
        }
        else {
            $(this).submit();
        }
        
    });

    $('.page-item').click(function (e) {
        e.preventDefault();
        var val = $(this).data('id');

        location.href = "/view-homes/" + $.query.set('pg', val);
    });
   
    $('form#advance-query-form #show').on('ifChanged', function () {
        var isChecked = $(this).is(':checked');
        if (isChecked)
            $('.invited-block').removeClass('hide');
        else
            $('.invited-block').addClass('hide');
    });

    $('#advance-query-btn').click(function () {

        var qstr = $.query;
        var $parent = $('form#advance-query-form');
        var country = $('#search-country').val();

        var selectedCountry = $('#search-country').find('option:selected');
        var selectedCountryType = selectedCountry.data('type');

        if (country.length > 0) {
            if (selectedCountryType === 'c') {
                qstr = qstr.set('c', country);
            }
            else {
                qstr = qstr.set('c', "AU");
                qstr = qstr.set('r', country);
            }
        }
        
        var city = $('#city').val();
        if (city.length > 0) {
            qstr = qstr.set('k', city);
        }

        var from = $('#from').val();
        if (from > 0) {
            qstr = qstr.set('s', from);
        }

        var to = $('#to').val();
        if (to > 0) {
            qstr = qstr.set('e', to);
        }

        var show = $('#show').is(':checked');

        if(show){
            var country_b = $('#country_b').val();
            if (country_b.length > 0) {
                qstr = qstr.set('rc', country_b);
            }
        
            var city_b = $('#city_b').val();
            if (city_b.length > 0) {
                qstr = qstr.set('rk', city_b);
            }
        }


        // features
        var checkedFeatures = [];
        var features = $parent.find('input[name=f]');
        
        $.each(features, function (i, ele) {
            var val = $(ele).data('id');
            var isChecked = $(ele).is(':checked');

            if(isChecked){
                checkedFeatures.push(val);
            }

        });

        if (checkedFeatures.length > 0) {
            qstr = qstr.set('f', checkedFeatures.join(",").toString());
        }
        
        //console.log(qstr.toString());
        location.href = '/view-homes/' + qstr;


    });

    
    // TABS
    // custom code
    $('.oeleans-nav a').click(function (e) {
        e.preventDefault();
        
        var $tabheaders = $('.oeleans-nav a');
        var $this = $(this);
        var $target = $this.data('target');
        var $tabcontent = $('.oeleans-section');

        $tabheaders.removeClass('active');
        $this.addClass('active');

        $tabcontent.addClass('hide');
        $('#' + $target).removeClass('hide');

    });



    $('#pop-status-ok').click(function () {
        $('#pop-status').trigger('close');
    });


    $('textarea#homehighlights').keyup(function () {
        var target = $('#char-count');
        var charCount = $(this).val().length;
        var charLeft = 150 - charCount;
        target.html(charLeft);
    });

    $('span[rel=tipsy], a[rel=tipsy]').tipsy({ gravity: 's' });


    // Admin
    $('#admin-member-search #submit').click(function () {
        var adminmembersearch = $('#admin-member-search');
        var memberid = adminmembersearch.find('input[name=memberid]');
        var homeid = adminmembersearch.find('input[name=homeid]');
        var firstname = adminmembersearch.find('input[name=firstname]');
        var givenname = adminmembersearch.find('input[name=givenname]');
        var emailaddress = adminmembersearch.find('input[name=emailaddress]');
        var showcurrentonly = adminmembersearch.find('input[name=showcurrentonly]');
        var results = $('#admin-member-results');
        var recordcount = $('#record-count');
        

        var summaryurl = $('#summary-url').val();
        var sendemailurl = $('#send-email-url').val();

        $.ajax({
            method: 'post',
            url: "/Umbraco/Api/AdminApi/SearchMembers",
            data: {
                memberid: memberid.val(),
                homeid: homeid.val(),
                firstname: firstname.val(),
                givenname: givenname.val(),
                emailaddress: emailaddress.val(),
                showcurrentonly: showcurrentonly.is(':checked')
            },
            success: function (result) {

                $('#admin-member-results').html('');
                
                $.each(result, function (index, item) {

                    var createdDate = item.DateJoined;
                    var lastLoggedInDate = item.LastLoggedIn;
                    var ele = "";
                    ele = ele + "<ul>";
                    ele = ele +     "<li><span><a href='" + summaryurl + "?id=" + item.ID + "' target='_blank'>" + item.Name + "</a></span></li>";
                    ele = ele +     "<li><span><a href='" + sendemailurl + "?id=" + item.ID + "' target='_blank'>" + item.Email +  "</a></span></li>";
                    ele = ele +     "<li><small>" + item.DateJoined + "</small></li>";
                    ele = ele +     "<li><small>" + item.LastLoggedIn + "</small></li>";
                    ele = ele +     "<li><small>" + item.MemberStatus + "</small></li>";
                    ele = ele + "</ul>";
                    ele = ele + "<div class='clear'></div>";
                    results.append(ele);
                });
                
                recordcount.html(result.length);
            },
            error: function () {
                console.log('error on ajax');
            }

        });

    });



    //$('form#editDetails');

    $('input#updatepassword').on('ifChanged', function (event) {
        var isChecked = $(this).is(':checked');
        var $form = $('form#editDetails');
        $form.find('#password').val('');
        $form.find('#confirmpassword').val('');

        var $target = $('.update-password');
        if (isChecked)
            $target.removeClass('hide');
        else
            $target.addClass('hide');

    });


    

    
    $(document).ajaxStart(function () {
        $('.ajax-loading').removeClass('hide');
    })
    .ajaxStop(function () {
        $('.ajax-loading').addClass('hide');
    });

    $('#edit-details-btn').click(function () {
        
        // elements
        var $form = $('form#editDetails');
        var $firstname = $form.find('#firstname');
        var $lastname = $form.find('#lastname');
        var $emailaddress = $form.find('#emailaddress');
        var $confirmemail = $form.find('#confirmemail');
        var $updatepassword = $form.find('#updatepassword');
        var $password = $form.find('#password');
        var $confirmpassword = $form.find('#confirmpassword');
        var $introduction = $form.find('#introduction');
        var $language1 = $form.find('#language1');
        var $acceptterms = $form.find('#acceptterms');

        //error wrappers
        var $errFirstname = $form.find('#err-firstname');
        var $errLastname = $form.find('#err-lastname');
        var $errEmail = $form.find('#err-emailaddress');
        var $errConfirmEmail = $form.find('#err-confirmemail');
        var $errEmailNotMatch = $form.find('#err-emailnotmatch');
        var $errPass = $form.find('#err-password');
        var $errConfirmPass = $form.find('#err-confirmpassword');
        var $errPassNotMatch = $form.find('#err-passnotmatch');
        var $errIntro = $form.find('#err-introduction');
        var $errLang1 = $form.find('#err-language1');
        var $errTerms = $form.find('#err-acceptterms');
        
        var updatePassword = $updatepassword.is(':checked');
        var acceptTerms = $acceptterms.is(':checked');

        //do the validation
        var err = false;
        if ($firstname.val() === '') {
            $errFirstname.show();
            $firstname.focus();
            err = true;
        }
        else
            $errFirstname.hide();

        if ($lastname.val() === '') {
            $errLastname.show();
            $lastname.focus();
            err = true;
        }
        else
            $errLastname.hide();

        if ($emailaddress.val() === '') {
            $errEmail.show();
            $emailaddress.focus();
            err = true;
        }
        else
            $errEmail.hide();

        if ($confirmemail.val() === '') {
            $errConfirmEmail.show();
            $confirmemail.focus();
            err = true;
        }
        else
            $errConfirmEmail.hide();

        if ($emailaddress.val() !== '' && $confirmemail.val() !== '' &&
            ($emailaddress.val() !== $confirmemail.val())) {
            $errEmailNotMatch.show();
            $confirmemail.focus();
            err = true;
        }
        else
            $errEmailNotMatch.hide();

        if (updatePassword) {

            if ($password.val() === '') {
                $errPass.show();
                $password.focus();
                err = true;
            }
            else
                $errPass.hide();

            if ($confirmpassword.val() === '') {
                $errConfirmPass.show();
                $confirmpassword.focus();
                err = true;
            }
            else
                $errConfirmPass.hide();

            if ($password.val() !== '' && $confirmpassword.val() !== '' &&
                ($password.val() !== $confirmpassword.val())) {
                $errPassNotMatch.show();
                err = true;
                $confirmpassword.focus();

            }
            else
                $errPassNotMatch.hide();

        }


        if ($introduction.val() === '') {
            $errIntro.show();
            $introduction.focus();
            err = true;
        }
        else
            $errIntro.hide();

        if ($language1 === 'Not Specified') {
            $errLang1.show();
            err = true;
        }
        else
            $errLang1.hide();

        if (!acceptTerms) {
            $errTerms.show();
            err = true;
        }
        else
            $errTerms.hide();

        if (err) 
            return;
        else
            $form.submit();

    });


});


// RTE Hacks
$(function() {
    $.each($(".promobox1 ul li"), function(i, el) {
        $(el).prepend($("<i class=\"fa fa-check-square\"></i>"));
    });

});