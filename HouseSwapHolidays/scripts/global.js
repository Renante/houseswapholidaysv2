﻿$(function() {
    $('#sign-out').click(function (e) {
        e.preventDefault();
        $.ajax({
            method: 'post',
            url: "/Umbraco/Api/AuthenticateApi/SignOut",
            data: { test: 'test'},
            success: function (result) {
                window.location.href = "/";
            }
        });
    });

    $(".tipsyme").tipsy();

    $(".location-autocomplete").click(function() {
        $(this).val('');    
    });
    
});

$(function() {

        var formError = $("#signInForm").find(".form-error");

        $("#signInForm").validate({
            rules: {
                emailaddress: {
                    required: true,
                    email: true,
                    remote: "/umbraco/api/accountapi/isemailexist"
                },
                password: "required"
            },
            messages: {
                emailaddress: {
                    remote: "Email is not existing."
                }
            },

            submitHandler: function(form) {
                $.ajax({
                    method: "post",
                    url: "/Umbraco/Api/AuthenticateApi/Login",
                    data: {
                        emailAddress: form.emailaddress.value,
                        password: form.password.value
                    },
                    success: function(result) {
                        if (result.Success) {
                            window.location.href = result.RedirectUrl;
                        } else {
                            formError.removeClass("hide");
                            formError.html(result.Message);
                        }
                    },
                    error: function(result) {
                        console.log("error!");
                        console.log(result);
                    }
                });
                return false;
            }
        });

        $("#signInForm input").focus(function() {
            formError.addClass("hide");
            formError.html("");
        });

    });

    $(function() {
        $("#forgot_pass").validate({

            // Specify the validation rules
            rules: {
                email: {
                    required: true,
                    email: true
                }
            },

            // Specify the validation error messages
            messages: {
                email: "Please enter valid email"
            },

            submitHandler: function(form) {
                form.submit();
                //$.ajax({
                //    type: $(form).attr('method'),
                //    url: $(form).attr('action'),
                //    data: $(form).serialize(),

                //}).done(function(response) {
                //    $("#pass_forgot").val("");
                //    /* $("#success_msg").css('display', 'block');
                //             setTimeout($("#success_msg").css('none', 'block'), 2000);*/
                //});
                //return false;
            }
        });
    });
    $(function() {
        $("#locationFieldSubmit").click(function(e) {
            e.preventDefault();
            $("#quickSearchForm").submit();
        });
    });