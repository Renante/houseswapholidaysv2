$(document).ready(function () {

    if ($('#back-to-top').length) {
        var scrollTrigger = 100,  //px 
        backToTop = function () {
            var scrollTop = $(window).scrollTop();
            if (scrollTop > scrollTrigger) {
                $('#back-to-top').addClass('show');
            } else {
                $('#back-to-top').removeClass('show');
            }
        };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $('#back-to-top').on('click', function (e) {
            e.preventDefault();
            $('html , body').animate({ scrollTop: 0 }, 1200);
        });
    }

    $('.home_bnr').slick({
        dots: false,
        arrows: false,
        infinite: true,
        autoplay: true,
        speed: 200,
        fade: true,
        cssEase: 'linear'
    });


    $('.ftr_carousal').slick({
        dots: true,
        infinite: false,
        arrows: false,
        speed: 800,
        autoplay: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [
          {
              breakpoint: 1024,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  infinite: true,
                  dots: true
              }
          },
          {
              breakpoint: 600,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
              }
          },
          {
              breakpoint: 480,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
              }
          }
          // You can unslick at a given breakpoint now by adding:
          // settings: "unslick"
          // instead of a settings object
        ]
    });





});


$(document).ready(function () {
    $("#forgotPass").click(function () {
        $('#login').modal('hide');
    });

});

$(window).load(function () {
    equalheight('.eql');
});

$(function () {
    var cubuk_seviye = $(document).scrollTop();
    var header_yuksekligi = $('.yapiskan').outerHeight();

    $(window).scroll(function () {
        var kaydirma_cubugu = $(document).scrollTop();

        if (kaydirma_cubugu > header_yuksekligi) { $('.yapiskan').addClass('gizle'); }
        else { $('.yapiskan').removeClass('gizle'); }

        if (kaydirma_cubugu > cubuk_seviye) { $('.yapiskan').removeClass('sabit'); }
        else { $('.yapiskan').addClass('sabit'); }

        cubuk_seviye = $(document).scrollTop();
    });
});


//$(window).resize(function(){
//  equalheight('.eql');
//});

//$(window).resize(function(){
//  $(window).load(function(){
//    equalheight('.eql');
//});
//});


$(window).scroll(function () {
    var sticky = $('.header_inn'),
      scroll = $(window).scrollTop();
    if (scroll >= 10) sticky.addClass('top');
    else sticky.removeClass('top');
});


$(window).on("load scroll resize ", function () {

    var winHeight = $(window).height();
    var winwidth = $(window).width();

    if ($(this).scrollTop() < 10 && winwidth >= 800) {
        $('.hdr_bttm.bttm_header').addClass('fixed-header');
    }
    else {
        $('.hdr_bttm.bttm_header').removeClass('fixed-header');

    }

});


$("#newsletters_form input[type=submit]").click(function() {
    if (typeof ga !== "undefined") { //check universal
        //ga("send", "event", cat, event, label);
        ga("send", "event", "Join us", "submit", "Join us submit");
        console.log("ga event triggered");
    }
    else if (typeof _gaq !== "undefined") { //check traditional
        //_gaq.push(["_trackEvent", cat, event, label]);
        _gaq.push(["_trackEvent", "Join us", "submit", "Join us submit"]);
        console.log("_gaq event triggered");
    }
    else {
        console.log("ga not defined!");
    }
});