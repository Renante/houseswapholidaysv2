﻿using Common;
using HouseSwapHolidays.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HouseSwapHolidays
{
    public partial class ProcessingMembership : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (User.Identity.IsAuthenticated)
            {

                var currentMemberId = HouseSwapHolidays.Core.Services.MemberService.Instance.GetCurrentMemberId();
                var member = MemberDAL.Instance.Get(currentMemberId);
                var umbracoHelper = new Umbraco.Web.UmbracoHelper(Umbraco.Web.UmbracoContext.Current);

                // subscribe to campaign monitor
                if (member.ReceiveNewsletter)
                {
                    CampaignMonitorService.Instance.Subscribe(member.EmailAddress);
                }

                // send welcome email
                // make sure member is new, or else this can page can spam if somebody multiple access this page.
                var isMemberNew = member.Status != Status.Active && member.Status != Status.Draft;
                if (isMemberNew) { 
                    var template =
                        EmailTemplateService.Instance.Get(HouseSwapHolidays.Core.Models.Items.EmailTemplate.WelcomeEmail);
                    template = template.Replace("[firstname]", member.FirstName);
                    template = template.Replace("[member email address]", member.EmailAddress);
                    template = template.Replace("[loginlink]", WebConfigurationManager.AppSettings["WebsiteUrl"] + "/?fp=y" );
                    EmailService.Instance.Send(member.EmailAddress, member.FirstName,
                        "Welcome to " + WebConfigurationManager.AppSettings["WebsiteName"], template);
                }

                // activate member
                member.Status = Status.Draft;
                MemberDAL.Instance.Save(member);

                Response.Redirect(umbracoHelper.Url(HouseSwapHolidays.Core.Models.Utilities.Constants.StepNodeId.Step1));

            }
            else
                Response.Redirect("/");
        }
    }
}