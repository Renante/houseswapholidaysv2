﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Configuration;
using Common;

public partial class system_ActivityMonitor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int timeInterval = 60;

        // Check if an interval is specified. If not then default to 60 minutes
        if (Request.QueryString["interval"] != null)
        {
            timeInterval = Int32.Parse(Request.QueryString["interval"]);
        }

        DateTime fromDate = DateTime.Now.AddMinutes(timeInterval);

        Response.Write(fromDate.ToString());

        // Check if new members have joined, and if they have send an alert
        CheckMemberStatusAndNotify(fromDate);

        // Check if there are pending messages to appove, and if there are generate an alert
        CheckMessageStatusAndNotify(fromDate);

    }

    private void CheckMessageStatusAndNotify(DateTime fromDate)
    {
        HomeEnquiryCollection homeEnquiryCollection = HomeEnquiryDAL.Instance.GetAllHomeEnquiries(-1, -1, true, -1, fromDate.ToString("dd MMM yyyy hh:mm "));

        if (homeEnquiryCollection.Count > 0)
        {
            // Send off a notification email that we have messages pending approval

            string subjectLine = String.Format("Messages pending approval have been received");

            string emailBody = "Visit <a href=\"www.christianhomeswap.com/admin/members/pendingmessages.aspx\">www.christianhomeswap.com/admin/members/pendingmessages.aspx</a> to view";

            try
            {
                string alertEmailReceipient = ConfigurationSettings.AppSettings["AlertEmailRecipient"];
                GenerateEmail.Instance.SendEmail(alertEmailReceipient, alertEmailReceipient, "Message Manager", "info@christianhomeswap.com", subjectLine, emailBody, 1);
            }
            catch (Exception ex)
            {
                _log.Error("Error sending pending message notification email", ex);
            }
        }
    }

    private void CheckMemberStatusAndNotify(DateTime fromDate)
    {
        // Get new members (if any)
        MemberCollection newMembers = MemberDAL.Instance.GetAllRecentMembers(fromDate.ToString("dd MMM yyyy hh:mm "));

        // Calculate new member total
        int newMemberCount = 0;
        if (newMembers.Count > 0)
        {
            newMemberCount = newMembers.Count;
        }

        StringBuilder memberList = new StringBuilder();

        memberList.Append("<br>");
        memberList.Append("<strong>Member Details:</strong>");
        memberList.Append("<table>");
        foreach (Member member in newMembers)
        {
            Response.Write(string.Format("<br/>MemberID: {0}, Member Name: {1}", member.ID, member.FirstName + ' ' + member.GivenName));
            memberList.Append(String.Format("<tr><td>Member ID : </td><td>{0}</td></tr>", member.ID.ToString()));
            memberList.Append(String.Format("<tr><td>Name : </td><td>{0} {1}</td></tr>", member.FirstName, member.GivenName));
        }
        memberList.Append("</table>");

        // Get new home listings (if any)
        HomeCollection newHomes = HomeDAL.Instance.GetAllRecentHomes(fromDate.ToString("dd MMM yyyy hh:mm "));

        StringBuilder homeList = new StringBuilder();

        foreach (Home home in newHomes)
        {
            Channel channel = ChannelDAL.Instance.Get(home.DefaultChannel);

            homeList.Append("<br/>");
            homeList.Append("<strong>Home details:</strong>");
            homeList.Append("<table>");

            if (home.ThumbnailURL.Length > 0)
            {
                homeList.Append("<tr><td><table>");
                homeList.Append(String.Format("<tr><td>Channel: </td><td>{0}</td></tr>", channel.Name));
                homeList.Append(String.Format("<tr><td>Location: </td><td>{0}</td></tr>", home.Location));
                homeList.Append(String.Format("<tr><td><a href = 'http://www.christianhomeswap.com/home/view.aspx?homeid={0}'>View home details</a></td><td>&nbsp;</td></tr>", home.ID.ToString()));
                homeList.Append("</table></td><td>");
                homeList.Append(String.Format("<img src=\"http://www.christianhomeswap.com{0}\">", home.ThumbnailURL));
                homeList.Append("</td></tr>");
                homeList.Append("</table>");
            }
            else
            {
                homeList.Append(String.Format("<tr><td>Channel: </td><td>{0}</td></tr>", channel.Name));
                homeList.Append(String.Format("<tr><td>Location: </td><td>{0}</td></tr>", home.Location));
                homeList.Append(String.Format("<tr><td>&nbsp;</td><td><a href = 'http://www.christianhomeswap.com/home/view.aspx?homeid={0}'>View details</a></td></tr>", home.ID.ToString()));
            }

            homeList.Append("</table>");

            homeList.Append("<table>");
            homeList.Append(String.Format("<tr><td>Description: </td><td>{0}</td></tr>", home.Description));
            homeList.Append(String.Format("<tr><td>Region Description:</td><td>{0}</td></tr>", home.RegionDescription));
            homeList.Append(String.Format("<tr><td>Other Information:</td><td>{0}</td></tr>", home.OtherInformation));
            homeList.Append("</table>");
        }

        // Calculate new home total
        int newHomeCount = 0;
        if (newHomes.Count > 0)
        {
            newHomeCount = newMembers.Count;
        }

        string subjectLine = String.Format("Status report {0} : m - {1} h - {2}", DateTime.Now.ToString(), newMemberCount.ToString(), newHomeCount.ToString());

        //Response.Write(subjectLine);

        string emailBody = memberList + "<br>" + homeList;
        //Response.Write(emailBody);

        if (newHomeCount > 0 || newMemberCount > 0)
        {
            try
            {
                string alertEmailReceipient = ConfigurationSettings.AppSettings["AlertEmailRecipient"];
                GenerateEmail.Instance.SendEmail(alertEmailReceipient, alertEmailReceipient, "Administrator", "info@christianhomeswap.com", subjectLine, emailBody, 1);
            }
            catch (Exception ex)
            {
                _log.Error("Error sending activity monitor notification email", ex);
            }
        }
    }

    private log4net.ILog _log = log4net.LogManager.GetLogger(typeof(system_ActivityMonitor));


}