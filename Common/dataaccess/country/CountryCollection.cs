using System;
using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for CountryCollection.
	/// </summary>
	public class CountryCollection : CollectionBase
	{
		public CountryCollection()
		{
		}

		public void Add(Country country)
		{
			InnerList.Add(country);
		}

		public void Remote(Country country)
		{
			InnerList.Remove(country);
		}

		public Country this[int index]
		{
			get
			{
				return (Country)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}
	
	}
}
