using System;

namespace Common
{
	/// <summary>
	/// Summary description for Country.
	/// </summary>
	public class Country
	{
		public Country()
		{
		}

		public int ID
		{
			get
			{
				return _id;
			}
			set
			{
				_id = value;
			}
		}

		public string Code
		{
			get
			{
				return _code;
			}
			set
			{
				_code = value;
			}
		}

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		public bool IsShortList
		{
			get
			{
				return _isShortList;
			}
			set
			{
				_isShortList = value;
			}
		}

		public bool HasListings
		{
			get
			{
				return _hasListings;
			}
			set
			{
				_hasListings = value;
			}
		}

		private int _id;
		private string _code;
		private string _name;
		private bool _isShortList;
		private bool _hasListings;
	}
}
