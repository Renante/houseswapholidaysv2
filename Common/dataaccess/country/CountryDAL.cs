using System;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;

namespace Common
{
    /// <summary>
    /// Summary description for CountryDAL.
    /// </summary>
    public class CountryDAL : DataAccess
    {
        #region Types
        private sealed class CountryFields
        {
            public readonly int CountryID = -1;
            public readonly int Code = -1;
            public readonly int Name = -1;
            public readonly int IsShortlist = -1;
        }
        #endregion

        public CountryDAL()
        {
        }

        public Country GetCountryByName(string countryName)
        {
            const string spName = "up_GetCountryByName";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, countryName))
                {
                    if (reader.Read())
                    {
                        Country country = new Country();

                        country.ID = GetFieldByInteger(reader, "CountryID");
                        country.Code = GetFieldByString(reader, "Code");
                        country.Name = GetFieldByString(reader, "Name");
                        country.IsShortList = GetFieldByBoolean(reader, "IsShortList");

                        return country;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        public Country GetCountryByCode(string countryCode)
        {
            return GetCountryByCode(countryCode, true);
        }

        public Country GetCountryByCode(string countryCode, bool useCache)
        {
            CacheManager cacheManager = new CacheManager();

            if (useCache)
            {
                // Check if we can obtain it from the cache
                CountryCollection countryCollection = CountryDAL.Instance.GetAll();

                foreach (Country country in countryCollection)
                {
                    if (country.Code == countryCode)
                    {
                        return country;
                    }
                }
            }
            else
            {
                const string spName = "up_GetCountryByCode";

                using (var connection = GetConnection())
                {
                    using (SqlDataReader reader = ExecuteReader(spName, connection, countryCode))
                    {
                        if (reader.Read())
                        {
                            Country country = new Country();

                            country.ID = GetFieldByInteger(reader, "CountryID");
                            country.Code = GetFieldByString(reader, "Code");
                            country.Name = GetFieldByString(reader, "Name");
                            country.IsShortList = GetFieldByBoolean(reader, "IsShortList");

                            return country;
                        }
                    }
                }
            }

            return null;
        }

        public CountryCollection GetAll()
        {
            return GetAll(false);
        }

        public CountryCollection GetAll(bool flushCache)
        {
            CacheManager cacheManager = new CacheManager();
            CountryCollection countryCollection = new CountryCollection();

            if (!flushCache)
            {
                // Check if we can obtain it from the cache

                countryCollection = (CountryCollection)cacheManager.Get("countryCollection");
            }

            if ((flushCache) || (countryCollection == null))
            {

                countryCollection = new CountryCollection();

                const string spName = "up_GetAllCountries";

                using (var connection = GetConnection())
                {
                    using (SqlDataReader reader = ExecuteReader(spName, connection, true))
                    {
                        while (reader.Read())
                        {

                            Country country = new Country();

                            country.ID = GetFieldByInteger(reader, "CountryID");
                            country.Code = GetFieldByString(reader, "Code");
                            country.Name = GetFieldByString(reader, "Name");
                            country.IsShortList = GetFieldByBoolean(reader, "IsShortList");

                            // Add to collection
                            countryCollection.Add(country);
                        }

                        // Save country collection to cache
                        cacheManager.Set("countryCollection", countryCollection);
                    }
                }
            }

            return countryCollection;

        }

        public CountryCollection GetAllByShortlist()
        {
            return (CountryCollection)GetAllByShortlist(true, false);
        }

        public CountryCollection GetAllByShortlist(bool isShortlist)
        {
            return (CountryCollection)GetAllByShortlist(isShortlist, false);
        }

        /*
        public CountryCollection GetAllByShortlist(bool isShortlist, bool flushCache)
        {
            CacheManager cacheManager = new CacheManager();
            CountryCollection countryCollection = new CountryCollection();

            if (!flushCache)
            {
                // Check if we can obtain it from the cache

                countryCollection = (CountryCollection)cacheManager.Get("shortlistCountryCollection");
            }

            if ((flushCache) || (countryCollection == null))
            {

                countryCollection = new CountryCollection();

                const string spName = "up_GetCountriesByShortlist";

                using (DataSet dataSet = ExecuteDataset(spName, isShortlist))
                {
                    DataTable countryList = dataSet.Tables[0];

                    for (int i=0; i<=countryList.Rows.Count-1; i++)
                    {
                        Country country = new Country();

                        country.ID = GetFieldByInteger(countryList.Rows[i], "CountryID");
                        country.Code = GetFieldByString(countryList.Rows[i], "Code");
                        country.Name = GetFieldByString(countryList.Rows[i], "Name");
                        country.IsShortList = GetFieldByBoolean(countryList.Rows[i], "IsShortList");

                        // Add to collection
                        countryCollection.Add(country);
                    }

                    // Save country collection to cache
                    cacheManager.Set("shortlistCountryCollection", countryCollection);
					
                    countryList.Dispose();
                    dataSet.Dispose();
                }
            }
            else
            {
                // All countries are in the cache, so lets just filter out the shortlist
            }

            return countryCollection;
        }
        */

        public CountryCollection GetAllByShortlist(bool isShortlist, bool flushCache)
        {
            CacheManager cacheManager = new CacheManager();
            CountryCollection countryCollection = new CountryCollection();

            if (!flushCache)
            {
                // Check if we can obtain it from the cache

                countryCollection = (CountryCollection)cacheManager.Get("shortlistCountryCollection");
            }

            if ((flushCache) || (countryCollection == null))
            {

                countryCollection = new CountryCollection();

                const string spName = "up_GetCountriesByShortlist";

                using (var connection = GetConnection())
                {
                    using (SqlDataReader reader = ExecuteReader(spName, connection, isShortlist))
                    {
                        while (reader.Read())
                        {

                            Country country = new Country();

                            country.ID = (int)reader["CountryID"];
                            country.Code = (string)reader["Code"];
                            country.Name = (string)reader["Name"];

                            // Retrieve shortlist. Set as false is blank
                            if (reader["IsShortlist"] == DBNull.Value)
                            {
                                country.IsShortList = false;
                            }
                            else
                            {
                                country.IsShortList = (bool)reader["IsShortList"];
                            }

                            // Add to collection
                            countryCollection.Add(country);
                        }

                        // Save country collection to cache
                        cacheManager.Set("shortlistCountryCollection", countryCollection);
                    }
                }
            }
            else
            {
                // All countries are in the cache, so lets just filter out the shortlist
            }

            return countryCollection;
        }

        public CountryCollection GetAllByDefault()
        {
            CountryCollection countryCollection = new CountryCollection();

            const string spName = "up_GetCountriesByDefault";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection))
                {
                    while (reader.Read())
                    {

                        Country country = new Country();

                        country.ID = GetFieldByInteger(reader, "CountryID");
                        country.Code = GetFieldByString(reader, "Code");
                        country.Name = GetFieldByString(reader, "Name");
                        country.HasListings = GetFieldByBoolean(reader, "HasListings");

                        // Retrieve shortlist. Set as false is blank
                        if (reader["IsShortlist"] == DBNull.Value)
                        {
                            country.IsShortList = false;
                        }
                        else
                        {
                            country.IsShortList = (bool)reader["IsShortList"];
                        }

                        // Add to collection
                        countryCollection.Add(country);
                    }
                }
            }

            return countryCollection;
        }

        public CountryCollection GetAllByDefault(int channelID)
        {
            CountryCollection countryCollection = new CountryCollection();

            const string spName = "up_GetCountriesByDefaultChannel";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, channelID))
                {
                    while (reader.Read())
                    {

                        Country country = new Country();

                        country.ID = GetFieldByInteger(reader, "CountryID");
                        country.Code = GetFieldByString(reader, "Code");
                        country.Name = GetFieldByString(reader, "Name");
                        country.HasListings = GetFieldByBoolean(reader, "HasListings");

                        // Retrieve shortlist. Set as false is blank
                        if (reader["IsShortlist"] == DBNull.Value)
                        {
                            country.IsShortList = false;
                        }
                        else
                        {
                            country.IsShortList = (bool)reader["IsShortList"];
                        }

                        // Add to collection
                        countryCollection.Add(country);
                    }
                }
            }

            return countryCollection;
        }

        public CountryCollection GetAllWithListings()
        {
            CountryCollection countryCollection = new CountryCollection();

            const string spName = "up_GetCountriesWithListings";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection))
                {
                    while (reader.Read())
                    {

                        Country country = new Country();

                        country.ID = GetFieldByInteger(reader, "CountryID");
                        country.Code = GetFieldByString(reader, "Code");
                        country.Name = GetFieldByString(reader, "Name");
                        country.HasListings = GetFieldByBoolean(reader, "HasListings");

                        // Retrieve shortlist. Set as false is blank
                        if (reader["IsShortlist"] == DBNull.Value)
                        {
                            country.IsShortList = false;
                        }
                        else
                        {
                            country.IsShortList = (bool)reader["IsShortList"];
                        }

                        // Add to collection
                        countryCollection.Add(country);
                    }
                }
            }

            return countryCollection;
        }

        private Country LoadFromReader(SqlDataReader reader, CountryFields fields)
        {
            Country country = new Country();

            country.ID = reader.GetInt32(fields.CountryID);
            country.Code = reader.GetString(fields.Code);
            country.Name = reader.GetString(fields.Name);
            country.IsShortList = reader.GetBoolean(fields.IsShortlist);

            return country;
        }

        #region Singleton instance
        public static CountryDAL Instance
        {
            [DebuggerStepThrough]
            get
            {
                if (_instance == null)
                {
                    lock (typeof(CountryDAL))
                    {
                        if (_instance == null)
                        {
                            _instance = new CountryDAL();
                        }
                    }
                }

                return _instance;
            }

        }

        #endregion

        private static CountryDAL _instance;

    }
}
