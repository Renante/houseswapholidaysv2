using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Reflection;
using Microsoft.ApplicationBlocks.Data;

namespace Common
{
	/// <summary>
	/// Base class for data access.
	/// </summary>
	public class DataAccess
	{
		/// <summary>
		/// The default number of retries.
		/// </summary>
		public const int DefaultRetries = 3;

		/// <summary>
		/// Protected constuctor, can't be created without a subclass.
		/// </summary>
		protected DataAccess()
		{
		}

		/// <summary>
		/// Gets a connection.
		/// </summary>
		/// <returns>An SQL Connection object</returns>
		/// <remarks>Uses the DBConnection value in config for it's connection string.</remarks>
		protected SqlConnection GetConnection()
		{
            
			return new SqlConnection(ConfigurationSettings.AppSettings["DBConnection"]);
		}

		/// <summary>
		/// Executes a stored procedure, returns a data set.
		/// </summary>
		/// <param name="sqlProcedureName">The stored procedure name</param>
		/// <param name="parameters">The stored procedure parameters</param>
		/// <returns>A DataSet</returns>
		/// <remarks>Has a built-in retry count of DefaultRetries</remarks>
		protected DataSet ExecuteDataset(string sqlProcedureName, params object[] parameters)
		{
			return ExecuteDataset(DefaultRetries, sqlProcedureName, parameters);
		}

		/// <summary>
		/// Executes a stored procedure, returns a data set.
		/// </summary>
		/// <param name="retries">The max number of retries</param>
		/// <param name="sqlProcedureName">The stored procedure name</param>
		/// <param name="parameters">The stored procedure parameters</param>
		/// <returns>A DataSet</returns>
		protected DataSet ExecuteDataset(int retries, string sqlProcedureName, params object[] parameters)
		{
			if (retries < 0)
			{
				throw new ArgumentException("Cannot execute a SQL data set, retries cannot be less than zero", "retries");
			}

			while (retries >= 0)
			{
				try
				{
                    using (var connection = GetConnection())
                    {
                        return SqlHelper.ExecuteDataset(connection, sqlProcedureName, parameters);
                    }
				}
				catch (SqlException sqlex)
				{
					if (retries == 0)
					{
						throw new DataAccessException(String.Format("Error executing data set for stored procedure '{0}'.", sqlProcedureName), sqlex, sqlProcedureName, parameters);
					}
					else
					{
						_log.Warn(String.Format("Error executing data set for stored procedure '{0}' - retries remaining: {1}.", sqlProcedureName, retries), sqlex);
					}
				}

				retries--;
			}

			return null; // Should never end up here.
		}

		/// <summary>
		/// Executes a stored procedure, returns a data reader.
		/// </summary>
		/// <param name="sqlProcedureName">The stored procedure name</param>
		/// <param name="parameters">The stpred procedure parameters</param>
		/// <returns>A DataReader</returns>
		/// <remarks>Has a built-in retry count of DefaultRetries</remarks>
        [Obsolete("Depreciated. Please use an overload with 'connection' parameter")]
        protected SqlDataReader ExecuteReader(string sqlProcedureName, params object[] parameters)
		{
			return ExecuteReader(DefaultRetries, sqlProcedureName, parameters);
		}

		/// <summary>
		/// Executes a stored procedure, returns a data reader.
		/// </summary>
		/// <param name="retries">The max number of retries</param>
		/// <param name="sqlProcedureName">The stored procedure name</param>
		/// <param name="parameters">The stored procedure parameters</param>
		/// <returns>A DataReader</returns>
        [Obsolete("Depreciated. Please use an overload with 'connection' parameter")]
        protected SqlDataReader ExecuteReader(int retries, string sqlProcedureName, params object[] parameters)
		{
			if (retries < 0)
			{
				throw new ArgumentException("Cannot execute a SQL data reader, retries cannot be less than zero", "retries");
			}
			
			while (retries >= 0)
			{
				try
				{
					return SqlHelper.ExecuteReader(GetConnection(), sqlProcedureName, parameters);
				}
				catch (SqlException sqlex)
				{
					if (retries == 0)
					{
						throw new DataAccessException(String.Format("Error executing data reader for stored procedure '{0}'.", sqlProcedureName), sqlex, sqlProcedureName, parameters);
					}
					else
					{
						_log.Warn(String.Format("Error executing data reader for stored procedure '{0}' - retries remaining: {1}.", sqlProcedureName, retries), sqlex);
					}
				}

				retries--;
			}

			return null; // Should never end up here.
		}

        // wooncherk
        protected SqlDataReader ExecuteReader(string sqlProcedureName, SqlConnection connection, params object[] parameters)
        {
            var retries = DefaultRetries;

            if (retries < 0)
            {
                throw new ArgumentException("Cannot execute a SQL data reader, retries cannot be less than zero", "retries");
            }

            while (retries >= 0)
            {
                try
                {
                    
                    return SqlHelper.ExecuteReader(connection, sqlProcedureName, parameters);
                }
                catch (SqlException sqlex)
                {
                    if (retries == 0)
                    {
                        throw new DataAccessException(String.Format("Error executing data reader for stored procedure '{0}'.", sqlProcedureName), sqlex, sqlProcedureName, parameters);
                    }
                    else
                    {
                        _log.Warn(String.Format("Error executing data reader for stored procedure '{0}' - retries remaining: {1}.", sqlProcedureName, retries), sqlex);
                    }
                }

                retries--;
            }

            return null; // Should never end up here.
        }

		/// <summary>
		/// Executes an stored procedure.
		/// </summary>
		/// <param name="sqlProcedureName">The stored procedure name</param>
		/// <param name="parameters">The stored procedure parameters</param>
		/// <remarks>Has no built-in retry count</remarks>
        protected void ExecuteNonQuery(string sqlProcedureName, params object[] parameters)
		{
			ExecuteNonQuery(0, sqlProcedureName, parameters);
		}

		/// <summary>
		/// Executes an stored procedure.
		/// </summary>
		/// <param name="retries">The max number of retries</param>
		/// <param name="sqlProcedureName">The stored procedure name</param>
		/// <param name="parameters">The stored procedure parameters</param>
        protected void ExecuteNonQuery(int retries, string sqlProcedureName, params object[] parameters)
		{
			if (retries < 0)
			{
				throw new ArgumentException("Cannot execute a SQL non-query, retries cannot be less than zero", "retries");
			}

			while (retries >= 0)
			{
				try
				{
                    using (var connection = GetConnection())
                    {
                        SqlHelper.ExecuteNonQuery(connection, sqlProcedureName, parameters);
                    }
				}
				catch (SqlException sqlex)
				{
					if (retries == 0)
					{
						throw new DataAccessException(String.Format("Error executing non-query for stored procedure '{0}'.", sqlProcedureName), sqlex, sqlProcedureName, parameters);
					}
					else
					{
						_log.Warn(String.Format("Error executing non-query for stored procedure '{0}' - retries remaining: {1}.", sqlProcedureName, retries), sqlex);
					}
				}

				retries--;
			}
		}

		/// <summary>
		/// Executes a scalar procedure.
		/// </summary>
		/// <param name="sqlProcedureName">The stored procedure name</param>
		/// <param name="parameters">The stored procedure parameters</param>
		/// <returns>The return value as an object</returns>
		/// <remarks>Has no built-in retry count</remarks>
        protected object ExecuteScalar(string sqlProcedureName, params object[] parameters)
		{
			return ExecuteScalar(0, sqlProcedureName, parameters);
		}

		/// <summary>
		/// Executes a scalar procedure.
		/// </summary>
		/// <param name="retries">The max number of retries</param>
		/// <param name="sqlProcedureName">The stored procedure name</param>
		/// <param name="parameters">The stored procedure parameters</param>
		/// <returns>The return value as an object</returns>		
        protected object ExecuteScalar(int retries, string sqlProcedureName, params object[] parameters)
		{
			if (retries < 0)
			{
				throw new ArgumentException("Cannot execute a SQL scalar statement, retries cannot be less than zero", "retries");
			}

			while (retries >= 0)
			{
				try
				{
                    using (var connection = GetConnection())
                    {
                        return SqlHelper.ExecuteScalar(connection, sqlProcedureName, parameters);
                    }
				}
				catch (SqlException sqlex)
				{
					if (retries == 0)
					{
						throw new DataAccessException(String.Format("Error executing scalar statement for stored procedure '{0}'.", sqlProcedureName), sqlex, sqlProcedureName, parameters);
					}
					else
					{
						_log.Warn(String.Format("Error executing a scalar statement for stored procedure '{0}' - retries remaining: {1}.", sqlProcedureName, retries), sqlex);
					}
				}

				retries--;
			}

			return null; // Should never end up here.
		}

		/// <summary>
		/// Returns an element from a datarow as a string.
		/// </summary>
		/// <param name="dataRow">The data row</param>
		/// <param name="fieldName">The field name</param>
		/// <returns>The value as a string</returns>
		protected string GetFieldByString(DataRow dataRow, string fieldName)
		{
			string fieldValue = String.Empty;

			if (dataRow[fieldName] != DBNull.Value)
			{
				fieldValue = dataRow[fieldName].ToString();
			}
	
			return fieldValue;
		}

		/// <summary>
		/// Returns an element from a datareader as a string.
		/// </summary>
		/// <param name="reader">The data reader</param>
		/// <param name="fieldName">The field name</param>
		/// <returns>The value as a string</returns>
		protected string GetFieldByString(SqlDataReader reader, string fieldName)
		{
			string fieldValue = "";

			if (reader[fieldName] != DBNull.Value)
			{
				fieldValue = reader[fieldName].ToString();
			}
	
			return fieldValue;
		}

		/// <summary>
		/// Returns an element from a datarow as an integer.
		/// </summary>
		/// <param name="dataRow">The data row</param>
		/// <param name="fieldName">The field name</param>
		/// <returns>The value as an integer</returns>
		protected int GetFieldByInteger(DataRow dataRow, string fieldName)
		{
			int fieldValue = -1;

			if (dataRow[fieldName] != DBNull.Value)
			{
				fieldValue = (int)dataRow[fieldName];
			}
	
			return fieldValue;
		}

		/// <summary>
		/// Returns an element from a data reader as an integer.
		/// </summary>
		/// <param name="reader">The data reader</param>
		/// <param name="fieldName">The field name</param>
		/// <returns>The value as an integer</returns>
		protected int GetFieldByInteger(SqlDataReader reader, string fieldName)
		{
			int fieldValue = -1;

			if (reader[fieldName] != DBNull.Value)
			{
				fieldValue = (int)reader[fieldName];
			}
	
			return fieldValue;
		}

		/// <summary>
		/// Returns an element from a data row as an integer.
		/// </summary>
		/// <param name="dataRow">The data row</param>
		/// <param name="fieldName">The field name</param>
		/// <returns>The value as a decimal</returns>
		protected decimal GetFieldByDecimal(DataRow dataRow, string fieldName)
		{
			decimal fieldValue = -1;

			if (dataRow[fieldName] != DBNull.Value)
			{
				fieldValue = (Decimal)dataRow[fieldName];
			}
	
			return fieldValue;
		}

		/// <summary>
		/// Returns an element from a data reader as an integer.
		/// </summary>
		/// <param name="reader">The data reader</param>
		/// <param name="fieldName">The field name</param>
		/// <returns>The value as a decimal</returns>
		protected decimal GetFieldByDecimal(SqlDataReader reader, string fieldName)
		{
			decimal fieldValue = -1;

			if (reader[fieldName] != DBNull.Value)
			{
				fieldValue = (Decimal)reader[fieldName];
			}
	
			return fieldValue;
		}

		/// <summary>
		/// Returns an element from a data row as a DateTime
		/// </summary>
		/// <param name="dataRow">The data row</param>
		/// <param name="fieldName">The field name</param>
		/// <returns>The value as a DataTime</returns>
		protected DateTime GetFieldByDate(DataRow dataRow, string fieldName)
		{
			DateTime fieldValue = DateTime.MinValue;

			if (dataRow[fieldName] != DBNull.Value)
			{
				fieldValue = (DateTime)dataRow[fieldName];
			}
	
			return fieldValue;
		}

		/// <summary>
		/// Returns an element from a data reader as a DateTime
		/// </summary>
		/// <param name="reader">The data reader</param>
		/// <param name="fieldName">The field name</param>
		/// <returns>The value as a DataTime</returns>
		protected DateTime GetFieldByDate(SqlDataReader reader, string fieldName)
		{
			DateTime fieldValue = DateTime.MinValue;

			if (reader[fieldName] != DBNull.Value)
			{
				fieldValue = (DateTime)reader[fieldName];
			}
	
			return fieldValue;
		}

		/// <summary>
		/// Returns an element from a data row as a boolean
		/// </summary>
		/// <param name="dataRow">The data row</param>
		/// <param name="fieldName">The field name</param>
		/// <returns>The value as a boolean</returns>
		protected bool GetFieldByBoolean(DataRow dataRow, string fieldName)
		{
			bool fieldValue = false;

			if (dataRow[fieldName] != DBNull.Value)
			{
				fieldValue = (bool)dataRow[fieldName];
			}
	
			return fieldValue;
		}
		
		/// <summary>
		/// Returns an element from a data reader as a boolean
		/// </summary>
		/// <param name="reader">The data reader</param>
		/// <param name="fieldName">The field name</param>
		/// <returns>The value as a boolean</returns>
		protected bool GetFieldByBoolean(SqlDataReader reader, string fieldName)
		{
			bool fieldValue = false;

			if (reader[fieldName] != DBNull.Value)
			{
				fieldValue = (bool)reader[fieldName];
			}
	
			return fieldValue;
		}

		/*
		protected SqlConnection Connection
		{
			get
			{
				return _sqlConnection;
			}
			set
			{
				_sqlConnection = value;
			}
		}

		private SqlConnection _sqlConnection;
		*/

		private log4net.ILog _log = log4net.LogManager.GetLogger(typeof(DataAccess));

	}
}
