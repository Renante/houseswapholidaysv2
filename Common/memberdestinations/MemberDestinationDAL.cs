using System;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Common
{
	/// <summary>
	/// Summary description for HomeAvailabilityDAL.
	/// </summary>
	public class MemberDestinationDAL : DataAccess
	{
		public MemberDestinationDAL()
		{
		}

		#region Types
		private sealed class HomeImageFields
		{
			public readonly int ID = -1;
			public readonly int MemberID = -1;
			public readonly int FromDate = -1;
			public readonly int ThruDate = -1;
		}
		#endregion
	
		public MemberDestination Get(int memberID, int memberDestinationID)
		{
			MemberDestination memberDestination = new MemberDestination();

			const string spName = "up_GetMemberDestination";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, memberID, memberDestinationID))
                {

                    while (reader.Read())
                    {
                        memberDestination.ID = (int)reader["ID"];
                        memberDestination.MemberID = (int)reader["MemberID"];
                        memberDestination.CountryID = (int)reader["CountryID"];
                        memberDestination.CountryCode = (string)reader["CountryCode"];
                        memberDestination.CountryName = (string)reader["CountryName"];

                        if (reader["RegionCode"] == DBNull.Value)
                        {
                            memberDestination.RegionCode = "";
                        }
                        else
                        {
                            memberDestination.RegionCode = (string)reader["RegionCode"];
                        }

                        // TODO: below is not working??? check stored procedure
                        //if (reader["Destination"] == DBNull.Value)
                        //    memberDestination.Destination = "";
                        //else
                        //{
                        //    memberDestination.Destination = (string)reader["Destination"];
                        //}

                        memberDestination.January = (bool)reader["January"];
                        memberDestination.February = (bool)reader["February"];
                        memberDestination.March = (bool)reader["March"];
                        memberDestination.April = (bool)reader["April"];
                        memberDestination.May = (bool)reader["May"];
                        memberDestination.June = (bool)reader["June"];
                        memberDestination.July = (bool)reader["July"];
                        memberDestination.August = (bool)reader["August"];
                        memberDestination.September = (bool)reader["September"];
                        memberDestination.October = (bool)reader["October"];
                        memberDestination.November = (bool)reader["November"];
                        memberDestination.December = (bool)reader["December"];
                    }
                } 
            }

			return memberDestination;
		}


		public MemberDestinationCollection GetAllForMember(int memberID)
		{
			MemberDestinationCollection memberDestinationCollection = new MemberDestinationCollection();

			const string spName = "up_GetMemberDestinations";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, memberID))
                {
                    while (reader.Read())
                    {
                        MemberDestination memberDestination = new MemberDestination();

                        memberDestination.ID = (int)reader["ID"];
                        memberDestination.MemberID = (int)reader["MemberID"];
                        memberDestination.CountryID = (int)reader["CountryID"];
                        memberDestination.CountryCode = (string)reader["CountryCode"];
                        memberDestination.CountryName = (string)reader["CountryName"];

                        if (reader["RegionCode"] == DBNull.Value)
                        {
                            memberDestination.RegionCode = "";
                        }
                        else
                        {
                            memberDestination.RegionCode = (string)reader["RegionCode"];
                        }

                        if (reader["Destination"] == DBNull.Value)
                            memberDestination.Destination = "";
                        else
                        {
                            memberDestination.Destination = (string)reader["Destination"];
                        }

                        memberDestination.RegionName = GetFieldByString(reader, "RegionName");

                        memberDestination.January = (bool)reader["January"];
                        memberDestination.February = (bool)reader["February"];
                        memberDestination.March = (bool)reader["March"];
                        memberDestination.April = (bool)reader["April"];
                        memberDestination.May = (bool)reader["May"];
                        memberDestination.June = (bool)reader["June"];
                        memberDestination.July = (bool)reader["July"];
                        memberDestination.August = (bool)reader["August"];
                        memberDestination.September = (bool)reader["September"];
                        memberDestination.October = (bool)reader["October"];
                        memberDestination.November = (bool)reader["November"];
                        memberDestination.December = (bool)reader["December"];

                        memberDestinationCollection.Add(memberDestination);
                    }
                } 
            }

			return memberDestinationCollection;
		}

		/// <summary>
		/// Save the specified home destination to the database
		/// </summary>
		/// <param name="memberDestination"></param>
		/// <returns></returns>
		public void Save(MemberDestination memberDestination)
		{
			// determine if we should add or update a member destination
			if (memberDestination.ID > 0)
			{
				const string spName = "up_UpdateMemberDestination";

				ExecuteNonQuery(spName, 
						   memberDestination.ID, memberDestination.MemberID, memberDestination.CountryID, 
						   memberDestination.CountryCode, memberDestination.RegionCode,
						   memberDestination.Destination, memberDestination.January,
						   memberDestination.February, memberDestination.March,
						   memberDestination.April, memberDestination.May,
						   memberDestination.June, memberDestination.July,
						   memberDestination.August, memberDestination.September,
						   memberDestination.October, memberDestination.November,
						   memberDestination.December);
			}
			else
			{
				// Add new member destination
				const string spName = "up_InsertMemberDestination";

				ExecuteNonQuery(spName, 
						   memberDestination.MemberID, memberDestination.CountryID, 
						   memberDestination.CountryCode, memberDestination.RegionCode,
						   memberDestination.Destination, memberDestination.January,
						   memberDestination.February, memberDestination.March,
						   memberDestination.April, memberDestination.May,
						   memberDestination.June, memberDestination.July,
						   memberDestination.August, memberDestination.September,
						   memberDestination.October, memberDestination.November,
						   memberDestination.December);
			}
		}

		/// <summary>
		/// Save the member destination details (and return ID if insert)
		/// </summary>
		/// <param name="memberDestination"></param>
		/// <returns></returns>
		public MemberDestination Save2(MemberDestination memberDestination)
		{
			// determine if we should add or update a member destination
			if (memberDestination.ID > 0)
			{
				const string spName = "up_UpdateMemberDestination";

				ExecuteNonQuery(spName, 
					memberDestination.ID, memberDestination.MemberID, memberDestination.CountryID, 
					memberDestination.CountryCode, memberDestination.RegionCode,
					memberDestination.Destination, memberDestination.January,
					memberDestination.February, memberDestination.March,
					memberDestination.April, memberDestination.May,
					memberDestination.June, memberDestination.July,
					memberDestination.August, memberDestination.September,
					memberDestination.October, memberDestination.November,
					memberDestination.December);
			}
			else
			{
				// Add new member destination
				const string spName = "up_InsertMemberDestination2";

				memberDestination.ID = (int)ExecuteScalar(spName, 
					memberDestination.MemberID, memberDestination.CountryID, 
					memberDestination.CountryCode, memberDestination.RegionCode,
					memberDestination.Destination, memberDestination.January,
					memberDestination.February, memberDestination.March,
					memberDestination.April, memberDestination.May,
					memberDestination.June, memberDestination.July,
					memberDestination.August, memberDestination.September,
					memberDestination.October, memberDestination.November,
					memberDestination.December);
			}

			return memberDestination;
		}

		public void Delete(int memberID, int memberDestinationID)
		{
			const string spName = "up_DeleteMemberDestination";

			ExecuteNonQuery(spName, memberID, memberDestinationID);
		}

		private MemberDestination LoadFromReader(SqlDataReader reader, HomeImageFields fields)
		{
			MemberDestination memberDestination = new MemberDestination();

			memberDestination.ID = (int)reader["ID"];
			memberDestination.MemberID = (int)reader["MemberID"];
			memberDestination.CountryID = (int)reader["CountryID"];

			return memberDestination;
		}

		#region Singleton instance
		public static MemberDestinationDAL Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
					lock (typeof(MemberDestinationDAL))
					{
						if (_instance == null)
						{
							_instance = new MemberDestinationDAL();
						}
					}
				}

				return _instance;
			}
			
		}

		#endregion


		private static MemberDestinationDAL _instance;

	}
}
