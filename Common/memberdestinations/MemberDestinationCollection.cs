using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for HomeAvailabilityCollection.
	/// </summary>
	public class MemberDestinationCollection : CollectionBase
	{
		public MemberDestinationCollection()
		{
		}

		public void Add(MemberDestination MemberDestination)
		{
			InnerList.Add(MemberDestination);
		}

		public void Remote(MemberDestination MemberDestination)
		{
			InnerList.Remove(MemberDestination);
		}

		public MemberDestination this[int index]
		{
			get
			{
				return (MemberDestination)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}

	
	}
}
