using System;

namespace Common
{
	/// <summary>
	/// Summary description for HomeAvailability.
	/// </summary>
	public class MemberDestination
	{
		public MemberDestination()
		{
		}

		public int ID
		{
			get
			{
				return _id;
			}
			set
			{
				_id = value;
			}
		}

		public int MemberID
		{
			get
			{
				return _memberID;
			}
			set
			{
				_memberID = value;
			}
		}

		public int CountryID
		{
			get
			{
				return _countryID;
			}
			set
			{
				_countryID = value;
			}
		}

		public string CountryCode
		{
			get
			{
				return _countryCode;
			}
			set
			{
				_countryCode = value;
			}
		}

		public string CountryName
		{
			get
			{
				return _countryName;
			}
			set
			{
				_countryName = value;
			}
		}

		public string RegionCode
		{
			get
			{
				return _regionCode;
			}
			set
			{
				_regionCode = value;
			}
		}

		public string RegionName
		{
			get
			{
				return _regionName;
			}
			set
			{
				_regionName = value;
			}
		}

		public string Destination
		{
			get
			{
				return _destination;
			}
			set
			{
				_destination = value;
			}
		}

		public bool January
		{
			get
			{
				return _january;
			}
			set
			{
				_january = value;
			}
		}

		public bool February
		{
			get
			{
				return _february;
			}
			set
			{
				_february = value;
			}
		}

		public bool March
		{
			get
			{
				return _march;
			}
			set
			{
				_march = value;
			}
		}

		public bool April
		{
			get
			{
				return _april;
			}
			set
			{
				_april = value;
			}
		}

		public bool May
		{
			get
			{
				return _may;
			}
			set
			{
				_may = value;
			}
		}

		public bool June
		{
			get
			{
				return _june;
			}
			set
			{
				_june = value;
			}
		}

		public bool July
		{
			get
			{
				return _july;
			}
			set
			{
				_july = value;
			}
		}

		public bool August
		{
			get
			{
				return _august;
			}
			set
			{
				_august = value;
			}
		}

		public bool September
		{
			get
			{
				return _september;
			}
			set
			{
				_september = value;
			}
		}

		public bool October
		{
			get
			{
				return _october;
			}
			set
			{
				_october = value;
			}
		}

		public bool November
		{
			get
			{
				return _november;
			}
			set
			{
				_november = value;
			}
		}

		public bool December
		{
			get
			{
				return _december;
			}
			set
			{
				_december = value;
			}
		}

		private int _id;
		private int _memberID;
		private int _countryID;
		private string _countryCode;
		private string _countryName;
		private string _regionCode;
		private string _regionName;
		private string _destination;
		private bool _january;
		private bool _february;
		private bool _march;
		private bool _april;
		private bool _may;
		private bool _june;
		private bool _july;
		private bool _august;
		private bool _september;
		private bool _october;
		private bool _november;
		private bool _december;

	}
}
