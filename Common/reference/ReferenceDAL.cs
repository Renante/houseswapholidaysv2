using System;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Common
{
	/// <summary>
	/// Summary description for FeatureDAL.
	/// </summary>
	public class ReferenceDAL : DataAccess
	{

		public ReferenceDAL()
		{
		}

		public ReferenceCollection GetAllForMember(int memberID)
		{
			ReferenceCollection referenceCollection = new ReferenceCollection();

			const string spName = "up_GetAllReferencesForMember";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, memberID))
                {
                    while (reader.Read())
                    {
                        Reference reference = new Reference();

                        reference.ID = GetFieldByInteger(reader, "ID");
                        reference.MemberID = GetFieldByInteger(reader, "MemberID");
                        reference.Name = GetFieldByString(reader, "Name");
                        reference.EmailAddress = GetFieldByString(reader, "EmailAddress");
                        reference.Rating = GetFieldByInteger(reader, "Rating");
                        // TODO : Reenable below
                        //reference.Type = (ReferenceType)GetFieldByInteger(reader, "Type");
                        reference.Comment = GetFieldByString(reader, "Comment");
                        reference.IsContactable = GetFieldByBoolean(reader, "IsContactable");
                        reference.IsContactable = GetFieldByBoolean(reader, "IsContactable");
                        reference.Date = GetFieldByDate(reader, "Date");

                        referenceCollection.Add(reference);
                    }

                    // reader.Close();
                }
            }

			return referenceCollection;
		}

		public Reference Save(Reference reference)
		{

				// Update existing promotion
				const string spName = "up_InsertReference";

				ExecuteNonQuery(spName, 
					reference.MemberID, reference.Name, reference.EmailAddress,
					reference.Rating, reference.Type,
					reference.Comment, reference.IsContactable);

			return reference;
		}

		#region Singleton instance
		public static ReferenceDAL Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
					lock (typeof(ReferenceDAL))
					{
						if (_instance == null)
						{
							_instance = new ReferenceDAL();
						}
					}
				}

				return _instance;
			}
			
		}

		#endregion

		private static ReferenceDAL _instance;

	}
}
