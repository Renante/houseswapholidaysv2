using System;
using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for HomeCollection.
	/// </summary>
	public class ReferenceCollection : CollectionBase
	{
		public ReferenceCollection()
		{
		}

		public void Add(Reference reference)
		{
			InnerList.Add(reference);
		}

		public void Remote(Reference reference)
		{
			InnerList.Remove(reference);
		}

		public Reference this[int index]
		{
			get
			{
				return (Reference)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}

	}
}
