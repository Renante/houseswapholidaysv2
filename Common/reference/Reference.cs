using System;

namespace Common
{
	/// <summary>
	/// Summary description for Feature.
	/// </summary>
	public class Reference
	{
		public Reference()
		{
		}

		public int ID
		{
			get
			{
				return _id;
			}
			set
			{
				_id = value;
			}
		}

		public int MemberID
		{
			get
			{
				return _memberID;
			}
			set
			{
				_memberID = value;
			}
		}

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		public string EmailAddress
		{
			get
			{
				return _emailAddress;
			}
			set
			{
				_emailAddress = value;
			}
		}

		public int Rating
		{
			get
			{
				return _rating;
			}
			set
			{
				_rating = value;
			}
		}

		public ReferenceType Type
		{
			get
			{
				return _type;
			}
			set
			{
				_type = value;
			}
		}

		public string Comment
		{
			get
			{
				return _comment;
			}
			set
			{
				_comment = value;
			}
		}

		public bool IsContactable
		{
			get
			{
				return _isContactable;
			}
			set
			{
				_isContactable = value;
			}
		}

		public DateTime Date
		{
			get
			{
				return _date;
			}
			set
			{
				_date = value;
			}
		}

		private int _id;
		private int _memberID;
		private string _name;
		private string _emailAddress;
		private int _rating;
		private ReferenceType _type;
		private string _comment;
		private bool _isContactable;
		private DateTime _date;
	}
}
