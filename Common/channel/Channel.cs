using System;

namespace Common
{
	/// <summary>
	/// Summary description for Blog.
	/// </summary>
	public class Channel
	{
		public Channel()
		{
		}

		public int ID
		{
			get
			{
				return _ID;
			}
			set
			{
				_ID = value;
			}
		}

		public string ShortURL
		{
			get
			{
				return _shortURL;
			}
			set
			{
				_shortURL = value;
			}
		}

		public string LongURL
		{
			get
			{
				return _longURL;
			}
			set
			{
				_longURL = value;
			}
		}

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		public string Description
		{
			get
			{
				return _description;
			}
			set
			{
				_description = value;
			}
		}

		public int OwnerID
		{
			get
			{
				return _ownerID;
			}
			set
			{
				_ownerID = value;
			}
		}

		public bool IsDefault
		{
			get
			{
				return _isDefault;
			}
			set
			{
				_isDefault = value;
			}
		}

		public string SupportEmailAddress
		{
			get
			{
				return _supportEmailAddress;
			}
			set
			{
				_supportEmailAddress = value;
			}
		}

		public string SalesEmailAddress
		{
			get
			{
				return _salesEmailAddress;
			}
			set
			{
				_salesEmailAddress = value;
			}
		}

		public string AdvertisingEmailAddress
		{
			get
			{
				return _advertisingEmailAddress;
			}
			set
			{
				_advertisingEmailAddress = value;
			}
		}

		public string ShortPageTitle
		{
			get
			{
				return _shortPageTitle;
			}
			set
			{
				_shortPageTitle = value;
			}
		}

		public string LongPageTitle
		{
			get
			{
				return _longPageTitle;
			}
			set
			{
				_longPageTitle = value;
			}
		}

		public string StyleSheet
		{
			get
			{
				return _styleSheet;
			}
			set
			{
				_styleSheet = value;
			}
		}

		public string MetaDescriptionGeneric
		{
			get
			{
				return _metaDescriptionGeneric;
			}
			set
			{
				_metaDescriptionGeneric = value;
			}
		}

		public string MetaDescriptionResults
		{
			get
			{
				return _metaDescriptionResults;
			}
			set
			{
				_metaDescriptionResults = value;
			}
		}

		public string MetaKeywordsGeneric
		{
			get
			{
				return _metaKeywordsGeneric;
			}
			set
			{
				_metaKeywordsGeneric = value;
			}
		}

		public string AnalyticsTrackingCode
		{
			get
			{
				return _analyticsTrackingCode;
			}
			set
			{
				_analyticsTrackingCode = value;
			}
		}

		public bool SupportMultiChannel
		{
			get
			{
				return _supportMultiChannel;
			}
			set
			{
				_supportMultiChannel = value;
			}
		}

		public double SubscriptionRate
		{
			get
			{
				return _subscriptionRate;
			}
			set
			{
				_subscriptionRate = value;
			}
		}

		public string MapKey
		{
			get
			{
				return _mapKey;
			}
			set
			{
				_mapKey = value;
			}
		}

		public int ExchangeType
		{
			get
			{
				return _exchangeType;
			}
			set
			{
				_exchangeType = value;
			}
		}

		public int SubscriptionPeriod
		{
			get
			{
                return _subscriptionPeriod;
			}
			set
			{
                _subscriptionPeriod = value;
			}
		}

		#region Private instance variables
		
		private int _ID;
		private string _shortURL;
		private string _longURL;
		private string _name;
		private string _description;
		private int _ownerID;
		private bool _isDefault;
		private string _supportEmailAddress;
		private string _salesEmailAddress;
		private string _advertisingEmailAddress;
		private string _shortPageTitle;
		private string _longPageTitle;
		private string _styleSheet;

		private string _metaDescriptionGeneric;
		private string _metaDescriptionResults;
		private string _metaKeywordsGeneric;

		private bool _supportMultiChannel;

		private double _subscriptionRate;

		private string _analyticsTrackingCode;
		private string _mapKey;

		private int _exchangeType;

        private int _subscriptionPeriod;

		#endregion Private instance variables

	
	}
}
