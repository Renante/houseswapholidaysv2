using System;
using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for BlogCollection.
	/// </summary>
	public class ChannelCollection : CollectionBase
	{
		public ChannelCollection()
		{
		}

		public void Add(Channel channel)
		{
			InnerList.Add(channel);
		}

		public void Remove(Blog channel)
		{
			InnerList.Remove(channel);
		}

		public Channel this[int index]
		{
			get
			{
				return (Channel)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}
	
	}
}
