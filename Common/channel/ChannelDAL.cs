using System;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;

namespace Common
{
	/// <summary>
	/// Summary description for BlogDAL.
	/// </summary>
	public class ChannelDAL : DataAccess
	{
		public ChannelDAL()
		{
		}

		/// <summary>
		/// Get blog by supplied ID
		/// </summary>
		/// <param name="blogID">Identifier of blog to retrieve</param>
		/// <returns></returns>
		public Channel Get(int channelID)
		{

			// always ensure channel info is cached. 
			// TODO: Change eventually
			CacheManager cacheManager = new CacheManager();

			// Check if channel collection is cached
			ChannelCollection channelCollection = (ChannelCollection)cacheManager.Get("channelCollection");

			if (channelCollection == null)
			{
				channelCollection = ChannelDAL.Instance.GetAll();
				cacheManager.Set("channelCollection", channelCollection);
			}

			foreach(Channel channel in channelCollection)
			{
				if (channel.ID == channelID)
				{
					return channel;
				}
			}

			throw new ChannelNotFoundException(String.Format("Channel ID {0} not found.", channelID));
			// return null;

		}


		/// <summary>
		/// Get channel by website name
		/// </summary>
		/// <param name="blogID">Identifier of blog to retrieve</param>
		/// <returns></returns>
		public Channel Get(string longWebsiteName)
		{

			// always ensure channel info is cached. 
			// TODO: Change eventually
			CacheManager cacheManager = new CacheManager();

			longWebsiteName = longWebsiteName.ToLower();

			// hack below for when testing against production
			//longWebsiteName = "http://www.christianhomeswap.com";

			// Check if channelcollection is cached
			ChannelCollection channelCollection = (ChannelCollection)cacheManager.Get("channelCollection");

			if (channelCollection == null)
			{
				channelCollection = ChannelDAL.Instance.GetAll();
				cacheManager.Set("channelCollection", channelCollection);
			}
			/* else
			{
				if (channelCollection[0].LongURL.Length == 0)
				{
					_log.Error("First channel entry is empty, so reload channels into cache");
					channelCollection = ChannelDAL.Instance.GetAll();
					cacheManager.Set("channelCollection", channelCollection);
				}
			} */

			// remove any secure page reference so we can still match on channel
			string generalWebsiteURL = longWebsiteName.Replace("https","http");

			foreach(Channel channel in channelCollection)
			{

				//if (channel.LongURL == longWebsiteName)
				//_log.Error("Checking for a match on  : " + channel.LongURL + " and " + generalWebsiteURL);
				

				// TODO: Commented out as is referencing production settings.
				//if (channel.LongURL == generalWebsiteURL)
				
				if (channel.LongURL == generalWebsiteURL)
				{
					return channel;
				}
			}

			/* // No channel found so throw error
			_log.Error("Unable to retrieve channel for : " + longWebsiteName);
			_log.Error("generalWebsiteURL is : " + generalWebsiteURL);

			// Write out error matches to log file
			foreach(Channel channel in channelCollection)
			{
				//string generalWebsiteURL;

				// remove any secure page reference so we can still match on channel
				//generalWebsiteURL = longWebsiteName.Replace("https","http");

				//if (channel.LongURL == longWebsiteName)
				//_log.Error("Checking for a match on  : " + channel.LongURL + " and " + generalWebsiteURL);
				if (channel.LongURL.Length > 0)
				{
					_log.Error("No match found on : " + channel.LongURL);
				}
				else
				{
					_log.Error("channel.LongURL has a length of zero");
				}
			} */

			
			throw new ChannelNotFoundException(String.Format("Channel name '{0}' not found.", longWebsiteName));
			// return null;
		}

		/*
		public ChannelCollection GetAll()
		{
			ChannelCollection channelCollection = new ChannelCollection();

			const string spName = "up_GetAllChannels";

			using (DataSet dataSet = ExecuteDataset(spName))
			{
				DataTable channelData = dataSet.Tables[0];

				//p.Name = myDataSet.Tables["Publisher"].Rows[i]["PublisherName"].ToString();

				for (int i=0; i<=channelData.Rows.Count-1; i++)
				{
					Channel channel = new Channel();

					channel.ID = GetFieldByInteger(channelData.Rows[i], "ChannelID");
					channel.Name = GetFieldByString(channelData.Rows[i], "Name");
					channel.Description = GetFieldByString(channelData.Rows[i], "Description");
					channel.OwnerID = GetFieldByInteger(channelData.Rows[i], "OwnerID");
					channel.ShortURL = GetFieldByString(channelData.Rows[i], "ShortURL");
					channel.LongURL = GetFieldByString(channelData.Rows[i], "LongURL");
					channel.IsDefault = GetFieldByBoolean(channelData.Rows[i], "IsDefault");
					channel.SupportEmailAddress = GetFieldByString(channelData.Rows[i], "SupportEmailAddress");
					channel.SalesEmailAddress = GetFieldByString(channelData.Rows[i], "SalesEmailAddress");
					channel.AdvertisingEmailAddress = GetFieldByString(channelData.Rows[i], "AdvertisingEmailAddress");
					channel.ShortPageTitle = GetFieldByString(channelData.Rows[i], "ShortPageTitle");
					channel.LongPageTitle = GetFieldByString(channelData.Rows[i], "LongPageTitle");
					channel.StyleSheet = GetFieldByString(channelData.Rows[i], "Stylesheet");
					channel.MetaDescriptionGeneric = GetFieldByString(channelData.Rows[i], "MetaDescriptionGeneric");
					channel.MetaDescriptionResults = GetFieldByString(channelData.Rows[i], "MetaDescriptionResults");
					channel.MetaKeywordsGeneric = GetFieldByString(channelData.Rows[i], "MetaKeywordsGeneric");
					channel.SupportMultiChannel = GetFieldByBoolean(channelData.Rows[i], "SupportMultiChannel");
					channel.SubscriptionRate = (double)GetFieldByDecimal(channelData.Rows[i], "SubscriptionRate");
					channel.AnalyticsTrackingCode = GetFieldByString(channelData.Rows[i], "AnalyticsTrackingCode");
					channel.MapKey = GetFieldByString(channelData.Rows[i], "MapKey");
					channel.ExchangeType = GetFieldByInteger(channelData.Rows[i], "ExchangeType");

					channelCollection.Add(channel);

				}

				channelData.Dispose();
				dataSet.Dispose();
			}

			return channelCollection;
		}
		*/
		
		public ChannelCollection GetAll()
		{
			ChannelCollection channelCollection = new ChannelCollection();

			const string spName = "up_GetAllChannels";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection))
                {
                    while (reader.Read())
                    {
                        Channel channel = new Channel();

                        channel.ID = GetFieldByInteger(reader, "ChannelID");
                        channel.Name = GetFieldByString(reader, "Name");
                        channel.Description = GetFieldByString(reader, "Description");
                        channel.OwnerID = GetFieldByInteger(reader, "OwnerID");
                        channel.ShortURL = GetFieldByString(reader, "ShortURL");
                        channel.LongURL = GetFieldByString(reader, "LongURL");
                        channel.IsDefault = GetFieldByBoolean(reader, "IsDefault");
                        channel.SupportEmailAddress = GetFieldByString(reader, "SupportEmailAddress");
                        channel.SalesEmailAddress = GetFieldByString(reader, "SalesEmailAddress");
                        channel.AdvertisingEmailAddress = GetFieldByString(reader, "AdvertisingEmailAddress");
                        channel.ShortPageTitle = GetFieldByString(reader, "ShortPageTitle");
                        channel.LongPageTitle = GetFieldByString(reader, "LongPageTitle");
                        channel.StyleSheet = GetFieldByString(reader, "Stylesheet");
                        channel.MetaDescriptionGeneric = GetFieldByString(reader, "MetaDescriptionGeneric");
                        channel.MetaDescriptionResults = GetFieldByString(reader, "MetaDescriptionResults");
                        channel.MetaKeywordsGeneric = GetFieldByString(reader, "MetaKeywordsGeneric");
                        channel.SupportMultiChannel = GetFieldByBoolean(reader, "SupportMultiChannel");
                        channel.SubscriptionRate = (double)GetFieldByDecimal(reader, "SubscriptionRate");
                        channel.AnalyticsTrackingCode = GetFieldByString(reader, "AnalyticsTrackingCode");
                        channel.MapKey = GetFieldByString(reader, "MapKey");
                        channel.ExchangeType = GetFieldByInteger(reader, "ExchangeType");
                        channel.SubscriptionPeriod = GetFieldByInteger(reader, "SubscriptionPeriod");

                        channelCollection.Add(channel);
                    }
                } 
            }

			return channelCollection;
		}

        public bool IsChsReferred(int memberId)
        {
            using (var connection = GetConnection())
            {
                const string query = "SELECT TOP 1 MemberID FROM CHSReferred WHERE MemberID = @memberId";
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@memberId", memberId);
                    connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        #region Singleton instance
        public static ChannelDAL Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
					lock (typeof(ChannelDAL))
					{
						if (_instance == null)
						{
							_instance = new ChannelDAL();
						}
					}
				}

				return _instance;
			}
			
		}

		#endregion

		private static ChannelDAL _instance;

		private log4net.ILog _log = log4net.LogManager.GetLogger(typeof(ChannelDAL));	

	}
}
