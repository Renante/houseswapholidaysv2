using System;

namespace Common
{
	/// <summary>
	/// Cannot find the channed (by ID or URL).
	/// </summary>
	public class ChannelNotFoundException : ApplicationException
	{
		public ChannelNotFoundException(string message) : base(message)
		{
		}
	}
}
