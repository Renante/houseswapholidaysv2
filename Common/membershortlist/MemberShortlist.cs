using System;

namespace Common
{
	/// <summary>
	/// Summary description for Home.
	/// </summary>
	public class MemberShortlist
	{
		public MemberShortlist()
		{
		}

		public int ID
		{
			get
			{
				return _id;
			}
			set
			{
				_id = value;
			}
		}

		public int MemberID
		{
			get
			{
				return _memberID;
			}
			set
			{
				_memberID = value;
			}
		}

		public int HomeID
		{
			get
			{
				return _homeID;
			}
			set
			{
				_homeID = value;
			}
		}

		public DateTime CreatedDate
		{
			get
			{
				return _createdDate;
			}
			set
			{
				_createdDate = value;
			}
		}

		public string Location
		{
			get
			{
				return _location;
			}
			set
			{
				_location = value;
			}
		}

        public int ListingType
        {
            get
            {
                return _listingType;
            }
            set
            {
                _listingType = value;
            }
        }

		private int _id;
		private int _memberID;
		private int _homeID;
		private string _location;
		private DateTime _createdDate;
        private int _listingType;

	}
}
