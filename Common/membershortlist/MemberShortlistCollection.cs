using System;
using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for HomeCollection.
	/// </summary>
	public class MemberShortlistCollection : CollectionBase
	{
		public MemberShortlistCollection()
		{
		}

		public void Add(MemberShortlist memberShortlist)
		{
			InnerList.Add(memberShortlist);
		}

		public void Remote(MemberShortlist memberShortlist)
		{
			InnerList.Remove(memberShortlist);
		}

		public MemberShortlist this[int index]
		{
			get
			{
				return (MemberShortlist)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}

	}
}
