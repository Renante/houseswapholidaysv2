using System;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;


namespace Common
{
	/// <summary>
	/// Summary description for HomeEnquiryDAL.
	/// </summary>
	public class MemberShortlistDAL : DataAccess
	{
		public MemberShortlistDAL()
		{
		}

		public void Save(MemberShortlist memberShortlist)
		{
			// Add new enquiry
			const string spName = "up_InsertMemberShortlist";

            ExecuteNonQuery(spName, memberShortlist.MemberID, memberShortlist.HomeID);
		}

		public void Delete(int memberShortListID, int memberID)
		{
			const string spName = "up_DeleteMemberShortlist";

            ExecuteNonQuery(spName, memberShortListID, memberID);
		}

		public MemberShortlistCollection GetAllForMember(int memberID)
		{
			MemberShortlistCollection memberShortlistCollection = new MemberShortlistCollection();

			// Retrieve all home enquiries based on given member ID
			const string spName = "up_GetAllMemberShortlists";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, memberID))
                {

                    while (reader.Read())
				    {
					    MemberShortlist memberShortlist = new MemberShortlist();

					    memberShortlist.ID = (int)reader["ID"];
					    memberShortlist.MemberID = GetFieldByInteger(reader,"MemberID");
					    memberShortlist.HomeID = GetFieldByInteger(reader,"HomeID");
					    memberShortlist.CreatedDate = GetFieldByDate(reader,"CreatedDate");
					    memberShortlist.Location = GetFieldByString(reader, "Location");
                        memberShortlist.ListingType = GetFieldByInteger(reader, "ListingType");
                        //memberShortlist.HomeEnquiriesCount = GetFieldByInteger(reader, "HomeEnquiriesCount"); // wooncherk - issue #32
					    memberShortlistCollection.Add(memberShortlist);
				    }
                }
            }

			return memberShortlistCollection;
		}

		#region Singleton instance
		public static MemberShortlistDAL Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
					lock (typeof(MemberShortlistDAL))
					{
						if (_instance == null)
						{
							_instance = new MemberShortlistDAL();
						}
					}
				}

				return _instance;
			}
			
		}

		#endregion

		private static MemberShortlistDAL _instance;
	
	}
}
