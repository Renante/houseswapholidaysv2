using System;

namespace Common
{
	/// <summary>
	/// Summary description for RestaurantCriteria.
	/// </summary>
	public class HomeCriteria
	{
		public HomeCriteria()
		{
		}

		public PageCriteria PageCriteria
		{
			get
			{
				return _pageCriteria;
			}
			set
			{
				_pageCriteria = value; 
			}
		}

		public int HomeCount
		{
			get
			{
				return _homeCount;
			}
			set
			{
				_homeCount = value; 
			}
		}

		public int Bedrooms
		{
			get
			{
				return _bedrooms;
			}
			set
			{
				_bedrooms = value; 
			}
		}

		public string Keyword
		{
			get
			{
				return _keyword;
			}
			set
			{
				_keyword = value; 
			}
		}

		public string CountryCode
		{
			get
			{
				return _countryCode;
			}
			set
			{
				_countryCode = value; 
			}
		}

		public string RegionCode
		{
			get
			{
				return _regionCode;
			}
			set
			{
				_regionCode = value; 
			}
		}

		public string FeatureList
		{
			get
			{
				return _featureList;
			}
			set
			{
				_featureList = value; 
			}
		}

		public bool IsMultiChannel
		{
			get
			{
				return _isMultiChannel;
			}
			set
			{
				_isMultiChannel = value;
			}
		}

		public int DefaultChannel
		{
			get
			{
				return _defaultChannel;
			}
			set
			{
				_defaultChannel = value;
			}
		}

		public int StartMonth
		{
			get
			{
				return _startMonth;
			}
			set
			{
				_startMonth = value;
			}
		}

		public int EndMonth
		{
			get
			{
				return _endMonth;
			}
			set
			{
				_endMonth = value;
			}
		}

		public int ContinentID
		{
			get
			{
				return _continentID;
			}
			set
			{
				_continentID = value;
			}
		}

		public HomeCollection homeCollection
		{
			get
			{
				return _homeCollection;
			}
			set
			{
				_homeCollection = value; 
			}
		}

		public SortBy SortOrder
		{
			get
			{
				return _sortBy;
			}
			set
			{
				_sortBy = value;
			}
		}

		public SortField SortField
		{
			get
			{
				return _sortField;
			}
			set
			{
				_sortField = value;
			}
		}

		public string ReverseCountryCode
		{
			get
			{
				return _reverseCountryCode;
			}
			set
			{
				_reverseCountryCode = value;
			}
		}

		public string ReverseDestination
		{
			get
			{
				return _reverseDestination;
			}
			set
			{
				_reverseDestination = value;
			}
		}

		public int ReverseFilterType
		{
			get
			{
				return _reverseFilterType;
			}
			set
			{
				_reverseFilterType = value;
			}
		}

		public int CurrentMemberID
		{
			get
			{
				return _currentMemberID;
			}
			set
			{
				_currentMemberID = value;
			}
		}

		private PageCriteria _pageCriteria;
		private SortBy _sortBy;
		private SortField _sortField;
		
		private int _homeCount;
		private HomeCollection _homeCollection;
		private int _bedrooms;
		private string _keyword;
		private string _countryCode;
		private string _regionCode;
		private string _featureList;
		private int _startMonth;
		private int _endMonth;
		private int _continentID;
		private int _currentMemberID;

		// Fields to support reverse search (i.e. who wants to come to my destination)
		private string _reverseCountryCode;
		private string _reverseDestination;
		private int _reverseFilterType;

		private bool _isMultiChannel;
		private int _defaultChannel;

	
	}
}
