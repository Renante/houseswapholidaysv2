using System.Configuration;
using System;

namespace Common
{
	/// <summary>
	/// Summary description for Criteria.
	/// </summary>
	public class PageCriteria
	{
		public PageCriteria()
		{
		}
		
		#region Public instance variables
		public int NumResults
		{
			get
			{
				return _numResults;
			}
			set
			{
				_numResults = value;
			}
		}
		
		public int PageSize
		{
			get
			{
                // wooncherk
                //if (ConfigurationSettings.AppSettings["NumResultsPerPage"] != null)
                //{
                //    return Int32.Parse(ConfigurationSettings.AppSettings["NumResultsPerPage"]);
                //}
                //else
                //{
                //    return 10;
                //}
                if (_pageSize > 0)
                    return _pageSize;
                else if (ConfigurationSettings.AppSettings["NumResultsPerPage"] != null)
                {
                    return Int32.Parse(ConfigurationSettings.AppSettings["NumResultsPerPage"]);
                }
                else
                {
                    return 10;
                }
                // wooncherk

				// return _pageSize;
			}
			set
			{
				_pageSize = value;
			}
		}

		public int CurrentPage
		{
			get
			{
				return _currentPage;
			}
			set
			{
				_currentPage = value; 
			}
		}
		#endregion

		#region Private instance variables
		private int _numResults = -1;
		private int _pageSize = 10;
		private int _currentPage = 1;
		#endregion

	}
}
