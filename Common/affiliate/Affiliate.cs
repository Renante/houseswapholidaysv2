using System;

namespace Common
{
	/// <summary>
	/// Summary description for Blog.
	/// </summary>
	public class Affiliate
	{
		public Affiliate()
		{
		}

		public int ID
		{
			get
			{
				return _ID;
			}
			set
			{
				_ID = value;
			}
		}

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		public int MemberID
		{
			get
			{
				return _memberID;
			}
			set
			{
				_memberID = value;
			}
		}

		public int ChannelID
		{
			get
			{
				return _channelID;
			}
			set
			{
				_channelID = value;
			}
		}

		#region Private instance variables
		
		private int _ID;
		private string _name;
		private int _memberID;
		private int _channelID;
		
		#endregion Private instance variables
	
	}
}
