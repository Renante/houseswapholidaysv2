using System;
using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for BlogCollection.
	/// </summary>
	public class AffiliateCollection : CollectionBase
	{
		public AffiliateCollection()
		{
		}

		public void Add(Affiliate affiliate)
		{
			InnerList.Add(affiliate);
		}

		public void Remove(Affiliate affiliate)
		{
			InnerList.Remove(affiliate);
		}

		public Affiliate this[int index]
		{
			get
			{
				return (Affiliate)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}
	}
}
