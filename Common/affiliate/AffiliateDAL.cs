using System;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Common
{
	/// <summary>
	/// Summary description for BlogDAL.
	/// </summary>
	public class AffiliateDAL : DataAccess
	{
		public AffiliateDAL()
		{
		}

		/// <summary>
		/// Gets the affilate based on given affiliate ID
		/// </summary>
		/// <param name="blogID">Identifier of affiliate to retrieve</param>
		/// <returns></returns>
		public Affiliate Get(int affiliateID)
		{
			const string spName = "up_GetAffiliate";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, affiliateID))
                {

                    if (reader.Read())
                    {
                        Affiliate affiliate = new Affiliate();

                        affiliate.ID = GetFieldByInteger(reader, "AffiliateID");
                        affiliate.Name = GetFieldByString(reader, "Name");
                        affiliate.MemberID = GetFieldByInteger(reader, "MemberID");
                        affiliate.ChannelID = GetFieldByInteger(reader, "ChannelID");

                        return affiliate;
                    }
                    else
                    {
                        return null;
                    }
                } 
            }
		}

		/// <summary>
		/// Save the specified affiliate
		/// </summary>
		/// <param name="blog"></param>
		/// <returns></returns>
		public Affiliate Save(Affiliate affiliate)
		{

			if (affiliate.ID <= 0)
			{
				// Add new blog entry
				const string spName = "up_InsertAffiliate";

				affiliate.ID = (int)ExecuteScalar(spName, 
						   affiliate.Name, affiliate.MemberID, affiliate.ChannelID);
			}
			else
			{
				// Update existing blog entry
				const string spName = "up_UpdateAffiliate";

				ExecuteNonQuery(spName, 
					affiliate.Name, affiliate.MemberID, affiliate.ChannelID);
			}

			return affiliate;
		}
	
		public AffiliateCollection Search(string affiliateName)
		{
			AffiliateCollection affiliateCollection = new AffiliateCollection();

			const string spName = "up_SearchAffiliates";

			if (affiliateName.Length == 0)
			{
				affiliateName = null;
			}

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, affiliateName))
                {
                    while (reader.Read())
                    {
                        Affiliate affiliate = new Affiliate();

                        affiliate.ID = GetFieldByInteger(reader, "AffiliateID");
                        affiliate.Name = GetFieldByString(reader, "Name");
                        affiliate.MemberID = GetFieldByInteger(reader, "MemberID");
                        affiliate.ChannelID = GetFieldByInteger(reader, "ChannelID");

                        affiliateCollection.Add(affiliate);
                    }
                } 
            }

			return affiliateCollection;
		}


		#region Singleton instance
		public static AffiliateDAL Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
					lock (typeof(AffiliateDAL))
					{
						if (_instance == null)
						{
							_instance = new AffiliateDAL();
						}
					}
				}

				return _instance;
			}
			
		}

		#endregion

		private static AffiliateDAL _instance;

	}
}
