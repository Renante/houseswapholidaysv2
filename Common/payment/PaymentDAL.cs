using System;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Common
{
	public class PaymentDAL : DataAccess
	{
		public PaymentDAL()
		{
		}

		public Payment Get(int ID)
		{
			const string spName = "up_GetMemberPayment";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, ID))
                {
                    if (reader.Read())
                    {
                        Payment payment = new Payment();

                        payment.ID = GetFieldByInteger(reader, "MemberPaymentID");
                        payment.MemberID = GetFieldByInteger(reader, "MemberID");
                        payment.ChannelID = GetFieldByInteger(reader, "ChannelID");
                        payment.PromotionID = GetFieldByInteger(reader, "PromotionID");
                        payment.HomeID = GetFieldByInteger(reader, "HomeID");
                        payment.PaymentInitiateDate = GetFieldByDate(reader, "PaymentInitiateDate");
                        payment.PaymentCompleteDate = GetFieldByDate(reader, "PaymentCompleteDate");
                        payment.PaymentAmount = GetFieldByDecimal(reader, "PaymentAmount");
                        payment.GSTAmount = GetFieldByDecimal(reader, "GSTAmount");
                        payment.ErrorNumber = GetFieldByString(reader, "ErrorNumber");
                        payment.ReceiptNumber = GetFieldByString(reader, "ReceiptNumber");
                        payment.Status = (PaymentStatus)GetFieldByInteger(reader, "Status");
                        payment.Comments = GetFieldByString(reader, "Comments");

                        return payment;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
		}

		public Payment Get(string guid, int paymentID, string emailAddress)
		{
			const string spName = "up_GetMemberPaymentByExtras";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, paymentID, guid, emailAddress))
                {
                    if (reader.Read())
                    {
                        Payment payment = new Payment();

                        payment.ID = GetFieldByInteger(reader, "MemberPaymentID");
                        payment.MemberID = GetFieldByInteger(reader, "MemberID");
                        payment.ChannelID = GetFieldByInteger(reader, "ChannelID");
                        payment.PromotionID = GetFieldByInteger(reader, "PromotionID");
                        payment.HomeID = GetFieldByInteger(reader, "HomeID");
                        payment.PaymentInitiateDate = GetFieldByDate(reader, "PaymentInitiateDate");
                        payment.PaymentCompleteDate = GetFieldByDate(reader, "PaymentCompleteDate");
                        payment.PaymentAmount = GetFieldByDecimal(reader, "PaymentAmount");
                        payment.GSTAmount = GetFieldByDecimal(reader, "GSTAmount");
                        payment.ErrorNumber = GetFieldByString(reader, "ErrorNumber");
                        payment.ReceiptNumber = GetFieldByString(reader, "ReceiptNumber");
                        payment.Status = (PaymentStatus)GetFieldByInteger(reader, "Status");
                        payment.Comments = GetFieldByString(reader, "Comments");

                        return payment;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
		}

		public Payment Save(Payment payment)
		{
			if (payment.ID <= 0)
			{
				// Create new payment record
				const string spName = "up_InsertMemberPayment";

				payment.ID = (int)ExecuteScalar(spName, 
						   payment.MemberID, payment.ChannelID, payment.PromotionID, payment.HomeID, payment.PaymentInitiateDate, payment.PaymentAmount, payment.GSTAmount, payment.ErrorNumber, payment.ReceiptNumber, payment.Status, payment.Comments);
			}
			else
			{
				// Update existing payment record
				const string spName = "up_UpdateMemberPayment";

				ExecuteNonQuery(spName, 
					payment.ID, payment.MemberID, payment.ChannelID, payment.PromotionID, payment.HomeID, payment.PaymentInitiateDate, payment.PaymentCompleteDate, payment.PaymentAmount, payment.GSTAmount, payment.ErrorNumber, payment.ReceiptNumber, payment.Status, payment.Comments);
			}
		
			return payment;
		}

		#region Singleton instance
		public static PaymentDAL Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
					lock (typeof(PaymentDAL))
					{
						if (_instance == null)
						{
							_instance = new PaymentDAL();
						}
					}
				}

				return _instance;
			}
			
		}

		#endregion

		private static PaymentDAL _instance;

	}

}
