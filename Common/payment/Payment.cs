using System;

namespace Common
{
	/// <summary>
	/// Summary description for Payment.
	/// </summary>
	public class Payment
	{
		public Payment()
		{
		}

		public int ID
		{
			get
			{
				return _id;
			}
			set
			{
				_id = value;
			}
		}

		public int MemberID
		{
			get
			{
				return _memberID;
			}
			set
			{
				_memberID = value;
			}
		}

		public int ChannelID
		{
			get
			{
				return _channelID;
			}
			set
			{
				_channelID = value;
			}
		}

		public int PromotionID
		{
			get
			{
				return _promotionID;
			}
			set
			{
				_promotionID = value;
			}
		}

		public int HomeID
		{
			get
			{
				return _homeID;
			}
			set
			{
				_homeID = value;
			}
		}

		public DateTime PaymentInitiateDate
		{
			get
			{
				return _paymentInitiateDate;
			}
			set
			{
				_paymentInitiateDate = value;
			}
		}

		public DateTime PaymentCompleteDate
		{
			get
			{
				return _paymentCompleteDate;
			}
			set
			{
				_paymentCompleteDate = value;
			}
		}

		public decimal PaymentAmount
		{
			get
			{
				return _paymentAmount;
			}
			set
			{
				_paymentAmount = value;
			}
		}

		public decimal GSTAmount
		{
			get
			{
				return _gstAmount;
			}
			set
			{
				_gstAmount = value;
			}
		}

		public string ErrorNumber
		{
			get
			{
				return _errorNumber;
			}
			set
			{
				_errorNumber = value;
			}
		}

		public string ReceiptNumber
		{
			get
			{
				return _receiptNumber;
			}
			set
			{
				_receiptNumber = value;
			}
		}

		public PaymentStatus Status
		{
			get
			{
				return _status;
			}
			set
			{
				_status = value;
			}
		}

		public string Comments
		{
			get
			{
				return _comments;
			}
			set
			{
				_comments = value;
			}
		}

		#region Private instance variables

		private int _id = -1;
		private int _memberID = -1;
		private int _channelID = -1;
		private int _promotionID = -1;
		private int _homeID = -1;
		private DateTime _paymentInitiateDate;
		private DateTime _paymentCompleteDate;
		private decimal _paymentAmount;
		private decimal _gstAmount;
		private string _errorNumber;
		private string _receiptNumber;
		private PaymentStatus _status;
		private string _comments;

		#endregion
	
	}
}
