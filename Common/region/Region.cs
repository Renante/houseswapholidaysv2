using System;

namespace Common
{
	/// <summary>
	/// Summary description for Region.
	/// </summary>
	public class Region
	{
		public Region()
		{
		}

		public string CountryCode
		{
			get
			{
				return _countryCode;
			}
			set
			{
				_countryCode = value;
			}
		}

		public string RegionCode
		{
			get
			{
				return _regionCode;
			}
			set
			{
				_regionCode = value;
			}
		}

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		public int HomeCount
		{
			get
			{
				return _homeCount;
			}
			set
			{
				_homeCount = value;
			}
		}

		private string _countryCode;
		private string _regionCode;
		private string _name;
		private int _homeCount;
	
	}
}
