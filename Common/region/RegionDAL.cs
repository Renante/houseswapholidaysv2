using System;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Common
{
	/// <summary>
	/// Summary description for RegionDAL.
	/// </summary>
	public class RegionDAL : DataAccess
	{

		#region Types
		private sealed class RegionFields
		{
			public readonly int CountryCode = -1;
			public readonly int RegionCode = -1;
			public readonly int Name = -1;
		}
		#endregion

		public RegionDAL()
		{
		}

		public RegionCollection GetByCountryCode(string countryCode)
		{
			RegionCollection regionCollection = new RegionCollection();

			const string spName = "up_GetRegionsByCountryCode";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, countryCode))
                {
                    while (reader.Read())
                    {

                        Region region = new Region();

                        region.CountryCode = GetFieldByString(reader, "CountryCode");
                        region.RegionCode = GetFieldByString(reader, "RegionCode");
                        region.Name = GetFieldByString(reader, "Name");
                        if (reader["HomeCount"] == DBNull.Value)
                        {
                            region.HomeCount = 0;
                        }
                        else
                        {
                            region.HomeCount = (int)reader["HomeCount"];
                        }

                        // Add to collection
                        regionCollection.Add(region);
                    }

                    return regionCollection;
                }
            }
		}

		private Region LoadFromReader(SqlDataReader reader, RegionFields fields)
		{
			Region region = new Region();

			region.CountryCode = reader.GetString(fields.CountryCode);
			region.RegionCode = reader.GetString(fields.RegionCode);
			region.Name = reader.GetString(fields.Name);

			return region;
		}

		#region Singleton instance
		public static RegionDAL Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
					lock (typeof(RegionDAL))
					{
						if (_instance == null)
						{
							_instance = new RegionDAL();
						}
					}
				}

				return _instance;
			}
			
		}

		#endregion

		private static RegionDAL _instance;

	}
}
