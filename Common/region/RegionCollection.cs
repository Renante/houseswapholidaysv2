using System;
using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for RegionCollection.
	/// </summary>
	public class RegionCollection : CollectionBase
	{
		public RegionCollection()
		{
		}

		public void Add(Region region)
		{
			InnerList.Add(region);
		}

		public void Remote(Region region)
		{
			InnerList.Remove(region);
		}

		public Region this[int index]
		{
			get
			{
				return (Region)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}
	}
}
