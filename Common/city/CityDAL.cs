using System;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;

namespace Common
{
	/// <summary>
	/// Summary description for RegionDAL.
	/// </summary>
	public class CityDAL : DataAccess
	{

		public CityDAL()
		{
		}

		public CityCollection GetByName(string name, string countryCode)
		{
			CityCollection cityCollection = new CityCollection();

			const string spName = "up_GetCitiesByName";

            using (var connection = GetConnection())
            {
                using (DataSet dataSet = ExecuteDataset(spName, connection, name, countryCode))
                {
                    DataTable cityList = dataSet.Tables[0];

                    for (int i = 0; i <= cityList.Rows.Count - 1; i++)
                    {

                        City city = new City();

                        city.Name = GetFieldByString(cityList.Rows[i], "Name");
                        city.RegionCode = GetFieldByString(cityList.Rows[i], "RegionCode");
                        city.RegionName = GetFieldByString(cityList.Rows[i], "RegionName");
                        city.CountryCode = GetFieldByString(cityList.Rows[i], "CountryCode");
                        city.Longitude = GetFieldByString(cityList.Rows[i], "Longitude");
                        city.Latitude = GetFieldByString(cityList.Rows[i], "Latitude");

                        // Add to collection
                        cityCollection.Add(city);
                    }

                    cityList.Dispose();
                    dataSet.Dispose();

                    return cityCollection;
                }
            }
		}

		/*private Region LoadFromReader(SqlDataReader reader, RegionFields fields)
		{
			Region region = new Region();

			region.CountryCode = reader.GetString(fields.CountryCode);
			region.RegionCode = reader.GetString(fields.RegionCode);
			region.Name = reader.GetString(fields.Name);

			return region;
		}*/

		#region Singleton instance
		public static CityDAL Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
					lock (typeof(CityDAL))
					{
						if (_instance == null)
						{
							_instance = new CityDAL();
						}
					}
				}

				return _instance;
			}
			
		}

		#endregion

		private static CityDAL _instance;

	}
}
