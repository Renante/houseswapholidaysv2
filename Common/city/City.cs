using System;

namespace Common
{
	/// <summary>
	/// Summary description for Region.
	/// </summary>
	public class City
	{
		public City()
		{
		}

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		public string RegionCode
		{
			get
			{
				return _regionCode;
			}
			set
			{
				_regionCode = value;
			}
		}

		public string RegionName
		{
			get
			{
				return _regionName;
			}
			set
			{
				_regionName = value;
			}
		}

		public string CountryCode
		{
			get
			{
				return _countryCode;
			}
			set
			{
				_countryCode = value;
			}
		}

		public string CountryName
		{
			get
			{
				return _countryName;
			}
			set
			{
				_countryName = value;
			}
		}

		public string Longitude
		{
			get
			{
				return _longitude;
			}
			set
			{
				_longitude = value;
			}
		}

		public string Latitude
		{
			get
			{
				return _latitude;
			}
			set
			{
				_latitude = value;
			}
		}

		public int HomeCount
		{
			get
			{
				return _homeCount;
			}
			set
			{
				_homeCount = value;
			}
		}

		public int ContinentID
		{
			get
			{
				return _continentID;
			}
			set
			{
				_continentID = value;
			}
		}

		private string _name;
		private string _regionCode;
		private string _regionName;
		private string _countryCode;
		private string _countryName;
		private int _continentID;

		private string _longitude;
		private string _latitude;

		private int _homeCount;
	
	}
}
