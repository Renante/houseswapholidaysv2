using System;
using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for RegionCollection.
	/// </summary>
	public class CityCollection : CollectionBase
	{
		public CityCollection()
		{
		}

		public void Add(City city)
		{
			InnerList.Add(city);
		}

		public void Remove(City city)
		{
			InnerList.Remove(city);
		}

		public City this[int index]
		{
			get
			{
				return (City)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}
	}
}
