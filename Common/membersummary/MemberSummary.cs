using System;

namespace Common
{
	/// <summary>
	/// Summary description for Home.
	/// </summary>
	public class MemberSummary
	{
		public MemberSummary()
		{
		}

		public int ID
		{
			get
			{
				return _id;
			}
			set
			{
				_id = value;
			}
		}

		public string FirstName
		{
			get
			{
				return _firstName;
			}
			set
			{
				_firstName = value;
			}
		}

		public string GivenName
		{
			get
			{
				return _givenName;
			}
			set
			{
				_givenName = value;
			}
		}

		public string EmailAddress
		{
			get
			{
				return _emailAddress;
			}
			set
			{
				_emailAddress = value;
			}
		}

		public string Password
		{
			get
			{
				return _password;
			}
			set
			{
				_password = value;
			}
		}


		public DateTime CreatedDate
		{
			get
			{
				return _createdDate;
			}
			set
			{
				_createdDate = value;
			}
		}

		public DateTime UpdatedDate
		{
			get
			{
				return _updatedDate;
			}
			set
			{
				_updatedDate = value;
			}
		}

		public DateTime LastLoggedInDate
		{
			get
			{
				return _lastLoggedInDate;
			}
			set
			{
				_lastLoggedInDate = value;
			}
		}

		public int ReferrerID
		{
			get
			{
				return _referrerID;
			}
			set
			{
				_referrerID = value;
			}
		}

		public int PromotionID
		{
			get
			{
				return _promotionID;
			}
			set
			{
				_promotionID = value;
			}
		}

		public Status HomeStatus
		{
			get
			{
				return _homeStatus;
			}
			set
			{
				_homeStatus = value;
			}
		}

		public Status MemberStatus
		{
			get
			{
				return _memberStatus;
			}
			set
			{
				_memberStatus = value;
			}
		}

		public bool ReceiveNewsletter
		{
			get
			{
				return _receiveNewsletter;
			}
			set
			{
				_receiveNewsletter = value;
			}
		}

		public bool IsMultiChannel
		{
			get
			{
				return _isMultiChannel;
			}
			set
			{
				_isMultiChannel = value;
			}
		}

		public int DefaultChannel
		{
			get
			{
				return _defaultChannel;
			}
			set
			{
				_defaultChannel = value;
			}
		}

		public int UnreadMessageCount
		{
			get
			{
				return _unreadMessageCount;
			}
			set
			{
				_unreadMessageCount = value;
			}
		}

		private int _id;
		private string _firstName;
		private string _givenName;
		private string _emailAddress;
		private string _password;
		private DateTime _createdDate; // TODO: Nothing happens to below fields
		private DateTime _updatedDate;
		private DateTime _lastLoggedInDate;
		private int _referrerID;
		private bool _receiveNewsletter;
		private Status _homeStatus;
		private Status _memberStatus;
		private int _promotionID; // TODO: Integrate into DB

		// Channel information
		private bool _isMultiChannel;
		private int _defaultChannel;

		// Number of members unread messages
		private int _unreadMessageCount;

	}
}
