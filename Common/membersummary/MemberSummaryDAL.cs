using System;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;

namespace Common
{
	/// <summary>
	/// Summary description for HomeDAL.
	/// </summary>
	public class MemberSummaryDAL : DataAccess
	{

		public MemberSummaryDAL()
		{
		}

		public MemberSummaryCollection GetAll(string firstName, string givenName, string email)
		{
			return GetAll(firstName, givenName, email, 0, 0, 0);
		}

		public MemberSummaryCollection GetAll(string firstName, string givenName, string email, int status)
		{
			return GetAll(firstName, givenName, email, status, 0, 0);
		}

		public MemberSummaryCollection GetAll(string firstName, string givenName, string email, int homeID, int memberID)
		{
			return GetAll(firstName, givenName, email, 0, homeID, memberID);
		}

		public MemberSummaryCollection GetAll(string firstName, string givenName, string email, int status, int homeID, int memberID)
		{
			MemberSummaryCollection memberSummaryCollection = new MemberSummaryCollection();

			const string spName = "up_GetAllMemberSummaries";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection,
                               firstName, givenName, email, status, homeID, memberID))
                {
                    while (reader.Read())
                    {
                        MemberSummary memberSummary = new MemberSummary();

                        memberSummary.ID = GetFieldByInteger(reader, "MemberID");
                        memberSummary.FirstName = GetFieldByString(reader, "FirstName");
                        memberSummary.GivenName = GetFieldByString(reader, "GivenName");
                        memberSummary.EmailAddress = GetFieldByString(reader, "EmailAddress");
                        memberSummary.Password = GetFieldByString(reader, "Password");

                        memberSummary.CreatedDate = GetFieldByDate(reader, "CreatedDate");
                        memberSummary.UpdatedDate = GetFieldByDate(reader, "UpdatedDate");
                        memberSummary.LastLoggedInDate = GetFieldByDate(reader, "LastLoggedInDate");
                        memberSummary.ReferrerID = GetFieldByInteger(reader, "ReferrerID");
                        memberSummary.HomeStatus = (Status)reader["Status"];

                        memberSummaryCollection.Add(memberSummary);
                    }
                } 
            }

			return memberSummaryCollection;
		}


		#region Singleton instance
		public static MemberSummaryDAL Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
					lock (typeof(MemberSummaryDAL))
					{
						if (_instance == null)
						{
							_instance = new MemberSummaryDAL();
						}
					}
				}

				return _instance;
			}
			
		}

		#endregion

		private static MemberSummaryDAL _instance;
	
	}
}
