using System;
using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for HomeCollection.
	/// </summary>
	public class MemberSummaryCollection : CollectionBase
	{
		public MemberSummaryCollection()
		{
		}

		public void Add(MemberSummary memberSummary)
		{
			InnerList.Add(memberSummary);
		}

		public void Remote(MemberSummary memberSummary)
		{
			InnerList.Remove(memberSummary);
		}

		public MemberSummary this[int index]
		{
			get
			{
				return (MemberSummary)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}

	}
}
