using System;
using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for HomeCollection.
	/// </summary>
	public class HomeImageCollection : CollectionBase
	{
		public HomeImageCollection()
		{
		}

		public void Add(HomeImage homeImage)
		{
			InnerList.Add(homeImage);
		}

		public void Remote(HomeImage homeImage)
		{
			InnerList.Remove(homeImage);
		}

		public HomeImage this[int index]
		{
			get
			{
				return (HomeImage)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}

	}
}
