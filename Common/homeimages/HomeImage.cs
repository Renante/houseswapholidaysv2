using System;

namespace Common
{
	/// <summary>
	/// Summary description for Home.
	/// </summary>
	public class HomeImage
	{
		public HomeImage()
		{
		}

		public int ID
		{
			get
			{
				return _id;
			}
			set
			{
				_id = value;
			}
		}

		public int HomeID
		{
			get
			{
				return _homeID;
			}
			set
			{
				_homeID = value;
			}
		}

		public bool IsFeature
		{
			get
			{
				return _isFeature;
			}
			set
			{
				_isFeature = value;
			}
		}

		public string ImageURL
		{
			get
			{
				return _imageURL;
			}
			set
			{
				_imageURL = value;
			}
		}


		public string ImageDescription
		{
			get
			{
				return _imageDescription;
			}
			set
			{
				_imageDescription = value;
			}
		}

		public string ThumbnailImage
		{
			get
			{
				return _thumbnailImage;
			}
			set
			{
				_thumbnailImage = value;
			}
		}

		public string MediumImage
		{
			get
			{
				return _mediumImage;
			}
			set
			{
				_mediumImage = value;
			}
		}

		public string LargeImage
		{
			get
			{
				return _largeImage;
			}
			set
			{
				_largeImage = value;
			}
		}

		public string GiantImage
		{
			get
			{
				return _giantImage;
			}
			set
			{
				_giantImage = value;
			}
		}

		private int _id;
		private int _homeID;
		private bool _isFeature;
		private string _imageURL;
		private string _imageDescription;
		private string _thumbnailImage;
		private string _mediumImage;
		private string _largeImage;
		private string _giantImage;
	
	}
}
