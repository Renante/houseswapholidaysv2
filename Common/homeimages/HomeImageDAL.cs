using System;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Common
{
	/// <summary>
	/// Summary description for HomeDAL.
	/// </summary>
	public class HomeImageDAL : DataAccess
	{

		#region Types
		private sealed class HomeImageFields
		{
			public readonly int HomeImageID = -1;
			public readonly int ImageURL = -1;
			public readonly int ImageDescription = -1;
		}
		#endregion

		public HomeImageDAL()
		{
		}

		public HomeImage Get(int homeImageID)
		{
			const string spName = "up_GetHomeImage";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, homeImageID))
                {
                    if (reader.Read())
                    {
                        HomeImage homeImage = new HomeImage();

                        homeImage.ID = GetFieldByInteger(reader, "HomeImageID");
                        homeImage.HomeID = GetFieldByInteger(reader, "HomeID");
                        homeImage.IsFeature = GetFieldByBoolean(reader, "IsFeature");
                        // TODO : Can we remove below ImageURL field?
                        //homeImage.ImageURL = (string)reader["ImageURL"];
                        homeImage.ImageDescription = GetFieldByString(reader, "ImageDescription");
                        homeImage.ThumbnailImage = GetFieldByString(reader, "ThumbnailImage");
                        homeImage.MediumImage = GetFieldByString(reader, "MediumImage");
                        homeImage.LargeImage = GetFieldByString(reader, "LargeImage");
                        homeImage.GiantImage = GetFieldByString(reader, "GiantImage");

                        return homeImage;
                    }
                    else
                    {
                        return null;
                    }
                } 
            }
		}

		public HomeImage Get(int homeImageID, int memberID)
		{
			const string spName = "up_GetHomeImageByMember";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, homeImageID, memberID))
                {
                    if (reader.Read())
                    {
                        HomeImage homeImage = new HomeImage();

                        homeImage.ID = GetFieldByInteger(reader, "HomeImageID");
                        homeImage.HomeID = GetFieldByInteger(reader, "HomeID");
                        homeImage.IsFeature = GetFieldByBoolean(reader, "IsFeature");
                        // TODO : Can we remove below ImageURL field?
                        //homeImage.ImageURL = (string)reader["ImageURL"];
                        homeImage.ImageDescription = GetFieldByString(reader, "ImageDescription");
                        homeImage.ThumbnailImage = GetFieldByString(reader, "ThumbnailImage");
                        homeImage.MediumImage = GetFieldByString(reader, "MediumImage");
                        homeImage.LargeImage = GetFieldByString(reader, "LargeImage");
                        homeImage.GiantImage = GetFieldByString(reader, "GiantImage");

                        return homeImage;
                    }
                    else
                    {
                        return null;
                    }
                } 
            }
		}

		public void Delete(int homeImageID, int memberID)
		{
			const string spName = "up_DeleteHomeImage";

			ExecuteNonQuery(spName, homeImageID, memberID);
		}

		public HomeImage Save(HomeImage homeImage)
		{

			if (homeImage.ID <= 0)
			{
				// Add new home
				const string spName = "up_InsertHomeImage";

				homeImage.ID = (int)ExecuteScalar(spName, 
						   homeImage.HomeID, homeImage.IsFeature, homeImage.ImageURL, homeImage.ImageDescription, homeImage.ThumbnailImage, homeImage.MediumImage, homeImage.LargeImage, homeImage.GiantImage);
			}
			else
			{
				// Update existing home
				const string spName = "up_UpdateHomeImage";

				ExecuteNonQuery(spName, 
					homeImage.ID, homeImage.HomeID, homeImage.IsFeature, homeImage.ImageDescription, homeImage.ThumbnailImage, homeImage.MediumImage, homeImage.LargeImage, homeImage.GiantImage);
			}

			return homeImage;
		}

		public void SetDefaultImage(int homeID, int homeImageID)
		{

			// Update existing home
			const string spName = "up_UpdateHomeImageDefault";
			ExecuteNonQuery(spName,	homeID, homeImageID);
		}

		public HomeImageCollection GetAllForHome(int homeID)
		{
			HomeImageCollection homeImageCollection = new HomeImageCollection();

			const string spName = "up_GetHomeImages";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, homeID))
                {
                    while (reader.Read())
                    {
                        HomeImage homeImage = new HomeImage();

                        homeImage.ID = GetFieldByInteger(reader, "HomeImageID");
                        homeImage.HomeID = GetFieldByInteger(reader, "HomeID");
                        homeImage.IsFeature = GetFieldByBoolean(reader, "IsFeature");
                        // TODO : Can we remove below ImageURL field?
                        //homeImage.ImageURL = (string)reader["ImageURL"];
                        homeImage.ImageDescription = GetFieldByString(reader, "ImageDescription");
                        homeImage.ThumbnailImage = GetFieldByString(reader, "ThumbnailImage");
                        homeImage.MediumImage = GetFieldByString(reader, "MediumImage");
                        homeImage.LargeImage = GetFieldByString(reader, "LargeImage");
                        homeImage.GiantImage = GetFieldByString(reader, "GiantImage");

                        homeImageCollection.Add(homeImage);
                    }
                } 
            }

			return homeImageCollection;
		}

		private HomeImage LoadFromReader(SqlDataReader reader, HomeImageFields fields)
		{
			HomeImage homeImage = new HomeImage();

			homeImage.ID = reader.GetInt32(fields.HomeImageID);
			homeImage.ImageURL = reader.GetString(fields.HomeImageID);
			homeImage.ImageDescription = reader.GetString(fields.ImageDescription);

			return homeImage;
		}


		#region Singleton instance
		public static HomeImageDAL Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
					lock (typeof(HomeImageDAL))
					{
						if (_instance == null)
						{
							_instance = new HomeImageDAL();
						}
					}
				}

				return _instance;
			}
			
		}

		#endregion


		private static HomeImageDAL _instance;

	}
}
