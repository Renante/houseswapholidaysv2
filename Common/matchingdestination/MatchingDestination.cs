using System;

namespace Common
{
	/// <summary>
	/// Summary description for Home.
	/// </summary>
	public class MatchingDestination
	{
		public MatchingDestination()
		{
		}

		public CityCollection cityCollection
		{
			get
			{
				return _cityCollection;
			}
			set
			{
				_cityCollection = value;
			}
		}

		public RegionCollection regionCollection
		{
			get
			{
				return _regionCollection;
			}
			set
			{
				_regionCollection = value;
			}
		}

		private CityCollection _cityCollection;
		private RegionCollection _regionCollection;
	}
}
