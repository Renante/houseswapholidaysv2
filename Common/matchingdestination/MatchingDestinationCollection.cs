using System;
using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for HomeCollection.
	/// </summary>
	public class MatchingDestinationCollection : CollectionBase
	{
		public MatchingDestinationCollection()
		{
		}

		public void Add(MatchingDestination matchingDestination)
		{
			InnerList.Add(matchingDestination);
		}

		public void Remote(MatchingDestination matchingDestination)
		{
			InnerList.Remove(matchingDestination);
		}

		public MatchingDestination this[int index]
		{
			get
			{
				return (MatchingDestination )InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}

	}
}
