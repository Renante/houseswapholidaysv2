using System;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Common
{
	/// <summary>
	/// Summary description for HomeDAL.
	/// </summary>
	public class MatchingDestinationDAL : DataAccess
	{

		public MatchingDestinationDAL()
		{
		}

		public MatchingDestination Get(string countryCode, string location)
		{
			MatchingDestination matchingDestination = new MatchingDestination();

			const string spName = "up_GetDestinationsByLocation";

			using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, countryCode, location))
                {
                    CityCollection cityCollection = new CityCollection();

                    while (reader.Read())
                    {
                        City city = new City();

                        city.Name = GetFieldByString(reader, "Name");
                        city.RegionCode = GetFieldByString(reader, "RegionCode");
                        city.RegionName = GetFieldByString(reader, "RegionName");
                        city.CountryCode = (string)reader["CountryCode"];
                        //city.Longitude = GetFieldByString(reader, "Longitude");
                        //city.Latitude = GetFieldByString(reader, "Latitude");

                        cityCollection.Add(city);
                        //matchingDestination.cityCollection.Add(city);
                    }

                    matchingDestination.cityCollection = cityCollection;

                    if (reader.NextResult())
                    {
                        RegionCollection regionCollection = new RegionCollection();

                        while (reader.Read())
                        {
                            Region region = new Region();

                            region.CountryCode = GetFieldByString(reader, "CountryCode");
                            region.RegionCode = GetFieldByString(reader, "RegionCode");
                            region.Name = GetFieldByString(reader, "Name");

                            regionCollection.Add(region);
                        }

                        matchingDestination.regionCollection = regionCollection;
                    }
                }
            }

			return matchingDestination;
		}

		#region Singleton instance
		public static MatchingDestinationDAL Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
					lock (typeof(MatchingDestinationDAL))
					{
						if (_instance == null)
						{
							_instance = new MatchingDestinationDAL();
						}
					}
				}

				return _instance;
			}
			
		}
		#endregion

		private static MatchingDestinationDAL _instance;

	}
}
