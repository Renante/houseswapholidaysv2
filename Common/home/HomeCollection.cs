using System;
using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for HomeCollection.
	/// </summary>
	public class HomeCollection : CollectionBase
	{
		public HomeCollection()
		{
		}

		public void Add(Home home)
		{
			InnerList.Add(home);
		}

		public void Remote(Home home)
		{
			InnerList.Remove(home);
		}

		public Home this[int index]
		{
			get
			{
				return (Home)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}

	}
}
