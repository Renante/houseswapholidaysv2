using System;

namespace Common
{
	/// <summary>
	/// Summary description for Home.
	/// </summary>
	public class Home
	{
		public Home()
		{
		}

		public int ID
		{
			get
			{
				return _id;
			}
			set
			{
				_id = value;
			}
		}

		public int MemberID
		{
			get
			{
				return _memberID;
			}
			set
			{
				_memberID = value;
			}
		}

		public string Address1
		{
			get
			{
				return _address1;
			}
			set
			{
				_address1 = value;
			}
		}

		public string Address2
		{
			get
			{
				return _address2;
			}
			set
			{
				_address2 = value;
			}
		}

		public string City
		{
			get
			{
				return _city;
			}
			set
			{
				_city = value;
			}
		}

		public string Summary
		{
			get
			{
				return _summary;
			}
			set
			{
				_summary = value;
			}
		}

		public string Description
		{
			get
			{
				return _description;
			}
			set
			{
				_description = value;
			}
		}

		public string RegionDescription
		{
			get
			{
				return _regionDescription;
			}
			set
			{
				_regionDescription = value;
			}
		}

		public string OtherInformation
		{
			get
			{
				return _otherInformation;
			}
			set
			{
				_otherInformation = value;
			}
		}

		public string AboutUs
		{
			get
			{
				return _aboutUs;
			}
			set
			{
				_aboutUs = value;
			}
		}

		public string AirportName
		{
			get
			{
				return _airportName;
			}
			set
			{
				_airportName = value;
			}
		}


		public int AirportDistance
		{
			get
			{
				return _airportDistance;
			}
			set
			{
				_airportDistance = value;
			}
		}


		public int Bedrooms
		{
			get
			{
				return _bedrooms;
			}
			set
			{
				_bedrooms = value;
			}
		}

		public int Bathrooms
		{
			get
			{
				return _bathrooms;
			}
			set
			{
				_bathrooms = value;
			}
		}

		public int ComfortablySleeps
		{
			get
			{
				return _comfortablySleeps;
			}
			set
			{
				_comfortablySleeps = value;
			}
		}

		public int MaxSleeps
		{
			get
			{
				return _maxsleeps;
			}
			set
			{
				_maxsleeps = value;
			}
		}

		public string ThumbnailURL
		{
			get
			{
				return _thumbnailURL;
			}
			set
			{
				_thumbnailURL = value;
			}
		}

		public string CountryCode
		{
			get
			{
				return _countryCode;
			}
			set
			{
				_countryCode = value;
			}
		}

		public string RegionCode
		{
			get
			{
				return _regionCode;
			}
			set
			{
				_regionCode = value;
			}
		}

		public string Location
		{
			get
			{
				return _location;
			}
			set
			{
				_location = value;
			}
		}

		public string Postcode
		{
			get
			{
				return _postcode;
			}
			set
			{
				_postcode = value;
			}
		}

		public string Suburb
		{
			get
			{
				return _suburb;
			}
			set
			{
				_suburb = value;
			}
		}

		public string Longitude
		{
			get
			{
				return _longitude;
			}
			set
			{
				_longitude = value;
			}
		}

		public string Latitude
		{
			get
			{
				return _latitude;
			}
			set
			{
				_latitude = value;
			}
		}

		public MapType MapType
		{
			get
			{
				return _mapType;
			}
			set
			{
				_mapType = value;
			}
		}

		public bool AllowDateOffers
		{
			get
			{
				return _allowDateOffers;
			}
			set
			{
				_allowDateOffers = value;
			}
		}

		public bool AllowDestinationOffers
		{
			get
			{
				return _allowDestinationOffers;
			}
			set
			{
				_allowDestinationOffers = value;
			}
		}

		public bool JanuaryAvailability
		{
			get
			{
				return _januaryAvailability;
			}
			set
			{
				_januaryAvailability = value;
			}
		}

		public bool FebruaryAvailability
		{
			get
			{
				return _februaryAvailability;
			}
			set
			{
				_februaryAvailability = value;
			}
		}

		public bool MarchAvailability
		{
			get
			{
				return _marchAvailability;
			}
			set
			{
				_marchAvailability = value;
			}
		}

		public bool AprilAvailability
		{
			get
			{
				return _aprilAvailability;
			}
			set
			{
				_aprilAvailability = value;
			}
		}

		public bool MayAvailability
		{
			get
			{
				return _mayAvailability;
			}
			set
			{
				_mayAvailability = value;
			}
		}

		public bool JuneAvailability
		{
			get
			{
				return _juneAvailability;
			}
			set
			{
				_juneAvailability = value;
			}
		}

		public bool JulyAvailability
		{
			get
			{
				return _julyAvailability;
			}
			set
			{
				_julyAvailability = value;
			}
		}

		public bool AugustAvailability
		{
			get
			{
				return _augustAvailability;
			}
			set
			{
				_augustAvailability = value;
			}
		}

		public bool SeptemberAvailability
		{
			get
			{
				return _septemberAvailability;
			}
			set
			{
				_septemberAvailability = value;
			}
		}

		public bool OctoberAvailability
		{
			get
			{
				return _octoberAvailability;
			}
			set
			{
				_octoberAvailability = value;
			}
		}

		public bool NovemberAvailability
		{
			get
			{
				return _novemberAvailability;
			}
			set
			{
				_novemberAvailability = value;
			}
		}

		public bool DecemberAvailability
		{
			get
			{
				return _decemberAvailability;
			}
			set
			{
				_decemberAvailability = value;
			}
		}

        // wooncherk
        public bool ChristmasAvailability { get; set; }
        public bool LastMinuteAvailability { get; set; }

        public string DestinationsList { get; set; }
        // wooncherk

        // chris
        public string PrimaryOrHoliday { get; set; }    //has values "P" or "H" only (for now)

		public bool IsMultiChannel
		{
			get
			{
				return _isMultiChannel;
			}
			set
			{
				_isMultiChannel = value;
			}
		}

		public int AverageReview
		{
			get
			{
				return _averageReview;
			}
			set
			{
				_averageReview = value;
			}
		}

		public int ReviewCount
		{
			get
			{
				return _reviewCount;
			}
			set
			{
				_reviewCount = value;
			}
		}

		public int DefaultChannel
		{
			get
			{
				return _defaultChannel;
			}
			set
			{
				_defaultChannel = value;
			}
		}

        public int PriorityRank
		{
			get
			{
				return _priorityRank;
			}
			set
			{
                _priorityRank = value;
			}
		}

        public int ListingType
		{
			get
			{
                return _listingType;
			}
			set
			{
                _listingType = value;
			}
		}

		public DateTime CreatedDate
		{
			get
			{
				return _createdDate;
			}
			set
			{
				_createdDate = value;
			}
		}

		public DateTime UpdatedDate
		{
			get
			{
				return _updatedDate;
			}
			set
			{
				_updatedDate = value;
			}
		}

		public Status Status
		{
			get
			{
				return _status;
			}
			set
			{
				_status = value;
			}
		}

		public DateTime SubscriptionExpiryDate
		{
			get
			{
				return _subscriptionExpiryDate;
			}
			set
			{
				_subscriptionExpiryDate = value;
			}
		}


		public HomeFeatureCollection HomeFeatures
		{
			get
			{
				return _homeFeatures;
			}
		}

		#region private fields
		private int _id;
		private int _memberID;
		private Status _status;
		private string _address1;
		private string _address2;
		private string _city;
		private string _regionCode;
		private string _countryCode;
		private string _location;

		private string _longitude;
		private string _latitude;
		private MapType _mapType;
		
		private string _thumbnailURL;

		private string _summary;
		private string _description;
		private string _regionDescription;
		private string _otherInformation;
		private string _aboutUs;
		private string _airportName;

		private string _postcode;
		private string _suburb;

		private int _airportDistance;
		private int _bedrooms;
		private int _bathrooms;
		private int _comfortablySleeps;
		private int _maxsleeps;

		private bool _allowDateOffers;
		private bool _allowDestinationOffers;

		private bool _januaryAvailability;
		private bool _februaryAvailability;
		private bool _marchAvailability;
		private bool _aprilAvailability;
		private bool _mayAvailability;
		private bool _juneAvailability;
		private bool _julyAvailability;
		private bool _augustAvailability;
		private bool _septemberAvailability;
		private bool _octoberAvailability;
		private bool _novemberAvailability;
		private bool _decemberAvailability;

		private bool _isMultiChannel;
		private int _defaultChannel;

		private int _averageReview;
		private int _reviewCount;

		private DateTime _createdDate;
		private DateTime _updatedDate;
		private DateTime _subscriptionExpiryDate = Convert.ToDateTime("1753-01-01");

        private int _priorityRank;
        private int _listingType;

		private HomeFeatureCollection _homeFeatures = new HomeFeatureCollection();
		#endregion
    }
}
