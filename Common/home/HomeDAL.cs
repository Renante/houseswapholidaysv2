using System;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;
using System.Collections.Generic;

namespace Common
{
    /// <summary>
    /// Summary description for HomeDAL.
    /// </summary>
    public class HomeDAL : DataAccess
    {

        #region Types
        private sealed class HomeFields
        {
            public readonly int HomeID = -1;
            public readonly int Description = -1;
        }
        #endregion

        public HomeDAL()
        {
        }

        public Home Save(Home home)
        {
            if (home.ID <= 0)
            {
                // Add new home
                const string spName = "up_InsertHome";

                home.ID = (int)ExecuteScalar(spName, home.MemberID, (int)home.Status, home.CountryCode, home.RegionCode, home.Postcode, home.Suburb, home.Summary, home.Description, home.RegionDescription, home.OtherInformation, home.AboutUs, home.ThumbnailURL, home.City, home.Location, home.Longitude, home.Latitude, (int)home.MapType, home.Bedrooms, home.Bathrooms, home.ComfortablySleeps, home.MaxSleeps, home.AirportName, home.AirportDistance, home.AllowDateOffers, home.AllowDestinationOffers, home.JanuaryAvailability, home.FebruaryAvailability, home.MarchAvailability, home.AprilAvailability, home.MayAvailability, home.JuneAvailability, home.JulyAvailability, home.AugustAvailability, home.SeptemberAvailability, home.OctoberAvailability, home.NovemberAvailability, home.DecemberAvailability, home.IsMultiChannel, home.DefaultChannel);
            }
            else
            {
                // Update existing home
                const string spName = "up_HSHUpdateHome2014";

                // TODO: Subscription date is currently hardcoded. Also missing city???
                // wooncherk - last parameter - DU updated to lastminute 3/11
                // chris - added PrimaryOrHoliday as last parameter 7/29/2014
                ExecuteNonQuery(spName,
                    home.ID, home.MemberID, (int)home.Status, home.CountryCode, home.RegionCode, home.City, home.Location, home.Postcode, home.Suburb, home.Longitude,
                    home.Latitude, home.MapType, home.Summary, home.Description, home.RegionDescription, home.OtherInformation, home.AboutUs, home.ThumbnailURL, home.Bedrooms, home.Bathrooms,
                    home.ComfortablySleeps, home.MaxSleeps, home.AirportName, home.AirportDistance, home.AllowDateOffers, home.AllowDestinationOffers, home.JanuaryAvailability, home.FebruaryAvailability, home.MarchAvailability, home.AprilAvailability,
                    home.MayAvailability, home.JuneAvailability,
                    home.JulyAvailability, home.AugustAvailability,
                    home.SeptemberAvailability, home.OctoberAvailability,
                    home.NovemberAvailability, home.DecemberAvailability,
                    home.IsMultiChannel, home.DefaultChannel,
                    home.SubscriptionExpiryDate, home.LastMinuteAvailability, home.PrimaryOrHoliday);
            }

            return home;
        }

        public void UpdateMemberListingType(string memberKey, int homeID, int status, int listingType)
        {
            // Update Member Listing (typically when a member has paid for an upgrade)
            const string spName = "up_UpdateMemberListingType";

            ExecuteNonQuery(spName, memberKey, homeID, status, listingType);
        }

        public void UpdateHomeAvailability(string memberKey, int homeID, bool availability)
        {
            // Update Member Listing (typically when a member has paid for an upgrade)
            const string spName = "up_UpdateHomeAvailability";

            ExecuteNonQuery(spName, memberKey, homeID, availability);
        }


        public HomeAdmin SaveAdminDetails(HomeAdmin homeAdmin)
        {
            // Update existing homeadmin details
            const string spName = "up_UpdateHomeAdminDetails";

            // TODO: Subscription date is currently hardcoded. Also missing city???
            ExecuteNonQuery(spName,
                homeAdmin.ID, homeAdmin.MemberID, (int)homeAdmin.Status, homeAdmin.CountryCode, homeAdmin.RegionCode, homeAdmin.City, homeAdmin.Location, homeAdmin.Postcode, homeAdmin.Suburb, homeAdmin.Longitude, homeAdmin.Latitude, homeAdmin.MapType, homeAdmin.Summary, homeAdmin.Description, homeAdmin.RegionDescription, homeAdmin.OtherInformation, homeAdmin.AboutUs, homeAdmin.ThumbnailURL, homeAdmin.Bedrooms, homeAdmin.Bathrooms, homeAdmin.ComfortablySleeps, homeAdmin.MaxSleeps, homeAdmin.AirportName, homeAdmin.AirportDistance, homeAdmin.AllowDateOffers, homeAdmin.AllowDestinationOffers, homeAdmin.JanuaryAvailability, homeAdmin.FebruaryAvailability, homeAdmin.MarchAvailability, homeAdmin.AprilAvailability, homeAdmin.MayAvailability, homeAdmin.JuneAvailability, homeAdmin.JulyAvailability, homeAdmin.AugustAvailability, homeAdmin.SeptemberAvailability, homeAdmin.OctoberAvailability, homeAdmin.NovemberAvailability, homeAdmin.DecemberAvailability, homeAdmin.IsMultiChannel, homeAdmin.DefaultChannel, homeAdmin.SubscriptionExpiryDate, homeAdmin.PriorityRank);

            return homeAdmin;
        }

        public void GetMemberCountry(ref string countryName, int memberID, ref int homeID)
        {
            const string spName = "up_GetMemberCountryFromHome";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, memberID))
                {
                    if (reader.Read())
                    {
                        //memberID = GetFieldByInteger(reader,"MemberID");
                        homeID = GetFieldByInteger(reader, "HomeID");
                        countryName = GetFieldByString(reader, "CountryName");
                    }
                }
            }
        }

        public void GetMemberCountry(ref string countryName, int memberID)
        {
            const string spName = "up_GetMemberCountryFromHome";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, memberID))
                {
                    if (reader.Read())
                    {
                        //memberID = GetFieldByInteger(reader,"MemberID");
                        countryName = GetFieldByString(reader, "CountryName");
                    }
                }
            }
        }

        public void GetMemberCountry(int homeID, ref string countryName, ref int memberID)
        {
            const string spName = "up_GetHomeMemberCountry";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, homeID))
                {
                    if (reader.Read())
                    {
                        memberID = GetFieldByInteger(reader, "MemberID");
                        countryName = GetFieldByString(reader, "CountryName");
                    }
                }
            }
        }

        public Home Get(int homeID)
        {
            const string spName = "up_GetHome";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, homeID))
                {
                    if (reader.Read())
                    {
                        Home home = new Home();

                        home = GetHomeFromReader(reader);
                        home.AverageReview = GetFieldByInteger(reader, "AverageReview");
                        home.ReviewCount = GetFieldByInteger(reader, "ReviewCount");
                        return home;
                    }
                    else
                    {
                        throw new ApplicationException(String.Format("Home not found - home ID {0} does not exist.", homeID));
                        // return null;
                    }
                }
            }
        }
        /// <summary>
        /// Returns all default home details **PLUS** details required specifically for admin purposes (e.g. priorityrank etc)
        /// </summary>
        /// <param name="homeID"></param>
        /// <returns></returns>
        public HomeAdmin GetAdminDetails(int homeID)
        {
            const string spName = "up_GetHomeAdminDetails";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, homeID))
                {
                    if (reader.Read())
                    {
                        HomeAdmin homeAdmin = new HomeAdmin();

                        // read all home admin details
                        homeAdmin.ID = (int)reader["HomeID"];
                        homeAdmin.MemberID = (int)reader["MemberID"];
                        homeAdmin.Status = (Status)GetFieldByInteger(reader, "Status");

                        homeAdmin.Postcode = GetFieldByString(reader, "Postcode");
                        homeAdmin.Suburb = GetFieldByString(reader, "Suburb");

                        homeAdmin.Location = GetFieldByString(reader, "Location");
                        homeAdmin.Longitude = GetFieldByString(reader, "Longitude");
                        homeAdmin.Latitude = GetFieldByString(reader, "Latitude");
                        homeAdmin.MapType = (MapType)reader["MapType"];


                        // Get airport info
                        homeAdmin.AirportName = GetFieldByString(reader, "AirportName");
                        homeAdmin.AirportDistance = GetFieldByInteger(reader, "AirportDistance");

                        homeAdmin.Address1 = GetFieldByString(reader, "Address1");
                        homeAdmin.Address2 = GetFieldByString(reader, "Address2");
                        homeAdmin.City = GetFieldByString(reader, "City");
                        homeAdmin.CountryCode = GetFieldByString(reader, "CountryCode");
                        homeAdmin.RegionCode = GetFieldByString(reader, "RegionCode");

                        homeAdmin.Summary = GetFieldByString(reader, "Summary");
                        homeAdmin.Description = GetFieldByString(reader, "Description");
                        homeAdmin.RegionDescription = GetFieldByString(reader, "RegionDescription");
                        homeAdmin.OtherInformation = GetFieldByString(reader, "OtherInformation");
                        homeAdmin.AboutUs = GetFieldByString(reader, "AboutUs");
                        homeAdmin.ThumbnailURL = GetFieldByString(reader, "ThumbnailURL");

                        // Get bedrooms etc
                        homeAdmin.Bedrooms = GetFieldByInteger(reader, "Bedrooms");
                        homeAdmin.Bathrooms = GetFieldByInteger(reader, "Bathrooms");
                        homeAdmin.ComfortablySleeps = GetFieldByInteger(reader, "ComfortablySleeps");
                        homeAdmin.MaxSleeps = GetFieldByInteger(reader, "MaxSleeps");

                        // Get availability info
                        homeAdmin.JanuaryAvailability = GetFieldByBoolean(reader, "JanuaryAvailability");
                        homeAdmin.FebruaryAvailability = GetFieldByBoolean(reader, "FebruaryAvailability");
                        homeAdmin.MarchAvailability = GetFieldByBoolean(reader, "MarchAvailability");
                        homeAdmin.AprilAvailability = GetFieldByBoolean(reader, "AprilAvailability");
                        homeAdmin.MayAvailability = GetFieldByBoolean(reader, "MayAvailability");
                        homeAdmin.JuneAvailability = GetFieldByBoolean(reader, "JuneAvailability");
                        homeAdmin.JulyAvailability = GetFieldByBoolean(reader, "JulyAvailability");
                        homeAdmin.AugustAvailability = GetFieldByBoolean(reader, "AugustAvailability");
                        homeAdmin.SeptemberAvailability = GetFieldByBoolean(reader, "SeptemberAvailability");
                        homeAdmin.OctoberAvailability = GetFieldByBoolean(reader, "OctoberAvailability");
                        homeAdmin.NovemberAvailability = GetFieldByBoolean(reader, "NovemberAvailability");
                        homeAdmin.DecemberAvailability = GetFieldByBoolean(reader, "DecemberAvailability");

                        // Get home channel information
                        homeAdmin.DefaultChannel = GetFieldByInteger(reader, "DefaultChannel");
                        homeAdmin.IsMultiChannel = GetFieldByBoolean(reader, "IsMultiChannel");

                        // Allow offers?
                        homeAdmin.AllowDateOffers = GetFieldByBoolean(reader, "AllowDateOffers");
                        homeAdmin.AllowDestinationOffers = GetFieldByBoolean(reader, "AllowDestinationOffers");

                        // Get home list subscription expiry date
                        homeAdmin.SubscriptionExpiryDate = GetFieldByDate(reader, "SubscriptionExpiryDate");

                        // Get additional information required just for admin purposes
                        homeAdmin.PriorityRank = GetFieldByInteger(reader, "PriorityRank");

                        return homeAdmin;
                    }
                    else
                    {
                        throw new ApplicationException(String.Format("Home not found - home ID {0} does not exist.", homeID));
                        // return null;
                    }
                }
            }
        }

        public HomeCollection GetAllActive(int channelID)
        {
            HomeCollection homeCollection = new HomeCollection();

            const string spName = "up_GetAllActiveHomes";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, channelID))
                {
                    while (reader.Read())
                    {
                        Home home = new Home();

                        home = GetHomeFromReader(reader);

                        // Add to collection
                        homeCollection.Add(home);
                    }

                }
            }

            return homeCollection;
        }

        public HomeCollection GetByMemberID(int memberID)
        {
            HomeCollection homeCollection = new HomeCollection();

            const string spName = "up_GetHomeByMemberID";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, memberID))
                {
                    while (reader.Read())
                    {
                        Home home = new Home();

                        home = GetHomeFromReader(reader);
                        // wooncherk
                        home.ChristmasAvailability = GetFieldByBoolean(reader, "ChristmasAvailability");
                        // wooncherk

                        // Add to collection
                        homeCollection.Add(home);
                    }

                }
            }

            return homeCollection;
        }

        public HomeCollection GetNew(int hoursBack)
        {
            HomeCollection homeCollection = new HomeCollection();

            const string spName = "up_GetNewHomes";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, hoursBack))
                {
                    while (reader.Read())
                    {
                        Home home = new Home();

                        home = GetHomeFromReader(reader);

                        // Add to collection
                        homeCollection.Add(home);
                    }

                }
            }

            return homeCollection;
        }

        public Home GetHomeFromReader(SqlDataReader reader)
        {

            Home home = new Home();

            home.ID = (int)reader["HomeID"];
            home.MemberID = (int)reader["MemberID"];
            home.Status = (Status)GetFieldByInteger(reader, "Status");

            home.Postcode = GetFieldByString(reader, "Postcode");
            home.Suburb = GetFieldByString(reader, "Suburb");

            home.Location = GetFieldByString(reader, "Location");
            home.Longitude = GetFieldByString(reader, "Longitude");
            home.Latitude = GetFieldByString(reader, "Latitude");
            home.MapType = (MapType)reader["MapType"];


            // Get airport info
            home.AirportName = GetFieldByString(reader, "AirportName");
            home.AirportDistance = GetFieldByInteger(reader, "AirportDistance");

            home.Address1 = GetFieldByString(reader, "Address1");
            home.Address2 = GetFieldByString(reader, "Address2");
            home.City = GetFieldByString(reader, "City");
            home.CountryCode = GetFieldByString(reader, "CountryCode");
            home.RegionCode = GetFieldByString(reader, "RegionCode");

            home.Summary = GetFieldByString(reader, "Summary");
            home.Description = GetFieldByString(reader, "Description");
            home.RegionDescription = GetFieldByString(reader, "RegionDescription");
            home.OtherInformation = GetFieldByString(reader, "OtherInformation");
            home.AboutUs = GetFieldByString(reader, "AboutUs");
            home.ThumbnailURL = GetFieldByString(reader, "ThumbnailURL");

            // Get bedrooms etc
            home.Bedrooms = GetFieldByInteger(reader, "Bedrooms");
            home.Bathrooms = GetFieldByInteger(reader, "Bathrooms");
            home.ComfortablySleeps = GetFieldByInteger(reader, "ComfortablySleeps");
            home.MaxSleeps = GetFieldByInteger(reader, "MaxSleeps");

            // Get availability info
            home.JanuaryAvailability = GetFieldByBoolean(reader, "JanuaryAvailability");
            home.FebruaryAvailability = GetFieldByBoolean(reader, "FebruaryAvailability");
            home.MarchAvailability = GetFieldByBoolean(reader, "MarchAvailability");
            home.AprilAvailability = GetFieldByBoolean(reader, "AprilAvailability");
            home.MayAvailability = GetFieldByBoolean(reader, "MayAvailability");
            home.JuneAvailability = GetFieldByBoolean(reader, "JuneAvailability");
            home.JulyAvailability = GetFieldByBoolean(reader, "JulyAvailability");
            home.AugustAvailability = GetFieldByBoolean(reader, "AugustAvailability");
            home.SeptemberAvailability = GetFieldByBoolean(reader, "SeptemberAvailability");
            home.OctoberAvailability = GetFieldByBoolean(reader, "OctoberAvailability");
            home.NovemberAvailability = GetFieldByBoolean(reader, "NovemberAvailability");
            home.DecemberAvailability = GetFieldByBoolean(reader, "DecemberAvailability");

            // Get home channel information
            home.DefaultChannel = GetFieldByInteger(reader, "DefaultChannel");
            home.IsMultiChannel = GetFieldByBoolean(reader, "IsMultiChannel");

            // Allow offers?
            home.AllowDateOffers = GetFieldByBoolean(reader, "AllowDateOffers");
            home.AllowDestinationOffers = GetFieldByBoolean(reader, "AllowDestinationOffers");

            // Get home list subscription expiry date
            home.SubscriptionExpiryDate = GetFieldByDate(reader, "SubscriptionExpiryDate");

            home.ListingType = GetFieldByInteger(reader, "ListingType");
            home.LastMinuteAvailability = GetFieldByBoolean(reader, "LastMinuteAvailability");
            home.PrimaryOrHoliday = GetFieldByString(reader, "PrimaryOrHoliday");

            return home;
        }

        public HomeCollection GetByMemberID(int homeID, int memberID)
        {
            HomeCollection homeCollection = new HomeCollection();

            const string spName = "up_GetHomeByHomeIDAndMemberID";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, homeID, memberID))
                {
                    while (reader.Read())
                    {
                        Home home = new Home();

                        home = GetHomeFromReader(reader);

                        // Add to collection
                        homeCollection.Add(home);
                    }

                }
            }

            return homeCollection;
        }

        public HomeCollection GetRecentListings(int numberToDisplay)
        {
            return GetRecentListings(numberToDisplay, -1);
        }

        public HomeCollection GetRecentListings(int numberToDisplay, int channelID)
        {
            HomeCollection homeCollection = new HomeCollection();

            const string spName = "up_GetRecentHomeListings";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, numberToDisplay, channelID))
                {
                    while (reader.Read())
                    {
                        Home home = new Home();

                        home.ID = (int)reader["HomeID"];
                        home.MemberID = (int)reader["MemberID"];
                        home.Status = (Status)reader["Status"];

                        // Get home location details and comments
                        home.Address1 = GetFieldByString(reader, "Address1");
                        home.Address2 = GetFieldByString(reader, "Address2");
                        home.City = GetFieldByString(reader, "City");
                        home.RegionCode = GetFieldByString(reader, "RegionCode");
                        home.CountryCode = GetFieldByString(reader, "CountryCode");
                        home.Description = GetFieldByString(reader, "Description");
                        home.RegionDescription = GetFieldByString(reader, "RegionDescription");
                        home.OtherInformation = GetFieldByString(reader, "OtherInformation");
                        home.AboutUs = GetFieldByString(reader, "AboutUs");
                        home.Location = GetFieldByString(reader, "Location");
                        //home.Summary = GetFieldByString(reader, "Summary");

                        // Get airport info
                        home.AirportName = GetFieldByString(reader, "AirportName");
                        home.AirportDistance = GetFieldByInteger(reader, "AirportDistance");

                        // Get home details
                        home.Bedrooms = GetFieldByInteger(reader, "Bedrooms");
                        home.Bathrooms = GetFieldByInteger(reader, "Bathrooms");
                        home.ComfortablySleeps = GetFieldByInteger(reader, "ComfortablySleeps");
                        home.MaxSleeps = GetFieldByInteger(reader, "MaxSleeps");

                        home.ThumbnailURL = GetFieldByString(reader, "ThumbnailURL");
                        home.CreatedDate = GetFieldByDate(reader, "CreatedDate");
                        home.UpdatedDate = GetFieldByDate(reader, "UpdatedDate");

                        // Add to collection
                        homeCollection.Add(home);
                    }

                    // reader.Close();
                }
            }

            return homeCollection;
        }

        public HomeCollection GetAllRecentHomes(string fromDateTime)
        {
            HomeCollection homeCollection = new HomeCollection();

            const string spName = "up_GetAllRecentHomes";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, fromDateTime))
                {
                    while (reader.Read())
                    {
                        Home home = new Home();

                        home.ID = (int)reader["HomeID"];
                        home.MemberID = (int)reader["MemberID"];
                        home.Status = (Status)reader["Status"];

                        // Get home location details and comments
                        home.Address1 = GetFieldByString(reader, "Address1");
                        home.Address2 = GetFieldByString(reader, "Address2");
                        home.City = GetFieldByString(reader, "City");
                        home.RegionCode = GetFieldByString(reader, "RegionCode");
                        home.CountryCode = GetFieldByString(reader, "CountryCode");
                        home.Description = GetFieldByString(reader, "Description");
                        home.RegionDescription = GetFieldByString(reader, "RegionDescription");
                        home.OtherInformation = GetFieldByString(reader, "OtherInformation");
                        home.AboutUs = GetFieldByString(reader, "AboutUs");
                        home.Location = GetFieldByString(reader, "Location");

                        // Get airport info
                        home.AirportName = GetFieldByString(reader, "AirportName");
                        home.AirportDistance = GetFieldByInteger(reader, "AirportDistance");

                        // Get home details
                        home.Bedrooms = GetFieldByInteger(reader, "Bedrooms");
                        home.Bathrooms = GetFieldByInteger(reader, "Bathrooms");
                        home.ComfortablySleeps = GetFieldByInteger(reader, "ComfortablySleeps");
                        home.MaxSleeps = GetFieldByInteger(reader, "MaxSleeps");

                        home.ThumbnailURL = GetFieldByString(reader, "ThumbnailURL");
                        home.CreatedDate = GetFieldByDate(reader, "CreatedDate");
                        home.UpdatedDate = GetFieldByDate(reader, "UpdatedDate");

                        home.DefaultChannel = GetFieldByInteger(reader, "DefaultChannel");

                        // Add to collection
                        homeCollection.Add(home);
                    }
                }
            }

            return homeCollection;
        }

        // Generate SQL query string for months search criteria
        // Implemented in c#, rather than a proc due to the complexity involved.
        public string SQLMonthCriteria(int startMonth, int endMonth)
        {
            string searchcriteria = "(";

            if (startMonth == 0 || endMonth == 0) return "()";
            if (startMonth > endMonth) return "()";


            if (startMonth <= endMonth)
            {
                while (startMonth <= endMonth)
                {

                    if (searchcriteria.Length > 1)
                    {
                        searchcriteria = searchcriteria + " or ";
                    }

                    switch (startMonth)
                    {
                        case 1:
                            searchcriteria = searchcriteria + " januaryavailability = 1 ";
                            break;

                        case 2:
                            searchcriteria = searchcriteria + " februaryavailability = 1 ";
                            break;

                        case 3:
                            searchcriteria = searchcriteria + " marchavailability = 1 ";
                            break;

                        case 4:
                            searchcriteria = searchcriteria + " aprilavailability = 1 ";
                            break;

                        case 5:
                            searchcriteria = searchcriteria + " mayavailability = 1 ";
                            break;

                        case 6:
                            searchcriteria = searchcriteria + " juneavailability = 1 ";
                            break;

                        case 7:
                            searchcriteria = searchcriteria + " julyavailability = 1 ";
                            break;

                        case 8:
                            searchcriteria = searchcriteria + " augustavailability = 1 ";
                            break;

                        case 9:
                            searchcriteria = searchcriteria + " septemberavailability = 1 ";
                            break;

                        case 10:
                            searchcriteria = searchcriteria + " octoberavailability = 1 ";
                            break;

                        case 11:
                            searchcriteria = searchcriteria + " novemberavailability = 1 ";
                            break;

                        case 12:
                            searchcriteria = searchcriteria + " decemberavailability = 1 ";
                            break;
                    }

                    startMonth++;
                }
            }
            else if (startMonth > endMonth)
            {
                int monthCount = 1;
                // append all months up until the endmonth
                while (monthCount <= endMonth)
                {
                    if (searchcriteria.Length > 1)
                    {
                        searchcriteria = searchcriteria + " or ";
                    }

                    switch (monthCount)
                    {
                        case 1:
                            searchcriteria = searchcriteria + " januaryavailability = 1 ";
                            break;

                        case 2:
                            searchcriteria = searchcriteria + " februaryavailability = 1 ";
                            break;

                        case 3:
                            searchcriteria = searchcriteria + " marchavailability = 1 ";
                            break;

                        case 4:
                            searchcriteria = searchcriteria + " aprilavailability = 1 ";
                            break;

                        case 5:
                            searchcriteria = searchcriteria + " mayavailability = 1 ";
                            break;

                        case 6:
                            searchcriteria = searchcriteria + " juneavailability = 1 ";
                            break;

                        case 7:
                            searchcriteria = searchcriteria + " julyavailability = 1 ";
                            break;

                        case 8:
                            searchcriteria = searchcriteria + " augustavailability = 1 ";
                            break;

                        case 9:
                            searchcriteria = searchcriteria + " septemberavailability = 1 ";
                            break;

                        case 10:
                            searchcriteria = searchcriteria + " octoberavailability = 1 ";
                            break;

                        case 11:
                            searchcriteria = searchcriteria + " novemberavailability = 1 ";
                            break;

                        case 12:
                            searchcriteria = searchcriteria + " decemberavailability = 1 ";
                            break;

                    }

                    monthCount++;
                }

                // append all months from the startmonth till the end of the year
                monthCount = startMonth;
                while (monthCount <= 12)
                {
                    if (searchcriteria.Length > 1)
                    {
                        searchcriteria = searchcriteria + " or ";
                    }

                    switch (monthCount)
                    {
                        case 1:
                            searchcriteria = searchcriteria + " januaryavailability = 1 ";
                            break;

                        case 2:
                            searchcriteria = searchcriteria + " februaryavailability = 1 ";
                            break;

                        case 3:
                            searchcriteria = searchcriteria + " marchavailability = 1 ";
                            break;

                        case 4:
                            searchcriteria = searchcriteria + " aprilavailability = 1 ";
                            break;

                        case 5:
                            searchcriteria = searchcriteria + " mayavailability = 1 ";
                            break;

                        case 6:
                            searchcriteria = searchcriteria + " juneavailability = 1 ";
                            break;

                        case 7:
                            searchcriteria = searchcriteria + " julyavailability = 1 ";
                            break;

                        case 8:
                            searchcriteria = searchcriteria + " augustavailability = 1 ";
                            break;

                        case 9:
                            searchcriteria = searchcriteria + " septemberavailability = 1 ";
                            break;

                        case 10:
                            searchcriteria = searchcriteria + " octoberavailability = 1 ";
                            break;

                        case 11:
                            searchcriteria = searchcriteria + " novemberavailability = 1 ";
                            break;

                        case 12:
                            searchcriteria = searchcriteria + " decemberavailability = 1 ";
                            break;

                    }

                    monthCount++;
                }

            }

            searchcriteria = searchcriteria + ")";

            return searchcriteria;
        }

        public HomeCriteria Search(HomeCriteria homeCriteria)
        {
            string sqlMonthCriteria = SQLMonthCriteria(homeCriteria.StartMonth, homeCriteria.EndMonth);

            const string spName = "up_SearchHomes";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection,
                               homeCriteria.PageCriteria.CurrentPage, homeCriteria.PageCriteria.PageSize, homeCriteria.Bedrooms, homeCriteria.Keyword, homeCriteria.ContinentID, homeCriteria.CountryCode, homeCriteria.RegionCode, homeCriteria.FeatureList, homeCriteria.DefaultChannel, homeCriteria.IsMultiChannel, homeCriteria.StartMonth, homeCriteria.EndMonth, sqlMonthCriteria))
                {

                    if (reader.Read())
                    {
                        homeCriteria.HomeCount = reader.GetInt32(reader.GetOrdinal("RecordCount"));
                    }

                    bool gotResults = reader.NextResult();
                    if (!gotResults)
                    {
                        throw new ApplicationException(String.Format("No results found for search, stored procedure '{0}' returned no results set.", spName));
                    }

                    HomeCollection homeCollection = new HomeCollection();

                    while (reader.Read())
                    {
                        Home home = new Home();

                        home.ID = (int)reader["HomeID"];
                        home.Description = (string)reader["Description"];

                        home.Bedrooms = GetFieldByInteger(reader, "Bedrooms");
                        home.Bathrooms = GetFieldByInteger(reader, "Bathrooms");
                        home.Location = GetFieldByString(reader, "Location");
                        home.ThumbnailURL = GetFieldByString(reader, "ThumbnailURL");

                        homeCollection.Add(home);
                    }

                    homeCriteria.homeCollection = homeCollection;
                }
            }

            return homeCriteria;
        }

        public HomeCriteria SearchByPreferredDestinationByChannel(HomeCriteria homeCriteria)
        {
            string sqlMonthCriteria = SQLMonthCriteria(homeCriteria.StartMonth, homeCriteria.EndMonth);

            const string spName = "up_HSHSearchHomesByPreferredDestinationByChannel2014";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection,
                               homeCriteria.PageCriteria.CurrentPage, homeCriteria.PageCriteria.PageSize, homeCriteria.Bedrooms, homeCriteria.Keyword, homeCriteria.ContinentID, homeCriteria.CountryCode, homeCriteria.RegionCode, homeCriteria.FeatureList, homeCriteria.DefaultChannel, homeCriteria.IsMultiChannel, homeCriteria.StartMonth, homeCriteria.EndMonth, sqlMonthCriteria, homeCriteria.ReverseCountryCode, homeCriteria.ReverseDestination, homeCriteria.ReverseFilterType, (int)homeCriteria.SortField, (int)homeCriteria.SortOrder))
                {

                    if (reader.Read())
                    {
                        homeCriteria.HomeCount = reader.GetInt32(reader.GetOrdinal("RecordCount"));
                    }

                    bool gotResults = reader.NextResult();
                    if (!gotResults)
                    {
                        throw new ApplicationException(String.Format("No results found for search, stored procedure '{0}' returned no results set.", spName));
                    }

                    HomeCollection homeCollection = new HomeCollection();

                    while (reader.Read())
                    {
                        Home home = new Home();

                        home.ID = (int)reader["HomeID"];
                        home.Description = (string)reader["Description"];

                        home.Bedrooms = GetFieldByInteger(reader, "Bedrooms");
                        home.Bathrooms = GetFieldByInteger(reader, "Bathrooms");
                        home.Location = GetFieldByString(reader, "Location");
                        home.City = GetFieldByString(reader, "City");
                        home.Summary = GetFieldByString(reader, "Summary");
                        home.CreatedDate = GetFieldByDate(reader, "CreatedDate");
                        home.ThumbnailURL = GetFieldByString(reader, "ThumbnailURL");

                        home.PriorityRank = GetFieldByInteger(reader, "PriorityRank");
                        home.ListingType = GetFieldByInteger(reader, "ListingType");

                        // wooncherk
                        home.MemberID = GetFieldByInteger(reader, "MemberID");
                        home.AllowDestinationOffers =
                            GetFieldByBoolean(reader, "AllowDestinationOffers");
                        home.JanuaryAvailability = GetFieldByBoolean(reader, "JanuaryAvailability");
                        home.FebruaryAvailability = GetFieldByBoolean(reader, "FebruaryAvailability");
                        home.MarchAvailability = GetFieldByBoolean(reader, "MarchAvailability");
                        home.AprilAvailability = GetFieldByBoolean(reader, "AprilAvailability");
                        home.MayAvailability = GetFieldByBoolean(reader, "MayAvailability");
                        home.JuneAvailability = GetFieldByBoolean(reader, "JuneAvailability");
                        home.JulyAvailability = GetFieldByBoolean(reader, "JulyAvailability");
                        home.AugustAvailability = GetFieldByBoolean(reader, "AugustAvailability");
                        home.SeptemberAvailability =
                            GetFieldByBoolean(reader, "SeptemberAvailability");
                        home.OctoberAvailability = GetFieldByBoolean(reader, "OctoberAvailability");
                        home.NovemberAvailability = GetFieldByBoolean(reader, "NovemberAvailability");
                        home.DecemberAvailability = GetFieldByBoolean(reader, "DecemberAvailability");
                        home.ChristmasAvailability = GetFieldByBoolean(reader, "ChristmasAvailability");
                        home.LastMinuteAvailability = GetFieldByBoolean(reader, "LastMinuteAvailability");

                        try
                        {
                            home.DestinationsList = GetFieldByString(reader, "DestinationsList");
                        }
                        catch { }
                        // wooncherk

                        homeCollection.Add(home);
                    }

                    homeCriteria.homeCollection = homeCollection;
                }
            }

            return homeCriteria;
        }

        public HomeCriteria Search2(HomeCriteria homeCriteria)
        {
            string sqlMonthCriteria = SQLMonthCriteria(homeCriteria.StartMonth, homeCriteria.EndMonth);


            _log.Info("homeCriteria.PageCriteria.CurrentPage: " + homeCriteria.PageCriteria.CurrentPage);
            _log.Info("homeCriteria.PageCriteria.PageSize: " + homeCriteria.PageCriteria.PageSize);
            _log.Info("homeCriteria.Bedrooms: " + homeCriteria.Bedrooms);
            _log.Info("homeCriteria.Keyword: " + homeCriteria.Keyword);
            _log.Info("homeCriteria.ContinentID: " + homeCriteria.ContinentID);
            _log.Info("homeCriteria.CountryCode: " + homeCriteria.CountryCode);
            _log.Info("homeCriteria.RegionCode: " + homeCriteria.RegionCode);
            _log.Info("homeCriteria.FeatureList: " + homeCriteria.FeatureList);
            _log.Info("homeCriteria.DefaultChannel: " + homeCriteria.DefaultChannel);
            _log.Info("homeCriteria.IsMultiChannel: " + homeCriteria.IsMultiChannel);
            _log.Info("homeCriteria.StartMonth: " + homeCriteria.StartMonth);
            _log.Info("homeCriteria.EndMonth: " + homeCriteria.EndMonth);
            _log.Info("sqlMonthCriteria: " + sqlMonthCriteria);
            _log.Info("(int)homeCriteria.SortField: " + (int)homeCriteria.SortField);
            _log.Info("(int)homeCriteria.SortOrder: " + (int)homeCriteria.SortOrder);
            _log.Info("__");

            if (homeCriteria.CurrentMemberID <= 0)
            {
                
                const string spName = "up_SearchHomes2";
                _log.Info(spName);
                using (var connection = GetConnection())
                {
                    using (SqlDataReader reader = ExecuteReader(spName, connection,
                                       homeCriteria.PageCriteria.CurrentPage, homeCriteria.PageCriteria.PageSize, homeCriteria.Bedrooms, homeCriteria.Keyword, homeCriteria.ContinentID, homeCriteria.CountryCode, homeCriteria.RegionCode, homeCriteria.FeatureList, homeCriteria.DefaultChannel, homeCriteria.IsMultiChannel, homeCriteria.StartMonth, homeCriteria.EndMonth, sqlMonthCriteria, (int)homeCriteria.SortField, (int)homeCriteria.SortOrder))
                    {

                        if (reader.Read())
                        {
                            homeCriteria.HomeCount = reader.GetInt32(reader.GetOrdinal("RecordCount"));
                        }

                        bool gotResults = reader.NextResult();
                        if (!gotResults)
                        {
                            throw new ApplicationException(String.Format("No results found for search, stored procedure '{0}' returned no results set.", spName));
                        }

                        HomeCollection homeCollection = new HomeCollection();

                        while (reader.Read())
                        {
                            Home home = new Home();

                            home.ID = (int)reader["HomeID"];
                            home.Description = (string)reader["Description"];

                            home.Bedrooms = GetFieldByInteger(reader, "Bedrooms");
                            home.Bathrooms = GetFieldByInteger(reader, "Bathrooms");
                            home.Location = GetFieldByString(reader, "Location");
                            home.ThumbnailURL = GetFieldByString(reader, "ThumbnailURL");
                            home.Summary = GetFieldByString(reader, "Summary");

                            home.CreatedDate = GetFieldByDate(reader, "CreatedDate");
                            home.City = GetFieldByString(reader, "City");
                            home.PriorityRank = GetFieldByInteger(reader, "PriorityRank");
                            home.ListingType = GetFieldByInteger(reader, "ListingType");
                            // wooncherk
                            home.MemberID = GetFieldByInteger(reader, "MemberID");
                            home.AllowDestinationOffers =
                                GetFieldByBoolean(reader, "AllowDestinationOffers");
                            home.JanuaryAvailability = GetFieldByBoolean(reader, "JanuaryAvailability");
                            home.FebruaryAvailability = GetFieldByBoolean(reader, "FebruaryAvailability");
                            home.MarchAvailability = GetFieldByBoolean(reader, "MarchAvailability");
                            home.AprilAvailability = GetFieldByBoolean(reader, "AprilAvailability");
                            home.MayAvailability = GetFieldByBoolean(reader, "MayAvailability");
                            home.JuneAvailability = GetFieldByBoolean(reader, "JuneAvailability");
                            home.JulyAvailability = GetFieldByBoolean(reader, "JulyAvailability");
                            home.AugustAvailability = GetFieldByBoolean(reader, "AugustAvailability");
                            home.SeptemberAvailability =
                                GetFieldByBoolean(reader, "SeptemberAvailability");
                            home.OctoberAvailability = GetFieldByBoolean(reader, "OctoberAvailability");
                            home.NovemberAvailability = GetFieldByBoolean(reader, "NovemberAvailability");
                            home.DecemberAvailability = GetFieldByBoolean(reader, "DecemberAvailability");
                            home.ChristmasAvailability = GetFieldByBoolean(reader, "ChristmasAvailability");
                            home.LastMinuteAvailability = GetFieldByBoolean(reader, "LastMinuteAvailability");

                            try
                            {
                                home.DestinationsList = GetFieldByString(reader, "DestinationsList");
                            }
                            catch { }

                            homeCollection.Add(home);
                        }

                        homeCriteria.homeCollection = homeCollection;
                    }
                }
            }
            else // member has been provided so include messages in search
            {
                const string spName = "up_SearchHomes2WithMemberEnquiries";
                _log.Info(spName);
                using (var connection = GetConnection())
                {
                    using (SqlDataReader reader = ExecuteReader(spName, connection,
                                       homeCriteria.PageCriteria.CurrentPage, homeCriteria.PageCriteria.PageSize, homeCriteria.Bedrooms, homeCriteria.Keyword, homeCriteria.ContinentID, homeCriteria.CountryCode, homeCriteria.RegionCode, homeCriteria.FeatureList, homeCriteria.DefaultChannel, homeCriteria.IsMultiChannel, homeCriteria.StartMonth, homeCriteria.EndMonth, sqlMonthCriteria, (int)homeCriteria.SortField, (int)homeCriteria.SortOrder, (int)homeCriteria.CurrentMemberID))
                    {

                        if (reader.Read())
                        {
                            homeCriteria.HomeCount = reader.GetInt32(reader.GetOrdinal("RecordCount"));
                        }

                        bool gotResults = reader.NextResult();
                        if (!gotResults)
                        {
                            throw new ApplicationException(String.Format("No results found for search, stored procedure '{0}' returned no results set.", spName));
                        }

                        HomeCollection homeCollection = new HomeCollection();

                        while (reader.Read())
                        {
                            Home home = new Home();

                            home.ID = (int)reader["HomeID"];
                            home.Description = (string)reader["Description"];

                            home.Bedrooms = GetFieldByInteger(reader, "Bedrooms");
                            home.Bathrooms = GetFieldByInteger(reader, "Bathrooms");
                            home.Location = GetFieldByString(reader, "Location");
                            home.ThumbnailURL = GetFieldByString(reader, "ThumbnailURL");
                            home.Summary = GetFieldByString(reader, "Summary");

                            home.CreatedDate = GetFieldByDate(reader, "CreatedDate");
                            home.City = GetFieldByString(reader, "City");
                            home.PriorityRank = GetFieldByInteger(reader, "PriorityRank");
                            home.ListingType = GetFieldByInteger(reader, "ListingType");
                            // wooncherk
                            home.MemberID = GetFieldByInteger(reader, "MemberID");
                            home.AllowDestinationOffers =
                                GetFieldByBoolean(reader, "AllowDestinationOffers");
                            home.JanuaryAvailability = GetFieldByBoolean(reader, "JanuaryAvailability");
                            home.FebruaryAvailability = GetFieldByBoolean(reader, "FebruaryAvailability");
                            home.MarchAvailability = GetFieldByBoolean(reader, "MarchAvailability");
                            home.AprilAvailability = GetFieldByBoolean(reader, "AprilAvailability");
                            home.MayAvailability = GetFieldByBoolean(reader, "MayAvailability");
                            home.JuneAvailability = GetFieldByBoolean(reader, "JuneAvailability");
                            home.JulyAvailability = GetFieldByBoolean(reader, "JulyAvailability");
                            home.AugustAvailability = GetFieldByBoolean(reader, "AugustAvailability");
                            home.SeptemberAvailability =
                                GetFieldByBoolean(reader, "SeptemberAvailability");
                            home.OctoberAvailability = GetFieldByBoolean(reader, "OctoberAvailability");
                            home.NovemberAvailability = GetFieldByBoolean(reader, "NovemberAvailability");
                            home.DecemberAvailability = GetFieldByBoolean(reader, "DecemberAvailability");
                            home.ChristmasAvailability = GetFieldByBoolean(reader, "ChristmasAvailability");
                            home.LastMinuteAvailability = GetFieldByBoolean(reader, "LastMinuteAvailability");

                            //try
                            //{
                            //    home.DestinationsList = GetFieldByString(reader, "DestinationsList");
                            //}
                            //catch { }

                            // TODO: HACK - Temporarily use member ID below to return if currently logged in member has sent this listing an enquiry
                            //home.MemberID = GetFieldByInteger(reader, "SenderMemberID");

                            homeCollection.Add(home);
                        }

                        homeCriteria.homeCollection = homeCollection;
                    }
                }
            }

            //return homeCollection;

            return homeCriteria;
        }

        private log4net.ILog _log = log4net.LogManager.GetLogger(typeof(HomeDAL));

        public HomeCriteria SearchByChannel(HomeCriteria homeCriteria)
        {
            string sqlMonthCriteria = SQLMonthCriteria(homeCriteria.StartMonth, homeCriteria.EndMonth);

            if (homeCriteria.CurrentMemberID <= 0)
            {
                const string spName = "up_HSHSearchHomesByChannel2014";
                using (var connection = GetConnection())
                {
                    using (SqlDataReader reader = ExecuteReader(spName, connection,
                                       homeCriteria.PageCriteria.CurrentPage, homeCriteria.PageCriteria.PageSize, homeCriteria.Bedrooms, homeCriteria.Keyword, homeCriteria.ContinentID, homeCriteria.CountryCode, homeCriteria.RegionCode, homeCriteria.FeatureList, homeCriteria.DefaultChannel, homeCriteria.IsMultiChannel, homeCriteria.StartMonth, homeCriteria.EndMonth, sqlMonthCriteria, (int)homeCriteria.SortField, (int)homeCriteria.SortOrder))
                    {

                        if (reader.Read())
                        {
                            homeCriteria.HomeCount = reader.GetInt32(reader.GetOrdinal("RecordCount"));
                        }

                        bool gotResults = reader.NextResult();
                        //if (!gotResults)
                        //{
                        //    throw new ApplicationException(String.Format("No results found for search, stored procedure '{0}' returned no results set.", spName));
                        //}

                        HomeCollection homeCollection = new HomeCollection();

                        if (gotResults)
                        {
                            while (reader.Read())
                            {
                                Home home = new Home();

                                home.ID = (int)reader["HomeID"];
                                home.Description = (string)reader["Description"];

                                home.Bedrooms = GetFieldByInteger(reader, "Bedrooms");
                                home.Bathrooms = GetFieldByInteger(reader, "Bathrooms");
                                home.Location = GetFieldByString(reader, "Location");
                                home.ThumbnailURL = GetFieldByString(reader, "ThumbnailURL");
                                home.Summary = GetFieldByString(reader, "Summary");
                                home.CreatedDate = GetFieldByDate(reader, "CreatedDate");
                                home.City = GetFieldByString(reader, "City");
                                home.PriorityRank = GetFieldByInteger(reader, "PriorityRank");
                                home.ListingType = GetFieldByInteger(reader, "ListingType");

                                home.Latitude = GetFieldByString(reader, "Latitude");
                                home.Longitude = GetFieldByString(reader, "Longitude");

                                // wooncherk
                                home.MemberID = GetFieldByInteger(reader, "MemberID");
                                home.AllowDestinationOffers =
                                    GetFieldByBoolean(reader, "AllowDestinationOffers");
                                home.JanuaryAvailability = GetFieldByBoolean(reader, "JanuaryAvailability");
                                home.FebruaryAvailability = GetFieldByBoolean(reader, "FebruaryAvailability");
                                home.MarchAvailability = GetFieldByBoolean(reader, "MarchAvailability");
                                home.AprilAvailability = GetFieldByBoolean(reader, "AprilAvailability");
                                home.MayAvailability = GetFieldByBoolean(reader, "MayAvailability");
                                home.JuneAvailability = GetFieldByBoolean(reader, "JuneAvailability");
                                home.JulyAvailability = GetFieldByBoolean(reader, "JulyAvailability");
                                home.AugustAvailability = GetFieldByBoolean(reader, "AugustAvailability");
                                home.SeptemberAvailability =
                                    GetFieldByBoolean(reader, "SeptemberAvailability");
                                home.OctoberAvailability = GetFieldByBoolean(reader, "OctoberAvailability");
                                home.NovemberAvailability = GetFieldByBoolean(reader, "NovemberAvailability");
                                home.DecemberAvailability = GetFieldByBoolean(reader, "DecemberAvailability");
                                home.ChristmasAvailability = GetFieldByBoolean(reader, "ChristmasAvailability");
                                home.LastMinuteAvailability = GetFieldByBoolean(reader, "LastMinuteAvailability");

                                try
                                {
                                    home.DestinationsList = GetFieldByString(reader, "DestinationsList");
                                }
                                catch { }
                                // wooncherk

                                homeCollection.Add(home);
                            }
                        }

                        homeCriteria.homeCollection = homeCollection;
                    }
                }
            }
            else
            {
                const string spName = "up_HSHSearchHomesByChannelWithEnquiries2014";
                using (var connection = GetConnection())
                {
                    using (SqlDataReader reader = ExecuteReader(spName, connection,
                                       homeCriteria.PageCriteria.CurrentPage, homeCriteria.PageCriteria.PageSize, homeCriteria.Bedrooms, homeCriteria.Keyword, homeCriteria.ContinentID, homeCriteria.CountryCode, homeCriteria.RegionCode, homeCriteria.FeatureList, homeCriteria.DefaultChannel, homeCriteria.IsMultiChannel, homeCriteria.StartMonth, homeCriteria.EndMonth, sqlMonthCriteria, (int)homeCriteria.SortField, (int)homeCriteria.SortOrder, (int)homeCriteria.CurrentMemberID))
                    {

                        if (reader.Read())
                        {
                            homeCriteria.HomeCount = reader.GetInt32(reader.GetOrdinal("RecordCount"));
                        }

                        bool gotResults = reader.NextResult();
                        if (!gotResults)
                        {
                            throw new ApplicationException(String.Format("No results found for search, stored procedure '{0}' returned no results set.", spName));
                        }

                        HomeCollection homeCollection = new HomeCollection();

                        while (reader.Read())
                        {
                            Home home = new Home();

                            home.ID = (int)reader["HomeID"];
                            home.Description = (string)reader["Description"];

                            home.Bedrooms = GetFieldByInteger(reader, "Bedrooms");
                            home.Bathrooms = GetFieldByInteger(reader, "Bathrooms");
                            home.Location = GetFieldByString(reader, "Location");
                            home.ThumbnailURL = GetFieldByString(reader, "ThumbnailURL");

                            // TODO: HACK - Temporarily use member ID below to return if currently logged in member has sent this listing an enquiry
                            home.MemberID = GetFieldByInteger(reader, "SenderMemberID");
                            home.Summary = GetFieldByString(reader, "Summary");
                            home.City = GetFieldByString(reader, "City");
                            home.PriorityRank = GetFieldByInteger(reader, "PriorityRank");
                            home.ListingType = GetFieldByInteger(reader, "ListingType");

                            home.CreatedDate = GetFieldByDate(reader, "CreatedDate");

                            home.Latitude = GetFieldByString(reader, "Latitude");
                            home.Longitude = GetFieldByString(reader, "Longitude");

                            // wooncherk
                            //home.MemberID = GetFieldByInteger(reader, "MemberID");
                            home.AllowDestinationOffers =
                                GetFieldByBoolean(reader, "AllowDestinationOffers");
                            home.JanuaryAvailability = GetFieldByBoolean(reader, "JanuaryAvailability");
                            home.FebruaryAvailability = GetFieldByBoolean(reader, "FebruaryAvailability");
                            home.MarchAvailability = GetFieldByBoolean(reader, "MarchAvailability");
                            home.AprilAvailability = GetFieldByBoolean(reader, "AprilAvailability");
                            home.MayAvailability = GetFieldByBoolean(reader, "MayAvailability");
                            home.JuneAvailability = GetFieldByBoolean(reader, "JuneAvailability");
                            home.JulyAvailability = GetFieldByBoolean(reader, "JulyAvailability");
                            home.AugustAvailability = GetFieldByBoolean(reader, "AugustAvailability");
                            home.SeptemberAvailability =
                                GetFieldByBoolean(reader, "SeptemberAvailability");
                            home.OctoberAvailability = GetFieldByBoolean(reader, "OctoberAvailability");
                            home.NovemberAvailability = GetFieldByBoolean(reader, "NovemberAvailability");
                            home.DecemberAvailability = GetFieldByBoolean(reader, "DecemberAvailability");
                            home.ChristmasAvailability = GetFieldByBoolean(reader, "ChristmasAvailability");
                            home.LastMinuteAvailability = GetFieldByBoolean(reader, "LastMinuteAvailability");

                            try
                            {
                                home.DestinationsList = GetFieldByString(reader, "DestinationsList");
                            }
                            catch { }
                            // wooncherk

                            homeCollection.Add(home);
                        }

                        homeCriteria.homeCollection = homeCollection;
                    }
                }
            }

            return homeCriteria;

        }

        // wooncherk
        public HomeCriteria SearchByMap(HomeCriteria homeCriteria)
        {
            const string spName = "up_SearchHomesByMap";
            string sqlMonthCriteria = SQLMonthCriteria(homeCriteria.StartMonth, homeCriteria.EndMonth);

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, homeCriteria.Bedrooms, homeCriteria.Keyword,
                        homeCriteria.ContinentID, homeCriteria.CountryCode, homeCriteria.RegionCode, homeCriteria.FeatureList,
                        homeCriteria.DefaultChannel, homeCriteria.IsMultiChannel, homeCriteria.StartMonth,
                        homeCriteria.EndMonth, sqlMonthCriteria))
                {
                    if (reader.Read())
                    {
                        homeCriteria.HomeCount = reader.GetInt32(reader.GetOrdinal("RecordCount"));
                    }

                    bool gotResults = reader.NextResult();
                    if (!gotResults)
                    {
                        throw new ApplicationException(String.Format("No results found for search, stored procedure '{0}' returned no results set.", spName));
                    }

                    HomeCollection homeCollection = new HomeCollection();

                    while (reader.Read())
                    {
                        Home home = new Home();

                        home.ID = (int)reader["HomeID"];
                        //home.Description = (string)reader["Description"];

                        home.Bedrooms = GetFieldByInteger(reader, "Bedrooms");
                        home.Bathrooms = GetFieldByInteger(reader, "Bathrooms");
                        home.Location = GetFieldByString(reader, "Location");
                        home.ThumbnailURL = GetFieldByString(reader, "ThumbnailURL");
                        home.Summary = GetFieldByString(reader, "Summary");
                        //home.City = GetFieldByString(reader, "City");
                        //home.PriorityRank = GetFieldByInteger(reader, "PriorityRank");
                        //home.ListingType = GetFieldByInteger(reader, "ListingType");
                        //home.MemberID = GetFieldByInteger(reader, "MemberID");
                        //home.AllowDestinationOffers =
                        //    GetFieldByBoolean(reader, "AllowDestinationOffers");
                        home.Latitude = GetFieldByString(reader, "Latitude");
                        home.Longitude = GetFieldByString(reader, "Longitude");
                        //try
                        //{
                        //    home.DestinationsList = GetFieldByString(reader, "DestinationsList");
                        //}
                        //catch { }

                        homeCollection.Add(home);
                    }

                    homeCriteria.homeCollection = homeCollection;
                }
            }

            return homeCriteria;

        }
        // wooncherk

        public HomeCriteria SearchByPreferredDestination(HomeCriteria homeCriteria)
        {
            string sqlMonthCriteria = SQLMonthCriteria(homeCriteria.StartMonth, homeCriteria.EndMonth);

            const string spName = "up_SearchHomesByPreferredDestination";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection,
                               homeCriteria.PageCriteria.CurrentPage, homeCriteria.PageCriteria.PageSize, homeCriteria.Bedrooms, homeCriteria.Keyword, homeCriteria.ContinentID, homeCriteria.CountryCode, homeCriteria.RegionCode, homeCriteria.FeatureList, homeCriteria.DefaultChannel, homeCriteria.IsMultiChannel, homeCriteria.StartMonth, homeCriteria.EndMonth, sqlMonthCriteria, homeCriteria.ReverseCountryCode, homeCriteria.ReverseDestination, homeCriteria.ReverseFilterType, (int)homeCriteria.SortField, (int)homeCriteria.SortOrder))
                {

                    if (reader.Read())
                    {
                        homeCriteria.HomeCount = reader.GetInt32(reader.GetOrdinal("RecordCount"));
                    }

                    bool gotResults = reader.NextResult();
                    if (!gotResults)
                    {
                        throw new ApplicationException(String.Format("No results found for search, stored procedure '{0}' returned no results set.", spName));
                    }

                    HomeCollection homeCollection = new HomeCollection();

                    while (reader.Read())
                    {
                        Home home = new Home();

                        home.ID = (int)reader["HomeID"];
                        home.Description = (string)reader["Description"];

                        home.Bedrooms = GetFieldByInteger(reader, "Bedrooms");
                        home.Bathrooms = GetFieldByInteger(reader, "Bathrooms");
                        home.Location = GetFieldByString(reader, "Location");
                        home.ThumbnailURL = GetFieldByString(reader, "ThumbnailURL");

                        homeCollection.Add(home);
                    }

                    homeCriteria.homeCollection = homeCollection;
                }
            }

            return homeCriteria;
        }

        public HomeCriteria SearchByPreferredDestinationByChannelWithEnquiries(HomeCriteria homeCriteria)
        {
            string sqlMonthCriteria = SQLMonthCriteria(homeCriteria.StartMonth, homeCriteria.EndMonth);

            const string spName = "up_SearchHomesByPreferredDestinationByChannelWithEnquiries2014";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection,
                               homeCriteria.PageCriteria.CurrentPage, homeCriteria.PageCriteria.PageSize, homeCriteria.Bedrooms, homeCriteria.Keyword, homeCriteria.ContinentID, homeCriteria.CountryCode, homeCriteria.RegionCode, homeCriteria.FeatureList, homeCriteria.DefaultChannel, homeCriteria.IsMultiChannel, homeCriteria.StartMonth, homeCriteria.EndMonth, sqlMonthCriteria, homeCriteria.ReverseCountryCode, homeCriteria.ReverseDestination, homeCriteria.ReverseFilterType, (int)homeCriteria.SortField, (int)homeCriteria.SortOrder, (int)homeCriteria.CurrentMemberID))
                {

                    if (reader.Read())
                    {
                        homeCriteria.HomeCount = reader.GetInt32(reader.GetOrdinal("RecordCount"));
                    }

                    bool gotResults = reader.NextResult();
                    if (!gotResults)
                    {
                        throw new ApplicationException(String.Format("No results found for search, stored procedure '{0}' returned no results set.", spName));
                    }

                    HomeCollection homeCollection = new HomeCollection();

                    while (reader.Read())
                    {
                        Home home = new Home();

                        home.ID = (int)reader["HomeID"];
                        home.Description = (string)reader["Description"];

                        home.Bedrooms = GetFieldByInteger(reader, "Bedrooms");
                        home.Bathrooms = GetFieldByInteger(reader, "Bathrooms");
                        home.Location = GetFieldByString(reader, "Location");
                        home.City = GetFieldByString(reader, "City");
                        home.Summary = GetFieldByString(reader, "Summary");
                        home.CreatedDate = GetFieldByDate(reader, "CreatedDate");
                        home.ThumbnailURL = GetFieldByString(reader, "ThumbnailURL");

                        home.PriorityRank = GetFieldByInteger(reader, "PriorityRank");
                        home.ListingType = GetFieldByInteger(reader, "ListingType");

                        // Temporarily use member ID below to return if currently logged in member has sent this listing an enquiry
                        home.MemberID = GetFieldByInteger(reader, "SenderMemberID");

                        // wooncherk
                        //home.MemberID = GetFieldByInteger(reader, "MemberID");
                        home.AllowDestinationOffers =
                            GetFieldByBoolean(reader, "AllowDestinationOffers");
                        home.JanuaryAvailability = GetFieldByBoolean(reader, "JanuaryAvailability");
                        home.FebruaryAvailability = GetFieldByBoolean(reader, "FebruaryAvailability");
                        home.MarchAvailability = GetFieldByBoolean(reader, "MarchAvailability");
                        home.AprilAvailability = GetFieldByBoolean(reader, "AprilAvailability");
                        home.MayAvailability = GetFieldByBoolean(reader, "MayAvailability");
                        home.JuneAvailability = GetFieldByBoolean(reader, "JuneAvailability");
                        home.JulyAvailability = GetFieldByBoolean(reader, "JulyAvailability");
                        home.AugustAvailability = GetFieldByBoolean(reader, "AugustAvailability");
                        home.SeptemberAvailability =
                            GetFieldByBoolean(reader, "SeptemberAvailability");
                        home.OctoberAvailability = GetFieldByBoolean(reader, "OctoberAvailability");
                        home.NovemberAvailability = GetFieldByBoolean(reader, "NovemberAvailability");
                        home.DecemberAvailability = GetFieldByBoolean(reader, "DecemberAvailability");
                        home.ChristmasAvailability = GetFieldByBoolean(reader, "ChristmasAvailability");
                        home.LastMinuteAvailability = GetFieldByBoolean(reader, "LastMinuteAvailability");

                        try
                        {
                            home.DestinationsList = GetFieldByString(reader, "DestinationsList");
                        }
                        catch { }
                        // wooncherk

                        homeCollection.Add(home);
                    }

                    homeCriteria.homeCollection = homeCollection;
                }
            }

            return homeCriteria;
        }

        public HomeCriteria SearchByPreferredDestinationWithMemberEnquiries(HomeCriteria homeCriteria)
        {
            string sqlMonthCriteria = SQLMonthCriteria(homeCriteria.StartMonth, homeCriteria.EndMonth);

            const string spName = "up_SearchHomesByPreferredDestinationWithMemberEnquiries";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection,
                       homeCriteria.PageCriteria.CurrentPage, homeCriteria.PageCriteria.PageSize, homeCriteria.Bedrooms, homeCriteria.Keyword, homeCriteria.ContinentID, homeCriteria.CountryCode, homeCriteria.RegionCode, homeCriteria.FeatureList, homeCriteria.DefaultChannel, homeCriteria.IsMultiChannel, homeCriteria.StartMonth, homeCriteria.EndMonth, sqlMonthCriteria, homeCriteria.ReverseCountryCode, homeCriteria.ReverseDestination, homeCriteria.ReverseFilterType, (int)homeCriteria.SortField, (int)homeCriteria.SortOrder, (int)homeCriteria.CurrentMemberID))
                {

                    if (reader.Read())
                    {
                        homeCriteria.HomeCount = reader.GetInt32(reader.GetOrdinal("RecordCount"));
                    }

                    bool gotResults = reader.NextResult();
                    if (!gotResults)
                    {
                        throw new ApplicationException(String.Format("No results found for search, stored procedure '{0}' returned no results set.", spName));
                    }

                    HomeCollection homeCollection = new HomeCollection();

                    while (reader.Read())
                    {
                        Home home = new Home();

                        home.ID = (int)reader["HomeID"];
                        home.Description = (string)reader["Description"];

                        home.Bedrooms = GetFieldByInteger(reader, "Bedrooms");
                        home.Bathrooms = GetFieldByInteger(reader, "Bathrooms");
                        home.Location = GetFieldByString(reader, "Location");
                        home.ThumbnailURL = GetFieldByString(reader, "ThumbnailURL");

                        // Temporarily use member ID below to return if currently logged in member has sent this listing an enquiry
                        home.MemberID = GetFieldByInteger(reader, "SenderMemberID");

                        homeCollection.Add(home);
                    }

                    homeCriteria.homeCollection = homeCollection;
                }
            }

            //return homeCollection;

            return homeCriteria;
        }

        public List<string> GetFreeMembershipCountry()
        {
            List<string> retval = new List<string>();
            const string spName = "up_GetFreeMembershipPriceCountry";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection))
                {
                    while (reader.Read())
                    {
                        string countryCode = GetFieldByString(reader, "CountryCode");
                        retval.Add(countryCode);
                    }
                }
            }
            return retval;
        }

        private Home LoadFromReader(SqlDataReader reader, HomeFields fields)
        {
            Home home = new Home();

            home.ID = reader.GetInt32(fields.HomeID);
            home.Description = reader.GetString(fields.Description);

            return home;
        }

        #region Singleton instance
        public static HomeDAL Instance
        {
            [DebuggerStepThrough]
            get
            {
                if (_instance == null)
                {
                    lock (typeof(HomeDAL))
                    {
                        if (_instance == null)
                        {
                            _instance = new HomeDAL();
                        }
                    }
                }

                return _instance;
            }

        }

        #endregion

        private static HomeDAL _instance;

    }
}