using System;
using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for BlogCollection.
	/// </summary>
	public class BlogCollection : CollectionBase
	{
		public BlogCollection()
		{
		}

		public void Add(Blog blog)
		{
			InnerList.Add(blog);
		}

		public void Remove(Blog blog)
		{
			InnerList.Remove(blog);
		}

		public Blog this[int index]
		{
			get
			{
				return (Blog)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}
	
	}
}
