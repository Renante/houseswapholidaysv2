using System;
using System.Text;
using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;

namespace Common
{
    /// <summary>
    /// Summary description for BlogDAL.
    /// </summary>
    public class BlogDAL : DataAccess
    {
        public BlogDAL()
        {
        }

        /// <summary>
        /// Returns the matching blog based on the given blogcode, and the specified status
        /// </summary>
        /// <param name="blogCode"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public Blog GetByID(int blogID)
        {
            const string spName = "up_GetBlogByID";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, blogID))
                {

                    if (reader.Read())
                    {
                        Blog blog = new Blog();

                        blog.ID = GetFieldByInteger(reader, "BlogID");
                        blog.Code = GetFieldByString(reader, "Code");
                        blog.BlogTypeID = GetFieldByInteger(reader, "BlogTypeID");
                        blog.IsTemplate = GetFieldByBoolean(reader, "Istemplate");
                        blog.IsEmail = GetFieldByBoolean(reader, "IsEmail");
                        blog.Name = GetFieldByString(reader, "Name");
                        blog.Description = GetFieldByString(reader, "Description");
                        blog.Body = GetFieldByString(reader, "Body");
                        blog.FromDate = GetFieldByDate(reader, "FromDate");
                        blog.ThruDate = GetFieldByDate(reader, "ThruDate");
                        blog.Status = (Status)GetFieldByInteger(reader, "Status");

                        return blog;
                    }
                    else
                    {
                        throw new ApplicationException(String.Format("Cannot get a blog with ID {0}, the blog cannot be found in the database.", blogID));
                        // return null;
                    }
                }
            }
        }

        public Blog Get(string blogCode, bool isActive, bool allowCaching)
        {
            // Retrieve blog. Assign default channel to 0
            return Get(blogCode, isActive, allowCaching, 0);
        }

        public Blog Get(string blogCode, int channelID)
        {
            // Retrieve blog. Assign default channel to 0
            return Get(blogCode, true, true, channelID);
        }


        /// <summary>
        /// Returns the matching blog based on the given blogcode, and the specified status
        /// </summary>
        /// <param name="blogCode"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public Blog Get(string blogCode, bool isActive, bool allowCaching, int channelID)
        {
            if (allowCaching)
            {
                CacheManager cacheManager = new CacheManager();

                BlogCollection blogCollection = new BlogCollection();

                // Check if blogcollection is cached
                blogCollection = (BlogCollection)cacheManager.Get("blogCollection");

                if (blogCollection == null)
                {
                    blogCollection = BlogDAL.Instance.GetAllBlogs();
                    cacheManager.Set("blogCollection", blogCollection);
                    //cache["blogCollection"]= blogCollection;
                }
                else
                {
                    blogCollection = (BlogCollection)cacheManager.Get("blogCollection");
                    //blogCollection = (BlogCollection)cache["blogCollection"];
                }

                foreach (Blog blog in blogCollection)
                {
                    if ((blog.Code == blogCode) && (blog.ChannelID == channelID))
                    {
                        return blog;
                    }
                }

                return null;
            }
            else
            {
                const string spName = "up_GetBlog";
                //int channelID = 0; // assign to dfault channel

                if ((channelID < 0) && (ConfigurationSettings.AppSettings["Channel"] != null))
                {
                    // webconfig has a channel specified, so lets use it
                    channelID = Int32.Parse(ConfigurationSettings.AppSettings["Channel"]);
                }

                // TODO: Pass in the channel ID (currently defaults to 0).

                using (var connection = GetConnection())
                {
                    using (SqlDataReader reader = ExecuteReader(spName, connection, blogCode))
                    {

                        if (reader.Read())
                        {
                            Blog blog = new Blog();

                            blog.ID = GetFieldByInteger(reader, "BlogID");
                            blog.Code = GetFieldByString(reader, "Code");
                            blog.BlogTypeID = GetFieldByInteger(reader, "BlogTypeID");
                            blog.IsTemplate = GetFieldByBoolean(reader, "Istemplate");
                            blog.IsEmail = GetFieldByBoolean(reader, "IsEmail");
                            blog.Name = GetFieldByString(reader, "Name");
                            blog.Description = GetFieldByString(reader, "Description");
                            blog.Body = GetFieldByString(reader, "Body");
                            blog.FromDate = GetFieldByDate(reader, "FromDate");
                            blog.ThruDate = GetFieldByDate(reader, "ThruDate");
                            blog.Status = (Status)GetFieldByInteger(reader, "Status");

                            return blog;
                        }
                        else
                        {
                            throw new ApplicationException(String.Format("Cannot get a blog with code {0}, the blog cannot be found in the database.", blogCode));
                            // return null;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Get blog by supplied ID
        /// </summary>
        /// <param name="blogID">Identifier of blog to retrieve</param>
        /// <returns></returns>
        public Blog Get(string blogCode)
        {
            // If no alternative status is specified, then return any status based on matching blogcode
            return Get(blogCode, false, false);
        }

        public Blog Get(string blogCode, bool AllowCaching)
        {
            // Only return active blogs, and use caching
            return Get(blogCode, true, true);
        }

        public string FormatForHTML(string bodyText)
        {
            return bodyText.Replace("\n", "<br>");
        }

        /*
        /// <summary>
        /// Get blog by supplied ID
        /// </summary>
        /// <param name="blogID">Identifier of blog to retrieve</param>
        /// <returns></returns>
        public Blog GetByType(int channelID, int blogTypeID)
        {
            const string spName = "up_GetBlogByType";

            using (DataSet dataSet = ExecuteDataset(spName, channelID, blogTypeID))
            {
                DataTable blogList = dataSet.Tables[0];

                if (blogList != null)
                {
                    Blog blog = new Blog();
					
                    blog.ChannelID = channelID;
                    blog.ID = GetFieldByInteger(blogList.Rows[0],"BlogID");
                    blog.Code = GetFieldByString(blogList.Rows[0], "Code");
                    blog.BlogTypeID = GetFieldByInteger(blogList.Rows[0],"BlogTypeID");
                    blog.IsTemplate = GetFieldByBoolean(blogList.Rows[0],"Istemplate");
                    blog.IsEmail = GetFieldByBoolean(blogList.Rows[0],"IsEmail");
                    blog.Name = GetFieldByString(blogList.Rows[0], "Name");
                    blog.Description = GetFieldByString(blogList.Rows[0], "Description");
                    blog.Body = GetFieldByString(blogList.Rows[0], "Body");
                    blog.FromDate = GetFieldByDate(blogList.Rows[0], "FromDate");
                    blog.ThruDate = GetFieldByDate(blogList.Rows[0], "ThruDate");

                    blogList.Dispose();
                    dataSet.Dispose();

                    return blog;
                }

                blogList.Dispose();
                dataSet.Dispose();
            }

            return null;
        }
        */

        /// <summary>
        /// Get blog by supplied channel and type ID
        /// </summary>
        /// <param name="channelID">The channel ID</param>
        /// <param name="blogTypeID">The blog type ID</param>
        /// <returns>The blog</returns>
        public Blog GetByType(int channelID, int blogTypeID)
        {
            const string spName = "up_GetBlogByType";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, channelID, blogTypeID))
                {

                    if (reader.Read())
                    {
                        Blog blog = new Blog();

                        blog.ChannelID = channelID;
                        blog.ID = GetFieldByInteger(reader, "BlogID");
                        blog.Code = GetFieldByString(reader, "Code");
                        blog.BlogTypeID = GetFieldByInteger(reader, "BlogTypeID");
                        blog.IsTemplate = GetFieldByBoolean(reader, "Istemplate");
                        blog.IsEmail = GetFieldByBoolean(reader, "IsEmail");
                        blog.Name = GetFieldByString(reader, "Name");
                        blog.Description = GetFieldByString(reader, "Description");
                        blog.Body = GetFieldByString(reader, "Body");
                        blog.FromDate = GetFieldByDate(reader, "FromDate");
                        blog.ThruDate = GetFieldByDate(reader, "ThruDate");

                        return blog;
                    }
                    else
                    {
                        throw new ApplicationException(String.Format("Cannot get a blog for channel {0} and type {1}, the blog cannot be found in the database.", channelID, blogTypeID));
                        // return null;
                    }
                } 
            }
        }

        /// <summary>
        /// Save the specified blog
        /// </summary>
        /// <param name="blog"></param>
        /// <returns></returns>
        public Blog Save(Blog blog)
        {
            if (blog.ID <= 0)
            {
                // Add new blog entry
                const string spName = "up_InsertBlog";

                blog.ID = (int)ExecuteScalar(spName,
                           blog.Code, blog.ChannelID, blog.BlogTypeID, blog.IsTemplate, blog.Name, blog.Description, blog.Body, blog.FromDate, blog.ThruDate);
            }
            else
            {
                // Update existing blog entry
                const string spName = "up_UpdateBlog";

                ExecuteNonQuery(spName,
                    blog.ID, blog.Code, blog.ChannelID, blog.BlogTypeID, blog.IsTemplate, blog.Name, blog.Description, blog.Body, blog.FromDate, blog.ThruDate);
            }

            return blog;
        }

        public BlogCollection GetAllBlogs()
        {
            BlogCollection blogCollection = new BlogCollection();

            int channelID = 0; // assign to default channel

            if (ConfigurationSettings.AppSettings["Channel"] != null)
            {
                // webconfig has a channel specified, so lets use it
                //channelID = Int32.Parse(ConfigurationSettings.AppSettings["Channel"]);
                // TODO temporarily set channelID as -1 to return all channels
                channelID = -1;
            }

            const string spName = "up_GetAllBlogs";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, channelID, (int)Status.Active))
                {
                    while (reader.Read())
                    {
                        Blog blog = new Blog();

                        blog.ID = (int)reader["BlogID"];
                        blog.ChannelID = (int)reader["ChannelID"];
                        blog.Code = (string)reader["Code"];
                        blog.Name = (string)reader["Name"];
                        blog.Description = (string)reader["Description"];
                        blog.IsEmail = GetFieldByBoolean(reader, "IsEmail");
                        blog.Body = (string)reader["Body"];
                        blog.Status = (Status)GetFieldByInteger(reader, "Status");
                        blog.ChannelName = GetFieldByString(reader, "ChannelName");

                        blogCollection.Add(blog);
                    }
                } 
            }

            return blogCollection;
        }


        public BlogCollection Search(string blogName, int channelID, bool isActive)
        {
            BlogCollection blogCollection = new BlogCollection();

            const string spName = "up_SearchBlogs";

            if (blogName.Length == 0)
            {
                blogName = null;
            }

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, blogName, channelID, isActive))
                {
                    while (reader.Read())
                    {
                        Blog blog = new Blog();

                        blog.ID = (int)reader["BlogID"];
                        blog.Code = (string)reader["Code"];
                        blog.Name = (string)reader["Name"];
                        blog.Description = (string)reader["Description"];
                        blog.IsEmail = GetFieldByBoolean(reader, "IsEmail");
                        blog.Body = (string)reader["Body"];
                        blog.Status = (Status)GetFieldByInteger(reader, "Status");
                        blog.ChannelName = GetFieldByString(reader, "ChannelName");

                        blogCollection.Add(blog);
                    }
                } 
            }

            return blogCollection;
        }


        #region Singleton instance
        public static BlogDAL Instance
        {
            [DebuggerStepThrough]
            get
            {
                if (_instance == null)
                {
                    lock (typeof(BlogDAL))
                    {
                        if (_instance == null)
                        {
                            _instance = new BlogDAL();
                        }
                    }
                }

                return _instance;
            }

        }

        #endregion

        private static BlogDAL _instance;


    }
}
