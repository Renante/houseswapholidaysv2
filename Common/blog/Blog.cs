using System;

namespace Common
{
	/// <summary>
	/// Summary description for Blog.
	/// </summary>
	public class Blog
	{
		public Blog()
		{
		}

		public int ID
		{
			get
			{
				return _ID;
			}
			set
			{
				_ID = value;
			}
		}

		public string Code
		{
			get
			{
				return _code;
			}
			set
			{
				_code = value;
			}
		}

		public int BlogTypeID
		{
			get
			{
				return _blogTypeID;
			}
			set
			{
				_blogTypeID = value;
			}
		}

		public int ChannelID
		{
			get
			{
				return _channelID;
			}
			set
			{
				_channelID = value;
			}
		}

		public int AuthorID
		{
			get
			{
				return _authorID;
			}
			set
			{
				_authorID = value;
			}
		}

		public bool IsEmail
		{
			get
			{
				return _isEmail;
			}
			set
			{
				_isEmail = value;
			}
		}

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		public string Description
		{
			get
			{
				return _description;
			}
			set
			{
				_description = value;
			}
		}

		public string Body
		{
			get
			{
				return _body;
			}
			set
			{
				_body = value;
			}
		}

		public Status Status
		{
			get
			{
				return _status;
			}
			set
			{
				_status = value;
			}
		}

		public DateTime FromDate
		{
			get
			{
				return _fromDate;
			}
			set
			{
				_fromDate = value;
			}
		}

		public DateTime ThruDate
		{
			get
			{
				return _thruDate;
			}
			set
			{
				_thruDate = value;
			}
		}

		public bool IsTemplate
		{
			get
			{
				return _isTemplate;
			}
			set
			{
				_isTemplate = value;
			}
		}

		public string ChannelName
		{
			get
			{
				return _channelName;
			}
			set
			{
				_channelName = value;
			}
		}

		#region Private instance variables
		
		private int _ID;
		private string _code;
		private int _blogTypeID;
		private int _channelID;
		private int _authorID;
		private bool _isEmail;
		private string _name;
		private string _description;
		private string _body;
		private Status _status;
		private DateTime _fromDate;
		private DateTime _thruDate;
		private bool _isTemplate;
		private string _channelName;
		
		#endregion Private instance variables

	
	}
}
