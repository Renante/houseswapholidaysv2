using System;

namespace Common
{
	/// <summary>
	/// Control to store a lightweight user session state
	/// </summary>
	public class MemberSession
	{
		public MemberSession()
		{
		}
	
		public int MemberID
		{
			get
			{
				return _memberID;
			}
			set
			{
				_memberID = value;
			}
		}

		public int HomeID
		{
			get
			{
				return _homeID;
			}
			set
			{
				_homeID = value;
			}
		}

		public string FirstName
		{
			get
			{
				return _firstName;
			}
			set
			{
				_firstName = value;
			}
		}

		public string GivenName
		{
			get
			{
				return _givenName;
			}
			set
			{
				_givenName = value;
			}
		}

		public Status HomeStatus
		{
			get
			{
				return _homeStatus;
			}
			set
			{
				_homeStatus = value;
			}
		}

		public Status MemberStatus
		{
			get
			{
				return _memberStatus;
			}
			set
			{
				_memberStatus = value;
			}
		}

		#region private instance variables

		private int _memberID;
		private int _homeID;
		private string _firstName;
		private string _givenName;
		private Status _homeStatus;
		private Status _memberStatus;

		#endregion private instance variables

	
	}
}
