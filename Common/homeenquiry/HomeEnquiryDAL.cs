using System;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;


namespace Common
{
	/// <summary>
	/// Summary description for HomeEnquiryDAL.
	/// </summary>
	public class HomeEnquiryDAL : DataAccess
	{
		public HomeEnquiryDAL()
		{
		}

		public HomeEnquiry Save(HomeEnquiry homeEnquiry)
		{
			if (homeEnquiry.ID <= 0)
			{
				// Add new enquiry
				const string spName = "up_InsertHomeEnquiry";

				homeEnquiry.ID = (int)ExecuteScalar(spName, 
					homeEnquiry.HomeID, homeEnquiry.SenderMemberID, 
					homeEnquiry.SenderChannelID, homeEnquiry.RecipientMemberID, 
					homeEnquiry.FirstName, homeEnquiry.GivenName, 
					homeEnquiry.Email, homeEnquiry.Subject, 
					homeEnquiry.Message, homeEnquiry.EnquiryStatus, 
					homeEnquiry.Reviewed, homeEnquiry.HoldMessage);
			}
			else
			{
				// Update existing home
				const string spName = "up_UpdateHomeEnquiry";

				// TODO: Subscription date is currently hardcoded. Also missing city???
				ExecuteNonQuery(spName, 
					homeEnquiry.ID, homeEnquiry.HomeID, homeEnquiry.SenderMemberID, homeEnquiry.RecipientMemberID, homeEnquiry.FirstName, homeEnquiry.GivenName, homeEnquiry.Email, homeEnquiry.Subject, homeEnquiry.Message, homeEnquiry.EnquiryStatus, homeEnquiry.Reviewed, homeEnquiry.HoldMessage);
			}

			return homeEnquiry;
		}

		/// <summary>
		/// Updates the DB to indicate that the specified homeenquiry has been released from hold (when pending approval) and updates the member recipients total message count
		/// </summary>
		/// <param name="homeEnquiry"></param>
		/// <returns></returns>
		public HomeEnquiry ReleaseFromHold(HomeEnquiry homeEnquiry)
		{
			// Update existing home
			const string spName = "up_ReleaseHomeEnquiry";

			// TODO: Subscription date is currently hardcoded. Also missing city???
			ExecuteNonQuery(spName, 
				homeEnquiry.ID, homeEnquiry.HomeID, homeEnquiry.SenderMemberID, homeEnquiry.RecipientMemberID, homeEnquiry.FirstName, homeEnquiry.GivenName, homeEnquiry.Email, homeEnquiry.Subject, homeEnquiry.Message, homeEnquiry.EnquiryStatus, homeEnquiry.Reviewed, homeEnquiry.HoldMessage);

			return homeEnquiry;
		}


		public void Delete(int homeEnquiryID, int memberRecipientID)
		{
			const string spName = "up_DeleteHomeEnquiry";

			ExecuteNonQuery(spName, homeEnquiryID, memberRecipientID);
		}

		public HomeEnquiry Get(int homeEnquiryID, int memberID, bool keepUnread)
		{
			// Retrieve all home enquiries based on given member ID
			const string spName = "up_GetHomeEnquiry";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection,
                               homeEnquiryID, memberID, keepUnread))
                {
                    if (reader.Read())
                    {
                        HomeEnquiry homeEnquiry = new HomeEnquiry();

                        homeEnquiry.ID = GetFieldByInteger(reader, "ID");
                        homeEnquiry.HomeID = GetFieldByInteger(reader, "HomeID");
                        homeEnquiry.SenderMemberID = GetFieldByInteger(reader, "SenderMemberID");
                        homeEnquiry.SenderChannelID = GetFieldByInteger(reader, "SenderChannelID");
                        homeEnquiry.RecipientMemberID = GetFieldByInteger(reader, "RecipientMemberID");
                        homeEnquiry.FirstName = GetFieldByString(reader, "Firstname");
                        homeEnquiry.GivenName = GetFieldByString(reader, "GivenName");
                        homeEnquiry.Email = GetFieldByString(reader, "Email");
                        homeEnquiry.Subject = GetFieldByString(reader, "Subject");
                        homeEnquiry.Message = GetFieldByString(reader, "Message");
                        homeEnquiry.EnquiryDate = GetFieldByDate(reader, "EnquiryDate");
                        homeEnquiry.Reviewed = GetFieldByBoolean(reader, "Reviewed");

                        // TODO: Note: Convert to enum
                        if (reader["EnquiryStatus"] == DBNull.Value)
                        {
                            homeEnquiry.EnquiryStatus = -1;
                        }
                        else
                        {
                            homeEnquiry.EnquiryStatus = (int)reader["EnquiryStatus"];
                        }

                        return homeEnquiry;
                    }
                    else
                    {
                        return null;
                    }
                } 
            }
		}

		public HomeEnquiry GetSentMessage(int homeEnquiryID, int memberID)
		{
			// Retrieve all home enquiries based on given member ID
			const string spName = "up_GetHomeEnquirySent";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection,
                               homeEnquiryID, memberID))
                {
                    if (reader.Read())
                    {
                        HomeEnquiry homeEnquiry = new HomeEnquiry();

                        homeEnquiry.ID = GetFieldByInteger(reader, "ID");
                        homeEnquiry.HomeID = GetFieldByInteger(reader, "HomeID");
                        homeEnquiry.SenderMemberID = GetFieldByInteger(reader, "SenderMemberID");
                        homeEnquiry.SenderChannelID = GetFieldByInteger(reader, "SenderChannelID");
                        homeEnquiry.RecipientMemberID = GetFieldByInteger(reader, "RecipientMemberID");
                        homeEnquiry.FirstName = GetFieldByString(reader, "Firstname");
                        homeEnquiry.GivenName = GetFieldByString(reader, "GivenName");
                        homeEnquiry.Email = GetFieldByString(reader, "Email");
                        homeEnquiry.Subject = GetFieldByString(reader, "Subject");
                        homeEnquiry.Message = GetFieldByString(reader, "Message");
                        homeEnquiry.EnquiryDate = GetFieldByDate(reader, "EnquiryDate");
                        homeEnquiry.Reviewed = GetFieldByBoolean(reader, "Reviewed");

                        // TODO: Note: Convert to enum
                        if (reader["EnquiryStatus"] == DBNull.Value)
                        {
                            homeEnquiry.EnquiryStatus = -1;
                        }
                        else
                        {
                            homeEnquiry.EnquiryStatus = (int)reader["EnquiryStatus"];
                        }

                        return homeEnquiry;
                    }
                    else
                    {
                        return null;
                    }
                } 
            }
		}

		public HomeEnquiry Get(int homeEnquiryID, int memberID)
		{
			// Retrieve all home enquiries based on given member ID
			const string spName = "up_GetHomeEnquiry";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection,
                               homeEnquiryID, memberID, false))
                {
                    if (reader.Read())
                    {
                        HomeEnquiry homeEnquiry = new HomeEnquiry();

                        homeEnquiry.ID = GetFieldByInteger(reader, "ID");
                        homeEnquiry.HomeID = GetFieldByInteger(reader, "HomeID");
                        homeEnquiry.SenderMemberID = GetFieldByInteger(reader, "SenderMemberID");
                        homeEnquiry.SenderChannelID = GetFieldByInteger(reader, "SenderChannelID");
                        homeEnquiry.RecipientMemberID = GetFieldByInteger(reader, "RecipientMemberID");
                        homeEnquiry.FirstName = GetFieldByString(reader, "Firstname");
                        homeEnquiry.GivenName = GetFieldByString(reader, "GivenName");
                        homeEnquiry.Email = GetFieldByString(reader, "Email");
                        homeEnquiry.Subject = GetFieldByString(reader, "Subject");
                        homeEnquiry.Message = GetFieldByString(reader, "Message");
                        homeEnquiry.EnquiryDate = GetFieldByDate(reader, "EnquiryDate");
                        homeEnquiry.Reviewed = GetFieldByBoolean(reader, "Reviewed");

                        // TODO: Note: Convert to enum
                        if (reader["EnquiryStatus"] == DBNull.Value)
                        {
                            homeEnquiry.EnquiryStatus = -1;
                        }
                        else
                        {
                            homeEnquiry.EnquiryStatus = (int)reader["EnquiryStatus"];
                        }

                        return homeEnquiry;
                    }
                    else
                    {
                        return null;
                    }
                } 
            }
		}

		public HomeEnquiryCollection GetAllForMember(int memberID)
		{
			HomeEnquiryCollection homeEnquiryCollection = new HomeEnquiryCollection();

			// Retrieve all home enquiries based on given member ID
			const string spName = "up_GetHomeEnquiriesByMember2";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection,
                               memberID))
                {
                    while (reader.Read())
                    {
                        HomeEnquiry homeEnquiry = new HomeEnquiry();

                        homeEnquiry.ID = GetFieldByInteger(reader, "ID");
                        homeEnquiry.SenderMemberID = GetFieldByInteger(reader, "SenderMemberID");
                        homeEnquiry.RecipientMemberID = GetFieldByInteger(reader, "RecipientMemberID");
                        homeEnquiry.FirstName = GetFieldByString(reader, "Firstname");
                        homeEnquiry.GivenName = GetFieldByString(reader, "GivenName");
                        homeEnquiry.Email = GetFieldByString(reader, "Email");
                        homeEnquiry.Subject = GetFieldByString(reader, "Subject");
                        homeEnquiry.Message = GetFieldByString(reader, "Message");
                        homeEnquiry.EnquiryDate = GetFieldByDate(reader, "EnquiryDate");
                        homeEnquiry.Reviewed = GetFieldByBoolean(reader, "Reviewed");
                        homeEnquiry.HomeID = GetFieldByInteger(reader, "HomeID");

                        // TODO: Note: Convert to enum
                        if (reader["EnquiryStatus"] == DBNull.Value)
                        {
                            homeEnquiry.EnquiryStatus = -1;
                        }
                        else
                        {
                            homeEnquiry.EnquiryStatus = (int)reader["EnquiryStatus"];
                        }

                        homeEnquiryCollection.Add(homeEnquiry);
                    }
                } 
            }
			
			return homeEnquiryCollection;
		}

		public HomeEnquiryCollection GetAllSentByMember(int memberID)
		{
			HomeEnquiryCollection homeEnquiryCollection = new HomeEnquiryCollection();

			// Retrieve all home enquiries based on given member ID
			const string spName = "up_GetHomeEnquiriesSentByMember";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, memberID))
                {
                    while (reader.Read())
                    {
                        HomeEnquiry homeEnquiry = new HomeEnquiry();

                        homeEnquiry.ID = GetFieldByInteger(reader, "ID");
                        homeEnquiry.SenderMemberID = GetFieldByInteger(reader, "SenderMemberID");
                        homeEnquiry.RecipientMemberID = GetFieldByInteger(reader, "RecipientMemberID");
                        homeEnquiry.FirstName = GetFieldByString(reader, "Firstname");
                        homeEnquiry.GivenName = GetFieldByString(reader, "GivenName");
                        homeEnquiry.Email = GetFieldByString(reader, "Email");
                        homeEnquiry.Subject = GetFieldByString(reader, "Subject");
                        homeEnquiry.Message = GetFieldByString(reader, "Message");
                        homeEnquiry.EnquiryDate = GetFieldByDate(reader, "EnquiryDate");
                        homeEnquiry.Reviewed = GetFieldByBoolean(reader, "Reviewed");
                        homeEnquiry.Location = GetFieldByString(reader, "Location");
                        homeEnquiry.HomeID = GetFieldByInteger(reader, "HomeID");

                        // TODO: Note: Convert to enum
                        if (reader["EnquiryStatus"] == DBNull.Value)
                        {
                            homeEnquiry.EnquiryStatus = -1;
                        }
                        else
                        {
                            homeEnquiry.EnquiryStatus = (int)reader["EnquiryStatus"];
                        }

                        homeEnquiryCollection.Add(homeEnquiry);
                    }
                } 
            }
			
			return homeEnquiryCollection;
		}

		public HomeEnquiryCollection GetNew(int hoursBack)
		{
			HomeEnquiryCollection homeEnquiryCollection = new HomeEnquiryCollection();

			// Retrieve all home enquiries based on given member ID
			const string spName = "up_GetNewEnquiries";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, hoursBack))
                {
                    while (reader.Read())
                    {
                        HomeEnquiry homeEnquiry = new HomeEnquiry();

                        homeEnquiry.ID = GetFieldByInteger(reader, "ID");
                        homeEnquiry.SenderMemberID = GetFieldByInteger(reader, "SenderMemberID");
                        homeEnquiry.RecipientMemberID = GetFieldByInteger(reader, "RecipientMemberID");
                        homeEnquiry.FirstName = GetFieldByString(reader, "Firstname");
                        homeEnquiry.GivenName = GetFieldByString(reader, "GivenName");
                        homeEnquiry.Email = GetFieldByString(reader, "Email");
                        homeEnquiry.Subject = GetFieldByString(reader, "Subject");
                        homeEnquiry.Message = GetFieldByString(reader, "Message");
                        homeEnquiry.EnquiryDate = GetFieldByDate(reader, "EnquiryDate");
                        homeEnquiry.Reviewed = GetFieldByBoolean(reader, "Reviewed");
                        homeEnquiry.HoldMessage = GetFieldByBoolean(reader, "HoldMessage");

                        // TODO: Note: Convert to enum
                        if (reader["EnquiryStatus"] == DBNull.Value)
                        {
                            homeEnquiry.EnquiryStatus = -1;
                        }
                        else
                        {
                            homeEnquiry.EnquiryStatus = (int)reader["EnquiryStatus"];
                        }

                        homeEnquiryCollection.Add(homeEnquiry);
                    }
                } 
            }
			
			return homeEnquiryCollection;
		}

		public HomeEnquiryCollection GetAllHomeEnquiries(int senderID, int recipientID, bool holdOnly, int previousDays)
		{
			return GetAllHomeEnquiries(senderID, recipientID, holdOnly, previousDays, "");
		}

		public HomeEnquiryCollection GetAllHomeEnquiries(int senderID, int recipientID, bool holdOnly, int previousDays, string fromDateTime)
		{
			HomeEnquiryCollection homeEnquiryCollection = new HomeEnquiryCollection();

			// Retrieve all home enquiries based on given member ID
			const string spName = "up_GetAllHomeEnquiries";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection,
                               senderID, recipientID, holdOnly, previousDays, fromDateTime))
                {
                    while (reader.Read())
                    {
                        HomeEnquiry homeEnquiry = new HomeEnquiry();

                        homeEnquiry.ID = GetFieldByInteger(reader, "ID");
                        homeEnquiry.SenderMemberID = GetFieldByInteger(reader, "SenderMemberID");
                        homeEnquiry.RecipientMemberID = GetFieldByInteger(reader, "RecipientMemberID");
                        homeEnquiry.FirstName = GetFieldByString(reader, "Firstname");
                        homeEnquiry.GivenName = GetFieldByString(reader, "GivenName");
                        homeEnquiry.Email = GetFieldByString(reader, "Email");
                        homeEnquiry.Subject = GetFieldByString(reader, "Subject");
                        homeEnquiry.Message = GetFieldByString(reader, "Message");
                        homeEnquiry.EnquiryDate = GetFieldByDate(reader, "EnquiryDate");
                        homeEnquiry.Reviewed = GetFieldByBoolean(reader, "Reviewed");
                        homeEnquiry.HoldMessage = GetFieldByBoolean(reader, "HoldMessage");

                        /* if (reader["HoldMessage"] == DBNull.Value)
                        {
                            homeEnquiry.HoldMessage = false;
                        }
                        else
                        {
                            if ((bool)reader["HoldMessage"])
                            {
                                homeEnquiry.HoldMessage = true;
                            }
                            else
                            {
                                homeEnquiry.HoldMessage = false;
                            }
                            // homeEnquiry.HoldMessage  = (bool)reader["HoldMessage"];
                        } */



                        // TODO: Note: Convert to enum
                        if (reader["EnquiryStatus"] == DBNull.Value)
                        {
                            homeEnquiry.EnquiryStatus = -1;
                        }
                        else
                        {
                            homeEnquiry.EnquiryStatus = (int)reader["EnquiryStatus"];
                        }

                        homeEnquiryCollection.Add(homeEnquiry);
                    }
                } 
            }
			
			return homeEnquiryCollection;
		}

		public HomeEnquiryCollection GetAllForMemberSender(int memberID, int senderID)
		{

			HomeEnquiryCollection homeEnquiryCollection = new HomeEnquiryCollection();

			// Retrieve all home enquiries based on given member ID
			const string spName = "up_GetHomeEnquiriesByMemberSender";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, memberID, senderID))
                {
                    while (reader.Read())
                    {
                        HomeEnquiry homeEnquiry = new HomeEnquiry();

                        homeEnquiry.ID = GetFieldByInteger(reader, "ID");
                        homeEnquiry.SenderMemberID = GetFieldByInteger(reader, "SenderMemberID");
                        homeEnquiry.RecipientMemberID = GetFieldByInteger(reader, "RecipientMemberID");
                        homeEnquiry.FirstName = GetFieldByString(reader, "Firstname");
                        homeEnquiry.GivenName = GetFieldByString(reader, "GivenName");
                        homeEnquiry.Email = GetFieldByString(reader, "Email");
                        homeEnquiry.Subject = GetFieldByString(reader, "Subject");
                        homeEnquiry.Message = GetFieldByString(reader, "Message");
                        homeEnquiry.EnquiryDate = GetFieldByDate(reader, "EnquiryDate");
                        homeEnquiry.Reviewed = GetFieldByBoolean(reader, "Reviewed");

                        // TODO: Note: Convert to enum
                        if (reader["EnquiryStatus"] == DBNull.Value)
                        {
                            homeEnquiry.EnquiryStatus = -1;
                        }
                        else
                        {
                            homeEnquiry.EnquiryStatus = (int)reader["EnquiryStatus"];
                        }

                        homeEnquiryCollection.Add(homeEnquiry);
                    }
                } 
            }
			
			return homeEnquiryCollection;
		}

		#region Singleton instance
		public static HomeEnquiryDAL Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
					lock (typeof(HomeEnquiryDAL))
					{
						if (_instance == null)
						{
							_instance = new HomeEnquiryDAL();
						}
					}
				}

				return _instance;
			}
			
		}

		#endregion

		private static HomeEnquiryDAL _instance;
	
	}
}
