using System;

namespace Common
{
	/// <summary>
	/// Summary description for HomeAvailability.
	/// </summary>
	public class HomeEnquiry
	{
		public HomeEnquiry()
		{
		}

		public int ID
		{
			get
			{
				return _id;
			}
			set
			{
				_id = value;
			}
		}

		public int HomeID
		{
			get
			{
				return _homeID;
			}
			set
			{
				_homeID = value;
			}
		}

		public int SenderMemberID
		{
			get
			{
				return _senderMemberID;
			}
			set
			{
				_senderMemberID = value;
			}
		}

		public int SenderChannelID
		{
			get
			{
				return _senderChannelID;
			}
			set
			{
				_senderChannelID = value;
			}
		}

		public int RecipientMemberID
		{
			get
			{
				return _recipientMemberID;
			}
			set
			{
				_recipientMemberID = value;
			}
		}

		public string FirstName
		{
			get
			{
				return _firstName;
			}
			set
			{
				_firstName = value;
			}
		}

		public string GivenName
		{
			get
			{
				return _givenName;
			}
			set
			{
				_givenName = value;
			}
		}

		public string Email
		{
			get
			{
				return _email;
			}
			set
			{
				_email = value;
			}
		}

		public string Subject
		{
			get
			{
				return _subject;
			}
			set
			{
				_subject = value;
			}
		}

		public string Message
		{
			get
			{
				return _message;
			}
			set
			{
				_message = value;
			}
		}

		public string Location
		{
			get
			{
				return _location;
			}
			set
			{
				_location = value;
			}
		}

		public DateTime EnquiryDate
		{
			get
			{
				return _enquiryDate;
			}
			set
			{
				_enquiryDate = value;
			}
		}

		public bool Reviewed
		{
			get
			{
				return _reviewed;
			}
			set
			{
				_reviewed = value;
			}
		}

		public int EnquiryStatus
		{
			get
			{
				return _enquiryStatus;
			}
			set
			{
				_enquiryStatus = value;
			}
		}

		public bool HoldMessage
		{
			get
			{
				return _holdMessage;
			}
			set
			{
				_holdMessage = value;
			}
		}

		private int _id;
		private int _homeID;
		private int _senderMemberID = -1;
		private int _senderChannelID = -1;
		private int _recipientMemberID = -1;
		private string _firstName;
		private string _givenName;
		private string _email;
		private string _subject;
		private string _message;
		private DateTime _enquiryDate;
		private bool _reviewed;
		private int _enquiryStatus;
		private bool _holdMessage;
		private string _location;

	}
}
