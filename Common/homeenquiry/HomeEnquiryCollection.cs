using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for HomeEnquiryCollection.
	/// </summary>
	public class HomeEnquiryCollection : CollectionBase
	{
		public HomeEnquiryCollection()
		{
		}

		public void Add(HomeEnquiry homeEnquiry)
		{
			InnerList.Add(homeEnquiry);
		}

		public void Remote(HomeEnquiry homeEnquiry)
		{
			InnerList.Remove(homeEnquiry);
		}

		public HomeEnquiry this[int index]
		{
			get
			{
				return (HomeEnquiry)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}

	}
}
