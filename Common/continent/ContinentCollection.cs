using System;
using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for RegionCollection.
	/// </summary>
	public class ContinentCollection : CollectionBase
	{
		public ContinentCollection()
		{
		}

		public void Add(Continent continent)
		{
			InnerList.Add(continent);
		}

		public void Remove(Continent continent)
		{
			InnerList.Remove(continent);
		}

		public Continent this[int index]
		{
			get
			{
				return (Continent)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}
	}
}
