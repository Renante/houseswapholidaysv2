using System;

namespace Common
{
	/// <summary>
	/// Summary description for Feature.
	/// </summary>
	public class Feature
	{
		public Feature()
		{
		}

		public int ID
		{
			get
			{
				return _id;
			}
			set
			{
				_id = value;
			}
		}

		public bool IsSelected
		{
			get
			{
				return _isSelected;
			}
			set
			{
				_isSelected = value;
			}
		}

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		public string Description
		{
			get
			{
				return _description;
			}
			set
			{
				_description = value;
			}
		}

		public int DisplayOrder
		{
			get
			{
				return _displayOrder;
			}
			set
			{
				_displayOrder = value;
			}
		}

		public int FeatureTypeID
		{
			get
			{
				return _featureTypeID;
			}
			set
			{
				_featureTypeID = value;
			}
		}

		private int _id;
		private string _name;
		private string _description;
		private int _displayOrder;
		private int _featureTypeID;
		private bool _isSelected;

	}
}
