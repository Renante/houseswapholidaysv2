using System;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;

namespace Common
{
	/// <summary>
	/// Summary description for FeatureDAL.
	/// </summary>
	public class FeatureDAL : DataAccess
	{
		#region Types
		private sealed class FeatureFields
		{
			public readonly int ID = -1;
			public readonly int Name = -1;
			public readonly int Description = -1;
			public readonly int DisplayOrder = -1;
			public readonly int FeatureTypeID = -1;

		}
		#endregion

		public FeatureDAL()
		{
		}

		public FeatureCollection GetByFeatureType(int featureTypeID, FeatureCollection featureCollection)
		{
			FeatureCollection filteredFeatureCollection = new FeatureCollection();
			foreach(Feature feature in featureCollection)
			{
				if (feature.FeatureTypeID == featureTypeID)
				{
					filteredFeatureCollection.Add(feature);
				}
			}

			return filteredFeatureCollection;
		}

        
		public Feature Get(int featureID)
		{
			const string spName = "up_Getfeature";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, featureID))
                {
                    if (reader.Read())
                    {
                        Feature feature = new Feature();

                        feature.ID = GetFieldByInteger(reader, "FeatureID");
                        feature.Name = GetFieldByString(reader, "Name");
                        feature.Description = GetFieldByString(reader, "Description");
                        feature.DisplayOrder = GetFieldByInteger(reader, "DisplayOrder");
                        feature.FeatureTypeID = GetFieldByInteger(reader, "FeatureTypeID");

                        return feature;
                    }
                    else
                    {
                        return null;
                    }
                } 
            }
		}

		public Feature GetByName(string featureName)
		{

			const string spName = "up_GetFeatureByName";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, featureName))
                {
                    if (reader.Read())
                    {
                        Feature feature = new Feature();

                        feature.ID = GetFieldByInteger(reader, "FeatureID");
                        feature.Name = GetFieldByString(reader, "Name");
                        feature.Description = GetFieldByString(reader, "Description");
                        feature.DisplayOrder = GetFieldByInteger(reader, "DisplayOrder");
                        feature.FeatureTypeID = GetFieldByInteger(reader, "FeatureTypeID");

                        return feature;
                    }
                    else
                    {
                        return null;
                    }
                } 
            }
		}

		public Feature Save(Feature feature)
		{

			if (feature.ID <= 0)
			{
				// Add new home
				const string spName = "up_InsertFeature";

				feature.ID = (int)ExecuteScalar(spName, 
						   feature.Name, feature.Description, feature.DisplayOrder, feature.FeatureTypeID);
			}
			else
			{
				// Update existing home
				const string spName = "up_UpdateFeature";

				ExecuteNonQuery(spName, 
					feature.ID, feature.Name, feature.Description, feature.DisplayOrder, feature.FeatureTypeID);
			}

			return feature;
		}

		public FeatureCollection GetAll()
		{
			return GetAll(false);
		}

		/*
		public FeatureCollection GetAll(bool flushCache)
		{

			CacheManager cacheManager = new CacheManager();
			FeatureCollection featureCollection = new FeatureCollection();

			if (!flushCache)
			{
				// Check if we can obtain it from the cache

				featureCollection = (FeatureCollection)cacheManager.Get("featureCollection");
			}

			if ((flushCache) || (featureCollection == null))
			{
				featureCollection = new FeatureCollection();

				const string spName = "up_GetFeatures";

				using (DataSet dataSet = ExecuteDataset(spName))
				{
					DataTable featureList = dataSet.Tables[0];

					for (int i=0; i<=featureList.Rows.Count-1; i++)
					{
						Feature feature = new Feature();
			
						feature.ID = GetFieldByInteger(featureList.Rows[i], "FeatureID");
						feature.Name = GetFieldByString(featureList.Rows[i], "Name");
						feature.Description = GetFieldByString(featureList.Rows[i], "Description");

						feature.DisplayOrder  = GetFieldByInteger(featureList.Rows[i], "DisplayOrder");
						feature.FeatureTypeID  = GetFieldByInteger(featureList.Rows[i], "FeatureTypeID");

						featureCollection.Add(feature);
					}

					// Save country collection to cache
					cacheManager.Set("featureCollection", featureCollection);
					
					featureList.Dispose();
					dataSet.Dispose();

				}
			}
			
			return featureCollection;
		}
		*/

		public FeatureCollection GetAll(bool flushCache)
		{
			CacheManager cacheManager = new CacheManager();
			FeatureCollection featureCollection = new FeatureCollection();

			if (!flushCache)
			{
				// Check if we can obtain it from the cache

				featureCollection = (FeatureCollection)cacheManager.Get("featureCollection");
			}

			if ((flushCache) || (featureCollection == null))
			{
				featureCollection = new FeatureCollection();

				const string spName = "up_GetFeatures";

                using (var connection = GetConnection())
                {
                    using (SqlDataReader reader = ExecuteReader(spName, connection))
                    {

                        while (reader.Read())
                        {
                            Feature feature = new Feature();

                            feature.ID = GetFieldByInteger(reader, "FeatureID");
                            feature.Name = GetFieldByString(reader, "Name");
                            feature.Description = GetFieldByString(reader, "Description");

                            feature.DisplayOrder = GetFieldByInteger(reader, "DisplayOrder");
                            feature.FeatureTypeID = GetFieldByInteger(reader, "FeatureTypeID");

                            featureCollection.Add(feature);
                        }

                        // Save country collection to cache
                        cacheManager.Set("featureCollection", featureCollection);
                    } 
                }
			}
			
			return featureCollection;
		}

		private Feature LoadFromReader(SqlDataReader reader, FeatureFields fields)
		{
			Feature feature = new Feature();

			feature.ID = reader.GetInt32(fields.ID);
			feature.Name = reader.GetString(fields.Name);
			feature.Description = reader.GetString(fields.Description);
			feature.DisplayOrder = reader.GetInt32(fields.DisplayOrder);
			feature.FeatureTypeID = reader.GetInt32(fields.FeatureTypeID);

			return feature;
		}

		#region Singleton instance
		public static FeatureDAL Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
					lock (typeof(FeatureDAL))
					{
						if (_instance == null)
						{
							_instance = new FeatureDAL();
						}
					}
				}

				return _instance;
			}
			
		}

		#endregion

		private static FeatureDAL _instance;

	}
}
