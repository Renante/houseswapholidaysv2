using System;
using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for FeatureCollection.
	/// </summary>
	public class FeatureCollection : CollectionBase
	{
		public FeatureCollection()
		{
		}

		public void Add(Feature feature)
		{
			InnerList.Add(feature);
		}

		public void Remote(Feature feature)
		{
			InnerList.Remove(feature);
		}

		public Feature this[int index]
		{
			get
			{
				return (Feature)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}

	    public void AddCollection(FeatureCollection toAdd)
	    {
            InnerList.AddRange(toAdd);
	    }
	}
}
