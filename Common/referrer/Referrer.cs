using System;

namespace Common
{
	/// <summary>
	/// Summary description for Feature.
	/// </summary>
	public class Referrer
	{
		public Referrer()
		{
		}

		public int ID
		{
			get
			{
				return _id;
			}
			set
			{
				_id = value;
			}
		}

		public string CompanyName
		{
			get
			{
				return _companyName;
			}
			set
			{
				_companyName = value;
			}
		}

		public int Status
		{
			get
			{
				return _status;
			}
			set
			{
				_status = value;
			}
		}

		public int PrimaryContactMemberID
		{
			get
			{
				return _primaryContactMemberID;
			}
			set
			{
				_primaryContactMemberID = value;
			}
		}

		public int SecondaryContactMemberID
		{
			get
			{
				return _secondaryContactMemberID;
			}
			set
			{
				_secondaryContactMemberID = value;
			}
		}

		public int DefaultChannelID
		{
			get
			{
				return _defaultChannelID;
			}
			set
			{
				_defaultChannelID = value;
			}
		}

		public bool IsMultiChannel
		{
			get
			{
				return _isMultiChannel;
			}
			set
			{
				_isMultiChannel = value;
			}
		}

		public int CommissionType
		{
			get
			{
				return _commissionType;
			}
			set
			{
				_commissionType = value;
			}
		}

		public double CommissionSignupRate
		{
			get
			{
				return _commissionSignupRate;
			}
			set
			{
				_commissionSignupRate = value;
			}
		}

		public double CommissionRenewalRate
		{
			get
			{
				return _commissionRenewalRate;
			}
			set
			{
				_commissionRenewalRate = value;
			}
		}

		public int CommissionDuration
		{
			get
			{
				return _commissionDuration;
			}
			set
			{
				_commissionDuration = value;
			}
		}

		public string PostalAddress1
		{
			get
			{
				return _postalAddress1;
			}
			set
			{
				_postalAddress1 = value;
			}
		}

		public string PostalAddress2
		{
			get
			{
				return _postalAddress2;
			}
			set
			{
				_postalAddress2 = value;
			}
		}

		public string PostalCode
		{
			get
			{
				return _postalCode;
			}
			set
			{
				_postalCode = value;
			}
		}

		public int PostalCountryID
		{
			get
			{
				return _postalCountryID;
			}
			set
			{
				_postalCountryID = value;
			}
		}

		public string Phone
		{
			get
			{
				return _phone;
			}
			set
			{
				_phone = value;
			}
		}

		public string Fax
		{
			get
			{
				return _fax;
			}
			set
			{
				_fax = value;
			}
		}

		public string Mobile
		{
			get
			{
				return _mobile;
			}
			set
			{
				_mobile = value;
			}
		}

		public string WebsiteURL
		{
			get
			{
				return _websiteURL;
			}
			set
			{
				_websiteURL = value;
			}
		}

		public string TaxNumber
		{
			get
			{
				return _taxNumber;
			}
			set
			{
				_taxNumber = value;
			}
		}

		public int MemberDiscountType
		{
			get
			{
				return _memberDiscountType;
			}
			set
			{
				_memberDiscountType = value;
			}
		}

		public double MemberDiscountSignupRate
		{
			get
			{
				return _memberDiscountSignupRate;
			}
			set
			{
				_memberDiscountSignupRate = value;
			}
		}

		public double MemberDiscountRenewalRate
		{
			get
			{
				return _memberDiscountRenewalRate;
			}
			set
			{
				_memberDiscountRenewalRate = value;
			}
		}

		public int ReferrerDurationAgreement
		{
			get
			{
				return _referrerDurationAgreement;
			}
			set
			{
				_referrerDurationAgreement = value;
			}
		}

		public string Notes
		{
			get
			{
				return _notes;
			}
			set
			{
				_notes = value;
			}
		}

		public int CookieDuration
		{
			get
			{
				return _cookieDuration;
			}
			set
			{
				_cookieDuration = value;
			}
		}

		public bool isCustomHeader
		{
			get
			{
				return _isCustomHeader;
			}
			set
			{
				_isCustomHeader = value;
			}
		}

		private int _id;
		private string _companyName;
		private int _status;
		private int _primaryContactMemberID;
		private int _secondaryContactMemberID;
		private int _defaultChannelID;
		private bool _isMultiChannel;
		private int _commissionType;
		private double _commissionSignupRate;
		private double _commissionRenewalRate;
		private int _commissionDuration;
		private string _postalAddress1;
		private string _postalAddress2;
		private string _postalCity;
		private string _postalCode;
		private int _postalCountryID;
		private string _phone;
		private string _fax;
		private string _mobile;
		private string _websiteURL;
		private string _taxNumber;
		private int	_memberDiscountType;
		private double _memberDiscountSignupRate;
		private double _memberDiscountRenewalRate;
		private int _referrerDurationAgreement;
		private string _notes;
		private int _cookieDuration;
		private bool _isCustomHeader;

	}
}
