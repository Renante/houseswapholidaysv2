using System;

namespace Common
{
	/// <summary>
	/// Summary description for ReferrerPaymentHistory.
	/// </summary>
	public class ReferrerPaymentHistory
	{
		public ReferrerPaymentHistory()
		{
		}

		public int MemberID
		{
			get
			{
				return _memberID;
			}
			set
			{
				_memberID = value;
			}
		}

		public int Status
		{
			get
			{
				return _status;
			}
			set
			{
				_status = value;
			}
		}

		public string FirstName
		{
			get
			{
				return _firstName;
			}
			set
			{
				_firstName = value;
			}
		}

		public string GivenName
		{
			get
			{
				return _givenName;
			}
			set
			{
				_givenName = value;
			}
		}

		public int HomeID
		{
			get
			{
				return _homeID;
			}
			set
			{
				_homeID = value;
			}
		}

		public string Location
		{
			get
			{
				return _location;
			}
			set
			{
				_location = value;
			}
		}

		public string EmailAddress
		{
			get
			{
				return _emailAddress;
			}
			set
			{
				_emailAddress = value;
			}
		}

		public DateTime CreatedDate
		{
			get
			{
				return _createdDate;
			}
			set
			{
				_createdDate = value;
			}
		}

		public DateTime UpdatedDate
		{
			get
			{
				return _updatedDate;
			}
			set
			{
				_updatedDate = value;
			}
		}

		public int ReferrerID
		{
			get
			{
				return _referrerID;
			}
			set
			{
				_referrerID = value;
			}
		}

		public DateTime LastPaymentDate
		{
			get
			{
				return _lastPaymentDate;
			}
			set
			{
				_lastPaymentDate = value;
			}
		}

		public Decimal LastPaymentAmount
		{
			get
			{
				return _lastPaymentAmount;
			}
			set
			{
				_lastPaymentAmount = value;
			}
		}

		public decimal LastGSTAmount
		{
			get
			{
				return _lastGSTAmount;
			}
			set
			{
				_lastGSTAmount = value;
			}
		}

		public string LastReceiptNumber
		{
			get
			{
				return _lastReceiptNumber;
			}
			set
			{
				_lastReceiptNumber = value;
			}
		}

		public DateTime LastCommissionPaymentDate
		{
			get
			{
				return _lastCommissionPaymentDate;
			}
			set
			{
				_lastCommissionPaymentDate = value;
			}
		}

		public decimal LastCommissionPaymentAmount
		{
			get
			{
				return _lastCommissionPaymentAmount;
			}
			set
			{
				_lastCommissionPaymentAmount = value;
			}
		}

		public decimal LastCommissionGSTAmount
		{
			get
			{
				return _lastCommissionGSTAmount;
			}
			set
			{
				_lastCommissionGSTAmount = value;
			}
		}

		public string LastCommissionReceiptNumber
		{
			get
			{
				return _lastCommissionReceiptNumber;
			}
			set
			{
				_lastCommissionReceiptNumber = value;
			}
		}

		public string LongURL
		{
			get
			{
				return _longURL;
			}
			set
			{
				_longURL = value;
			}
		}


		public bool IsCurrent
		{
			get
			{
				return _isCurrent;
			}
			set
			{
				_isCurrent = value;
			}
		}

		private int _memberID;
		private int _status;
		private string _firstName;
		private string _givenName;
		private int _homeID;
		private string _location;
		private string _longURL;
		private string _emailAddress;
		private DateTime _createdDate;
		private DateTime _updatedDate;
		private int _referrerID;
		private DateTime _lastPaymentDate;
		private decimal _lastPaymentAmount;
		private decimal _lastGSTAmount;
		private string _lastReceiptNumber;
		private DateTime _lastCommissionPaymentDate;
		private decimal _lastCommissionPaymentAmount;
		private decimal _lastCommissionGSTAmount;
		private string _lastCommissionReceiptNumber;
		private bool _isCurrent;


	}
}
