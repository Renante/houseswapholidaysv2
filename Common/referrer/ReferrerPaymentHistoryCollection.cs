using System;
using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for HomeCollection.
	/// </summary>
	public class ReferrerPaymentHistoryCollection : CollectionBase
	{
		public ReferrerPaymentHistoryCollection()
		{
		}

		public void Add(ReferrerPaymentHistory referrerPaymentHistory)
		{
			InnerList.Add(referrerPaymentHistory);
		}

		public void Remote(ReferrerPaymentHistory referrerPaymentHistory)
		{
			InnerList.Remove(referrerPaymentHistory);
		}

		public ReferrerPaymentHistory this[int index]
		{
			get
			{
				return (ReferrerPaymentHistory)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}

	}
}
