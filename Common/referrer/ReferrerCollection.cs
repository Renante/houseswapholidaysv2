using System;
using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for HomeCollection.
	/// </summary>
	public class ReferrerCollection : CollectionBase
	{
		public ReferrerCollection()
		{
		}

		public void Add(Referrer referrer)
		{
			InnerList.Add(referrer);
		}

		public void Remote(Referrer referrer)
		{
			InnerList.Remove(referrer);
		}

		public Referrer this[int index]
		{
			get
			{
				return (Referrer)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}

	}
}
