using System;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;

namespace Common
{
	/// <summary>
	/// Summary description for ReferrerDAL.
	/// </summary>
	public class ReferrerPaymentHistoryDAL : DataAccess
	{
		public ReferrerPaymentHistoryDAL()
		{
		}

		public ReferrerPaymentHistoryCollection GetByReferrer(int referrerID)
		{
			ReferrerPaymentHistoryCollection referrerPaymentHistoryCollection = new ReferrerPaymentHistoryCollection();

			const string spName = "up_GetMembersByReferrer";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, referrerID))
                {
                    while (reader.Read())
                    {
                        ReferrerPaymentHistory referrerPaymentHistory = new ReferrerPaymentHistory();

                        referrerPaymentHistory.MemberID = GetFieldByInteger(reader, "MemberID");
                        referrerPaymentHistory.Status = GetFieldByInteger(reader, "Status");
                        referrerPaymentHistory.FirstName = GetFieldByString(reader, "FirstName");
                        referrerPaymentHistory.GivenName = GetFieldByString(reader, "GivenName");
                        referrerPaymentHistory.HomeID = GetFieldByInteger(reader, "HomeID");
                        referrerPaymentHistory.Location = GetFieldByString(reader, "Location");
                        referrerPaymentHistory.EmailAddress = GetFieldByString(reader, "EmailAddress");
                        referrerPaymentHistory.CreatedDate = GetFieldByDate(reader, "CreatedDate");
                        referrerPaymentHistory.UpdatedDate = GetFieldByDate(reader, "UpdatedDate");
                        referrerPaymentHistory.ReferrerID = GetFieldByInteger(reader, "ReferrerID");
                        referrerPaymentHistory.LastPaymentDate = GetFieldByDate(reader, "LastPaymentDate");
                        referrerPaymentHistory.LastPaymentAmount = GetFieldByDecimal(reader, "LastPaymentAmount");
                        referrerPaymentHistory.LastGSTAmount = GetFieldByDecimal(reader, "LastGSTAmount");
                        referrerPaymentHistory.LastReceiptNumber = GetFieldByString(reader, "LastReceiptNumber");
                        referrerPaymentHistory.LongURL = GetFieldByString(reader, "LongURL");
                        referrerPaymentHistory.LastCommissionPaymentDate = GetFieldByDate(reader, "LastCommissionPaymentDate");
                        referrerPaymentHistory.LastCommissionPaymentAmount = GetFieldByDecimal(reader, "LastCommissionPaymentAmount");
                        referrerPaymentHistory.LastCommissionGSTAmount = GetFieldByDecimal(reader, "LastCommissionGSTAmount");
                        referrerPaymentHistory.LastCommissionReceiptNumber = GetFieldByString(reader, "LastCommissionReceiptNumber");

                        if (GetFieldByInteger(reader, "IsCurrent") == 1)
                        {
                            referrerPaymentHistory.IsCurrent = true;
                        }
                        else
                        {
                            referrerPaymentHistory.IsCurrent = false;
                        }

                        referrerPaymentHistoryCollection.Add(referrerPaymentHistory);
                    }
                }
            }
			
			return referrerPaymentHistoryCollection;
		}
	
		#region Singleton instance
		public static ReferrerPaymentHistoryDAL Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
					lock (typeof(ReferrerPaymentHistoryDAL))
					{
						if (_instance == null)
						{
							_instance = new ReferrerPaymentHistoryDAL();
						}
					}
				}

				return _instance;
			}
			
		}

		#endregion

		private static ReferrerPaymentHistoryDAL _instance;


	}
}
