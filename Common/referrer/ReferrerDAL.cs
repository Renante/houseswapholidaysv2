using System;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;

namespace Common
{
	/// <summary>
	/// Summary description for ReferrerDAL.
	/// </summary>
	public class ReferrerDAL : DataAccess
	{
		public ReferrerDAL()
		{
		}

		public Referrer Get(int referrerID)
		{
			Referrer referrer = new Referrer();

			const string spName = "up_GetReferrer";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, referrerID))
                {
                    if (reader.Read())
                    {

                        referrer.ID = GetFieldByInteger(reader, "ReferrerID");
                        referrer.CompanyName = GetFieldByString(reader, "CompanyName");
                        referrer.DefaultChannelID = GetFieldByInteger(reader, "DefaultChannelID");
                        referrer.IsMultiChannel = GetFieldByBoolean(reader, "IsMultiChannel");
                        referrer.CookieDuration = GetFieldByInteger(reader, "CookieDuration");
                        referrer.isCustomHeader = GetFieldByBoolean(reader, "IsCustomHeader");

                        // Status
                        // Notes

                        return referrer;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
		}

		public MemberCollection GetByReferrer(int referrerID)
		{
			MemberCollection memberCollection = new MemberCollection();

			const string spName = "up_GetMembersByReferrer";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, referrerID))
                {
                    while (reader.Read())
                    {
                        Member member = new Member();

                        member.ID = GetFieldByInteger(reader, "MemberID");
                        member.FirstName = GetFieldByString(reader, "FirstName");
                        member.GivenName = GetFieldByString(reader, "GivenName");
                        member.EmailAddress = GetFieldByString(reader, "EmailAddress");
                        member.Password = GetFieldByString(reader, "Password");

                        member.Status = (Status)GetFieldByInteger(reader, "Status");

                        member.Introduction = GetFieldByString(reader, "Introduction");
                        member.Language1 = GetFieldByString(reader, "Language1");
                        member.Language2 = GetFieldByString(reader, "Language2");
                        member.Language3 = GetFieldByString(reader, "Language3");
                        member.CreatedDate = GetFieldByDate(reader, "CreatedDate");
                        member.UpdatedDate = GetFieldByDate(reader, "UpdatedDate");
                        member.LastLoggedInDate = GetFieldByDate(reader, "LastLoggedInDate");
                        member.ReferrerID = GetFieldByInteger(reader, "ReferrerID");

                        memberCollection.Add(member);
                    }
                }
            }
			
			return memberCollection;
		}
	
		#region Singleton instance
		public static ReferrerDAL Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
					lock (typeof(ReferrerDAL))
					{
						if (_instance == null)
						{
							_instance = new ReferrerDAL();
						}
					}
				}

				return _instance;
			}
			
		}

		#endregion

		private static ReferrerDAL _instance;


	}
}
