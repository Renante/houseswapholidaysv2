﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class ResetPassword
    {
        public string EmailAddress { get; set; }
        public string ResetKey { get; set; }
        public DateTime DateRequest { get; set; }
    }
}
