﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Common
{
    public class ResetPasswordDAL : DataAccess
    {



        public void SavePasswordResetKey(string emailAddress, string resetKey)
        {
            const string spName = "up_InsertPasswordResetKey";

            ExecuteNonQuery(spName,
                emailAddress,
                resetKey);
            
        }

        public void DeleteResetPasswordKey(string emailAddress)
        {
            const string spName = "up_DeleteResetPasswordKey";

            ExecuteNonQuery(spName,
                emailAddress);
        }

        public ResetPassword GetResetPassword(string resetKey)
        {
            const string spName = "up_GetPasswordReset";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, resetKey))
                {
                    if (reader.Read())
                    {
                        ResetPassword resetPW = new ResetPassword();

                        resetPW.EmailAddress = (string)reader["EmailAddress"];
                        resetPW.ResetKey = (string)reader["ResetKey"];
                        resetPW.DateRequest = (DateTime)reader["DateRequest"];
                        
                        return resetPW;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        public string GenerateResetPasswordKey()
        {
            return Encryption.GenerateRandomString(16);
            
        }

        #region Singleton instance
        public static ResetPasswordDAL Instance
        {
            [DebuggerStepThrough]
            get
            {
                if (_instance == null)
                {
                    lock (typeof(ResetPasswordDAL))
                    {
                        if (_instance == null)
                        {
                            _instance = new ResetPasswordDAL();
                        }
                    }
                }

                return _instance;
            }

        }

        #endregion

        private static ResetPasswordDAL _instance;
    }
}
