using System;

namespace Common
{
	/// <summary>
	/// Summary description for Email.
	/// </summary>
	public class EmailLog
	{

		public EmailLog()
		{
		}

		public int ID
		{
			get
			{
				return _ID;
			}
			set
			{
				_ID = value;
			}
		}

		public int EmailTemplateID
		{
			get
			{
				return _emailTemplateID;
			}
			set
			{
				_emailTemplateID = value;
			}
		}

		public int MemberID
		{
			get
			{
				return _memberID;
			}
			set
			{
				_memberID = value;
			}
		}

		public int ChannelID
		{
			get
			{
				return _channelID;
			}
			set
			{
				_channelID = value;
			}
		}

		public DateTime CreatedDate
		{
			get
			{
				return _createdDate;
			}
			set
			{
				_createdDate = value;
			}
		}

		#region Private instance variables
		
		private int _ID;
		private int _emailTemplateID;
		private int _memberID;
		private int _channelID;
		private DateTime _createdDate;
		
		#endregion Private instance variables
	
	
	}
}
