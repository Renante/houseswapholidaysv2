using System;
using System.Text;
using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;

namespace Common
{
	/// <summary>
	/// Summary description for EmailDAL.
	/// </summary>
	public class EmailLogDAL : DataAccess
	{
		public EmailLogDAL()
		{
		}

		public void Insert(EmailLog emailLog)
		{
			// Save record of email to log file
			const string spName = "up_InsertEmailLog";

			ExecuteNonQuery(spName, 
				emailLog.EmailTemplateID, emailLog.MemberID, emailLog.ChannelID);
		}

		#region Singleton instance
		public static EmailLogDAL Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
					lock (typeof(EmailLogDAL))
					{
						if (_instance == null)
						{
							_instance = new EmailLogDAL();
						}
					}
				}

				return _instance;
			}
			
		}

		#endregion

		private static EmailLogDAL _instance;

	
	}
}
