using System;
using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for HomeCollection.
	/// </summary>
	public class FeaturedHomeCollection : CollectionBase
	{
		public FeaturedHomeCollection()
		{
		}

		public void Add(FeaturedHome featuredHome)
		{
			InnerList.Add(featuredHome);
		}

		public void Remote(FeaturedHome featuredHome)
		{
			InnerList.Remove(featuredHome);
		}

		public FeaturedHome this[int index]
		{
			get
			{
				return (FeaturedHome)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}

	}
}
