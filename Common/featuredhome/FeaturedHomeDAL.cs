using System;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;

namespace Common
{
	/// <summary>
	/// Summary description for HomeDAL.
	/// </summary>
	public class FeaturedHomeDAL : DataAccess
	{
		public FeaturedHomeDAL()
		{
		}

		/*
		public FeaturedHome Get()
		{
			FeaturedHome featuredHome = new FeaturedHome();

			const string spName = "up_GetFeaturedHome";

			using (DataSet dataSet = ExecuteDataset(spName))
			{
				DataTable featuredHomes = dataSet.Tables[0];

				for (int i=0; i<=featuredHomes.Rows.Count-1; i++)
				{

					featuredHome.ID = GetFieldByInteger(featuredHomes.Rows[i],"FeaturedHomeID");
					featuredHome.HomeID = GetFieldByInteger(featuredHomes.Rows[i],"HomeID");
					featuredHome.HomeImageID = GetFieldByInteger(featuredHomes.Rows[i],"HomeImageID");

					featuredHome.FromDate = GetFieldByDate(featuredHomes.Rows[i],"FromDate");
					featuredHome.ThruDate = GetFieldByDate(featuredHomes.Rows[i],"ThruDate");

					featuredHome.Description = GetFieldByString(featuredHomes.Rows[i],"Description");
					featuredHome.ImageURL = GetFieldByString(featuredHomes.Rows[i],"ImageURL");
				}					

				featuredHomes.Dispose();
				dataSet.Dispose();

				return featuredHome;
			}
		}
		*/

		public FeaturedHome Get()
		{
			const string spName = "up_GetFeaturedHome";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection))
                {
                    if (reader.Read())
                    {
                        FeaturedHome featuredHome = new FeaturedHome();

                        featuredHome.ID = (int)reader["FeaturedHomeID"];
                        featuredHome.HomeID = (int)reader["HomeID"];
                        featuredHome.HomeImageID = (int)reader["HomeImageID"];
                        if (reader["FromDate"] == DBNull.Value)
                        {
                            featuredHome.FromDate = DateTime.MinValue;
                        }
                        else
                        {
                            featuredHome.FromDate = (DateTime)reader["FromDate"];
                        }

                        if (reader["ThruDate"] == DBNull.Value)
                        {
                            featuredHome.ThruDate = DateTime.MaxValue;
                        }
                        else
                        {
                            featuredHome.ThruDate = (DateTime)reader["ThruDate"];
                        }

                        featuredHome.Description = (string)reader["Description"];
                        featuredHome.ImageURL = (string)reader["ImageURL"];

                        return featuredHome;
                    }
                    else
                    {
                        return null;
                    }
                } 
            }
		}

		#region Singleton instance
		public static FeaturedHomeDAL Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
					lock (typeof(FeaturedHomeDAL))
					{
						if (_instance == null)
						{
							_instance = new FeaturedHomeDAL();
						}
					}
				}

				return _instance;
			}
			
		}

		#endregion

		private static FeaturedHomeDAL _instance;

	}
}
