using System;

namespace Common
{
	/// <summary>
	/// Summary description for FeaturedHome.
	/// </summary>
	public class FeaturedHome
	{
		public FeaturedHome()
		{
		}

		public int ID
		{
			get
			{
				return _id;
			}
			set
			{
				_id = value;
			}
		}

		public int HomeID
		{
			get
			{
				return _homeID;
			}
			set
			{
				_homeID = value;
			}
		}

		public int HomeImageID
		{
			get
			{
				return _homeImageID;
			}
			set
			{
				_homeImageID = value;
			}
		}

		public DateTime FromDate
		{
			get
			{
				return _fromDate;
			}
			set
			{
				_fromDate = value;
			}
		}

		public DateTime ThruDate
		{
			get
			{
				return _thruDate;
			}
			set
			{
				_thruDate = value;
			}
		}

		public string Description
		{
			get
			{
				return _description;
			}
			set
			{
				_description = value;
			}
		}

		public string ImageURL
		{
			get
			{
				return _imageURL;
			}
			set
			{
				_imageURL = value;
			}
		}

		private int _id;
		private int _homeID;
		private int _homeImageID;
		private DateTime _fromDate;
		private DateTime _thruDate;
		private string _description;
		private string _imageURL;
	}
}
