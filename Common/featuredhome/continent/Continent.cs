using System;

namespace Common
{
	/// <summary>
	/// Summary description for Region.
	/// </summary>
	public class Continent
	{
		public Continent()
		{
		}

		public int ID
		{
			get
			{
				return _id;
			}
			set
			{
				_id = value;
			}
		}

		public string Code
		{
			get
			{
				return _code;
			}
			set
			{
				_code = value;
			}
		}

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		private string _name;
		private string _code;
		private int _id;
	
	}
}
