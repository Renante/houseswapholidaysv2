using System;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;

namespace Common
{
	/// <summary>
	/// Summary description for RegionDAL.
	/// </summary>
	public class ContinentDAL : DataAccess
	{

		public ContinentDAL()
		{
		}

		/*
		public ContinentCollection GetAll()
		{
			ContinentCollection continentCollection = new ContinentCollection();

			const string spName = "up_GetAllContinents";

			using (DataSet dataSet = ExecuteDataset(spName))
			{
				DataTable continentList = dataSet.Tables[0];

				for (int i=0; i<=continentList.Rows.Count-1; i++)
				{

					Continent continent = new Continent();

					continent.Name = GetFieldByString(continentList.Rows[i], "Name");
					continent.Code = GetFieldByString(continentList.Rows[i], "Code");
					continent.ID = GetFieldByInteger(continentList.Rows[i], "ID");

					// Add to collection
					continentCollection.Add(continent);
				}
				
				continentList.Dispose();
				dataSet.Dispose();

				return continentCollection;
			}
		}
		*/

		public ContinentCollection GetAll()
		{
			ContinentCollection continentCollection = new ContinentCollection();

			const string spName = "up_GetAllContinents";

			using (SqlDataReader reader = ExecuteReader(spName))
			{
				while (reader.Read())
				{

					Continent continent = new Continent();

					continent.Name = GetFieldByString(reader, "Name");
					continent.Code = GetFieldByString(reader, "Code");
					continent.ID = GetFieldByInteger(reader, "ID");

					// Add to collection
					continentCollection.Add(continent);
				}
				
				// reader.Close();

			}

			return continentCollection;
		}

		#region Singleton instance
		public static ContinentDAL Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
					lock (typeof(ContinentDAL))
					{
						if (_instance == null)
						{
							_instance = new ContinentDAL();
						}
					}
				}

				return _instance;
			}
			
		}

		#endregion

		private static ContinentDAL _instance;

	}
}
