using System;
using System.Collections;

namespace Common
{
	/// <summary>
	/// A collection of geocoding results.
	/// </summary>
	public class GeocodingResultCollection : CollectionBase
	{
		public void Add(GeocodingResult results)
		{
			InnerList.Add(results);
		}

		public GeocodingResult this[int index]
		{
			get
			{
				return (GeocodingResult)InnerList[index];
			}
		}
	}
}