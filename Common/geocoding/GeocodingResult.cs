using System;

namespace Common
{
	/// <summary>
	/// A geocoding result.
	/// </summary>
	public class GeocodingResult
	{
		/// <summary>
		/// Default constructor.
		/// </summary>
		public GeocodingResult() : this(String.Empty, String.Empty, 0.0M, 0.0M, 0.0M)
		{
		}

		/// <summary>
		/// Constructor with name and address, but no lat/long.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="address"></param>
		public GeocodingResult(string name, string address) : this(name, address, 0.0M, 0.0M, 0.0M)
		{
		}

		/// <summary>
		/// Constructor with the lot.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="address"></param>
		/// <param name="latitude"></param>
		/// <param name="longitude"></param>
		/// <param name="range"></param>
		public GeocodingResult(string name, string address, decimal latitude, decimal longitude, decimal range)
		{
			_name = name;
			_address = address;
			_latitude = latitude;
			_longitude = longitude;
			_range = range;
		}

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		public string Address
		{
			get
			{
				return _address;
			}
			set
			{
				_address = value;
			}
		}

		public decimal Latitude
		{
			get
			{
				return _latitude;
			}
			set
			{
				_latitude = value;
			}
		}

		public decimal Longitude
		{
			get
			{
				return _longitude;
			}
			set
			{
				_longitude = value;
			}
		}

		public decimal Range
		{
			get
			{
				return _range;
			}
			set
			{
				_range = value;
			}
		}

		public bool HasLatLong
		{
			get
			{
				return (_latitude != 0.0M && _longitude != 0.0M);
			}
		}

		private string _name;
		private string _address;
		private decimal _latitude;
		private decimal _longitude;
		private decimal _range;
	}
}