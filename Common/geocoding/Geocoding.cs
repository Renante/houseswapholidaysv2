using System;
using System.IO;
using System.Net;
using System.Web;
using System.Xml;

namespace Common
{
	/// <summary>
	/// Static class for talking to the Google Geocoding web service.
	/// </summary>
	public class Geocoding
	{
		/// <summary>
		/// Timeout for the Google Geocoding web service (in milliseconds)
		/// </summary>
		private const int Timeout = 5000;

		/// <summary>
		/// The base URL for talking to the Google Geocoding web service 
		/// </summary>
		private const string BaseUrl = @"http://maps.google.com/maps?output=kml&q=";

		/// <summary>
		/// Gets the Geocoding result for a location.
		/// </summary>
		/// <param name="location">The location</param>
		/// <returns>A geocoding result</returns>
		public static GeocodingResult GetLatLong(string location)
		{
			// Format the URL for connecting to the Google Geocoding web service.
			string url = BaseUrl + HttpUtility.UrlEncode(location);

			// Send the request, get the response.
			HttpWebResponse response;
			try
			{
				HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
				request.Timeout = Timeout;
				response = (HttpWebResponse)request.GetResponse();
			}
			catch (WebException webEx)
			{
				if (webEx.Status == WebExceptionStatus.Timeout)
				{
					_log.Warn("Timeout getting response from the Google Geocoding web service.", webEx);
				}
				else
				{
					_log.Error("Error getting response from the Google Geocoding web service.", webEx);
				}
				return new GeocodingResult();
			}

			if (response.StatusCode != HttpStatusCode.OK)
			{
				_log.ErrorFormat("Error getting response from the Google Geocoding web service, the web service returned HTTP status code {0}.", response.StatusCode);
				return new GeocodingResult();
			}

			// Get the resonse stream to get the XML returned.
			Stream responseStream = response.GetResponseStream();
			StreamReader responseReader = new StreamReader(responseStream);
			string xml = responseReader.ReadToEnd();
			if (xml.Length == 0)
			{
				_log.ErrorFormat("Empty response returned from Google Geocoding web service - possible invalid location '{0}'.", location);
				return new GeocodingResult();
			}

			_log.InfoFormat("XML returned from Google Geocoding web service:\n{0}", xml);

			// Parse the XML, get results collection.

            GeocodingResultCollection results;

            bool isValidlocation = true;

            try
            {
                results = ParseFromXml(xml);
            }
            catch (Exception exc)
            {
                _log.Error("Results from parsing in parsefromxml is : ", exc);
                isValidlocation = false;
                return null;
            }


			// Cleanup - must remember to close the response object.
			response.Close();

            if ((isValidlocation) && (results != null))
            {
                // Return results.
                if (results.Count == 0)
                {
                    _log.WarnFormat("The Google Geocoding web service returned no results for location '{0}'.", location);
                    return new GeocodingResult();
                }
                if (results.Count == 1)
                {
                    return results[0];
                }
                else
                {
                    _log.InfoFormat("The Google Geocoding web service returned more than one result for location '{0}' - returning first match.", location);
                    return results[0];
                    // TODO: Maybe try the webservice a second time, with the first matching address?
                    // TODO: return GetLatLong(results[0].Address);
                }
            }
            else
            {
                return null;
            }
		}

		private static GeocodingResultCollection ParseFromXml(string xml)
		{
			GeocodingResultCollection results = new GeocodingResultCollection();

			XmlDocument doc = new XmlDocument();


            try
            {
                doc.LoadXml(xml);
            }
            catch (Exception exc)
            {
                _log.Error("Error loading xml from map parsing : ", exc);
                return null;
            }

			foreach (XmlNode node in doc.ChildNodes)
			{
				if (node is XmlDeclaration)
				{
					// Do nothing - we expect the XML declaration to be the first node.
				}
				else if (node.Name == "kml")
				{
					ParseKml(node, results);
				}
				else
				{
					_log.InfoFormat("Unexpected XML node in response from the Google Geocoding web service: {0}.", node.Name);
				}
			}

			return results;
		}

		public static void ParseKml(XmlNode kmlNode, GeocodingResultCollection results)
		{
			foreach (XmlNode node in kmlNode.ChildNodes)
			{
				switch (node.Name)
				{
					case "Placemark":
						ParsePlacemark(node, results);
						break;

					case "Folder":
						ParseFolder(node, results);
						break;

					default:
						_log.InfoFormat("Unexpected XML node in kml root node from the Google Geocoding web service: {0}.", node.Name);
						break;
				}
			}
		}

		private static void ParsePlacemark(XmlNode placemarkNode, GeocodingResultCollection results)
		{
			GeocodingResult result = new GeocodingResult();

			foreach (XmlNode node in placemarkNode.ChildNodes)
			{
				switch (node.Name)
				{
					case "name":
						result.Name = node.InnerText;
						break;

					case "address":
						result.Address = node.InnerText;
						break;

					case "LookAt":
						ParseLookAt(node, result);
						break;

					default:
						_log.DebugFormat("Ignoring child node '{0}' in Placement.", node.Name);
						break;
				}
			}

			results.Add(result);
		}

		private static void ParseFolder(XmlNode folderNode, GeocodingResultCollection results)
		{
			foreach (XmlNode node in folderNode.ChildNodes)
			{
				switch (node.Name)
				{
					case "Placemark":
						ParsePlacemark(node, results);
						break;

					default:
						_log.DebugFormat("Ignoring child node '{0}' in Folder.", node.Name);
						break;
				}
			}
		}

		private static void ParseLookAt(XmlNode lookAtNode, GeocodingResult result)
		{
			foreach (XmlNode node in lookAtNode.ChildNodes)
			{
				switch (node.Name)
				{
					case "longitude":
						result.Longitude = Decimal.Parse(node.InnerText);
						break;

					case "latitude":
						result.Latitude = Decimal.Parse(node.InnerText);
						break;

					case "range":
						result.Range = Decimal.Parse(node.InnerText);
						break;

					default:
						_log.DebugFormat("Ignoring child node '{0}' in LookAt.", node.Name);
						break;
				}
			}
		}

		private static log4net.ILog _log = log4net.LogManager.GetLogger(typeof(Geocoding));
	}
}
