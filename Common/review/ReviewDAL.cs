using System;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;

namespace Common
{
	/// <summary>
	/// Summary description for ReviewDAL
	/// </summary>
	public class ReviewDAL : DataAccess
	{

		public ReviewDAL()
		{
		}

		public ReviewCollection GetByHome(int homeID)
		{
			ReviewCollection reviewCollection = new ReviewCollection();

			const string spName = "up_GetReviewsByHome";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, homeID))
                {
                    while (reader.Read())
                    {
                        Review review = new Review();

                        review.ReviewID = GetFieldByInteger(reader, "ReviewID");
                        review.HomeID = GetFieldByInteger(reader, "HomeID");
                        review.MemberReviewerID = GetFieldByInteger(reader, "MemberReviewerID");
                        review.CreatedDate = GetFieldByDate(reader, "CreatedDate");
                        review.UpdatedDate = GetFieldByDate(reader, "UpdatedDate");
                        review.SwapDuration = GetFieldByInteger(reader, "SwapDuration");
                        review.SwapCommencementDate = GetFieldByDate(reader, "SwapCommencementDate");
                        review.Rating = GetFieldByInteger(reader, "Rating");
                        review.Summary = GetFieldByString(reader, "Summary");
                        review.Comments = GetFieldByString(reader, "Comments");
                        review.IsVisible = GetFieldByBoolean(reader, "IsVisible");
                        review.ReviewerHomeID = GetFieldByInteger(reader, "ReviewerHomeID");
                        review.ReviewerName = GetFieldByString(reader, "ReviewerName");
                        review.ReviewerLocation = GetFieldByString(reader, "ReviewerLocation");

                        review.Status = (Status)reader["Status"];

                        // Add to collection
                        reviewCollection.Add(review);
                    }
                } 
            }

			return reviewCollection;
		}


		public Review GetByID(int reviewID)
		{
			const string spName = "up_GetReviewByID";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, reviewID))
                {
                    if (reader.Read())
                    {
                        Review review = new Review();

                        review.ReviewID = GetFieldByInteger(reader, "ReviewID");
                        review.HomeID = GetFieldByInteger(reader, "HomeID");
                        review.MemberReviewerID = GetFieldByInteger(reader, "MemberReviewerID");
                        review.CreatedDate = GetFieldByDate(reader, "CreatedDate");
                        review.UpdatedDate = GetFieldByDate(reader, "UpdatedDate");
                        review.SwapDuration = GetFieldByInteger(reader, "SwapDuration");
                        review.SwapCommencementDate = GetFieldByDate(reader, "SwapCommencementDate");
                        review.Rating = GetFieldByInteger(reader, "Rating");
                        review.Summary = GetFieldByString(reader, "Summary");
                        review.Comments = GetFieldByString(reader, "Comments");
                        review.IsVisible = GetFieldByBoolean(reader, "IsVisible");
                        review.ReviewerHomeID = GetFieldByInteger(reader, "ReviewerHomeID");
                        review.ReviewerName = GetFieldByString(reader, "ReviewerName");
                        review.ReviewerLocation = GetFieldByString(reader, "ReviewerLocation");

                        review.Status = (Status)reader["Status"];

                        return review;
                    }
                    else
                    {
                        throw new ApplicationException(String.Format("Review not found - review ID {0} does not exist.", reviewID));
                        // return null;
                    }
                } 
            }
		}

		public Review Save(Review review)
		{
			if (review.ReviewID <= 0)
			{
				// Add new home
				const string spName = "up_InsertReview";

				review.ReviewID = (int)ExecuteScalar(spName, review.HomeID, review.Rating, review.MemberReviewerID, review.Summary, review.Comments, review.SwapDuration, review.SwapCommencementDate);
			}
			else
			{
				// Update existing home
				const string spName = "up_UpdateReview";

				ExecuteNonQuery(spName, review.ReviewID, review.HomeID, review.Rating, review.MemberReviewerID, review.Summary, review.Comments, review.SwapDuration, review.SwapCommencementDate );
			}

			return review;
		}

		public void Delete(int reviewID, int MemberReviewerID)
		{
			const string spName = "up_DeleteReview";

			ExecuteNonQuery(spName, reviewID, MemberReviewerID);
		}
		#region Singleton instance
		public static ReviewDAL Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
					lock (typeof(ReviewDAL))
					{
						if (_instance == null)
						{
							_instance = new ReviewDAL();
						}
					}
				}

				return _instance;
			}
			
		}

		#endregion

		private static ReviewDAL _instance;

	}
}
