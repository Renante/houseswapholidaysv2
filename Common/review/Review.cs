using System;

namespace Common
{
	/// <summary>
	/// Summary description for review.
	/// </summary>
	public class Review
	{
		public Review()
		{
		}

		public int ReviewID
		{
			get
			{
				return _reviewID;
			}
			set
			{
				_reviewID = value;
			}
		}

		public int HomeID
		{
			get
			{
				return _homeID;
			}
			set
			{
				_homeID = value;
			}
		}

		public int MemberReviewerID
		{
			get
			{
				return _memberReviewerID;
			}
			set
			{
				_memberReviewerID = value;
			}
		}

		public DateTime CreatedDate
		{
			get
			{
				return _createdDate;
			}
			set
			{
				_createdDate = value;
			}
		}

		public DateTime UpdatedDate
		{
			get
			{
				return _updatedDate;
			}
			set
			{
				_updatedDate = value;
			}
		}

		public int SwapDuration
		{
			get
			{
				return _swapDuration;
			}
			set
			{
				_swapDuration = value;
			}
		}

		public DateTime SwapCommencementDate
		{
			get
			{
				return _swapCommencementDate;
			}
			set
			{
				_swapCommencementDate = value;
			}
		}

		public int Rating
		{
			get
			{
				return _rating;
			}
			set
			{
				_rating = value;
			}
		}

		public string Summary
		{
			get
			{
				return _summary;
			}
			set
			{
				_summary = value;
			}
		}

		public string Comments
		{
			get
			{
				return _comments;
			}
			set
			{
				_comments = value;
			}
		}

		public bool IsVisible
		{
			get
			{
				return _isVisible;
			}
			set
			{
				_isVisible = value;
			}
		}

		public Status Status
		{
			get
			{
				return _status;
			}
			set
			{
				_status = value;
			}
		}

		public int ReviewerHomeID
		{
			get
			{
				return _reviewerHomeID;
			}
			set
			{
				_reviewerHomeID = value;
			}
		}

		public string ReviewerName
		{
			get
			{
				return _reviewerName;
			}
			set
			{
				_reviewerName = value;
			}
		}

		public string ReviewerLocation
		{
			get
			{
				return _reviewerLocation;
			}
			set
			{
				_reviewerLocation = value;
			}
		}

		#region Private instance variables
		private int _reviewID;
		private int _homeID;
		private int _memberReviewerID;
		private DateTime _createdDate;
		private DateTime _updatedDate;
		private int _swapDuration;
		private DateTime _swapCommencementDate;
		private int _rating;
		private string _summary;
		private string _comments;
		private bool _isVisible;
		private Status _status;
		private int _reviewerHomeID;
		private string _reviewerName;
		private string _reviewerLocation;
		

		#endregion

	}
}
