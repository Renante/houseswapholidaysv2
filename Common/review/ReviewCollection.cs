using System;
using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for HomeCollection.
	/// </summary>
	public class ReviewCollection : CollectionBase
	{
		public ReviewCollection()
		{
		}

		public void Add(Review review)
		{
			InnerList.Add(review);
		}

		public void Remote(Review review)
		{
			InnerList.Remove(review);
		}

		public Review this[int index]
		{
			get
			{
				return (Review)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}

	}
}
