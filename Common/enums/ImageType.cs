using System;

namespace Common
{
		public enum ImageType 
		{ 
			DefaultImage = 1, 
			MiniImage = 2,
			Thumbnail = 3,
			MediumImage = 4
		}
}
