using System;

namespace Common
{
	public enum PaymentStatus
	{ 
		Successful	= 1,
		Pending		= 2,
		Failed		= 3,
		Refunded	= 4,
		Deleted		= 5
	}
}
