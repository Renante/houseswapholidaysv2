using System;

namespace Common
{
	public enum MemberType
	{ 
		Guest = 1, // Newsletter only subscriber
		Trial = 2, // Future use
		PendingApproval = 3, // New member waiting approval
		Full = 4, // Has current home listing
		SuperUser = 5, // User also has admin access
		Deleted = 6, // User 'no longer exists'
	}
}
