using System;

namespace Common
{
	public enum FieldType
	{ 
		AlphaNumeric = 1, 
		Alpha = 2,
		Numeric	= 3,
		Email = 4,
		Password = 5,
		Checkbox = 6,
		Optional = 7,
		Compulsory = 8
	}
}
