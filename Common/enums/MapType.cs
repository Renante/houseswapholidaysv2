using System;

namespace Common
{
	public enum MapType
	{ 
		None		= 0, 
		Street		= 1,
		Postcode	= 2,
		City		= 3,
	}
}
