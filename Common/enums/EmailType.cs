using System;

namespace Common
{
	public enum EmailType
	{ 
		NoSpecified = 0,
		Welcome = 1, 
		ExchangeEnquiry = 2,
		SubscriptionDue	= 3,
		SubscriptionReminder = 4,
		PaymentReceived = 5,
		PaymentFailed = 6,
		PasswordReminder = 7,
		ReferenceRequest = 8,
		GenericNotification = 9,
		HomeSuccessfullyListed = 10,
		SupportEnquiry = 11,
		PendingApproval = 12,
		ApprovalGranted = 13,
		MessageReleased = 14,
        PasswordReset = 15
	}
}
