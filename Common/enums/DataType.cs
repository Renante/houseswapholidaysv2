using System;

namespace Common
{
	public enum DataType
	{ 
		Checkbox = 1, 
		Textbox = 2
	}
}
