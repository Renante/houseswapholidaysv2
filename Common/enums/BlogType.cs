using System;

namespace Common
{
	public enum BlogType
	{ 
		Email = 1, 
		Page = 2,
		Other	= 3,
	}
}
