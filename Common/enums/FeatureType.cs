using System;

namespace Common
{
	public enum FeatureType 
	{ 
		HomeFacilities = 1, 
		HomeActivities = 2,
		HomePreferences	= 3,
		HomeRules = 4,
        HolidayTypesToOffer = 5,
        SwapTimePreferences = 6,
        SwapLength = 7
	}
}

