using System;

namespace Common
{
	public enum Status
	{ 
		NotSpecified	= 0,
		Active			= 1,
		PendingApproval	= 2,
		Archived		= 3,
		Draft			= 4,
		Deleted			= 5, 
		InvalidContact  = 6,
		Unavailable     = 7,
        PrePayment = 8,
        Expired = 9,
        Suspended = 10
	}
}
