using System;

namespace Common
{
	public enum SortBy
	{ 
		Ascending = 1, 
		Descending = 2
	}
}
