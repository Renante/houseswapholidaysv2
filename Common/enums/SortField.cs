using System;

namespace Common
{
    public enum SortField
    {
        NotSpecified = 0,
        DateListed = 1,
        City = 2,
        Region = 3,
        Country = 4,
        Bedrooms = 5,
        MostRecent = 6,
        LastMinute = 7 // wooncherk
    }
}
