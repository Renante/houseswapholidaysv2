using System;

namespace Common
{
	public enum ReferenceType
	{ 
		HomeExchangeReference = 1, 
		CharacterReference = 2
	}
}
