using System;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Common
{
	/// <summary>
	/// Summary description for HomeAvailabilityDAL.
	/// </summary>
	public class HomeAvailabilityDAL : DataAccess
	{
		public HomeAvailabilityDAL()
		{
		}

		#region Types
		private sealed class HomeImageFields
		{
			public readonly int ID = -1;
			public readonly int HomeID = -1;
			public readonly int FromDate = -1;
			public readonly int ThruDate = -1;
		}
		#endregion
	
		/*
		public HomeAvailability Get(int homeAvailabilityID)
		{
			//return ExecuteReader("up_getHome", homeImageID);
			HomeAvailability homeAvailability = new HomeAvailability();
			return homeAvailability;
		}
		*/

		public HomeAvailabilityCollection GetAllForHome(int homeID)
		{
			HomeAvailabilityCollection homeAvailabilityCollection = new HomeAvailabilityCollection();

			const string spName = "up_GetHomeAvailabilityByHomeID";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, homeID))
                {

                    while (reader.Read())
                    {
                        HomeAvailability homeAvailability = new HomeAvailability();

                        homeAvailability.ID = (int)reader["ID"];
                        homeAvailability.HomeID = (int)reader["HomeID"];
                        homeAvailability.FromDate = (DateTime)reader["FromDate"];
                        homeAvailability.ThruDate = (DateTime)reader["ThruDate"];

                        homeAvailabilityCollection.Add(homeAvailability);
                    }
                } 
            }

			return homeAvailabilityCollection;
		}

		private HomeAvailability LoadFromReader(SqlDataReader reader, HomeImageFields fields)
		{
			HomeAvailability homeAvailability = new HomeAvailability();

			homeAvailability.ID = reader.GetInt32(fields.ID);
			homeAvailability.HomeID = reader.GetInt32(fields.HomeID);
			homeAvailability.FromDate = reader.GetDateTime(fields.FromDate);
			homeAvailability.ThruDate = reader.GetDateTime(fields.ThruDate);

			return homeAvailability;
		}

		#region Singleton instance
		public static HomeAvailabilityDAL Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
					lock (typeof(HomeAvailabilityDAL))
					{
						if (_instance == null)
						{
							_instance = new HomeAvailabilityDAL();
						}
					}
				}

				return _instance;
			}
			
		}

		#endregion


		private static HomeAvailabilityDAL _instance;

	}
}
