using System;

namespace Common
{
	/// <summary>
	/// Summary description for HomeAvailability.
	/// </summary>
	public class HomeAvailability
	{
		public HomeAvailability()
		{
		}

		public int ID
		{
			get
			{
				return _id;
			}
			set
			{
				_id = value;
			}
		}

		public int HomeID
		{
			get
			{
				return _homeID;
			}
			set
			{
				_homeID = value;
			}
		}

		public DateTime FromDate
		{
			get
			{
				return _fromDate;
			}
			set
			{
				_fromDate = value;
			}
		}

		public DateTime ThruDate
		{
			get
			{
				return _thruDate;
			}
			set
			{
				_thruDate = value;
			}
		}

		private int _id;
		private int _homeID;
		private DateTime _fromDate;
		private DateTime _thruDate;


	}
}
