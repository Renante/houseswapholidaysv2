using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for HomeAvailabilityCollection.
	/// </summary>
	public class HomeAvailabilityCollection : CollectionBase
	{
		public HomeAvailabilityCollection()
		{
		}

		public void Add(HomeAvailability homeAvailability)
		{
			InnerList.Add(homeAvailability);
		}

		public void Remote(HomeAvailability homeAvailability)
		{
			InnerList.Remove(homeAvailability);
		}

		public HomeAvailability this[int index]
		{
			get
			{
				return (HomeAvailability)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}

	
	}
}
