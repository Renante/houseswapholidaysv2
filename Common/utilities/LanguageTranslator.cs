using RavSoft;
using System;

namespace Common
{
	/// <summary>
	/// Summary description for LanguageTranslator.
	/// </summary>
	public class LanguageTranslator
	{
		public LanguageTranslator()
		{
		
		}

		public string Translate(string sourceString)
		{
			GoogleTranslator gt =
				new GoogleTranslator (GoogleTranslator.Mode.EnglishToGerman);
			return gt.translate (sourceString);
		}
	}

	/// <summary>
	/// An object that uses Google's online language tools to translate
	/// text between English and French, German, Spanish, Italian or
	/// Portugese.
	/// </summary>
	public class GoogleTranslator : RavSoft.WebResourceProvider
	{

		/// <summary>
		/// Translation mode.
		/// </summary>
		public enum Mode 
		{
			EnglishToFrench = 0,
			FrenchToEnglish,
			EnglishToGerman,
			GermanToEnglish,
			EnglishToSpanish,
			SpanishToEnglish,
			EnglishToItalian,
			ItalianToEnglish,
			EnglishToPortugese,
			PortugeseToEnglish,
		}

		/// <summary>
		/// Constructs a GoogleTranslator for a specific translation mode.
		/// </summary>
		/// <param name="mode">Translation mode</param>
		public GoogleTranslator
			(GoogleTranslator.Mode mode)
		{
			Referer = "http://www.google.com";
			m_mode = mode;
		}

		/////////////
		// Operations

		public string translate
			(string strText)
		{
			m_strText = strText;
			m_strTranslation = "";
			fetchResource();
			return (m_strTranslation);
		}

		/////////////////
		// Implementation

		/// <summary>Translation mode.</summary>
		GoogleTranslator.Mode m_mode = GoogleTranslator.Mode.EnglishToFrench;

		/// <summary>Text to be translated.</summary>
		string m_strText = "";

		/// <summary>Translated text.</summary>
		string m_strTranslation = "";

		/////////////////////////////////////
		// WebResourceProvider implementation

		/// <summary>
		/// Returns the url to be fetched.
		/// </summary>
		/// <returns>The url to be fetched.</returns>
		protected override string getFetchUrl()
		{
			return "http://www.google.com/translate_t";
		}

		/// <summary>
		/// Retrieves the POST data (if any) to be sent to the url to be fetched.
		/// The data is returned as a string of the form "arg=val[&arg=val]...".
		/// </summary>
		/// <returns>A string containing the POST data or null if none.</returns>
		protected override string getPostData()
		{
			// Set translation mode
			string strPostData = "hl=en&ie=UTF8&oe=UTF8submit=Translate&langpair=";
			switch (m_mode) 
			{
				default:
				case Mode.EnglishToFrench:
					strPostData += "en|fr";
					break;
				case Mode.FrenchToEnglish:
					strPostData += "fr|en";
					break;
				case Mode.EnglishToGerman:
					strPostData += "en|de";
					break;
				case Mode.GermanToEnglish:
					strPostData += "de|en";
					break;
				case Mode.EnglishToSpanish:
					strPostData += "en|es";
					break;
				case Mode.SpanishToEnglish:
					strPostData += "es|en";
					break;
				case Mode.EnglishToItalian:
					strPostData += "en|it";
					break;
				case Mode.ItalianToEnglish:
					strPostData += "it|en";
					break;
				case Mode.EnglishToPortugese:
					strPostData += "en|pt";
					break;
				case Mode.PortugeseToEnglish:
					strPostData += "pt|en";
					break;
			}

			// Set text to be translated
			strPostData += "&text=" + m_strText;
			return strPostData;
		}

		/// <summary>
		/// Provides the derived class to parse the fetched content.
		/// </summary>
		protected override void parseContent()
		{
			m_strTranslation = "";
			string strContent = Content;
			RavSoft.StringParser  parser = new RavSoft.StringParser (strContent);
			if (parser.skipToStartOfNoCase ("<textarea name=q ")) 
			{
				if (parser.skipToEndOf (">")) 
				{
					bool bStatus = parser.extractToNoCase ("</textarea>", ref m_strTranslation);
				}
			}
		}
	}

}
