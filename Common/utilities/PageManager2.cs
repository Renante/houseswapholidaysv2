using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using Common;

namespace Common
{
	/// <summary>
	/// Updated page manager with improved session management
	/// </summary>
	public class PageManager2 : System.Web.UI.Page
	{
		public PageManager2()
		{
		}

		private Style primaryStyle = new Style();

		public void InitialisePage(HtmlGenericControl title, HtmlGenericControl stylesheetName)
		{

			// Set page title if necessary
			if (title.InnerText.Length <= 0)
			{
				title.InnerText = GetLongPageTitle();
			}

			// Set stylesheet if necessary
			//stylesheetName.Attributes["href"] = GetStyleSheet();

			// Set meta description (appears on search results listings)
			HtmlGenericControl metaDescription = (HtmlGenericControl)Page.FindControl("MetaDescription");
			if (metaDescription != null)
			{
				metaDescription.Attributes["content"] = GetMetaDescription();
			}

			// Set meta keywords (useful for SEO etc)
			HtmlGenericControl metaKeywords = (HtmlGenericControl)Page.FindControl("MetaKeywords");
			if (metaKeywords != null)
			{
				metaKeywords.Attributes["content"] = GetMetaKeywords();
			}

		}

		public string GetMetaDescription()
		{
			return Channel.MetaDescriptionGeneric;
		}

		public string GetMetaKeywords()
		{
			return Channel.MetaKeywordsGeneric;
		}

		public string GetShortPageTitle()
		{
			return GetShortPageTitle("");
		}

		public string GetShortPageTitle(string title)
		{
			if (title.Length == 0)
			{
				// Updated the below to read from the current channel, rather than webconfig
				return Channel.Name;
				//return ConfigurationSettings.AppSettings["ShortPageTitle"];
			}
			else
			{
				return title;
			}
		}

		public string GetLongPageTitle()
		{
			return GetLongPageTitle("");
		}

		public string GetLongPageTitle(string title)
		{
			if (title.Length == 0)
			{
				//return ConfigurationSettings.AppSettings["LongPageTitle"];
				return Channel.Name;
			}
			else
			{
				return title;
			}
		}

		public string GetStyleSheet()
		{
			// get channel
			//Channel channel = ChannelDAL.Instance.Get(GetWebsiteName());

			if (Channel != null)
			{
				return Channel.StyleSheet;
			}

			return @"/css/homeexchange.css";

		}

		public string GetDisplayName()
		{
			return Channel.Name;
		}

		public string GetExchangeType()
		{
			if (_exchangeName == "")
			{
				_exchangeName = "exchange";
				
				if (Channel.ExchangeType == 2)
					_exchangeName = "swap";
			}

			return _exchangeName;
			
		}

		public string GetWebsiteName()
		{
			return GetWebsiteName(false);
		}

		public string GetWebsiteName(bool isSecure)
		{
			string serverName = Request.ServerVariables["SERVER_NAME"];

			if (ConfigurationSettings.AppSettings["IsSecure"].ToUpper() != "TRUE")
			{
				isSecure = false;
			}

			if ((!isSecure) && (_websiteName != null))
			{
				return _websiteName;
			}
			else if ((isSecure) && (_websiteSecureName != null))
			{
				return _websiteSecureName;
			}
			else
			{
				if ((serverName.ToUpper() != "LOCALHOST") && (!serverName.StartsWith("www")))
				{
					serverName = String.Format("www.{0}", serverName);
				}

				if ((Request.ServerVariables["SERVER_PORT"].Length > 0) && (serverName.ToUpper() == "LOCALHOST"))
				{
					if (isSecure)
					{
						_websiteSecureName = String.Format("https://{0}:{1}", serverName, Request.ServerVariables["SERVER_PORT"]);
						//return String.Format("https://{0}:{1}", serverName, Request.ServerVariables["SERVER_PORT"]);
					}
					else
					{
						_websiteName = String.Format("http://{0}:{1}", serverName, Request.ServerVariables["SERVER_PORT"]);
					}
				}
				else
				{
					if (isSecure)
					{
						_websiteSecureName = String.Format("https://{0}", serverName);
					}
					else
					{
						_websiteName = String.Format("http://{0}", serverName);
					}
				}
			}

			if (isSecure)
			{
				return _websiteSecureName;
			}
			else
			{
				return _websiteName;
			}
			
			//return serverName;
		}

		public string GetSecureURL(string urlPath)
		{
			bool isSecure = false;

			if (ConfigurationSettings.AppSettings["IsSecure"].ToUpper() == "TRUE")
			{
				isSecure = true;
			}

			return String.Format("{0}{1}",GetWebsiteName(isSecure),urlPath);
		}

		public string SecureRedirect(string urlPath)
		{
			bool isSecure = false;

			if (ConfigurationSettings.AppSettings["IsSecure"].ToUpper() == "TRUE")
			{
				isSecure = true;
			}

			return String.Format("{0}{1}",GetWebsiteName(isSecure),urlPath);
		}

		public string NonSecureRedirect(string urlPath)
		{
			bool isSecure = false;

			return String.Format("{0}{1}",GetWebsiteName(isSecure),urlPath);
		}

		#region Promotion handling (via querystring)

		// Check if a promotion is passed in via the querystring
		public void CheckForPromotion(string promotionID)
		{
			if (promotionID != null)
			{
				// Check if promotion code is valid. If it is, set the details
				Promotion promotion = PromotionDAL.Instance.Get(promotionID);

				if ((promotion != null) && (promotion.Status == (int)Status.Active))
				{
					// Is an active promotion, so save to cookie
					SavePromotionToCookie(promotionID);
				}
				else
				{
					// Is not active so clear cookie
					HttpCookie promotionCookie = new HttpCookie("PromotionCookie", "");
					promotionCookie.Expires = DateTime.Now.AddDays(-1);
					Response.Cookies.Add(promotionCookie);
				}
			}
		}

		// Saves passed in promotion (if valid) to the query string
		public void SavePromotionToCookie(string promotionID)
		{
			// Save to referrer cookie
			if (promotionID != null)
			{
				// Save valid promotion reference to cookie
				HttpCookie promotionCookie = new HttpCookie("PromotionCookie", promotionID);
				promotionCookie.Expires = DateTime.Now.AddHours(24);
				Response.Cookies.Add(promotionCookie);
			}
		}

		public string GetPromotionalHomePageCopy()
		{
			string promotionCopy = "";

			// temporary hack - return nothing until we want to activate this functionality
			return "";

			if (Request.Cookies["PromotionCookie"] != null)
			{
				
				string promotionID = Request.Cookies["PromotionCookie"].Value.ToString();

				if (promotionID != null)
				{
					Promotion promotion = PromotionDAL.Instance.Get((string)promotionID);

					if (promotion != null)
					{
						if (promotion.HomePageCopy.Length > 0)
						{
							promotionCopy = promotion.HomePageCopy;
						}
						else
						{
							promotionCopy = "";
						}
					}
					else
					{
						promotionCopy = "";
					}
				}

			}
			else
			{
				promotionCopy = "";
			}

			return promotionCopy ;
		}

		public string GetPromotionalBrowseDestinationsCopy()
		{
			string promotionCopy = "";

			// temporary hack - return nothing until we want to activate this functionality
			return "";

			if (Request.Cookies["PromotionCookie"] != null)
			{
				
				string promotionID = Request.Cookies["PromotionCookie"].Value.ToString();

				if (promotionID != null)
				{
					Promotion promotion = PromotionDAL.Instance.Get((string)promotionID);

					if (promotion != null)
					{
						if (promotion.BrowseDestinationsCopy.Length > 0)
						{
							promotionCopy = promotion.BrowseDestinationsCopy;
						}
						else
						{
							promotionCopy = "";
						}
					}
					else
					{
						promotionCopy = "";
					}
				}

			}
			else
			{
				promotionCopy = "";
			}

			return promotionCopy;
		}

		public string GetPromotionalViewHomeCopy()
		{
			string promotionCopy = "";

			// temporary hack - return nothing until we want to activate this functionality
			return "";

			if (Request.Cookies["PromotionCookie"] != null)
			{
				
				string promotionID = Request.Cookies["PromotionCookie"].Value.ToString();

				if (promotionID != null)
				{
					Promotion promotion = PromotionDAL.Instance.Get((string)promotionID);

					if (promotion != null)
					{
						if (promotion.ViewHomeCopy.Length > 0)
						{
							promotionCopy = promotion.ViewHomeCopy;
						}
						else
						{
							promotionCopy = "";
						}
					}
					else
					{
						promotionCopy = "";
					}
				}

			}
			else
			{
				promotionCopy = "";
			}

			return promotionCopy;
		}

		//public string GetDefaultHomePagePromotionCopy()
		//{
			//return "<font color=\"red\">SPECIAL OFFER - <a href=\"https://www.christianhomeswap.com/member/register.aspx\"><font color=\"red\">join TODAY for a free TWO YEAR membership</font></a>! Hurry, offer expires soon!</font>";
		//}

		//public string GetDefaultBrowseDestinationsPromotionCopy()
		//{
			//return ConfigurationSettings.AppSettings["DefaultPromotionText"];
			//return ConfigurationSettings.AppSettings["DefaultPromotionText"];
		//	return "<font color=\"red\">SPECIAL LAUNCH OFFER - Join our friendly Christian holiday home swap community today and receive a complimentary two year membership! This offer will expire soon - </font><a href=\"/member/register.aspx\"><font color=\"red\">join TODAY for free!</font></a>";
		//}

		//public string GetDefaultViewHomePromotionCopy()
		//{
			//return ConfigurationSettings.AppSettings["DefaultPromotionText"];
			//return ConfigurationSettings.AppSettings["DefaultPromotionText"];
		//	return "<font color=\"red\">SPECIAL OFFER - <a href=\"https://www.christianhomeswap.com/member/register.aspx\"><font color=\"red\">Join TODAY for a free TWO YEAR membership</font></a>! Hurry, offer expires soon!</font>";
		//}

		#endregion


		#region Referrer handling

		// Saves referrer ID (from default 'rid' query string value) to a cookie
		public void CheckForReferrer()
		{
			if (Request.QueryString["rid"] != null)
			{
				SaveReferrerToCookie(Request.QueryString["rid"]);
			}
		}

		// Saves passed in referrer ID to a cookie
		public void SaveReferrerToCookie(string referrerID)
		{
			// Save to referrer cookie
			if (referrerID != null)
			{
				HttpCookie referrerCookie = new HttpCookie("ReferrerCookie", referrerID);
				referrerCookie.Expires = DateTime.Now.AddMonths(1);
				Response.Cookies.Add(referrerCookie);
			}
		}

		// Saves passed in referrer ID to a cookie
		public void SaveReferrerToCookie(string referrerID, int cookieDuration)
		{
			// Save to referrer cookie
			if (referrerID != null)
			{
				HttpCookie referrerCookie = new HttpCookie("ReferrerCookie", referrerID);
				referrerCookie.Expires = DateTime.Now.AddMonths(cookieDuration);
				Response.Cookies.Add(referrerCookie);
			}
		}

		// Get referrerID from cookie using default referrer cookie name
		public string GetReferrerFromCookie()
		{
			if (Request.Cookies["ReferrerCookie"] != null)
			{
				return Request.Cookies["ReferrerCookie"].Value.ToString();
			}

			return "-1";
		}

		// Get referrerID from cookie using specified referrer cookie name
		public string GetReferrerFromCookie(string cookieName)
		{
			if (Request.Cookies[cookieName] != null)
			{
				return Request.Cookies[cookieName].Value.ToString();
			}

			return "-1";
		}

		#endregion referrer handling

		/// <summary>
		/// Retrieves lightweight member information from session (if available)
		/// </summary>
		/// <returns></returns>
		public MemberSession GetMemberFromSession()
		{
			if (((MemberSession)Session["membersession"]) != null)
			{
				return (MemberSession)((MemberSession)Session["membersession"]);
			}
			else
			{

				String guidParser;
				if ((ViewState["sid"] != null) && (ViewState["sid"].ToString().Length > 0))
				{
					guidParser = ViewState["sid"].ToString();

					int delimiterPos = guidParser.IndexOf("hqw",0,30);

					int memberID = Int32.Parse(guidParser.Substring(0, delimiterPos));

					string guid = guidParser.Substring(delimiterPos+4);

					Member member = MemberDAL.Instance.GetByGuid(memberID, guid);

					_log.Info(String.Format("Session timeout. Attempting to retrieve guid {0}", guidParser));

					if (member != null)
					{
						// Repopulate session based on authenticated details
						MemberSession memberSession = new MemberSession();
						memberSession.MemberID = member.ID;
						memberSession.HomeID = member.HomeID;
						memberSession.MemberStatus = member.Status;
						
						Session["membersession"] = memberSession;
						
						return (memberSession);
					}
				}
				
				//TODO: Note: Secure redirect has temporarily been removed
				string returnURL = Request.FilePath;
				Response.Redirect(SecureRedirect(String.Format("/member/signin.aspx?e=100&return={0}", HttpUtility.UrlEncode(returnURL))), true);

			}

			return null;
		}

		public int GetMemberIDFromSession()
		{
			if (((MemberSession)Session["membersession"]) != null)
			{
				return ((MemberSession)Session["membersession"]).MemberID;
			}
			else
			{
				string returnURL = String.Format("{0}?homeid={1}", Request.FilePath, Request.QueryString["homeID"]);
				Response.Redirect(SecureRedirect(String.Format("/member/signin.aspx?return={0}", HttpUtility.UrlEncode(returnURL))), true);

				//Response.Redirect("/member/signin.aspx?e=100", true);
			}

			return -1;
		}

		public int GetMemberIDFromSession(bool redirectOnTimeout)
		{
			if (((MemberSession)Session["membersession"]) != null)
			{
				return ((MemberSession)Session["membersession"]).MemberID;
			}
			else
			{
				if (redirectOnTimeout)
				{
					string returnURL = String.Format("{0}?homeid={1}", Request.FilePath, Request.QueryString["homeID"]);
					Response.Redirect(SecureRedirect(String.Format("/member/signin.aspx?return={0}", HttpUtility.UrlEncode(returnURL))), true);
				}
			}

			return -1;

		}

		/// <summary>
		/// Retrieves lightweight member information from session (if available)
		/// </summary>
		/// <returns></returns>
		public void SetMemberSession(Member member)
		{
			if (member != null)
			{
				// Repopulate session based on authenticated details
				MemberSession memberSession = new MemberSession();
				
				memberSession.FirstName = member.FirstName;
				memberSession.GivenName = member.GivenName;
				memberSession.MemberID = member.ID;
				memberSession.HomeID = member.HomeID;
				memberSession.MemberStatus = member.Status;
				
				Session["membersession"] = memberSession;
			}
		}

		/// <summary>
		/// Checks if the user requires authentication on the current page. If they do, redirect them to the sign-in page
		/// </summary>
		public void VerifyAuthentication()
		{
			string rawURL = Request.RawUrl;

			// Check if they're on a page (or in an area) that requires authentication
			if ((rawURL.IndexOf("/member/") >= 0) && ((rawURL.IndexOf("/member/register.aspx") == -1) || (rawURL.IndexOf("/member/signin.aspx") == -1)) || (rawURL.IndexOf("/home/reviews.aspx") >= 0) || (rawURL.IndexOf("/home/enquiry.aspx") >= 0))
			{
				// Check they've got authentication to access the member directory, as they're not registering
				if (((MemberSession)Session["membersession"]) == null)
				{
					// they haven't authenticated, so redirect them to the sign-in page
					//Response.Redirect("/member/signin.aspx", true);
					// Response.Redirect(String.Format("/member/signin.aspx?return={0}", HttpUtility.UrlEncode(rawURL)), true);
					string returnURL = Request.FilePath;
					Response.Redirect(SecureRedirect(String.Format("/member/signin.aspx?e=100&return={0}", HttpUtility.UrlEncode(returnURL))), true);
					
				}
			}
		}

		public void ValidateField(string controlName, FieldType fieldType, FieldType selectionType, string errorMessage)
		{
			ValidateField(controlName, null, fieldType, selectionType, -1, -1, errorMessage);
		}

		public void ValidateField(string controlName, FieldType fieldType, FieldType selectionType)
		{
			ValidateField(controlName, null, fieldType, selectionType, -1, -1, "");
		}

		public void ValidateField(string controlName, string errorControlName, FieldType fieldType, FieldType selectionType)
		{
			ValidateField(controlName, errorControlName, fieldType, selectionType, -1, -1, "");
		}

		public void ValidateField(string controlName, FieldType fieldType, FieldType selectionType, int minSize, int maxSize)
		{
			ValidateField(controlName, null, fieldType, selectionType, minSize, maxSize, "");
		}


		/// <summary>
		/// Applies validation to specified object
		/// </summary>
		/// <param name="controlName">Name of object to validate</param>
		/// <param name="fieldType">Enum value indicating field type to validate</param>
		/// <param name="minSize">Minimum size of field</param>
		/// <param name="maxSize">Maximum size of field</param>
		public void ValidateField(string controlName, string errorControlName, FieldType fieldType, FieldType selectionType, int minSize, int maxSize, string errorMessage)
		{
			bool isValid = true;
			string text = String.Empty;

			Control control = Page.FindControl(controlName);
			if (control is TextBox)
			{
				text = ((TextBox)control).Text;
			}
			else if (control is DropDownList)
			{
				text = ((DropDownList)control).SelectedValue;
			}

			Label errorText;

			if (errorControlName == null)
			{
				// Look for default error control name
				errorText = (Label)Page.FindControl("invalid" + controlName);
			}
			else
			{
				errorText = (Label)Page.FindControl(errorControlName);
			}
			errorText.Visible = false;

			// Reset field style back to default setting
			if (fieldType != FieldType.Checkbox)
			{
				ResetField(control);
			}

			switch(fieldType)
			{
				case FieldType.AlphaNumeric:
					// Valid alphanumeric field
					// Validate field lengths (when appropriate)
					if ((selectionType == FieldType.Compulsory) && (text.Length == 0) && (minSize < 0) && (maxSize < 0))
					{
						// field length is less than minimum size or greater than maximum size
						errorText.Text = errorText.Text.Replace("%%errormessage%%", "This field must be completed");
						isValid = !isValid;
					}
					else if (!ValidFieldLength(text, minSize, maxSize))
					{
						// field length is less than minimum size or greater than maximum size
						errorText.Text = errorText.Text.Replace("%%errormessage%%", String.Format("Must be between {0} and {1} characters", minSize.ToString(), maxSize.ToString()));
						isValid = !isValid;
					}
					
					break;
			
				case FieldType.Numeric:
					// check field is numeric and within valid ranges
					if ((!Regex.IsMatch(text, @"^\d+$") || (Int32.Parse(text) < minSize) || ((Int32.Parse(text) > maxSize))))
					{
						// not valid
						errorText.Text = errorText.Text.Replace("%%errormessage%%", String.Format("Must be numeric and between {0} and {1}", minSize.ToString(), maxSize.ToString()));
						isValid = !isValid;
					}

					break;

				case FieldType.Email:
					// Validate email address
					if ((selectionType == FieldType.Optional) && (text.Length == 0))
					{
						// Field is optional and empty so let them through without validating anything
						//isValid = isValid;
						errorText.Text = "";
					}
					else
					{
						Regex emailregex = new Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
						Match m = emailregex.Match(text);

						if (!m.Success)
						{
							isValid = !isValid;
							errorText.Text = errorText.Text.Replace("%%errormessage%%", "Please enter a valid email address");
						}
						else
						{
							errorText.Text = errorText.Text.Replace("Please enter a valid email address", "%%errormessage%%");
						}
					}

					break;

				case FieldType.Password:
					if (!ValidFieldLength(text, minSize, maxSize))
					{
						isValid = !isValid;
						errorText.Text = errorText.Text.Replace("%%errormessage%%", "Password must be a minimum of six characters");
					}
					break;

				case FieldType.Checkbox:
					// check field is numeric and within valid ranges
					if ((selectionType == FieldType.Compulsory))
					{
						CheckBox checkbox = (CheckBox)Page.FindControl(controlName);
						if (!checkbox.Checked)
						{
							// not valid
							if (errorMessage.Length > 0)
								errorText.Text = errorText.Text.Replace("%%errormessage%%", errorMessage);
							else
								errorText.Text = errorText.Text.Replace("%%errormessage%%", String.Format("Members must accept the conditions of use before continuing"));

							isValid = !isValid;
						}
					}

					break;
			}

			if (!isValid)
			{
				if (fieldType != FieldType.Checkbox)
				{
					HighlightField(control);
				}
				_passedValidation = false;
				errorText.Visible = true;
			}
		}

		public void CreateCustomError(string controlName, string errorMessage)
		{
			TextBox sourceBox = (TextBox)Page.FindControl(controlName);
			Label errorText = (Label)Page.FindControl("invalid" + controlName);
			
			HighlightField(sourceBox);
			_passedValidation = false;
			
			errorText.Visible = true;
			errorText.Text = errorText.Text.Replace("%%errormessage%%", errorMessage);
		}

		public void DisplayError(string errorControlName, string errorMessage)
		{
			Label errorText = (Label)Page.FindControl(errorControlName);
			
			_passedValidation = false;
			
			errorText.Visible = true;
			errorText.Text = errorText.Text.Replace("%%errormessage%%", errorMessage);
		}

		public void ResetError(string errorControlName, string errorMessage)
		{
			Label errorText = (Label)Page.FindControl(errorControlName);
			errorText.Visible = false;
			errorText.Text = errorText.Text.Replace(errorMessage, "%%errormessage%%");
		}

		/// <summary>
		/// Validates length of field against minimum and maximum values
		/// </summary>
		/// <param name="sourceField"></param>
		/// <param name="minLength"></param>
		/// <param name="maxLength"></param>
		/// <returns></returns>
		public bool ValidFieldLength(string sourceField, int minSize, int maxSize)
		{
			if ((minSize > 0) || (maxSize > 0))
			{
				if ((sourceField.Length < minSize) || (sourceField.Length > maxSize))
				{
					return false;
				}
			}
			
			return true;
		}

		public bool CheckPasswordsMatch(string firstPasswordControl, string secondPasswordControl)
		{
			bool isValid = true;

			// check passwords match
			TextBox firstPassword = (TextBox)Page.FindControl(firstPasswordControl);
			TextBox secondPassword = (TextBox)Page.FindControl(secondPasswordControl);

			// Reset password styles back to default setting
			ResetField(firstPassword);
			ResetField(secondPassword);

			// Verify that the passwords match correctly, if not then handle error
			if ((firstPassword.Text) != (secondPassword.Text))
			{
				// Passwords do not match
				isValid = !IsValid;
				HighlightField(firstPassword);
				HighlightField(secondPassword);
				_passedValidation = false;

				// Get first error label and populate
				Label errorText = (Label)Page.FindControl("invalid" + firstPasswordControl);
				errorText.Text = errorText.Text.Replace("%%errormessage%%", "Your passwords must match");
				errorText.Visible = true;
			}

			return isValid;
		}

		/// <summary>
		/// Highlights the background colour for the specified textbox (e.g. when highlighting an error)
		/// </summary>
		/// <param name="control">The control (probably a text box)</param>
		private void HighlightField(Control control)
		{
			if (control is TextBox)
			{
				// Highlight background of specified field
				((TextBox)control).CssClass = "highlightfield";
			}
		}

		/// <summary>
		/// Resets the background colour for the specified textbox
		/// </summary>
		/// <param name="control">The control (probably a textbox)</param>
		private void ResetField(Control control)
		{
			if (control is TextBox)
			{
				// reset background of specified field to original setting
				((TextBox)control).CssClass = "resetfield";
			}
		}

		/// <summary>
		/// Indicates if the validated fields have passed the validation checks
		/// </summary>
		/// <returns></returns>
		public bool PassedFormValidation()
		{
			if (!_passedValidation)
			{
				throw new PageValidationException();
			}

			return true;
		}

		public bool PassedValidation
		{
			get
			{
				return _passedValidation;
			}
			set
			{
				_passedValidation = value;
			}
		}

       

		public Channel Channel
		{
			get
			{
				if (_channel == null)
				{
					if (ConfigurationSettings.AppSettings["ChannelID"].Length > 0)
					{
						_channel = ChannelDAL.Instance.Get(Int32.Parse(ConfigurationSettings.AppSettings["ChannelID"]));
					}
					else
					{
						_channel = ChannelDAL.Instance.Get(GetWebsiteName());
					}
					
				}
				
				return _channel;
			}
		}

		public log4net.ILog Logger
		{
			get
			{
				return _log;
			}
		}


		private string _websiteName;
		private string _websiteSecureName;

		private string _exchangeName = "";
		private bool _passedValidation = true;
		private Channel _channel;
		private log4net.ILog _log = log4net.LogManager.GetLogger(typeof(PageManager));
	}
}
