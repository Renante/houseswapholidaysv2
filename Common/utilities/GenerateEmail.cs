using System.Diagnostics;
using System.IO;
using System.Net.Mail;
using System.Configuration;
using System;
using System.Reflection;
using System.Text;
using System.Web;

namespace Common
{
	/// <summary>
	/// Summary description for GenerateEmail.
	/// </summary>
	public class GenerateEmail
	{
		public GenerateEmail()
		{
		}

        public void GenerateWelcomeEmail(int channelID, Member member)
        {
            Channel channel = ChannelDAL.Instance.Get(channelID);
            GenerateWelcomeEmail(channel, member);
        }

		public void GenerateWelcomeEmail(Channel channel, Member member)
		{
            try
			{
				// Retrieve welcome email layout from blog
				Blog blog = BlogDAL.Instance.GetByType(channel.ID, (int)EmailType.Welcome);
				string websiteDisplayName = "";

				
				if (channel != null)
				{
					websiteDisplayName = channel.Name;
				}
				else
				{
                    websiteDisplayName = "HouseSwapHolidays.com.au";
				}

				// Format for html
				blog.Body = BlogDAL.Instance.FormatForHTML(blog.Body);

				// Replace key fields with actual values (firstname, givenname etc)
				blog.Body = blog.Body.Replace("%%FirstName%%",member.FirstName);
				blog.Body = blog.Body.Replace("%%GivenName%%",member.GivenName);
				blog.Body = blog.Body.Replace("%%UserName%%",member.EmailAddress);
				blog.Body = blog.Body.Replace("%%Password%%",member.Password);

				// TODO: Email details are currently hardcoded
				if (ConfigurationSettings.AppSettings["GenerateEmails"].ToUpper() != "FALSE")
				{
					//SendEmail(String.Format("{0} {1}", member.FirstName, member.GivenName), member.EmailAddress, websiteDisplayName, String.Format("info@{0}", websiteDisplayName.ToLower()), String.Format("Welcome to {0}!", websiteDisplayName), blog.Body, channel.ID);
				}
			}
			catch (Exception exc)
			{
				_log.Error("Error sending welcome email", exc);
				throw;
			}
		}

		public void GenerateContactUsMessage(int channelID, string subject, string name, string emailAddress, string message)
		{
            try
			{
				string websiteDisplayName = "";

				Channel channel = ChannelDAL.Instance.Get(channelID);
				if (channel != null)
				{
					websiteDisplayName = channel.Name;
				}
				else
				{
                    websiteDisplayName = "HouseSwapHolidays.com.au";
				}

				string toName = String.Format("{0} Support", websiteDisplayName);
				string toEmail = String.Format("info@{0}", websiteDisplayName.ToLower()); 

				message = message.Replace("\n","<br>");

				if (ConfigurationSettings.AppSettings["GenerateEmails"].ToUpper() != "FALSE")
				{
					_log.Info(String.Format("To : {0} ({1}) From : {2} ({3}) Subject : {4} Message : {5}", String.Format("{0} support", websiteDisplayName), "daveutting@gmail.com", name, emailAddress, subject, message));
					SendEmail(String.Format("{0} support", websiteDisplayName), toEmail, name, emailAddress, subject, message, channelID);
				}
			}
			catch (Exception exc)
			{
				_log.Error("Error sending support email", exc);
				_log.Error(String.Format("Error sending support enquiry from {0} - {1}", name, emailAddress));
				throw;
			}
		}

		public void GenerateHomeSuccessfullyListedEmail(int channelID, int homeID, Member member, string expiryDate)
		{
            try
			{
				// Retrieve welcome email layout from blog
				Blog blog = BlogDAL.Instance.GetByType(channelID, (int)EmailType.HomeSuccessfullyListed);
				string websiteDisplayName = "";

				Channel channel = ChannelDAL.Instance.Get(channelID);
				if (channel != null)
				{
					websiteDisplayName = channel.Name;
				}
				else
				{
                    websiteDisplayName = "HouseSwapHolidays.com.au";
				}

				string homeURL = String.Format("http://www.{0}/home/view.aspx?homeid={1}",channel.ShortURL, homeID.ToString());
				string memberURL = String.Format("http://www.{0}/member/details.aspx",channel.ShortURL);
				
				// Format for html
				blog.Body = BlogDAL.Instance.FormatForHTML(blog.Body);

				// Replace key fields with actual values (firstname, givenname etc)
				blog.Body = blog.Body.Replace("%%FirstName%%", member.FirstName);
				blog.Body = blog.Body.Replace("%%GivenName%%", member.GivenName);
				blog.Body = blog.Body.Replace("%%HomeURL%%", homeURL);
				blog.Body = blog.Body.Replace("%%ExpiryDate%%", expiryDate);
				blog.Body = blog.Body.Replace("%%MemberURL%%", memberURL);
				blog.Body = blog.Body.Replace("%%FromEmail%%", String.Format("info@{0}", channel.ShortURL.ToLower()));
				blog.Body = blog.Body.Replace("%%WebsiteName%%", channel.Name);

				blog.Body = blog.Body.Replace("\n","<br>");

				// TODO: Email details are currently hardcoded
				if (ConfigurationSettings.AppSettings["GenerateEmails"].ToUpper() != "FALSE")
				{
					SendEmail(String.Format("{0} {1}", member.FirstName, member.GivenName), member.EmailAddress, websiteDisplayName, String.Format("info@{0}", websiteDisplayName.ToLower()), String.Format("Your home details on {0}!", websiteDisplayName), blog.Body, channelID);
				}
			}
			catch (Exception exc)
			{
				_log.Error("Error sending home listing email", exc);
				throw;
			}
		}

		public void GenerateHomeApprovedEmail(int channelID, int homeID, Member member, string expiryDate)
		{
            try
			{
				// Retrieve welcome email layout from blog
				Blog blog = BlogDAL.Instance.GetByType(channelID, (int)EmailType.ApprovalGranted);
				string websiteDisplayName = "";

				Channel channel = ChannelDAL.Instance.Get(channelID);
				if (channel != null)
				{
					websiteDisplayName = channel.Name;
				}
				else
				{
                    websiteDisplayName = "HouseSwapHolidays.com.au";
				}

				string homeURL = String.Format("http://www.{0}/home/view.aspx?homeid={1}",channel.ShortURL, homeID.ToString());
				string memberURL = String.Format("http://www.{0}/member/details.aspx",channel.ShortURL);
				
				// Format for html
				blog.Body = BlogDAL.Instance.FormatForHTML(blog.Body);

				// Replace key fields with actual values (firstname, givenname etc)
				blog.Body = blog.Body.Replace("%%FirstName%%", member.FirstName);
				blog.Body = blog.Body.Replace("%%GivenName%%", member.GivenName);
				blog.Body = blog.Body.Replace("%%HomeURL%%", homeURL);
				blog.Body = blog.Body.Replace("%%ExpiryDate%%", expiryDate);
				blog.Body = blog.Body.Replace("%%MemberURL%%", memberURL);
				blog.Body = blog.Body.Replace("%%FromEmail%%", String.Format("info@{0}", channel.ShortURL.ToLower()));
				blog.Body = blog.Body.Replace("%%WebsiteName%%", channel.Name);

				blog.Body = blog.Body.Replace("\n","<br>");

				// TODO: Email details are currently hardcoded
				if (ConfigurationSettings.AppSettings["GenerateEmails"].ToUpper() != "FALSE")
				{
					SendEmail(String.Format("{0} {1}", member.FirstName, member.GivenName), member.EmailAddress, websiteDisplayName, String.Format("info@{0}", websiteDisplayName.ToLower()), String.Format("Your home details on {0}!", websiteDisplayName), blog.Body, channelID);
				}
			}
			catch (Exception exc)
			{
				_log.Error("Error sending home listing email", exc);
				throw;
			}
		}

		/// <summary>
		/// Generates a generic email to the specified member
		/// </summary>
		/// <param name="channelID">Channel identifier to current site</param>
		/// <param name="emailType">Specifies type of email (from blog) to be sent</param>
		/// <param name="member">Member details - if available</param>
		/// <param name="homeID">Home identifier - if available</param>
		/// <param name="expiryDate">Expiry date - if available</param>
		/// <param name="subject">Subject line of email</param>
		public void GenerateGenericEmail(int channelID, EmailType emailType, Member member, int homeID, string expiryDate, string subject)
		{
            try
			{
				// Retrieve welcome email layout from blog
				Blog blog = BlogDAL.Instance.GetByType(channelID, (int)emailType);
				string websiteDisplayName = "";

				Channel channel = ChannelDAL.Instance.Get(channelID);
				if (channel != null)
				{
					websiteDisplayName = channel.Name;
				}
				else
				{
                    websiteDisplayName = "HouseSwapHolidays.com.au";
				}

				string homeURL = String.Format("http://www.{0}/home/view.aspx?homeid={1}",channel.ShortURL, homeID.ToString());
				string memberURL = String.Format("http://www.{0}/member/details.aspx",channel.ShortURL);
				
				// Format for html
				blog.Body = BlogDAL.Instance.FormatForHTML(blog.Body);

				// Replace key fields with actual values (firstname, givenname etc)
				blog.Body = blog.Body.Replace("%%FirstName%%", member.FirstName);
				blog.Body = blog.Body.Replace("%%GivenName%%", member.GivenName);
				blog.Body = blog.Body.Replace("%%HomeURL%%", homeURL);
				blog.Body = blog.Body.Replace("%%ExpiryDate%%", expiryDate);
				blog.Body = blog.Body.Replace("%%MemberURL%%", memberURL);
				blog.Body = blog.Body.Replace("%%FromEmail%%", String.Format("info@{0}", channel.ShortURL.ToLower()));
				blog.Body = blog.Body.Replace("%%WebsiteName%%", channel.Name);

				blog.Body = blog.Body.Replace("\n","<br>");

				// TODO: Email details are currently hardcoded
				if (ConfigurationSettings.AppSettings["GenerateEmails"].ToUpper() != "FALSE")
				{
					SendEmail(String.Format("{0} {1}", member.FirstName, member.GivenName), member.EmailAddress, websiteDisplayName, String.Format("info@{0}", websiteDisplayName.ToLower()), String.Format("Your home is now registered on {0}!", websiteDisplayName), blog.Body, channelID);
				}
			}
			catch (Exception exc)
			{
				_log.Error("Error sending generic email", exc);
				_log.Error("emailType is " + emailType.ToString());
				throw;
			}
		}

		/// <summary>
		/// Email generated for when a home has just been listed but is pending approval
		/// </summary>
		/// <param name="channelID"></param>
		/// <param name="homeID"></param>
		/// <param name="member"></param>
		/// <param name="expiryDate"></param>
		public void GeneratePendingApprovalEmail(int channelID, int homeID, Member member, string expiryDate)
		{
            try
			{
				// Retrieve welcome email layout from blog
				Blog blog = BlogDAL.Instance.GetByType(channelID, (int)EmailType.PendingApproval);
				string websiteDisplayName = "";

				Channel channel = ChannelDAL.Instance.Get(channelID);
				if (channel != null)
				{
					websiteDisplayName = channel.Name;
				}
				else
				{
                    websiteDisplayName = "HouseSwapHolidays.com.au";
				}

				string homeURL = String.Format("http://www.{0}/home/view.aspx?homeid={1}",channel.ShortURL, homeID.ToString());
				string memberURL = String.Format("http://www.{0}/member/details.aspx",channel.ShortURL);
				
				// Format for html
				blog.Body = BlogDAL.Instance.FormatForHTML(blog.Body);

				// Replace key fields with actual values (firstname, givenname etc)
				blog.Body = blog.Body.Replace("%%FirstName%%", member.FirstName);
				blog.Body = blog.Body.Replace("%%GivenName%%", member.GivenName);
				blog.Body = blog.Body.Replace("%%HomeURL%%", homeURL);
				blog.Body = blog.Body.Replace("%%ExpiryDate%%", expiryDate);
				blog.Body = blog.Body.Replace("%%MemberURL%%", memberURL);
				blog.Body = blog.Body.Replace("%%FromEmail%%", String.Format("info@{0}", channel.ShortURL.ToLower()));
				blog.Body = blog.Body.Replace("%%WebsiteName%%", channel.Name);

				blog.Body = blog.Body.Replace("\n","<br>");

				// TODO: Email details are currently hardcoded
				if (ConfigurationSettings.AppSettings["GenerateEmails"].ToUpper() != "FALSE")
				{
					SendEmail(String.Format("{0} {1}", member.FirstName, member.GivenName), member.EmailAddress, websiteDisplayName, String.Format("info@{0}", websiteDisplayName.ToLower()), String.Format("Your home is now registered on {0}", websiteDisplayName), blog.Body, channelID);
				}
			}
			catch (Exception exc)
			{
				_log.Error("Error sending home listing email", exc);
				throw;
			}
		}

		public void GenerateHomeGenericNotification(int channelID, HomeEnquiry homeEnquiry, string countryName, string messageLink)
		{
            
			// Retrieve welcome email layout from blog
			Blog blog = BlogDAL.Instance.GetByType(channelID, (int)EmailType.GenericNotification);
			
			// Get destination email details (firstname & email address)
			//Member memberRecipient = MemberDAL.Instance.GetMemberFromHome(homeEnquiry.HomeID);

			//Member memberSender = MemberDAL.Instance.Get(homeEnquiry.RecipientMemberID);

			
			Member memberRecipient = MemberDAL.Instance.Get(homeEnquiry.RecipientMemberID);

			// Get the channel of the recipient as that determines which 'sender' email we should use
			int recipientChannelID = memberRecipient.DefaultChannel;

			// Format for html
			blog.Body = BlogDAL.Instance.FormatForHTML(blog.Body);

			// Replace key fields with actual values (firstname, givenname etc)
			
			try
			{
				blog.Body = blog.Body.Replace("%%FirstName%%",memberRecipient.FirstName);
			}
			catch
			{
				throw new InvalidBlogContentException();
			}

			try
			{
				blog.Body = blog.Body.Replace("%%SenderFirstname%%", homeEnquiry.FirstName);
			}
			catch
			{
				throw new InvalidBlogContentException();
			}

			try
			{
				blog.Body = blog.Body.Replace("%%SenderCountry%%", countryName);
			}
			catch
			{
				throw new InvalidBlogContentException();
			}

			try
			{
				blog.Body = blog.Body.Replace("%%MessageLink%%", messageLink);
			}
			catch
			{
				throw new InvalidBlogContentException();
			}

			/*try
			{
				blog.Body = blog.Body.Replace("%%Message%%", homeEnquiry.Message);
			}
			catch
			{
				throw new InvalidBlogContentException();
			}*/

			// TODO: Email details are currently hardcoded
			if (ConfigurationSettings.AppSettings["GenerateEmails"].ToUpper() != "FALSE")
			{
				Channel channel = ChannelDAL.Instance.Get(recipientChannelID);
				string senderName;

				string siteName;
				if (channel != null)
					siteName = channel.Name;
				else
                    siteName = "HouseSwapHolidays.com.au";

				// Remove any domain extensions for sender email
				siteName = siteName.Replace(".com.au","");
				siteName = siteName.Replace(".com","");

				senderName = siteName + " Enquiries";

				// TODO : temporary override hack for homeexchange.com.au members as we only want to show basic channel name as sender
				if (channel.ID == 2)
				{
					senderName = channel.Name;
				}

				// TODO: Check this is from the correct email address
				SendEmail(memberRecipient.FirstName, memberRecipient.EmailAddress, senderName , String.Format("info@{0}", channel.ShortURL.ToLower()), homeEnquiry.Subject, blog.Body, recipientChannelID);
			}
		}

		public void GenerateHomeExchangeEnquiry(int channelID, HomeEnquiry homeEnquiry)
		{
            // Retrieve welcome email layout from blog
			Blog blog = BlogDAL.Instance.GetByType(channelID, (int)EmailType.ExchangeEnquiry);
			
			// Get destination email details (firstname & email address)
			Member member = MemberDAL.Instance.GetMemberFromHome(homeEnquiry.HomeID);


			// Format for html
			blog.Body = BlogDAL.Instance.FormatForHTML(blog.Body);

			// Replace key fields with actual values (firstname, givenname etc)
			
			try
			{
				blog.Body = blog.Body.Replace("%%FirstName%%",member.FirstName);
			}
			catch
			{
				throw new InvalidBlogContentException();
			}

			try
			{
				blog.Body = blog.Body.Replace("%%Message%%", homeEnquiry.Message);
			}
			catch
			{
				throw new InvalidBlogContentException();
			}

			try
			{
				blog.Body = blog.Body.Replace("%%GivenName%%",member.GivenName);
			}
			catch
			{
				// Do nothing as Given name is optional				
			}

			// TODO: Email details are currently hardcoded
			if (ConfigurationSettings.AppSettings["GenerateEmails"].ToUpper() != "FALSE")
			{
                SendEmail(member.FirstName, member.EmailAddress, "HouseSwapHolidays Enquiries", homeEnquiry.Email, "HouseSwapHolidays.com.au - Home exchange enquiry", blog.Body, channelID);
			}
		}

        public void GeneratePasswordResetLink(int channelID, Member member, string resetKey)
        {
            // Retrieve welcome email layout from blog
            Blog blog = BlogDAL.Instance.GetByType(channelID, (int)EmailType.PasswordReset);

            blog.Body = BlogDAL.Instance.FormatForHTML(blog.Body);

            // Replace key fields with actual values (firstname, givenname etc)
            blog.Body = blog.Body.Replace("%%FirstName%%", member.FirstName);
            
            string websiteDisplayName;
            Channel channel = ChannelDAL.Instance.Get(channelID);
            if (channel != null)
            {
                websiteDisplayName = channel.Name;
            }
            else
            {
                websiteDisplayName = "YourHomeForMine.com";
            }
            
            
            string fromName = String.Format("{0} Support", websiteDisplayName);
            string fromEmail = String.Format("info@{0}", websiteDisplayName.ToLower());

            string resetlink = string.Format("<a href='http://{0}/reset-password/?key={1}' target='_blank'>http://{0}/reset-password/?key={1}</a>",
                websiteDisplayName, HttpUtility.UrlEncode(resetKey));

            blog.Body = blog.Body.Replace("%%passwordresetlink%%", resetlink);
            
            
            // TODO: Email details are currently hardcoded
            if (ConfigurationSettings.AppSettings["GenerateEmails"] != null && ConfigurationSettings.AppSettings["GenerateEmails"].ToUpper() != "FALSE")
            {
                _log.Info("Generate emails: true");
                if (ConfigurationSettings.AppSettings["test"] != null && ConfigurationSettings.AppSettings["test"].ToUpper() == "TRUE")
                {
                    var client = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587)
                    {
                        Credentials = new System.Net.NetworkCredential("renanteabril@gmail.com", "Srcp2011!"),
                        EnableSsl = true
                        
                    };

                    MailMessage eMail = new MailMessage("haunter_004@yahoo.com", "renanteabril@yahoo.com");
                    eMail.Subject = "Password Reset Link";
                    eMail.Body = blog.Body;
                    eMail.IsBodyHtml = true;
                    client.Send(eMail);
                    _log.Info("Test email has been used");
                }
                else
                { 
                    SendEmail(member.FirstName, member.EmailAddress, fromName, fromEmail, "Password Reset Link", blog.Body, channelID);
                    _log.Info("reset link is : " + resetlink);
                    _log.Info("Prod email has been used");
                }
            }
            else { 
            _log.Info("Generate emails: false");
            }

        }

		public void GeneratePasswordReminder(int channelID, Member member)
		{
            // Retrieve welcome email layout from blog
			Blog blog = BlogDAL.Instance.GetByType(channelID, (int)EmailType.PasswordReminder);
			
			if (blog == null)
			{
				_log.Error(String.Format("Unable to retrieve password reminder ({0}) for channel {1} ", EmailType.PasswordReminder.ToString(), channelID.ToString()));
			}

			// Format for html
			blog.Body = BlogDAL.Instance.FormatForHTML(blog.Body);

			// Replace key fields with actual values (firstname, givenname etc)
			blog.Body = blog.Body.Replace("%%FirstName%%",member.FirstName);
			blog.Body = blog.Body.Replace("%%GivenName%%",member.GivenName);
			blog.Body = blog.Body.Replace("%%UserName%%",member.EmailAddress);
			blog.Body = blog.Body.Replace("%%Password%%",member.Password);

			string websiteDisplayName;
			Channel channel = ChannelDAL.Instance.Get(channelID);
			if (channel != null)
			{
				websiteDisplayName = channel.Name;
			}
			else
			{
				websiteDisplayName = "YourHomeForMine.com";
			}

			string fromName = String.Format("{0} Support", websiteDisplayName);
			string fromEmail = String.Format("info@{0}", websiteDisplayName.ToLower()); 


			// TODO: Email details are currently hardcoded
			if (ConfigurationSettings.AppSettings["GenerateEmails"].ToUpper() != "FALSE")
			{
				SendEmail(member.FirstName, member.EmailAddress, fromName, fromEmail, "Password reminder request", blog.Body, channelID);
			}
		}

		public void GenerateReferenceRequest(int channelID, string senderName, string senderEmail, string senderComment, string recipientFirstName, string recipientLastName,string recipientEmail, string destinationURL, string emailContent)
		{
            // Retrieve welcome email layout from blog
			//Blog blog = BlogDAL.Instance.GetByType(channelID, (int)EmailType.ReferenceRequest);
			
			// Format for html
			//blog.Body = BlogDAL.Instance.FormatForHTML(blog.Body);

			// Replace key fields with actual values (firstname, givenname etc)
			//blog.Body = blog.Body.Replace("%%SenderName%%", senderName);
			//blog.Body = blog.Body.Replace("%%SenderComment%%", senderComment);
			//blog.Body = blog.Body.Replace("%%SenderEmail%%", senderEmail);
			//blog.Body = blog.Body.Replace("%%RecipientEmail%%", recipientEmail);
			//blog.Body = blog.Body.Replace("%%DestinationURL%%", destinationURL);
			emailContent = String.Format("Hi {0},<p>{1}</p><p>P.S. To submit your review/reference, simply visit the <a href=\"{2}\">Reviews and References page</a> and follow the prompts!</p>", recipientFirstName, emailContent, destinationURL);

			emailContent = emailContent.Replace("\n","<br>");

			// TODO: Email details are currently hardcoded
			if (ConfigurationSettings.AppSettings["GenerateEmails"].ToUpper() != "FALSE")
			{
				SendEmail(String.Format("{0} {1}", recipientFirstName, recipientLastName), recipientEmail, senderName, senderEmail, "Request for reference", emailContent, channelID);
			}
		}



		/* public void GenerateHomeExchangeEnquiry(int channelID, HomeEnquiry homeEnquiry)
		{
			// Retrieve welcome email layout from blog
			Blog blog = BlogDAL.Instance.GetByType(channelID, (int)EmailType.ExchangeEnquiry);
			
			// Get destination email details (firstname & email address)
			Member member = MemberDAL.Instance.GetMemberFromHome(homeEnquiry.HomeID);


			// Format for html
			blog.Body = BlogDAL.Instance.FormatForHTML(blog.Body);

			// Replace key fields with actual values (firstname, givenname etc)
			
			try
			{
				blog.Body = blog.Body.Replace("%%FirstName%%",member.FirstName);
			}
			catch
			{
				throw new InvalidBlogContentException();
			}

			try
			{
				blog.Body = blog.Body.Replace("%%Message%%", homeEnquiry.Message);
			}
			catch
			{
				throw new InvalidBlogContentException();
			}

			try
			{
				blog.Body = blog.Body.Replace("%%GivenName%%",member.GivenName);
			}
			catch
			{
				// Do nothing as Given name is optional				
			}

			// TODO: Email details are currently hardcoded
			if (ConfigurationSettings.AppSettings["GenerateEmails"].ToUpper() != "FALSE")
			{
				SendEmail(member.FirstName, member.EmailAddress, "YourHomeForMine Enquiries", homeEnquiry.Email, "YourHomeForMine.com - Home exchange enquiry", blog.Body);
			}
		} */

		public void SendEmail(string toName, string toEmail, string fromName, string fromEmail, string subject, string body, int channelID)
		{
            // Determine secure email settings

			string SMTPServerName;
			string _SendFrom;
			string SMTPUser;
			string SMTPPassword;
            
			// smtp settings
			SMTPServerName = "mail.houseswapholidays.com.au";
			_SendFrom = "info@houseswapholidays.com.au";
			SMTPUser =  "info@houseswapholidays.com.au";
			SMTPPassword = ConfigurationSettings.AppSettings["EmailSupportPassword2"];
			_log.Info("Reading HouseSwapHolidays.com.au authentication settings");
					
            
			// TODO: Temporarily overwrite to test dynamic from email addresses 10/10/08
			_SendFrom = fromEmail;

            MailAddress from = new MailAddress(_SendFrom, fromName);
            MailAddress to = new MailAddress(toEmail, toName);
            
            MailMessage eMail = new MailMessage(from, to);
            eMail.IsBodyHtml = true;
			eMail.Subject = subject;
			eMail.Body = body;
            SmtpClient smtpClient = new SmtpClient(SMTPServerName, 25); //port is hard-coded by previous version
            bool enableSSL = (ConfigurationSettings.AppSettings["EnableEmailSSL"] != null ? ConfigurationSettings.AppSettings["EnableEmailSSL"] == "TRUE" : false);
		    smtpClient.EnableSsl = enableSSL;
            smtpClient.UseDefaultCredentials = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(SMTPUser, SMTPPassword);
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
			
			try
			{
                _log.Info(String.Format("About to send from {0} to {1} - {2}", fromEmail, toEmail, subject));
                // TODO @@@ enable sending email
                smtpClient.Send(eMail);

                _log.Info(String.Format("Sending email from {0} to {1} - {2}", fromEmail, toEmail, subject));
			}
			catch (Exception ex)
			{
                _log.Info("Error sending email " + ex.Message);
				if(ex.InnerException != null) _log.Info("Error sending email " + ex.InnerException.Message);
				_log.Info("Error sending from Sending from " + _SendFrom);
			}
		
		}

		public void SendContactEmail(string toName, string toEmail, string fromName, string fromEmail, string subject, string body)
		{
            
			string SMTPServerName = "mail.christianhomeswap.com";
			string _SendFrom = "info@christianhomeswap.com";
			string SMTPUser =  "info@christianhomeswap.com";
			string SMTPPassword = "h1ghwave";

			if (_SendFrom != fromEmail)
			{
				body = body + "<br><br>" + fromEmail;
			}

			// using System.Web.Mail;
			//eMail = new MailMessage();
            string from = String.Format("{0} <{1}>", fromName, _SendFrom);
            string to = String.Format("{0} <{1}>", toName, toEmail);
            MailMessage eMail = new MailMessage(from, to);
            eMail.IsBodyHtml = true;
            eMail.Subject = subject;
            eMail.Body = body;

            SmtpClient smtpClient = new SmtpClient(SMTPServerName, 25); //port is hard-coded by previous version
            smtpClient.UseDefaultCredentials = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(SMTPUser, SMTPPassword);
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            try
            {
                _log.Info(String.Format("About to send from {0} to {1} - {2}", fromEmail, toEmail, subject));
                smtpClient.Send(eMail);
                _log.Info(String.Format("Sending email from {0} to {1} - {2}", fromEmail, toEmail, subject));
            }
            catch (Exception ex)
            {
                _log.Info("Error sending email " + ex.Message);
                if (ex.InnerException != null) _log.Info("Error sending email " + ex.InnerException.Message);
                _log.Info("Error sending from Sending from " + _SendFrom);
            }
			
			_log.Info(String.Format("Sending email from {0} to {1} - {2}", fromEmail, toEmail, subject));
		
		}

		

		

		#region Singleton instance
		public static GenerateEmail Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
					lock (typeof(GenerateEmail))
					{
						if (_instance == null)
						{
							_instance = new GenerateEmail();
						}
					}
				}

				return _instance;
			}
			
		}

		#endregion

		private static GenerateEmail _instance;
		private log4net.ILog _log = log4net.LogManager.GetLogger(typeof(GenerateEmail));

	    private void Log(string msg)
	    {

	        string path =
	            Path.GetDirectoryName(
	                Uri.UnescapeDataString((new UriBuilder(Assembly.GetExecutingAssembly().CodeBase)).Path));
            path = Path.Combine(path, "emaillog.txt");
	        using (StreamWriter sw = new StreamWriter(path, true))
	        {
                sw.WriteLine(msg);
                sw.Close();
	        }
	        
            
	    }
	}
}
