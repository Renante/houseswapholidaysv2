using System;
using System.Data.SqlClient;
using System.Net;

namespace Common
{
	/// <summary>
	/// Summary description for IPConverter.
	/// </summary>
	public class IPConverter : DataAccess
	{
		public IPConverter()
		{
		}

		public int GetCountryIDByIP(string ipAddress)
		{
			long ipNum = 0;
			//string countryCode;
			
			// Convert IP address to IP number
			IPAddress addr = IPAddress.Parse(ipAddress);
			byte[] b = BitConverter.GetBytes(addr.Address);

			for (int i = 0; i < 4; ++i) 
			{
				long y = b[i];
				if (y < 0) 
				{
					y+= 256;
				}
				ipNum += y << ((3-i)*8);
			}

			//Console.WriteLine(ipnum);
			//return ipnum;
			
			// Get Country code from DB based on given IP number
			return GetCountryIDByIPNum(ipNum);
		}

		private int GetCountryIDByIPNum(long ipNumber)
		{
			int countryID = 0;
			const string spName = "up_GetCountryByIPNum";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, ipNumber))
                {
                    if (reader.Read())
                    {
                        countryID = GetFieldByInteger(reader, "CountryID");
                    }
                } 
            }

			return countryID;
		}
	}
}
