using System;
using System.Configuration;
using Common;

namespace Common
{
	/// <summary>
	/// Summary description for ControlManager.
	/// </summary>
	public class ControlManager : System.Web.UI.UserControl
	{
		public ControlManager()
		{
		}

		public string GetWebsiteName()
		{
			return GetWebsiteName(false);
		}

		public string GetWebsiteName(bool isSecure)
		{
			string serverName = Request.ServerVariables["SERVER_NAME"];

			if (ConfigurationSettings.AppSettings["IsSecure"].ToUpper() != "TRUE")
			{
				isSecure = false;
			}

			if ((!isSecure) && (_websiteName != null))
			{
				return _websiteName;
			}
			else if ((isSecure) && (_websiteSecureName != null))
			{
				return _websiteSecureName;
			}
			else
			{
				if ((serverName.ToUpper() != "LOCALHOST") && (!serverName.StartsWith("www")))
				{
					serverName = String.Format("www.{0}", serverName);
				}

				if ((Request.ServerVariables["SERVER_PORT"].Length > 0) && (serverName.ToUpper() == "LOCALHOST"))
				{
					if (isSecure)
					{
						_websiteSecureName = String.Format("https://{0}:{1}", serverName, Request.ServerVariables["SERVER_PORT"]);
						//return String.Format("https://{0}:{1}", serverName, Request.ServerVariables["SERVER_PORT"]);
					}
					else
					{
						_websiteName = String.Format("http://{0}:{1}", serverName, Request.ServerVariables["SERVER_PORT"]);
					}
				}
				else
				{
					if (isSecure)
					{
						_websiteSecureName = String.Format("https://{0}", serverName);
					}
					else
					{
						_websiteName = String.Format("http://{0}", serverName);
					}
				}
			}

			if (isSecure)
			{
				return _websiteSecureName;
			}
			else
			{
				return _websiteName;
			}
			
			//return serverName;
		}

		public string GetSecureURL(string urlPath)
		{
			bool isSecure = false;

			if (ConfigurationSettings.AppSettings["IsSecure"].ToUpper() == "TRUE")
			{
				isSecure = true;
			}

			return String.Format("{0}{1}",GetWebsiteName(isSecure),urlPath);
		}
		
		public string SecureRedirect(string urlPath)
		{
			bool isSecure = false;

			if (ConfigurationSettings.AppSettings["IsSecure"].ToUpper() != "FALSE")
			{
				isSecure = true;
			}

			return String.Format("{0}{1}",GetWebsiteName(isSecure),urlPath);
		}

		public string NonSecureRedirect(string urlPath)
		{
			bool isSecure = false;

			return String.Format("{0}{1}",GetWebsiteName(isSecure),urlPath);
		}

		public Channel Channel
		{
			get
			{
				if (_channel == null)
				{
					//					Common.Channel _channel = null;
					_channel = ChannelDAL.Instance.Get(Int32.Parse(ConfigurationSettings.AppSettings["ChannelID"]));
				}
				
				return _channel;
			}
		}

		private string _websiteName;
		private string _websiteSecureName;
		private Channel _channel;

	}
}
