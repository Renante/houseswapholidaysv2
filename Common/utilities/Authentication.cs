using System;
using System.Diagnostics;
using Common;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Common
{
	/// <summary>
	/// Summary description for Authentication.
	/// </summary>
	public class Authentication  : System.Web.UI.Page
	{
		public Authentication()
		{
		}

		#region Singleton instance
		public static Authentication Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
					lock (typeof(Authentication))
					{
						if (_instance == null)
						{
							_instance = new Authentication();
						}
					}
				}

				return _instance;
			}
			
		}

		#endregion

		private static Authentication _instance;


	}
}
