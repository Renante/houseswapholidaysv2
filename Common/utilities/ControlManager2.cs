using System;
using System.Configuration;
using Common;

namespace Common
{
	/// <summary>
	/// Summary description for ControlManager.
	/// </summary>
	public class ControlManager2 : System.Web.UI.UserControl
	{
		public ControlManager2()
		{
		}

		public string GetWebsiteName()
		{
			return GetWebsiteName(false);
		}

		public string GetWebsiteName(bool isSecure)
		{
			string serverName = Request.ServerVariables["SERVER_NAME"];

			if (ConfigurationSettings.AppSettings["IsSecure"].ToUpper() != "TRUE")
			{
				isSecure = false;
			}

			if ((!isSecure) && (_websiteName != null))
			{
				return _websiteName;
			}
			else if ((isSecure) && (_websiteSecureName != null))
			{
				return _websiteSecureName;
			}
			else
			{
				if ((serverName.ToUpper() != "LOCALHOST") && (!serverName.StartsWith("www")))
				{
					serverName = String.Format("www.{0}", serverName);
				}

				if ((Request.ServerVariables["SERVER_PORT"].Length > 0) && (serverName.ToUpper() == "LOCALHOST"))
				{
					if (isSecure)
					{
						_websiteSecureName = String.Format("https://{0}:{1}", serverName, Request.ServerVariables["SERVER_PORT"]);
						//return String.Format("https://{0}:{1}", serverName, Request.ServerVariables["SERVER_PORT"]);
					}
					else
					{
						_websiteName = String.Format("http://{0}:{1}", serverName, Request.ServerVariables["SERVER_PORT"]);
					}
				}
				else
				{
					if (isSecure)
					{
						_websiteSecureName = String.Format("https://{0}", serverName);
					}
					else
					{
						_websiteName = String.Format("http://{0}", serverName);
					}
				}
			}

			if (isSecure)
			{
				return _websiteSecureName;
			}
			else
			{
				return _websiteName;
			}
			
			//return serverName;
		}

		public string GetSecureURL(string urlPath)
		{
			bool isSecure = false;

			if (ConfigurationSettings.AppSettings["IsSecure"].ToUpper() == "TRUE")
			{
				isSecure = true;
			}

			return String.Format("{0}{1}",GetWebsiteName(isSecure),urlPath);
		}
		
		public string SecureRedirect(string urlPath)
		{
			bool isSecure = false;

			if (ConfigurationSettings.AppSettings["IsSecure"].ToUpper() != "FALSE")
			{
				isSecure = true;
			}

			return String.Format("{0}{1}",GetWebsiteName(isSecure),urlPath);
		}

		public string NonSecureRedirect(string urlPath)
		{
			bool isSecure = false;

			return String.Format("{0}{1}",GetWebsiteName(isSecure),urlPath);
		}

		public Channel Channel
		{
			get
			{
				if (_channel == null)
				{
					//					Common.Channel _channel = null;
					_channel = ChannelDAL.Instance.Get(GetWebsiteName());
				}
				
				return _channel;
			}
		}

		/// <summary>
		/// Retrieves lightweight member information from session (if available)
		/// </summary>
		/// <returns></returns>
		public MemberSession GetMemberFromSession()
		{
			if (((MemberSession)Session["membersession"]) != null)
			{
				return (MemberSession)((MemberSession)Session["membersession"]);
			}
			else
			{

				String guidParser;
				if ((ViewState["sid"] != null) && (ViewState["sid"].ToString().Length > 0))
				{
					guidParser = ViewState["sid"].ToString();

					int delimiterPos = guidParser.IndexOf("hqw",0,30);

					int memberID = Int32.Parse(guidParser.Substring(0, delimiterPos));

					string guid = guidParser.Substring(delimiterPos+4);

					Member member = MemberDAL.Instance.GetByGuid(memberID, guid);

					//_log.Info(String.Format("Session timeout. Attempting to retrieve guid {0}", guidParser));

					if (member != null)
					{
						// Repopulate session based on authenticated details
						MemberSession memberSession = new MemberSession();
						memberSession.MemberID = member.ID;
						memberSession.HomeID = member.HomeID;
						memberSession.MemberStatus = member.Status;
                        
						
						Session["membersession"] = memberSession;
						
						return (memberSession);
					}
				}

				// Note: No redirect here (e.g. as per pagemanager) as web controls don't support this
			}

			return null;
		}

		// Get referrerID from cookie using default referrer cookie name
		public string GetReferrerFromCookie()
		{
			if (Request.Cookies["ReferrerCookie"] != null)
			{
				return Request.Cookies["ReferrerCookie"].Value.ToString();
			}

			return "-1";
		}

		// Get referrerID from cookie using specified referrer cookie name
		public string GetReferrerFromCookie(string cookieName)
		{
			if (Request.Cookies[cookieName] != null)
			{
				return Request.Cookies[cookieName].Value.ToString();
			}

			return "-1";
		}

		private string _websiteName;
		private string _websiteSecureName;
		private Channel _channel;

	}
}
