using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.IO;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Common
{
	/// <summary>
	/// Summary description for ImageProcessor.
	/// </summary>
	public class ImageProcessor : System.Web.UI.Page
	{
		public ImageProcessor()
		{
		}

		public void SaveImage(HtmlInputFile imageFile, string fileName)
		{
			try
			{
				imageFile.PostedFile.SaveAs(fileName);
			}
			catch(Exception ex)
			{
				//Response.Write("An error occurred - " + ex.ToString());
				_log.Error("Error occured saving the image", ex);
				throw ex;
			}

		}

		//public void DeleteImage(HtmlInputFile imageFile, string filepath, string filename)
		public void DeleteImage(string filePath, string fileName)
		{
			try
			{
				string newFileName = String.Format(@"{0}/{1}", filePath, fileName);
				File.Delete(newFileName);
			}
			catch(Exception ex)
			{
			_log.Error("Error occured deleting the image", ex);
			throw ex;
			}

		}

		public void DeleteImage(string fileName)
		{
			try
			{
				File.Delete(fileName);
			}
			catch(Exception ex)
			{
				_log.Error("Error occured deleting the image", ex);
				throw ex;
			}
		}


		public void ResizeImage(string sourceFilename, string destinationFilename, string destinationPath, int memberID, double width, double height)
		{
			try
			{
				String UploadedFile = sourceFilename;

				int ExtractPos =	UploadedFile.LastIndexOf("\\") + 1;
		 
				//to retrieve only Filename from the complete path
				String UploadedFileName =  UploadedFile.Substring(ExtractPos,UploadedFile.Length - ExtractPos);

				//resize image creation starts
				//Read in the image filename whose thumbnail has to be created
				String imageUrl=  UploadedFileName;

				if (imageUrl.IndexOf("/") >= 0 || imageUrl.IndexOf("\\") >= 0 )
				{
					//We found a / or \
					//Response.End();
				}

				//the uploaded image will be stored in the Pics folder.
				//to get resize the image, the original image has to be accessed from the
				//Pics folder
				//imageUrl = imageUrl;
		
				System.Drawing.Image fullSizeImg = System.Drawing.Image.FromFile(sourceFilename);

				double ratio = 0;
				double ratioW = 0;
				double ratioH = 0;
				double imageWidth = (double) fullSizeImg.Width;
				double imageHeight = (double) fullSizeImg.Height;
				double targetWidth = (double) width;
				double targetHeight = (double) height;

				ratioW = targetWidth / imageWidth;
				ratioH = targetHeight / imageHeight;

				if (((imageWidth * ratioW) <= targetWidth) && (imageHeight * ratioW) <= targetHeight)
				{
					ratio = ratioW;
				}
				else
				{
					ratio = ratioH;
				}

				// Finally calculate exact new width & height
				int newImageWidth = (int)(imageWidth * ratio);
				int newImageHeight = (int)(imageHeight * ratio);

				Bitmap bm_source = new Bitmap(fullSizeImg);
				Bitmap bm_dest = new Bitmap(newImageWidth, newImageHeight);

				Graphics gr_dest = Graphics.FromImage(bm_dest);

				gr_dest.DrawImage(bm_source, 0, 0, bm_dest.Width, bm_dest.Height);

				bm_dest.Save(destinationPath + "\\" + destinationFilename, ImageFormat.Jpeg);

				// close files
				bm_dest.Dispose();
				fullSizeImg.Dispose();
			}
			catch(Exception ex)
			{
				//Response.Write("An error occurred - " + ex.ToString());
				_log.Error("Error occured resizing image", ex);
				throw ex;
			}
		}

		private log4net.ILog _log = log4net.LogManager.GetLogger(typeof(ImageProcessor));
	}
}
