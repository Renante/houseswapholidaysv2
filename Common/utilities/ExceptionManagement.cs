using System;

namespace Common
{
	/// <summary>
	/// Exception handler for promotions that have closed
	/// </summary>
	public class RecordExpiredException : System.ApplicationException
	{

		public RecordExpiredException()
		{
		}

	}

	/// <summary>
	/// Exception handler for promotions that have closed
	/// </summary>
	public class RecordNotFoundException : System.ApplicationException
	{

		public RecordNotFoundException()
		{
		}

	}

	/// <summary>
	/// Exception handler for promotions that have closed
	/// </summary>
	public class RecordAlreadyExistsException : System.ApplicationException
	{

		public RecordAlreadyExistsException()
		{
		}

	}

	/// <summary>
	/// Exception handler for failed form validation
	/// </summary>
	public class PageValidationException : System.ApplicationException
	{

		public PageValidationException()
		{
		}

	}

	/// <summary>
	/// Exception handler for promotions that have closed
	/// </summary>
	public class InvalidBlogContentException : System.ApplicationException
	{

		public InvalidBlogContentException()
		{
		}

	}

}
