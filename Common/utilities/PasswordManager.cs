﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Common
{
    public class PasswordManager
    {

        public bool IsPasswordValid(string password, string salt)
        {

            return true;
        }

        public string GenerateHashedPassword(string plainPassword, string salt)
        {
            HMACSHA1 hash = new HMACSHA1();
            hash.Key = Encoding.Unicode.GetBytes(plainPassword);
            string hashedPassword = Convert.ToBase64String(hash.ComputeHash(Encoding.Unicode.GetBytes(plainPassword + salt)));
            return hashedPassword;
        }

        public string GenerateRandomSalt()
        {
            return Encryption.GenerateRandomString(16);
        }


        #region Singleton instance
        public static PasswordManager Instance
        {
            [DebuggerStepThrough]
            get
            {
                if (_instance == null)
                {
                    lock (typeof(PasswordManager))
                    {
                        if (_instance == null)
                        {
                            _instance = new PasswordManager();
                        }
                    }
                }

                return _instance;
            }

        }
        #endregion
        private static PasswordManager _instance;
    }

    

}
