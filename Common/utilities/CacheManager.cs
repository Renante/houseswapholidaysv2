using System;

namespace Common
{
	/// <summary>
	/// Summary description for CacheManager.
	/// </summary>
	public class CacheManager
	{
		public CacheManager()
		{
		}

		public object Get(string objectName)
		{
			//if (Cache[objectName] != null)
			if (System.Web.HttpContext.Current.Cache[objectName] != null)
			{
				return System.Web.HttpContext.Current.Cache.Get(objectName);
			}
			
			return null;

		}

		public void Set(string cacheName, object cacheValue)
		{

			System.Web.HttpContext.Current.Cache.Insert(cacheName, cacheValue);
		}

	
	}
}
