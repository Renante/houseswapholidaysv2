using System;

namespace Common
{
	/// <summary>
	/// Summary description for Payment.
	/// </summary>
	public class PaymentManager : PageManager2
	{
		public PaymentManager()
		{
		}

		public void Initiate(Payment payment)
		{
			// Check if the member has received a promotion code or not
			if (payment.PromotionID > 0)
			{
				Promotion promotion = PromotionDAL.Instance.Get(payment.PromotionID);
				
				if ((promotion.ThruDate >= DateTime.Now) || ((promotion.MaximumMembers > 0) && (promotion.RedeemedMembers >= promotion.MaximumMembers)))
				{
					// Promotion is valid, so lets get the necessary information
					// TODO: For the moment, we are treating all members as new. This is probably okay
					payment.PaymentAmount = promotion.NewMemberRate;
					payment.PromotionID = promotion.ID;
				}
			}

			// Set payment dates, channelID etc
			payment.PaymentInitiateDate = DateTime.Now;
			payment.Status = PaymentStatus.Pending;

			// Save payment to database before we actually commence payment
			PaymentDAL.Instance.Save(payment);
			
		}
	}
}
