using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Text;
using Common;
using System;

namespace Common
{
	/// <summary>
	/// Summary description for ChannelExtension.
	/// </summary>
	public class ChannelExtension
	{
		public ChannelExtension()
		{
		}

		private String UTF8ByteArrayToString(Byte[] characters)
		{
			UTF8Encoding encoding = new UTF8Encoding();
			String constructedString = encoding.GetString(characters);
			return (constructedString);
		}

		/// <summary>
		/// Converts the String to UTF8 Byte array and is used in De serialization
		/// </summary>
		/// <param name="pXmlString"></param>
		/// <returns></returns>
		private Byte[] StringToUTF8ByteArray(String pXmlString)
		{
			UTF8Encoding encoding = new UTF8Encoding();
			Byte[] byteArray = encoding.GetBytes(pXmlString);
			return byteArray;
		}

		/// <summary>
		/// Method to convert a custom Object to XML string
		/// </summary>
		/// <param name="pObject">Object that is to be serialized to XML</param>
		/// <returns>XML string</returns>
		public String SerializeObject(Object pObject)
		{
			try
			{
				String XmlizedString = null;
				MemoryStream memoryStream = new MemoryStream();
				XmlSerializer xs = new XmlSerializer(typeof(Fields));
				XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);

				xs.Serialize(xmlTextWriter, pObject);
				memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
				XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
				return XmlizedString;
			}
			catch (Exception e)
			{
				System.Console.WriteLine(e);
				return null;
			}
		}

		public Object DeserializeObject(string serialisedObject)
		{
			XmlSerializer xs = new XmlSerializer(typeof(Fields));
			MemoryStream memoryStream = new MemoryStream(StringToUTF8ByteArray(serialisedObject));
			XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);

			return xs.Deserialize(memoryStream);
		}
	}

	public class Fields
	{
		public string name;
		public string heading;
		public string content;
		public DataType dataType;
	}

}
