﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Common
{
    public static class Encryption
    {
        public static string GenerateRandomString(int size)
        {
            var rng = new RNGCryptoServiceProvider();
            var bytes = new Byte[16];
            rng.GetBytes(bytes);
            return Convert.ToBase64String(bytes);
        }
    }
}
