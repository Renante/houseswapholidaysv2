using System;
using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for HomeCollection.
	/// </summary>
	public class MemberCollection : CollectionBase
	{
		public MemberCollection()
		{
		}

		public void Add(Member member)
		{
			InnerList.Add(member);
		}

		public void Remote(Member member)
		{
			InnerList.Remove(member);
		}

		public Member this[int index]
		{
			get
			{
				return (Member)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}

	}
}
