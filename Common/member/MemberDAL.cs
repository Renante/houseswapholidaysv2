using System;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;

namespace Common
{
	/// <summary>
	/// Summary description for HomeDAL.
	/// </summary>
	public class MemberDAL : DataAccess
	{

		#region Types
		private sealed class MemberFields
		{
			public readonly int ID = -1;
			public readonly int FirstName = -1;
			public readonly int GivenName = -1;
			public readonly int EmailAddress = -1;
			public readonly int Password = -1;
			public readonly int Introduction = -1;
			public readonly int Language1 = -1;
			public readonly int Language2 = -1;
			public readonly int Language3 = -1;
			public readonly int CreatedDate = -1;
			public readonly int UpdatedDate = -1;
			public readonly int LastLoggedInDate = -1;
			public readonly int ReferrerID = -1;
		}
		#endregion


		public MemberDAL()
		{
		}

		/*
		public Member Get(int memberID)
		{
			const string spName = "up_GetMember";

			using (DataSet dataSet = ExecuteDataset(spName, memberID))
			{
				if (dataSet.Tables.Count == 0)
				{
					throw new ApplicationException(String.Format("Error executing data set for procedure '{0}', member ID {1}, no data tables returned", spName, memberID));
				}

				DataTable memberList = dataSet.Tables[0];
				if (memberList != null)
				{
					Member member = new Member();

					member.ID = GetFieldByInteger(memberList.Rows[0],"MemberID");
					member.FirstName = GetFieldByString(memberList.Rows[0],"FirstName");
					member.GivenName = GetFieldByString(memberList.Rows[0],"GivenName");
					member.EmailAddress = GetFieldByString(memberList.Rows[0],"EmailAddress");
					member.Password = GetFieldByString(memberList.Rows[0],"Password");

					member.Introduction = GetFieldByString(memberList.Rows[0],"Introduction");
					member.Language1 = GetFieldByString(memberList.Rows[0],"Language1");
					member.Language2 = GetFieldByString(memberList.Rows[0],"Language2");
					member.Language3 = GetFieldByString(memberList.Rows[0],"Language3");
					member.CreatedDate = GetFieldByDate(memberList.Rows[0],"CreatedDate");
					member.UpdatedDate = GetFieldByDate(memberList.Rows[0],"UpdatedDate");
					member.LastLoggedInDate = GetFieldByDate(memberList.Rows[0],"LastLoggedInDate");
					member.ReferrerID = GetFieldByInteger(memberList.Rows[0],"ReferrerID");
					member.AcceptedTerms = GetFieldByBoolean(memberList.Rows[0],"AcceptedTerms");
					member.PromotionID = GetFieldByInteger(memberList.Rows[0],"PromotionID");

					// Get home channel information
					member.DefaultChannel = GetFieldByInteger(memberList.Rows[0], "DefaultChannel");
					member.IsMultiChannel = GetFieldByBoolean(memberList.Rows[0], "IsMultiChannel");

					// Get message setting to determine if user has current messages
					member.UnreadMessageCount = GetFieldByInteger(memberList.Rows[0], "UnreadMessageCount");

					member.ReceiveNewsletter = GetFieldByBoolean(memberList.Rows[0],"ReceiveNewsletter");

					member.AllowDestinationOffers = GetFieldByBoolean(memberList.Rows[0],"AllowDestinationOffers");
					member.AcceptSpecialConditions = GetFieldByBoolean(memberList.Rows[0],"AcceptSpecialConditions");

					return member;
				}
				else
				{
					return null;
				}
			}
		}
		*/

		public Member Get(int memberID)
		{
			const string spName = "up_GetMemberv2";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, memberID))
                {
                    if (reader.Read())
                    {
                        Member member = new Member();

                        member.ID = (int)reader["MemberID"];
                        member.FirstName = (string)reader["FirstName"];
                        member.GivenName = (string)reader["GivenName"];
                        member.EmailAddress = (string)reader["EmailAddress"];
                        member.Password = (string)reader["Password"];
                        member.Salt = (string)reader["Salt"];
                        member.HashedPassword = (string)reader["HashedPassword"];
                        member.Status = (Status)GetFieldByInteger(reader, "Status");

                        member.Introduction = GetFieldByString(reader, "Introduction");
                        member.Language1 = GetFieldByString(reader, "Language1");
                        member.Language2 = GetFieldByString(reader, "Language2");
                        member.Language3 = GetFieldByString(reader, "Language3");
                        member.CreatedDate = GetFieldByDate(reader, "CreatedDate");
                        member.UpdatedDate = GetFieldByDate(reader, "UpdatedDate");
                        member.LastLoggedInDate = GetFieldByDate(reader, "LastLoggedInDate");
                        member.ReferrerID = GetFieldByInteger(reader, "ReferrerID");
                        member.AcceptedTerms = GetFieldByBoolean(reader, "AcceptedTerms");
                        member.PromotionID = GetFieldByInteger(reader, "PromotionID");

                        // Get home channel information
                        member.DefaultChannel = GetFieldByInteger(reader, "DefaultChannel");
                        member.IsMultiChannel = GetFieldByBoolean(reader, "IsMultiChannel");

                        // Get message setting to determine if user has current messages
                        member.UnreadMessageCount = GetFieldByInteger(reader, "UnreadMessageCount");

                        member.ReceiveNewsletter = GetFieldByBoolean(reader, "ReceiveNewsletter");

                        member.AllowDestinationOffers = GetFieldByBoolean(reader, "AllowDestinationOffers");
                        member.AcceptSpecialConditions = GetFieldByBoolean(reader, "AcceptSpecialConditions");

                        member.Guid = GetFieldByString(reader, "Guid");
                        member.UpdateMode = GetFieldByInteger(reader, "UpdateMode");
                        member.UpdateTravelDetailsMode = GetFieldByInteger(reader, "UpdateTravelDetailsMode");

                        return member;
                    }
                    else
                    {
                        return null;
                    }
                } 
            }
		}

		public Member GetByEmailAddress(string emailAddress)
		{
			const string spName = "up_GetMemberByEmailAddress";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, emailAddress))
                {
                    if (reader.Read())
                    {
                        Member member = new Member();

                        member.ID = (int)reader["MemberID"];
                        member.FirstName = (string)reader["FirstName"];
                        member.GivenName = (string)reader["GivenName"];
                        member.EmailAddress = (string)reader["EmailAddress"];
                        member.Password = (string)reader["Password"];
                        

                        member.Status = (Status)GetFieldByInteger(reader, "Status");

                        member.Introduction = GetFieldByString(reader, "Introduction");
                        member.Language1 = GetFieldByString(reader, "Language1");
                        member.Language2 = GetFieldByString(reader, "Language2");
                        member.Language3 = GetFieldByString(reader, "Language3");
                        member.CreatedDate = GetFieldByDate(reader, "CreatedDate");
                        member.UpdatedDate = GetFieldByDate(reader, "UpdatedDate");
                        member.LastLoggedInDate = GetFieldByDate(reader, "LastLoggedInDate");
                        member.ReferrerID = GetFieldByInteger(reader, "ReferrerID");
                        member.AcceptedTerms = GetFieldByBoolean(reader, "AcceptedTerms");
                        member.PromotionID = GetFieldByInteger(reader, "PromotionID");

                        // Get home channel information
                        member.DefaultChannel = GetFieldByInteger(reader, "DefaultChannel");
                        member.IsMultiChannel = GetFieldByBoolean(reader, "IsMultiChannel");

                        member.AllowDestinationOffers = GetFieldByBoolean(reader, "AllowDestinationOffers");

                        return member;
                    }
                    else
                    {
                        return null;
                    }
                } 
            }
		}

		public Member GetMemberFromHome(int homeID)
		{
			const string spName = "up_GetMemberFromHome";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, homeID))
                {
                    if (reader.Read())
                    {
                        Member member = new Member();

                        member.ID = (int)reader["MemberID"];
                        member.FirstName = (string)reader["FirstName"];
                        member.GivenName = (string)reader["GivenName"];
                        member.EmailAddress = (string)reader["EmailAddress"];
                        member.Password = (string)reader["Password"];

                        member.Status = (Status)GetFieldByInteger(reader, "Status");

                        member.Introduction = GetFieldByString(reader, "Introduction");
                        member.Language1 = GetFieldByString(reader, "Language1");
                        member.Language2 = GetFieldByString(reader, "Language2");
                        member.Language3 = GetFieldByString(reader, "Language3");
                        member.CreatedDate = GetFieldByDate(reader, "CreatedDate");
                        member.UpdatedDate = GetFieldByDate(reader, "UpdatedDate");
                        member.LastLoggedInDate = GetFieldByDate(reader, "LastLoggedInDate");
                        member.ReferrerID = GetFieldByInteger(reader, "ReferrerID");
                        member.AcceptedTerms = GetFieldByBoolean(reader, "AcceptedTerms");
                        member.PromotionID = GetFieldByInteger(reader, "PromotionID");

                        // Get home channel information
                        member.DefaultChannel = GetFieldByInteger(reader, "DefaultChannel");
                        member.IsMultiChannel = GetFieldByBoolean(reader, "IsMultiChannel");

                        member.AllowDestinationOffers = GetFieldByBoolean(reader, "AllowDestinationOffers");

                        return member;
                    }
                    else
                    {
                        return null;
                    }
                } 
            }
		}

		public Status GetMemberHomeStatus(int memberID)
		{
			const string spName = "up_GetMemberHomeStatus";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, memberID))
                {
                    //Status homeStatus = Status.NotSpecified;
                    Status homeStatus = Status.NotSpecified;

                    if (reader.Read())
                    {
                        homeStatus = (Status)GetFieldByInteger(reader, "HomeStatus");
                        bool isPendingApproval = false;
                        bool isDraft = false;

                        switch (homeStatus)
                        {
                            case Status.Active:
                                return Status.Active;
                                break;

                            case Status.PendingApproval:
                                homeStatus = Status.PendingApproval;
                                isPendingApproval = true;
                                break;

                            case Status.Draft:
                                homeStatus = Status.Draft;
                                isDraft = true;
                                break;
                        }

                        if (isPendingApproval)
                        {
                            return Status.PendingApproval;
                        }
                        else
                        {
                            if (isDraft)
                            {
                                return Status.Draft;
                            }
                            else
                            {
                                return Status.NotSpecified;
                            }
                        }

                    }
                    else
                    {
                        // No home details available
                        return Status.NotSpecified;
                        //return null;
                    }
                } 
            }
		}

		public MemberCollection GetAll(string firstName, string givenName, string email)
		{
			MemberCollection memberCollection = new MemberCollection();

			const string spName = "up_GetAllMembers";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, firstName, givenName, email))
                {
                    while (reader.Read())
                    {
                        Member member = new Member();

                        member.ID = GetFieldByInteger(reader, "MemberID");
                        member.FirstName = GetFieldByString(reader, "FirstName");
                        member.GivenName = GetFieldByString(reader, "GivenName");
                        member.EmailAddress = GetFieldByString(reader, "EmailAddress");
                        member.Password = GetFieldByString(reader, "Password");

                        member.Status = (Status)GetFieldByInteger(reader, "Status");

                        member.Introduction = GetFieldByString(reader, "Introduction");
                        member.Language1 = GetFieldByString(reader, "Language1");
                        member.Language2 = GetFieldByString(reader, "Language2");
                        member.Language3 = GetFieldByString(reader, "Language3");
                        member.CreatedDate = GetFieldByDate(reader, "CreatedDate");
                        member.UpdatedDate = GetFieldByDate(reader, "UpdatedDate");
                        member.LastLoggedInDate = GetFieldByDate(reader, "LastLoggedInDate");
                        member.ReferrerID = GetFieldByInteger(reader, "ReferrerID");

                        memberCollection.Add(member);
                    }
                } 
            }

			return memberCollection;
		}


		/// <summary>
		/// Gets all members who have just joined
		/// </summary>
		/// <returns></returns>
		public MemberCollection GetAllRecentMembers(string fromDateTime)
		{
			MemberCollection memberCollection = new MemberCollection();

			const string spName = "up_GetAllRecentMembers";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, fromDateTime))
                {
                    while (reader.Read())
                    {
                        Member member = new Member();

                        member.ID = (int)reader["MemberID"];
                        member.FirstName = (string)reader["FirstName"];
                        member.GivenName = (string)reader["GivenName"];
                        member.EmailAddress = (string)reader["EmailAddress"];
                        member.Password = (string)reader["Password"];

                        member.Introduction = GetFieldByString(reader, "Introduction");
                        member.Language1 = GetFieldByString(reader, "Language1");
                        member.Language2 = GetFieldByString(reader, "Language2");
                        member.Language3 = GetFieldByString(reader, "Language3");
                        member.CreatedDate = GetFieldByDate(reader, "CreatedDate");
                        member.UpdatedDate = GetFieldByDate(reader, "UpdatedDate");
                        member.LastLoggedInDate = GetFieldByDate(reader, "LastLoggedInDate");
                        member.ReferrerID = GetFieldByInteger(reader, "ReferrerID");

                        memberCollection.Add(member);
                    }
                } 
            }

			return memberCollection;
		}

		/// <summary>
		/// Save the current member
		/// </summary>
		/// <param name="member"></param>
		/// <returns></returns>
		public Member Save(Member member)
		{

            // make sure we will save salt and hash
            if (string.IsNullOrEmpty(member.HashedPassword))
            {
                member.Salt = PasswordManager.Instance.GenerateRandomSalt();
                member.HashedPassword = PasswordManager.Instance.GenerateHashedPassword(member.Password, member.Salt);
            }

            // dont save password
            member.Password = "";

			if (member.ID > 0)
			{
				const string spName = "up_UpdateMemberv2";

				ExecuteNonQuery(spName, 
						   member.ID, member.Status, member.FirstName, member.GivenName, 
						   member.EmailAddress, member.Password, member.Introduction, 
						   member.Language1, member.Language2, member.Language3, 
						   member.CreatedDate, member.UpdatedDate, member.LastLoggedInDate, 
						   member.ReferrerID, member.CountryID, member.Type, member.PromotionID, 
						   member.ReceiveNewsletter, member.AcceptedTerms, member.IsMultiChannel, 
						   member.DefaultChannel, member.AllowDestinationOffers, member.AcceptSpecialConditions, member.Salt, member.HashedPassword);
			}
			else
			{
				const string spName = "up_InsertMember2v2";

				member.ID = (int)ExecuteScalar(spName, member.FirstName, member.GivenName, member.EmailAddress, member.Password, 
						   member.Introduction, member.Language1, member.Language2, member.Language3, 
						   member.CreatedDate, member.UpdatedDate, member.LastLoggedInDate, 
						   member.ReferrerID, member.CountryID, member.Type, member.PromotionID, member.ReceiveNewsletter, 
						   member.AcceptedTerms, member.IsMultiChannel, member.DefaultChannel, member.AcceptSpecialConditions, member.Guid, member.Status, member.Salt, member.HashedPassword);
			}

			return member;
		}

		public MemberCollection GetNew(int hoursBack)
		{
			MemberCollection memberCollection = new MemberCollection();

			const string spName = "up_GetNewMembers";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, hoursBack))
                {
                    while (reader.Read())
                    {
                        Member member = new Member();

                        member.ID = GetFieldByInteger(reader, "MemberID");
                        member.FirstName = GetFieldByString(reader, "FirstName");
                        member.GivenName = GetFieldByString(reader, "GivenName");
                        member.EmailAddress = GetFieldByString(reader, "EmailAddress");
                        member.Password = GetFieldByString(reader, "Password");

                        member.Status = (Status)GetFieldByInteger(reader, "Status");

                        member.Introduction = GetFieldByString(reader, "Introduction");
                        member.Language1 = GetFieldByString(reader, "Language1");
                        member.Language2 = GetFieldByString(reader, "Language2");
                        member.Language3 = GetFieldByString(reader, "Language3");
                        member.CreatedDate = GetFieldByDate(reader, "CreatedDate");
                        member.UpdatedDate = GetFieldByDate(reader, "UpdatedDate");
                        member.LastLoggedInDate = GetFieldByDate(reader, "LastLoggedInDate");
                        member.ReferrerID = GetFieldByInteger(reader, "ReferrerID");

                        memberCollection.Add(member);
                    }
                } 
            }

			return memberCollection;
		}

		/* uses reader so am temporarily commenting out
		/// <summary>
		/// Save the current member
		/// </summary>
		/// <param name="member"></param>
		/// <returns></returns>
		public Member Save(Member member)
		{
			if (member.ID > 0)
			{
				const string spName = "up_UpdateMember";

				using (SqlDataReader reader = ExecuteReader(spName, 
						member.ID, member.FirstName, member.GivenName, 
						member.EmailAddress, member.Password, member.Introduction, 
						member.Language1, member.Language2, member.Language3, 
						member.CreatedDate, member.UpdatedDate, member.LastLoggedInDate, 
						member.ReferrerID, member.CountryID, member.Type, member.PromotionID, 
						member.ReceiveNewsletter, member.AcceptedTerms, member.IsMultiChannel, 
						member.DefaultChannel, member.AllowDestinationOffers, member.AcceptSpecialConditions))
				{

					// TODO: There is a more efficient way of doing this - probably don't need to use a data reader
					reader.Close();
				}
			}
			else
			{
				const string spName = "up_InsertMember";

				using (SqlDataReader reader = ExecuteReader(spName, 
						   member.FirstName, member.GivenName, member.EmailAddress, member.Password, 
						   member.Introduction, member.Language1, member.Language2, member.Language3, 
						   member.CreatedDate, member.UpdatedDate, member.LastLoggedInDate, 
						   member.ReferrerID, member.CountryID, member.Type, member.PromotionID, member.ReceiveNewsletter, 
						   member.AcceptedTerms, member.IsMultiChannel, member.DefaultChannel, member.AcceptSpecialConditions))
				{

					if (reader.Read())
					{
						member.ID = reader.GetInt32(reader.GetOrdinal("MemberID"));
					}
					
					reader.Close();
				}
			}

			return member;
		}
		*/

		/// <summary>
		/// Save the current member
		/// </summary>
		/// <param name="member"></param>
		/// <returns></returns>
		public void UpdateLoginDate(int memberID)
		{
			const string spName = "up_UpdateMemberLoginDate";

			ExecuteNonQuery(spName, memberID, DateTime.Now);
		}

		/// <summary>
		/// Authenticate the requested member
		/// </summary>
		/// <param name="EmailAddress"></param>
		/// <param name="Password"></param>
		/// <returns></returns>
		public Member Authenticate(string EmailAddress, string plainPassword)
		{
			const string spName = "up_AuthenticateMemberv2";
            
            string memberSalt = MemberDAL.Instance.GetMemberSaltByEmail(EmailAddress);
            string HashedPassword = PasswordManager.Instance.GenerateHashedPassword(plainPassword, memberSalt);


            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, EmailAddress, HashedPassword))
                {
                    if (reader.Read())
                    {
                        Member member = new Member();

                        member.ID = (int)reader["MemberID"];
                        member.FirstName = (string)reader["FirstName"];
                        member.GivenName = (string)reader["GivenName"];
                        member.EmailAddress = (string)reader["EmailAddress"];
                        member.Password = (string)reader["Password"];
                        member.HashedPassword = (string)reader["HashedPassword"];
                        member.Status = (Status)GetFieldByInteger(reader, "Status");
                        member.DefaultChannel = GetFieldByInteger(reader, "DefaultChannel");
                        member.IsMultiChannel = GetFieldByBoolean(reader, "IsMultiChannel");

                        return member;
                    }
                    else
                    {
                        return null;
                    }
                } 
            }
		}

		/*
		public bool CheckEmailExists(string emailAddress)
		{
			const string spName = "up_GetMemberEmail";

			bool isMember = false;

			using (DataSet dataSet = ExecuteDataset(spName, emailAddress))
			{
				DataTable memberList = dataSet.Tables[0];

				if (memberList.Rows.Count > 0)
				{
					isMember = true;
				}

				memberList.Dispose();
				dataSet.Dispose();
			}

			return isMember;
		}
		*/

		public Member GetByGuid(int memberID, string guid)
		{
			const string spName = "up_GetMemberByGuid";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, memberID, guid))
                {
                    if (reader.Read())
                    {
                        Member member = new Member();

                        member.ID = (int)reader["MemberID"];
                        member.FirstName = (string)reader["FirstName"];
                        member.GivenName = (string)reader["GivenName"];
                        member.EmailAddress = (string)reader["EmailAddress"];
                        member.Password = (string)reader["Password"];
                        
                        member.Status = (Status)GetFieldByInteger(reader, "Status");

                        member.Introduction = GetFieldByString(reader, "Introduction");
                        member.Language1 = GetFieldByString(reader, "Language1");
                        member.Language2 = GetFieldByString(reader, "Language2");
                        member.Language3 = GetFieldByString(reader, "Language3");
                        member.CreatedDate = GetFieldByDate(reader, "CreatedDate");
                        member.UpdatedDate = GetFieldByDate(reader, "UpdatedDate");
                        member.LastLoggedInDate = GetFieldByDate(reader, "LastLoggedInDate");
                        member.ReferrerID = GetFieldByInteger(reader, "ReferrerID");
                        member.AcceptedTerms = GetFieldByBoolean(reader, "AcceptedTerms");
                        member.PromotionID = GetFieldByInteger(reader, "PromotionID");

                        // Get home channel information
                        member.DefaultChannel = GetFieldByInteger(reader, "DefaultChannel");
                        member.IsMultiChannel = GetFieldByBoolean(reader, "IsMultiChannel");

                        // Get message setting to determine if user has current messages
                        member.UnreadMessageCount = GetFieldByInteger(reader, "UnreadMessageCount");

                        member.ReceiveNewsletter = GetFieldByBoolean(reader, "ReceiveNewsletter");

                        member.AllowDestinationOffers = GetFieldByBoolean(reader, "AllowDestinationOffers");
                        member.AcceptSpecialConditions = GetFieldByBoolean(reader, "AcceptSpecialConditions");

                        return member;
                    }
                    else
                    {
                        return null;
                    }
                } 
            }
		}

		public bool CheckEmailExists(string emailAddress)
		{
			const string spName = "up_GetMemberEmail";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, emailAddress))
                {
                    return reader.HasRows;
                } 
            }
		}

        public string GetMemberSaltByEmail(string emailAddress)
        {
            const string spName = "up_GetMemberSaltByEmail";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, emailAddress))
                {
                    if (reader.Read())
                        return GetFieldByString(reader, "Salt");
                    else
                        return null;
                }
            }
        }

		private Member LoadFromReader(SqlDataReader reader, MemberFields fields)
		{
			Member member = new Member();

			member.ID = reader.GetInt32(fields.ID);
			member.FirstName = reader.GetString(fields.FirstName);
			member.GivenName = reader.GetString(fields.GivenName);
			member.EmailAddress = reader.GetString(fields.EmailAddress);
			member.Password = reader.GetString(fields.Password);

			return member;
		}

        public void SaveMemberUpdateMode(int memberID, int updateModeStep)
        {
            const string spName = "up_SetUpdateMode";

            ExecuteNonQuery(spName,
                    memberID,
                    updateModeStep);

        }

        public void SaveMemberUpdateTravelDetailsMode(int memberID, int mode)
        {
            const string spName = "up_SetUpdateTravelDetailsMode";

            ExecuteNonQuery(spName,
                    memberID,
                    mode);
        }

        public void UpdateMemberPassword(string emailAddress, string password)
        {
            const string spName = "up_UpdateMemberPassword";

            string salt = PasswordManager.Instance.GenerateRandomSalt();
            string hashedPassword = PasswordManager.Instance.GenerateHashedPassword(password, salt);

            // dont save password
            password = "";

            ExecuteNonQuery(spName,
                    emailAddress,
                    password,
                    salt,
                    hashedPassword);
        }

        

		#region Singleton instance
		public static MemberDAL Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
					lock (typeof(MemberDAL))
					{
						if (_instance == null)
						{
							_instance = new MemberDAL();
						}
					}
				}

				return _instance;
			}
			
		}

		#endregion

		private static MemberDAL _instance;
	
	}
}
