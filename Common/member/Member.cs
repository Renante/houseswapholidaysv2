using System;

namespace Common
{
	/// <summary>
	/// Summary description for Home.
	/// </summary>
	public class Member
	{
		public Member()
		{
		}

		public int ID
		{
			get
			{
				return _id;
			}
			set
			{
				_id = value;
			}
		}

		public string FirstName
		{
			get
			{
				return _firstName;
			}
			set
			{
				_firstName = value;
			}
		}

		public string GivenName
		{
			get
			{
				return _givenName;
			}
			set
			{
				_givenName = value;
			}
		}

		public string EmailAddress
		{
			get
			{
				return _emailAddress;
			}
			set
			{
				_emailAddress = value;
			}
		}

		public string Password
		{
			get
			{
				return _password;
			}
			set
			{
				_password = value;
			}
		}

        public string Salt { get; set; }

        public string HashedPassword { get; set; }

		public string Introduction
		{
			get
			{
				return _introduction;
			}
			set
			{
				_introduction = value;
			}
		}

		public string Language1
		{
			get
			{
				return _language1;
			}
			set
			{
				_language1 = value;
			}
		}

		public string Language2
		{
			get
			{
				return _language2;
			}
			set
			{
				_language2 = value;
			}
		}

		public string Language3
		{
			get
			{
				return _language3;
			}
			set
			{
				_language3 = value;
			}
		}

		public DateTime CreatedDate
		{
			get
			{
				return _createdDate;
			}
			set
			{
				_createdDate = value;
			}
		}

		public DateTime UpdatedDate
		{
			get
			{
				return _updatedDate;
			}
			set
			{
				_updatedDate = value;
			}
		}

		public DateTime LastLoggedInDate
		{
			get
			{
				return _lastLoggedInDate;
			}
			set
			{
				_lastLoggedInDate = value;
			}
		}

		public int ReferrerID
		{
			get
			{
				return _referrerID;
			}
			set
			{
				_referrerID = value;
			}
		}

		public int PromotionID
		{
			get
			{
				return _promotionID;
			}
			set
			{
				_promotionID = value;
			}
		}

		public int CountryID
		{
			get
			{
				return _countryID;
			}
			set
			{
				_countryID = value;
			}
		}

		public MemberType Type
		{
			get
			{
				return _type;
			}
			set
			{
				_type = value;
			}
		}

		public bool ReceiveNewsletter
		{
			get
			{
				return _receiveNewsletter;
			}
			set
			{
				_receiveNewsletter = value;
			}
		}

		public bool AcceptedTerms
		{
			get
			{
				return _acceptedTerms;
			}
			set
			{
				_acceptedTerms = value;
			}
		}

		public bool IsMultiChannel
		{
			get
			{
				return _isMultiChannel;
			}
			set
			{
				_isMultiChannel = value;
			}
		}

		public int DefaultChannel
		{
			get
			{
				return _defaultChannel;
			}
			set
			{
				_defaultChannel = value;
			}
		}

		public int HomeID
		{
			get
			{
				return _homeID;
			}
			set
			{
				_homeID = value;
			}
		}

		public int UnreadMessageCount
		{
			get
			{
				return _unreadMessageCount;
			}
			set
			{
				_unreadMessageCount = value;
			}
		}

		public bool AllowDestinationOffers
		{
			get
			{
				return _allowDestinationOffers;
			}
			set
			{
				_allowDestinationOffers = value;
			}
		}

		public bool AcceptSpecialConditions
		{
			get
			{
				return _acceptSpecialConditions;
			}
			set
			{
				_acceptSpecialConditions = value;
			}
		}

		public Status Status
		{
			get
			{
				return _status;
			}
			set
			{
				_status = value;
			}
		}

		public string Guid
		{
			get
			{
				return _guid;
			}
			set
			{
				_guid = value;
			}
		}

        public int UpdateMode { get; set; } //currently, used in conjunction with updatedetails.aspx.  if 0, redirect there.
        public int UpdateTravelDetailsMode { get; set; }    //similar in function to .UpdateMode, but used with updateTravelDetails.aspx

		private int _id;
		private string _firstName;
		private string _givenName;
		private string _emailAddress;
		private string _password;
		private string _introduction;
		private string _language1;
		private string _language2;
		private string _language3;
		private DateTime _createdDate; // TODO: Nothing happens to below fields
		private DateTime _updatedDate;
		private DateTime _lastLoggedInDate;
		private int _referrerID;
		private int _countryID;
		private bool _receiveNewsletter;
		private bool _acceptedTerms;
		private MemberType _type;
		private int _promotionID; // TODO: Integrate into DB

		private int _homeID = -1; // Used to save the homeid during the sign-up process

		// Channel information
		private bool _isMultiChannel;
		private int _defaultChannel;

		// Number of members unread messages
		private int _unreadMessageCount;

		private bool _allowDestinationOffers;
		private bool _acceptSpecialConditions;

		private Status _status;

		private string _guid;
	}
}
