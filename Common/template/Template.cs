using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Text;

using System.Web.Mail;

namespace Common
{
	/// <summary>
	/// Summary description for Template.
	/// </summary>
	public class Template
	{
		public Template()
		{
		}

		public string[,] GetElements(Member member)
		{
			// Get member property names 
			PropertyDescriptorCollection memberProperties =
				TypeDescriptor.GetProperties(member, true);

			// Create string array
			string[,] propertyNames = new string[memberProperties.Count,2];

			// Populate string array with property names
			int i=0;
			foreach(PropertyDescriptor selectedProperty in memberProperties)
			{
				propertyNames[i,0] = selectedProperty.Name;
				propertyNames[i,1] = "test"; //(member.(selectedProperty.Name));
				i++;
			}

			return propertyNames;
		}

		/// <summary>
		/// To convert a Byte Array of Unicode values (UTF-8 encoded) to a complete String.
		/// </summary>
		/// <param name="characters">Unicode Byte Array to be converted to String</param>
		/// <returns>String converted from Unicode Byte Array</returns>
		private String UTF8ByteArrayToString(Byte[] characters)
		{
			UTF8Encoding encoding = new UTF8Encoding();
			String constructedString = encoding.GetString(characters);
			return (constructedString);
		}

		/// <summary>
		/// Converts the String to UTF8 Byte array and is used in De serialization
		/// </summary>
		/// <param name="pXmlString"></param>
		/// <returns></returns>
		private Byte[] StringToUTF8ByteArray(String pXmlString)
		{
			UTF8Encoding encoding = new UTF8Encoding();
			Byte[] byteArray = encoding.GetBytes(pXmlString);
			return byteArray;
		}

		/// <summary>
		/// Method to convert a custom Object to XML string
		/// </summary>
		/// <param name="pObject">Object that is to be serialized to XML</param>
		/// <returns>XML string</returns>
		public String SerializeObject(Object pObject)
		{
			try
			{
				String XmlizedString = null;
				MemoryStream memoryStream = new MemoryStream();
				XmlSerializer xs = new XmlSerializer(typeof(Member));
				XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);

				xs.Serialize(xmlTextWriter, pObject);
				memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
				XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
				return XmlizedString;
			}
			catch (Exception e)
			{
				System.Console.WriteLine(e);
				return null;
			}
		}
	
	
	}
}
