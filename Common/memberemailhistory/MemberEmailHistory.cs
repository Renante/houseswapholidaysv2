using System;

namespace Common
{
	/// <summary>
	/// Summary description for Email.
	/// </summary>
    public class MemberEmailHistory
	{

		public MemberEmailHistory()
		{
		}

		public int MemberID
		{
			get
			{
				return _memberID;
			}
			set
			{
				_memberID = value;
			}
		}

        public int EmailTemplateID
        {
            get
            {
                return _emailTemplateID;
            }
            set
            {
                _emailTemplateID = value;
            }
        }


		#region Private instance variables
		
		private int _memberID;
        private int _emailTemplateID;
		
		#endregion Private instance variables
	
	
	}
}
