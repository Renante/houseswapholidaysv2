using System;
using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for EmailCollection.
	/// </summary>
	public class MemberEmailHistoryCollection : CollectionBase
	{
        public MemberEmailHistoryCollection()
		{
		}

        public void Add(MemberEmailHistory memberEmailHistory)
		{
            InnerList.Add(memberEmailHistory);
		}

        public void Remove(MemberEmailHistory memberEmailHistory)
		{
            InnerList.Remove(memberEmailHistory);
		}

        public MemberEmailHistory this[int index]
		{
			get
			{
                return (MemberEmailHistory)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}
	
	}
}
