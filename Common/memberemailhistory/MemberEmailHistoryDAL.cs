using System;
using System.Text;
using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;

namespace Common
{
	/// <summary>
	/// Summary description for EmailDAL.
	/// </summary>
	public class MemberEmailHistoryDAL : DataAccess
	{
        public MemberEmailHistoryDAL()
		{
		}

		/// <summary>
		/// Returns the matching blog based on the given blogcode, and the specified status
		/// </summary>
		/// <param name="blogCode"></param>
		/// <param name="status"></param>
		/// <returns></returns>
        public void Save(int memberID, int emailtemplateID)
        {
            // Add new blog entry
            const string spName = "up_InsertMemberEmailHistory";

            ExecuteNonQuery(spName, memberID, emailtemplateID);

        }

		#region Singleton instance
        public static MemberEmailHistoryDAL Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
                    lock (typeof(MemberEmailHistoryDAL))
					{
						if (_instance == null)
						{
                            _instance = new MemberEmailHistoryDAL();
						}
					}
				}

				return _instance;
			}
			
		}

		#endregion

        private static MemberEmailHistoryDAL _instance;

	
	}
}
