using System;

namespace Common
{
	/// <summary>
	/// Summary description for HomeFeature.
	/// </summary>
	public class HomeFeature
	{
		public HomeFeature()
		{
		}

		public int ID
		{
			get
			{
				return _id;
			}
			set
			{
				_id = value;
			}
		}

		public int HomeID
		{
			get
			{
				return _homeID;
			}
			set
			{
				_homeID = value;
			}
		}

		public int FeatureID
		{
			get
			{
				return _featureID;
			}
			set
			{
				_featureID = value;
			}
		}

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		public string Description
		{
			get
			{
				return _description;
			}
			set
			{
				_description = value;
			}
		}

		public int DisplayOrder
		{
			get
			{
				return _displayOrder;
			}
			set
			{
				_displayOrder = value;
			}
		}

		public int FeatureTypeID
		{
			get
			{
				return _featureTypeID;
			}
			set
			{
				_featureTypeID = value;
			}
		}

		public string ToDataListItem()
		{
			return String.Format("{0}|{1}|{2}", _homeID, _featureID, _featureTypeID);
		}

		private int _id;
		private int _homeID;
		private int _featureID;
		private string _name;
		private string _description;
		private int _displayOrder;
		private int _featureTypeID;
	
	}
}
