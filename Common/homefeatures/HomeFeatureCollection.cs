using System;
using System.Collections;
using System.Text;

namespace Common
{
    /// <summary>
    /// Summary description for HomeFeatureCollection.
    /// </summary>
    public class HomeFeatureCollection : CollectionBase
    {
        public HomeFeatureCollection()
        {
        }

        public void Add(HomeFeature homeFeature)
        {
            InnerList.Add(homeFeature);
        }

        public void Remote(HomeFeature homeFeature)
        {
            InnerList.Remove(homeFeature);
        }

        public HomeFeature this[int index]
        {
            get
            {
                return (HomeFeature)InnerList[index];
            }
            set
            {
                InnerList[index] = value;
            }
        }

        public string ToDataList()
        {
            StringBuilder sb = new StringBuilder();
            foreach (HomeFeature feature in InnerList)
            {
                sb.Append(feature.ToDataListItem());
                sb.Append(",");
            }

            return sb.ToString();
        }

        public void AddCollection(HomeFeatureCollection toAdd)
        {
            for (int i = 0; i < toAdd.Count; i++)
            {
                this.Add(toAdd[i]);
            }
        }
    }
}
