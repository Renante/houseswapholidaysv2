using System;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Common
{
	/// <summary>
	/// Summary description for HomeFeatureDAL.
	/// </summary>
	public class HomeFeatureDAL : DataAccess
	{

		#region Types
		private sealed class HomeFeatureFields
		{
			public readonly int ID = -1;
			public readonly int HomeID = -1;
			public readonly int FeatureID = -1;
			public readonly int Name = -1;
			public readonly int Description = -1;
			public readonly int DisplayOrder = -1;
			public readonly int FeatureTypeID = -1;

		}
		#endregion

		public HomeFeatureDAL()
		{
		}

		public void DeleteForHome(int homeID)
		{
			const string spName = "up_DeleteHomeFeatures";

			ExecuteNonQuery(spName, homeID);
		}

	    public void DeleteForHomeByFeatureType(int homeID, int featureType)
	    {
            const string spName = "up_DeleteHomeFeaturesByFeatureType";

            ExecuteNonQuery(spName, homeID, featureType);
	    }

		/*
		public void Save(HomeFeature homeFeature)
		{
			// Add new home
			const string spName = "up_InsertHomeFeature";

			ExecuteNonQuery(spName, 
						   homeFeature.HomeID, homeFeature.FeatureID, homeFeature.FeatureTypeID);
				

			//if (reader.Read())
			//{
			//	home.ID = reader.GetInt32(reader.GetOrdinal("HomeID"));
			//}
			//return home;
		}
		*/

		public void SaveCollection(HomeFeatureCollection collection)
		{
			const string spName = "up_InsertHomeFeatureList";

			ExecuteNonQuery(spName, collection.ToDataList());
		}

		public HomeFeatureCollection GetFromCacheByFeatureType(HomeFeatureCollection homeFeatureCollection, int featureTypeID)
		{
			HomeFeatureCollection filteredFeatureCollection = new HomeFeatureCollection();

			foreach (HomeFeature homeFeature in homeFeatureCollection)
			{
				if (homeFeature.FeatureTypeID == featureTypeID)
				{
					filteredFeatureCollection.Add(homeFeature);
				}
			}

			return filteredFeatureCollection;
		}

		public HomeFeatureCollection GetAllForHome(int homeID)
		{
			HomeFeatureCollection homeFeatureCollection = new HomeFeatureCollection();

			const string spName = "up_GetHomeFeatures";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, homeID))
                {

                    while (reader.Read())
                    {
                        HomeFeature homeFeature = new HomeFeature();

                        homeFeature.ID = (int)reader["HomeFeatureID"];
                        homeFeature.HomeID = (int)reader["HomeID"];
                        homeFeature.FeatureID = (int)reader["FeatureID"];
                        homeFeature.Name = (string)reader["Name"];
                        homeFeature.Description = (string)reader["Description"];

                        if (reader["DisplayOrder"] == DBNull.Value)
                        {
                            homeFeature.DisplayOrder = 0;
                        }
                        else
                        {
                            homeFeature.DisplayOrder = (int)reader["DisplayOrder"];
                        }

                        if (reader["FeatureTypeID"] == DBNull.Value)
                        {
                            homeFeature.FeatureTypeID = 0;
                        }
                        else
                        {
                            homeFeature.FeatureTypeID = (int)reader["FeatureTypeID"];
                        }

                        homeFeatureCollection.Add(homeFeature);
                    }
                } 
            }

			return homeFeatureCollection;
		}


		private HomeFeature LoadFromReader(SqlDataReader reader, HomeFeatureFields fields)
		{
			HomeFeature homeFeature = new HomeFeature();

			homeFeature.ID = reader.GetInt32(fields.ID);
			homeFeature.HomeID = reader.GetInt32(fields.HomeID);
			homeFeature.FeatureID = reader.GetInt32(fields.FeatureID);
			homeFeature.Name = reader.GetString(fields.Name);
			homeFeature.Description = reader.GetString(fields.Description);
			homeFeature.DisplayOrder = reader.GetInt32(fields.DisplayOrder);
			homeFeature.FeatureTypeID = reader.GetInt32(fields.FeatureTypeID);

			return homeFeature;
		}

		#region Singleton instance
		public static HomeFeatureDAL Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
					lock (typeof(HomeFeatureDAL))
					{
						if (_instance == null)
						{
							_instance = new HomeFeatureDAL();
						}
					}
				}

				return _instance;
			}
			
		}

		#endregion

		private static HomeFeatureDAL _instance;



	}
}
