using System;

namespace Common {
	public interface IRules {
		string Execute(System.Web.HttpApplication Appl, string Path, string SettingsSection);
	}
}
