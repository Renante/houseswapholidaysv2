using System;
using System.Web;
using System.Diagnostics;

namespace Common
{
	public class Rewrite : System.Web.IHttpModule 
	{

		/// <summary>
		/// Init is required from the IHttpModule interface
		/// </summary>
		/// <param name="Appl"></param>
		public void Init(System.Web.HttpApplication Appl) 
		{
			//make sure to wire up to BeginRequest
			Appl.BeginRequest+=new System.EventHandler(Rewrite_BeginRequest);
		}

		/// <summary>
		/// Dispose is required from the IHttpModule interface
		/// </summary>
		public void Dispose() 
		{
			//make sure you clean up after yourself
		}

		/// <summary>
		/// To handle the starting of the incoming request
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>
		public void Rewrite_BeginRequest(object sender, System.EventArgs args) 
		{
			//process rules here

			//cast the sender to a HttpApplication object
			System.Web.HttpApplication Appl=(System.Web.HttpApplication)sender;

			//load up the rules engine
			Common.Engine e = new Common.Engine(Appl);
			string r = e.Execute();		

			//only redirect if we have too
			System.Console.WriteLine("testing...");
			
			if(r!="" && r.ToLower()!=Common.Engine.Getpath(Appl).ToLower()) SendToNewUrl(r, Appl);


		}

		public void SendToNewUrl(string url, System.Web.HttpApplication Appl) 
		{
			Appl.Context.RewritePath(@"/search/browseby.aspx","/",@"test=hello");
		}


	}
}
