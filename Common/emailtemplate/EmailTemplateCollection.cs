using System;
using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for EmailCollection.
	/// </summary>
	public class EmailTemplateCollection : CollectionBase
	{
		public EmailTemplateCollection()
		{
		}

		public void Add(EmailTemplate emailTemplate)
		{
			InnerList.Add(emailTemplate);
		}

		public void Remove(EmailTemplate emailTemplate)
		{
			InnerList.Remove(emailTemplate);
		}

		public EmailTemplate this[int index]
		{
			get
			{
				return (EmailTemplate)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}
	
	}
}
