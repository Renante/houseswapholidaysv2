using System;
using System.Text;
using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;

namespace Common
{
    /// <summary>
    /// Summary description for EmailDAL.
    /// </summary>
    public class EmailTemplateDAL : DataAccess
    {
        public EmailTemplateDAL()
        {
        }

        /// <summary>
        /// Returns the matching blog based on the given blogcode, and the specified status
        /// </summary>
        /// <param name="blogCode"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public EmailTemplate Get(int emailTemplateID, int channelID)
        {
            const string spName = "up_GetEmailTemplate";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, emailTemplateID, channelID))
                {

                    if (reader.Read())
                    {
                        EmailTemplate emailTemplate = new EmailTemplate();

                        emailTemplate.ID = GetFieldByInteger(reader, "ID");
                        emailTemplate.ChannelID = GetFieldByInteger(reader, "ChannelID");
                        emailTemplate.Code = GetFieldByString(reader, "Code");
                        emailTemplate.IsTemplate = GetFieldByBoolean(reader, "Istemplate");
                        emailTemplate.IsEmail = GetFieldByBoolean(reader, "IsEmail");
                        emailTemplate.Name = GetFieldByString(reader, "Name");
                        emailTemplate.Subject = GetFieldByString(reader, "Subject");
                        emailTemplate.Body = GetFieldByString(reader, "Body");
                        emailTemplate.FromDate = GetFieldByDate(reader, "FromDate");
                        emailTemplate.ThruDate = GetFieldByDate(reader, "ThruDate");
                        emailTemplate.Status = (Status)GetFieldByInteger(reader, "Status");

                        return emailTemplate;
                    }
                    else
                    {
                        throw new ApplicationException(String.Format("Cannot get a email template with ID {0}, Channel {1}, the email cannot be found in the database.", emailTemplateID, channelID));
                        // return null;
                    }
                }
            }
        }

        public EmailTemplateCollection GetAll(int channelID)
        {
            EmailTemplateCollection emailTemplateCollection = new EmailTemplateCollection();

            const string spName = "up_GetAllEmailTemplates";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, channelID))
                {
                    while (reader.Read())
                    {
                        EmailTemplate emailTemplate = new EmailTemplate();

                        emailTemplate.ID = GetFieldByInteger(reader, "ID");
                        emailTemplate.ChannelID = GetFieldByInteger(reader, "ChannelID");
                        emailTemplate.Code = GetFieldByString(reader, "Code");
                        emailTemplate.IsTemplate = GetFieldByBoolean(reader, "Istemplate");
                        emailTemplate.IsEmail = GetFieldByBoolean(reader, "IsEmail");
                        emailTemplate.Name = GetFieldByString(reader, "Name");
                        emailTemplate.Subject = GetFieldByString(reader, "Subject");
                        emailTemplate.Body = GetFieldByString(reader, "Body");
                        emailTemplate.FromDate = GetFieldByDate(reader, "FromDate");
                        emailTemplate.ThruDate = GetFieldByDate(reader, "ThruDate");
                        emailTemplate.Status = (Status)GetFieldByInteger(reader, "Status");

                        emailTemplateCollection.Add(emailTemplate);
                    }
                }
            }

            return emailTemplateCollection;
        }

        #region Singleton instance
        public static EmailTemplateDAL Instance
        {
            [DebuggerStepThrough]
            get
            {
                if (_instance == null)
                {
                    lock (typeof(EmailTemplateDAL))
                    {
                        if (_instance == null)
                        {
                            _instance = new EmailTemplateDAL();
                        }
                    }
                }

                return _instance;
            }

        }

        #endregion

        private static EmailTemplateDAL _instance;


    }
}
