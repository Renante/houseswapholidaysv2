using System;

namespace Common
{
	/// <summary>
	/// Summary description for Feature.
	/// </summary>
	public class Promotion
	{
		public Promotion()
		{
		}

		public int ID
		{
			get
			{
				return _id;
			}
			set
			{
				_id = value;
			}
		}

		public string Code
		{
			get
			{
				return _code;
			}
			set
			{
				_code = value;
			}
		}

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		public string Description
		{
			get
			{
				return _description;
			}
			set
			{
				_description = value;
			}
		}

		public string HomePageCopy
		{
			get
			{
				return _homePageCopy;
			}
			set
			{
				_homePageCopy = value;
			}
		}

		public string BrowseDestinationsCopy
		{
			get
			{
				return _browseDestinationsCopy;
			}
			set
			{
				_browseDestinationsCopy = value;
			}
		}

		public string ViewHomeCopy
		{
			get
			{
				return _viewHomeCopy;
			}
			set
			{
				_viewHomeCopy = value;
			}
		}

		public int AuthorID
		{
			get
			{
				return _authorID;
			}
			set
			{
				_authorID = value;
			}
		}

		public int BlogID
		{
			get
			{
				return _blogID;
			}
			set
			{
				_blogID = value;
			}
		}

		public bool AllowNewMembers
		{
			get
			{
				return _allowNewMembers;
			}
			set
			{
				_allowNewMembers = value;
			}
		}

		public bool AllowRenewalMembers
		{
			get
			{
				return _allowRenewalMembers;
			}
			set
			{
				_allowRenewalMembers = value;
			}
		}

		public Decimal NewMemberRate
		{
			get
			{
				return _newMemberRate;
			}
			set
			{
				_newMemberRate = value;
			}
		}

		public Decimal RenewalMemberRate
		{
			get
			{
				return _renewalMemberRate;
			}
			set
			{
				_renewalMemberRate = value;
			}
		}

		public int SubscriptionPeriod
		{
			get
			{
				return _subscriptionPeriod;
			}
			set
			{
				_subscriptionPeriod = value;
			}
		}

		public int MaximumMembers
		{
			get
			{
				return _maximumMembers;
			}
			set
			{
				_maximumMembers = value;
			}
		}

		public int RedeemedMembers
		{
			get
			{
				return _redeemedMembers;
			}
			set
			{
				_redeemedMembers = value;
			}
		}


		public DateTime FromDate
		{
			get
			{
				return _fromDate;
			}
			set
			{
				_fromDate = value;
			}
		}

		public DateTime ThruDate
		{
			get
			{
				return _thruDate;
			}
			set
			{
				_thruDate = value;
			}
		}

		public DateTime CreatedDate
		{
			get
			{
				return _createdDate;
			}
			set
			{
				_createdDate = value;
			}
		}

		public int Status
		{
			get
			{
				return _status;
			}
			set
			{
				_status = value;
			}
		}

		private int _id;
		private string _code;
		private string _name;
		private string _description;
		private string _homePageCopy;
		private string _browseDestinationsCopy;
		private string _viewHomeCopy;
		private int _authorID;
		private int _blogID;
		private bool _allowNewMembers;
		private bool _allowRenewalMembers;
		private decimal _newMemberRate;
		private decimal _renewalMemberRate;
		private int _subscriptionPeriod;
		private int _maximumMembers;
		private int _redeemedMembers;
		private DateTime _fromDate;
		private DateTime _thruDate;
		private DateTime _createdDate;
		private int _status;


	}
}
