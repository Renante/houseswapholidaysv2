using System;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Common
{
    /// <summary>
    /// Summary description for FeatureDAL.
    /// </summary>
    public class PromotionDAL : DataAccess
    {

        public PromotionDAL()
        {
        }

        public Promotion Get(int promotionID)
        {
            const string spName = "up_GetPromotion";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, promotionID))
                {
                    if (reader.Read())
                    {
                        Promotion promotion = new Promotion();

                        promotion.ID = GetFieldByInteger(reader, "PromotionID");
                        promotion.Code = GetFieldByString(reader, "Code");
                        promotion.Name = GetFieldByString(reader, "Name");
                        promotion.Description = GetFieldByString(reader, "Description");
                        promotion.HomePageCopy = GetFieldByString(reader, "HomePageCopy");
                        promotion.BrowseDestinationsCopy = GetFieldByString(reader, "BrowseDestinationsCopy");
                        promotion.ViewHomeCopy = GetFieldByString(reader, "ViewHomeCopy");
                        promotion.AuthorID = GetFieldByInteger(reader, "AuthorID");
                        promotion.BlogID = GetFieldByInteger(reader, "BlogID");
                        promotion.AllowNewMembers = GetFieldByBoolean(reader, "AllowNewMembers");
                        promotion.AllowRenewalMembers = GetFieldByBoolean(reader, "AllowRenewalMembers");
                        promotion.NewMemberRate = GetFieldByDecimal(reader, "NewMemberRate");
                        promotion.RenewalMemberRate = GetFieldByDecimal(reader, "RenewalMemberRate");
                        promotion.SubscriptionPeriod = GetFieldByInteger(reader, "SubscriptionPeriod");
                        promotion.MaximumMembers = GetFieldByInteger(reader, "MaximumMembers");
                        promotion.RedeemedMembers = GetFieldByInteger(reader, "RedeemedMembers");
                        promotion.FromDate = GetFieldByDate(reader, "FromDate");
                        promotion.ThruDate = GetFieldByDate(reader, "ThruDate");
                        promotion.CreatedDate = GetFieldByDate(reader, "CreatedDate");
                        promotion.Status = GetFieldByInteger(reader, "Status");

                        // reader.Close();

                        return promotion;
                    }
                    else
                    {
                        // reader.Close();

                        return null;
                    }
                }
            }
        }

        public Promotion Get(string promotionCode)
        {
            const string spName = "up_GetPromotionByCode";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, promotionCode))
                {
                    if (reader.Read())
                    {
                        Promotion promotion = new Promotion();

                        promotion.ID = GetFieldByInteger(reader, "PromotionID");
                        promotion.Code = GetFieldByString(reader, "Code");
                        promotion.Name = GetFieldByString(reader, "Name");
                        promotion.Description = GetFieldByString(reader, "Description");
                        // promotion.HomePageCopy = GetFieldByString(reader,"HomePageCopy"); // wooncherk
                        // promotion.BrowseDestinationsCopy = GetFieldByString(reader,"BrowseDestinationsCopy"); // wooncherk
                        // promotion.ViewHomeCopy = GetFieldByString(reader,"ViewHomeCopy"); // wooncherk
                        promotion.AuthorID = GetFieldByInteger(reader, "AuthorID");
                        promotion.BlogID = GetFieldByInteger(reader, "BlogID");
                        promotion.AllowNewMembers = GetFieldByBoolean(reader, "AllowNewMembers");
                        promotion.AllowRenewalMembers = GetFieldByBoolean(reader, "AllowRenewalMembers");
                        promotion.NewMemberRate = GetFieldByDecimal(reader, "NewMemberRate");
                        promotion.RenewalMemberRate = GetFieldByDecimal(reader, "RenewalMemberRate");
                        promotion.SubscriptionPeriod = GetFieldByInteger(reader, "SubscriptionPeriod");
                        promotion.MaximumMembers = GetFieldByInteger(reader, "MaximumMembers");
                        promotion.RedeemedMembers = GetFieldByInteger(reader, "RedeemedMembers");
                        promotion.FromDate = GetFieldByDate(reader, "FromDate");
                        promotion.ThruDate = GetFieldByDate(reader, "ThruDate");
                        promotion.CreatedDate = GetFieldByDate(reader, "CreatedDate");
                        promotion.Status = GetFieldByInteger(reader, "Status");

                        // reader.Close();

                        return promotion;
                    }
                    else
                    {
                        // reader.Close();

                        return null;
                    }
                }
            }
        }

        public PromotionCollection GetAll(string code, string name)
        {
            PromotionCollection promotionCollection = new PromotionCollection();

            const string spName = "up_GetAllPromotions";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, code, name))
                {
                    while (reader.Read())
                    {
                        Promotion promotion = new Promotion();

                        promotion.ID = GetFieldByInteger(reader, "PromotionID");
                        promotion.Code = GetFieldByString(reader, "Code");
                        promotion.Name = GetFieldByString(reader, "Name");
                        promotion.Description = GetFieldByString(reader, "Description");
                        promotion.AuthorID = GetFieldByInteger(reader, "AuthorID");
                        promotion.BlogID = GetFieldByInteger(reader, "BlogID");
                        promotion.AllowNewMembers = GetFieldByBoolean(reader, "AllowNewMembers");
                        promotion.AllowRenewalMembers = GetFieldByBoolean(reader, "AllowRenewalMembers");
                        promotion.NewMemberRate = GetFieldByDecimal(reader, "NewMemberRate");
                        promotion.RenewalMemberRate = GetFieldByDecimal(reader, "RenewalMemberRate");
                        promotion.SubscriptionPeriod = GetFieldByInteger(reader, "SubscriptionPeriod");
                        promotion.MaximumMembers = GetFieldByInteger(reader, "MaximumMembers");
                        promotion.RedeemedMembers = GetFieldByInteger(reader, "RedeemedMembers");
                        promotion.FromDate = GetFieldByDate(reader, "FromDate");
                        promotion.ThruDate = GetFieldByDate(reader, "ThruDate");
                        promotion.CreatedDate = GetFieldByDate(reader, "CreatedDate");
                        promotion.Status = GetFieldByInteger(reader, "Status");

                        promotionCollection.Add(promotion);
                    }

                    // reader.Close();
                }
            }

            return promotionCollection;
        }

        public Promotion Save(Promotion promotion)
        {
            if (promotion.ID <= 0)
            {
                // Add new promotion
                const string spName = "up_InsertPromotion";

                promotion.ID = (int)ExecuteScalar(spName,
                           promotion.Code, promotion.Name,
                           promotion.Description, promotion.AuthorID,
                           promotion.BlogID, promotion.AllowNewMembers,
                           promotion.AllowRenewalMembers, promotion.NewMemberRate,
                           promotion.RenewalMemberRate, promotion.SubscriptionPeriod,
                           promotion.MaximumMembers, promotion.FromDate,
                           promotion.ThruDate, promotion.CreatedDate, promotion.Status);
            }
            else
            {
                // Update existing promotion
                const string spName = "up_UpdatePromotion";

                ExecuteNonQuery(spName,
                           promotion.ID, promotion.Code, promotion.Name,
                           promotion.Description, promotion.AuthorID,
                           promotion.BlogID, promotion.AllowNewMembers,
                           promotion.AllowRenewalMembers, promotion.NewMemberRate,
                           promotion.RenewalMemberRate, promotion.SubscriptionPeriod,
                           promotion.MaximumMembers, promotion.FromDate,
                           promotion.ThruDate, promotion.CreatedDate, promotion.Status);
            }

            return promotion;
        }

        #region Singleton instance
        public static PromotionDAL Instance
        {
            [DebuggerStepThrough]
            get
            {
                if (_instance == null)
                {
                    lock (typeof(PromotionDAL))
                    {
                        if (_instance == null)
                        {
                            _instance = new PromotionDAL();
                        }
                    }
                }

                return _instance;
            }

        }

        #endregion

        private static PromotionDAL _instance;

    }
}