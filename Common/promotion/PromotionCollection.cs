using System;
using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for HomeCollection.
	/// </summary>
	public class PromotionCollection : CollectionBase
	{
		public PromotionCollection()
		{
		}

		public void Add(Promotion promotion)
		{
			InnerList.Add(promotion);
		}

		public void Remote(Promotion promotion)
		{
			InnerList.Remove(promotion);
		}

		public Promotion this[int index]
		{
			get
			{
				return (Promotion)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}

	}
}
