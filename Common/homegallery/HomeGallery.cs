using System;

namespace Common
{
	/// <summary>
	/// Summary description for Home.
	/// </summary>
	public class HomeGallery
	{
		public HomeGallery()
		{
		}

		public int HomeID
		{
			get
			{
				return _homeID;
			}
			set
			{
				_homeID = value;
			}
		}

		public string Location
		{
			get
			{
				return _location;
			}
			set
			{
				_location = value;
			}
		}

		public string ImageURL
		{
			get
			{
				return _imageURL;
			}
			set
			{
				_imageURL = value;
			}
		}

		public Status Status
		{
			get
			{
				return _status;
			}
			set
			{
				_status = value;
			}
		}

		public int Rank
		{
			get
			{
				return _rank;
			}
			set
			{
				_rank = value;
			}
		}

		public string Description
		{
			get
			{
				return _description;
			}
			set
			{
				_description = value;
			}
		}

        public string Summary
        {
            get
            {
                return _summary;
            }
            set
            {
                _summary = value;
            }
        }

		#region private fields
		
		private int _homeID;
		private string _location;
		private string _imageURL;
		private string _description;
        private string _summary;
		private Status _status;
		private int _rank;

		#endregion
	}
}
