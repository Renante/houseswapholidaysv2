using System;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;

namespace Common
{
	/// <summary>
	/// Summary description for HomeDAL.
	/// </summary>
	public class HomeGalleryDAL : DataAccess
	{

		public HomeGalleryDAL()
		{
		}

		public HomeGalleryCollection GetRandomFeaturedHome(int channelID, int galleryTypeID)
		{
			HomeGalleryCollection homeGalleryCollection = new HomeGalleryCollection();

			const string spName = "up_GetHomeGalleryRandom";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, channelID, galleryTypeID))
                {
                    while (reader.Read())
                    {
                        HomeGallery homeGallery = new HomeGallery();


                        homeGallery.HomeID = GetFieldByInteger(reader, "HomeID");
                        homeGallery.Location = GetFieldByString(reader, "Location");
                        homeGallery.ImageURL = GetFieldByString(reader, "ImageURL");
                        homeGallery.Description = GetFieldByString(reader, "Description");

                        // Add to collection
                        homeGalleryCollection.Add(homeGallery);
                    }
                } 
            }

			return homeGalleryCollection;
		}

		public HomeGalleryCollection GetAll(int numberToDisplay)
		{
			HomeGalleryCollection homeGalleryCollection = new HomeGalleryCollection();

			const string spName = "up_GetHomeGallery";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, numberToDisplay))
                {
                    while (reader.Read())
                    {
                        HomeGallery homeGallery = new HomeGallery();


                        homeGallery.HomeID = GetFieldByInteger(reader, "HomeID");
                        homeGallery.Location = GetFieldByString(reader, "Location");
                        homeGallery.ImageURL = GetFieldByString(reader, "ImageURL");
                        homeGallery.Status = (Status)reader["Status"];

                        // Add to collection
                        homeGalleryCollection.Add(homeGallery);
                    }
                } 
            }

			return homeGalleryCollection;
		}

		public HomeGalleryCollection GetAll(int numberToDisplay, int channelID, int galleryTypeID)
		{
			HomeGalleryCollection homeGalleryCollection = new HomeGalleryCollection();

			const string spName = "up_GetHomeGalleryByChannel";

            using (var connection = GetConnection())
            {
                using (SqlDataReader reader = ExecuteReader(spName, connection, numberToDisplay, channelID, galleryTypeID))
                {
                    while (reader.Read())
                    {
                        HomeGallery homeGallery = new HomeGallery();


                        homeGallery.HomeID = GetFieldByInteger(reader, "HomeID");
                        homeGallery.Location = GetFieldByString(reader, "Location");
                        homeGallery.ImageURL = GetFieldByString(reader, "ImageURL");
                        homeGallery.Summary = GetFieldByString(reader, "Summary");
                        homeGallery.Status = (Status)reader["Status"];

                        // Add to collection
                        homeGalleryCollection.Add(homeGallery);
                    }
                } 
            }

			return homeGalleryCollection;
		}

		#region Singleton instance
		public static HomeGalleryDAL Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (_instance == null)
				{
					lock (typeof(HomeGalleryDAL))
					{
						if (_instance == null)
						{
							_instance = new HomeGalleryDAL();
						}
					}
				}

				return _instance;
			}
			
		}

		#endregion

		private static HomeGalleryDAL _instance;

	}
}
