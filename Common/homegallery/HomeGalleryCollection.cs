using System;
using System.Collections;

namespace Common
{
	/// <summary>
	/// Summary description for HomeCollection.
	/// </summary>
	public class HomeGalleryCollection : CollectionBase
	{
		public HomeGalleryCollection()
		{
		}

		public void Add(HomeGallery homeGallery)
		{
			InnerList.Add(homeGallery);
		}

		public void Remote(HomeGallery homeGallery)
		{
			InnerList.Remove(homeGallery);
		}

		public HomeGallery this[int index]
		{
			get
			{
				return (HomeGallery)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}

	}
}
