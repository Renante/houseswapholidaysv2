﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using System.Web;
using System.Configuration;

namespace ChristianHomeSwap.Core.App_Code
{
    public class BasePage
    {
        protected string SiteUrl
        {
            get { return ConfigurationSettings.AppSettings["Home_SiteURL"]; }
        }

        protected bool IsLoggedIn
        {
            get
            {
                try
                {
                    return (HouseSwapHolidays.Core.Services.MemberService.Instance.GetCurrentMember()) != null;
                }
                catch { /*do nothing */ }
                return false;
            }
        }

        protected bool UrlReferrerEndsWith(string[] paths)
        {
            foreach (string s in paths)
                if (UrlReferrerEndsWith(s)) return true;

            return false;
        }

        protected bool UrlReferrerEndsWith(string path)
        {
            return HttpContext.Current.Request.UrlReferrer != null
                && HttpContext.Current.Request.UrlReferrer.LocalPath.ToLower().EndsWith(path);
        }

        public int GetChannelID()
        {
            return Int32.Parse(ConfigurationSettings.AppSettings["ChannelID"]);
        }

        private Channel GetCurrentChannelByUrl()
        {
            Channel channel = ChannelDAL.Instance.Get(String.Format("http://www.{0}", HttpContext.Current.Request.Url.Authority));

            if (channel == null)
            {
                return null;
            }
            else
            {
                return channel;
            }
        }

        public string getPageContent(string blogCode)
        {
            Blog blog = BlogDAL.Instance.Get(blogCode, true, true, GetChannelID());

            if (blog != null)
                return blog.Body;
            else
                return "Undefined element";
        }
    }
}
