﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ChristianHomeSwap.Core.App_Code
{
    public class RegistrationPage
    {
        protected bool IsInRegistrationMode()
        {
            return HttpContext.Current.Session["reg"] != null;
            
        }

        protected void StartRegistrationMode()
        {
            HttpContext.Current.Session["reg"] = new object();
        }

        public void EndRegistrationMode()
        {
            HttpContext.Current.Session["reg"] = null;
        }
    }
}
