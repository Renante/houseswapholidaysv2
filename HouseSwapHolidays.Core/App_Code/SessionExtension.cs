﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

/// <summary>
/// Summary description for SessionExtensions
/// </summary>
public static class SessionExtensions
{
    public static bool IsUserAusOrNz(this HttpSessionState session)
    {
        return session["__IsUserAusOrNz"] != null && ((bool)session["__IsUserAusOrNz"]);
    }

    public static void SetUserAusOrNzFlag(this HttpSessionState session, bool value)
    {
        session["__IsUserAusOrNz"] = value;
    }

    public static void SetUserFreeMembership(this HttpSessionState session, bool value)
    {
        session["__IsUserFreeMembership"] = value;
    }
    public static bool IsUserFreeMembership(this HttpSessionState session)
    {
        return session["__IsUserFreeMembership"] != null && (bool)session["__IsUserFreeMembership"];
    }

    public static void SetCountryCode(this HttpSessionState session, string value)
    {
        session["__CountryCode"] = value;
    }
    public static string GetCountryCode(this HttpSessionState session)
    {
        return session["__CountryCode"] != null ? session["__CountryCode"].ToString().ToLower() : "";
    }
}