﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using MaxMind.GeoIP2;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web;
using Umbraco.Core.Logging;
using Common;
using System.Net;
using System.IO;
using Newtonsoft.Json;


namespace HouseSwapHolidays.Core
{
    public class GeoLocation
    {
        public static GeoLocationResult GetGeolocationOfIp(int[] ipaddress)
        {
            IPAddress ipa;

            bool invalidParam = false;
            if (ipaddress.Length != 4)
            {
                invalidParam = true;
            }
            foreach (int ip in ipaddress)
            {
                if (ip < 0 || ip > 255)
                {
                    invalidParam = true;
                    break;
                }
            }

            if (invalidParam)
            {
                throw new ArgumentException("Invalid IP Address");
            }


            try
            {
                var req =
                    HttpWebRequest.Create(string.Format("http://api.hostip.info/get_json.php?ip={0}", ToIPString(ipaddress)));
                req.ContentType = "application/json";
                req.Method = "GET";
                using (var resp = req.GetResponse())
                {
                    StreamReader sr = new StreamReader(resp.GetResponseStream());
                    return JsonConvert.DeserializeObject<GeoLocationResult>(sr.ReadToEnd());
                }
            }
            catch (Exception ex)
            {
                //_log.ErrorFormat("Geolocation.GeetGeolocationOfIp: {0}; {1}", ToIPString(ipaddress), ex.Message);
                return null;
            }
        }

        private static string ToIPString(int[] ip)
        {
            return string.Format("{0}.{1}.{2}.{3}", ip[0], ip[1], ip[2], ip[3]);
        }

        public static string GetCountryCode(string ipaddress)
        {
            if (HttpContext.Current.Request.Browser.Crawler)
            {
                return "";
            }
            int u_maxmind = Convert.ToInt32(WebConfigurationManager.AppSettings["MaxMind_Username"]);
            string p_maxmind = WebConfigurationManager.AppSettings["MaxMind_Password"];
            var client = new WebServiceClient(u_maxmind, p_maxmind);
            try
            {
                LogHelper.Info(typeof (GeoLocation), "Maxmind API triggered!");
                return client.Country(ipaddress).Country.IsoCode.ToLower();
            }
            catch (Exception ex)
            {
                LogHelper.Error(typeof (GeoLocation), "Maxmind error: " + ex.Message, ex);
                return "";
            }
        }

        public static bool isFreeMembershipCountry()
        {
            List<string> freeCountries = HomeDAL.Instance.GetFreeMembershipCountry();
            try
            {
                foreach (string code in freeCountries)
                {
                    if (HttpContext.Current.Session.GetCountryCode() == code.ToLower())
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public static bool IsAusOrNZ(int[] ipaddress)
        {
            GeoLocationResult result = GetGeolocationOfIp(ipaddress);
            return result != null && !string.IsNullOrWhiteSpace(result.country_name) &&
                (result.country_name.ToUpper().Equals("AUSTRALIA")
                || result.country_name.ToUpper().Equals("NEW ZEALAND"));
        }

        public static bool IsAusOrNZ()
        {
            try
            {
                return HttpContext.Current.Session.GetCountryCode() == "au" || HttpContext.Current.Session.GetCountryCode() == "nz";
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }

    public class GeoLocationResult
    {
        public string country_name { get; set; }
        public string country_code { get; set; }
        public string city { get; set; }
        public string ip { get; set; }

    }

}
