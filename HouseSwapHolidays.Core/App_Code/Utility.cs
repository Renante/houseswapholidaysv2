﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ChristianHomeSwap.Core.App_Code
{
    public class Utility
    {
        public static string DisplayLocationText(string localText, string internationalText)
        {
            return HttpContext.Current.Session.IsUserAusOrNz() ? localText : internationalText;
        }
    }
}
