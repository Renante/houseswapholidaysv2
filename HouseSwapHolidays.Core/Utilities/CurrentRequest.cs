﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Web;

namespace HouseSwapHolidays.Core.Utilities
{
    public class CurrentRequest
    {
        public const string UmbracoHelperKey = "HSH.UmbracoHelperRequest";

        public static UmbracoHelper GetUmbracoHelper()
        {
            return CacheManager.GetRequestCacheItem("Eb.UmbracoHelperRequest",
                () => new UmbracoHelper(UmbracoContext.Current));
        }
    }
}
