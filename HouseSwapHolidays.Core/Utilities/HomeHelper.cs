﻿using HouseSwapHolidays.Core.Models.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Utilities
{
    public static class HomeHelper
    {
        public static bool HasContactedAlready(int homeId, int memberId)
        {
            RLK797_HomeExchangeEntities _db = new RLK797_HomeExchangeEntities();

            return _db.Database.SqlQuery<int>(
                    "SELECT TOP 1 HomeEnquiryID FROM HomeEnquiry WHERE HomeID = {0} AND SenderMemberID = {1}", homeId,
                    memberId).Any();
        }

        public static bool IsFavoriteHome(int homeId, int memberId)
        {
            RLK797_HomeExchangeEntities _db = new RLK797_HomeExchangeEntities();

            return
                _db.Database.SqlQuery<int>("SELECT TOP 1 MemberShortlistID FROM MemberShortlist WHERE HomeID = {0} AND MemberID = {1}", homeId,
                    memberId).Any();
        }
    }
}
