﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core;
using Umbraco.Core.Cache;

namespace HouseSwapHolidays.Core.Utilities
{
    public class CacheManager
    {
        public static T GetCacheItem<T>(string cacheKey, Func<T> getCacheItem, TimeSpan? timespan, string[] dependentFiles)
        {
            return ApplicationContext.Current.ApplicationCache
                .RuntimeCache.GetCacheItem(cacheKey, getCacheItem, timespan, dependentFiles: dependentFiles);
        }

        public static T GetCacheItem<T>(string cacheKey, Func<T> getCacheItem, TimeSpan? timespan)
        {
            return ApplicationContext.Current.ApplicationCache
                .RuntimeCache.GetCacheItem(cacheKey, getCacheItem, timespan);
        }

        public static T GetCacheItem<T>(string cacheKey)
        {
            return ApplicationContext.Current.ApplicationCache.RuntimeCache.GetCacheItem<T>(cacheKey);
        }

        public static void InsertCacheItem(object obj, string cacheKey, TimeSpan? timespan, string[] dependentFiles)
        {
            ApplicationContext.Current.ApplicationCache
                .RuntimeCache.InsertCacheItem(cacheKey, () => obj, timespan, dependentFiles: dependentFiles);
        }

        public static void ClearCacheByKeySearch(string keyStartsWith)
        {
            ApplicationContext.Current.ApplicationCache.RuntimeCache.ClearCacheByKeySearch(keyStartsWith);
        }

        public static void ClearCacheItem(string cacheKey)
        {
            ApplicationContext.Current.ApplicationCache.RuntimeCache.ClearCacheItem(cacheKey);
        }

        public static T GetRequestCacheItem<T>(string cacheKey)
        {
            return ApplicationContext.Current.ApplicationCache.RequestCache.GetCacheItem<T>(cacheKey);
        }

        public static T GetRequestCacheItem<T>(string cacheKey, Func<T> getCacheItem)
        {
            return ApplicationContext.Current.ApplicationCache
                .RequestCache.GetCacheItem<T>(cacheKey, getCacheItem);
        }
    }
}
