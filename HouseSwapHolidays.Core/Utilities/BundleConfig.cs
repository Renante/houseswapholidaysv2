﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Optimization;

namespace HouseSwapHolidays.Core.Utilities
{
    public class BundleConfig
    {
        public class NonOrderingBundleOrderer : IBundleOrderer
        {
            public IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
            {
                return files;
            }
        }

        public static void RegisterBundles()
        {
            var js = new ScriptBundle("~/bundles/all.js").Include(
                "~/jsv2/bootstrap.js",
                "~/jsv2/slick.js",
                "~/jsv2/custom.js",
                "~/scripts/modernizr-2.6.2.min.js",
                "~/scripts/jquery.bxslider.js",
                "~/scripts/showup.js",
                "~/scripts/custom.js",
                "~/scripts/icheck.js",
                "~/scripts/jquery.lightbox_me.js",
                "~/scripts/tinymce/js/tinymce/tinymce.min.js",
                "~/scripts/jquery.jcarousel.js",
                "~/scripts/jquery.query-object.js",
                "~/scripts/jquery.tipsy.js",
                "~/scripts/custom.js"
                );
            js.Orderer = new NonOrderingBundleOrderer();
            BundleTable.Bundles.Add(js);

            var js2 = new ScriptBundle("~/bundles/all2.js").Include(
                "~/jsv2/jquery-1.10.2.js",
                "~/jsv2/bootstrap.js",
                "~/jsv2/slick.js",
                "~/jsv2/jquery-ui.js",
                "~/jsv2/equal_height.js",
                "~/jsv2/tipsy.js",
                "~/scripts/global.js",
                "~/jsv2/custom.js"
            );
            js2.Orderer = new NonOrderingBundleOrderer();
            BundleTable.Bundles.Add(js2);

            BundleTable.Bundles.Add(new StyleBundle("~/bundles/all.css").Include(
                    "~/cssv2/bootstrap.css",
                    "~/css/style.css",
                    "~/cssv2/style.css",
                    "~/cssv2/custom.css",
                    "~/cssv2/responsive.css",
                    "~/css/responsive.css",
                    "~/css/dd.css",
                    "~/css/jquery.bxslider.css",
                    "~/css/tipsy.css",
                    "~/css/all.css",
                    "~/css/validate.css",
                    "~/css/custom.css"
                ));

            BundleTable.Bundles.Add(new StyleBundle("~/bundles/all2.css").Include(
                "~/cssv2/bootstrap.css",
                "~/cssv2/slick-theme.css",
                "~/cssv2/slick.css",
                "~/cssv2/jquery-ui.css",
                "~/cssv2/tipsy.css",
                "~/cssv2/style.css",
                "~/cssv2/custom.css",
                "~/cssv2/responsive.css"
            ));


            BundleTable.EnableOptimizations = true;
        }
    }
}
