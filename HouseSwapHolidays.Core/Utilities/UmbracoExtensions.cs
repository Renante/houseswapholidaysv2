﻿using Archetype.Models;
using HouseSwapHolidays.Core.Models;
using RJP.MultiUrlPicker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace HouseSwapHolidays.Core.Utilities
{
    public static class UmbracoExtensions
    {
        public static MediaItem GetPropertyAsMediaItem(this IPublishedContent content, string propertyAlias)
        {
            var umbHelper = new UmbracoHelper(UmbracoContext.Current);
            if (content.HasValue(propertyAlias)) { 
                var mediaId = content.GetPropertyValue<int>(propertyAlias);
                return new MediaItem(umbHelper.TypedMedia(mediaId));
            }

            return null;
        }

        public static MediaItem GetValueAsMediaItem(this ArchetypeFieldsetModel fs, string alias)
        {
            var umbHelper = new UmbracoHelper(UmbracoContext.Current);
            if (fs.HasValue(alias))
            {
                var item =  fs.GetValue<int>(alias);
                return umbHelper.TypedMedia(item).ToMediaItem();
            }
            return null;
        }

        public static MediaItem ToMediaItem(this IPublishedContent content)
        {
            return new MediaItem(content);
        }

        public static IEnumerable<MediaItem> GetPropertyAsMediaItems(this IPublishedContent content, string propertyAlias)
        {
            var medias = content.GetPropertyValue<IEnumerable<IPublishedContent>>(propertyAlias);
            return medias == null ? null : medias.Select(media => new MediaItem(media));
        }

        public static IEnumerable<MediaItem> GetPropertyAsMediaItems(this ArchetypeFieldsetModel fs, string propertyAlias)
        {
            if (fs.HasValue(propertyAlias))
            {
                var umbHelper = new UmbracoHelper(UmbracoContext.Current);
                return fs.GetValue<string>(propertyAlias)
                    .Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => umbHelper.TypedMedia(x).ToMediaItem());
            }

            return Enumerable.Empty<MediaItem>();
        }

        public static IEnumerable<T> GetPropertyAsArchetypeObjectList<T>(this IPublishedContent content,
            string propertyAlias, params object[] args) where T : IArchetypeObject
        {
            Func<IEnumerable<object>, object, object[]> insertArgsAtStart = delegate(IEnumerable<object> objects, object additional)
            {
                if (args == null)
                    return new[] { additional };

                var list = args.ToList();
                list.Insert(0, additional);
                return list.ToArray();
            };

            var value = content.GetPropertyValue<ArchetypeModel>(propertyAlias);
            return value != null ? value.Select(x => (T)Activator.CreateInstance(typeof(T), insertArgsAtStart(args, x))) : Enumerable.Empty<T>();
        }

        public static IEnumerable<T> GetPropertyAsArchetypeObjectList<T>(this ArchetypeFieldsetModel content,
            string propertyAlias, params object[] args) where T : IArchetypeObject
        {
            Func<IEnumerable<object>, object, object[]> insertArgsAtStart = delegate(IEnumerable<object> objects, object additional)
            {
                if (args == null)
                    return new[] { additional };

                var list = args.ToList();
                list.Insert(0, additional);
                return list.ToArray();
            };

            var value = content.GetValue<ArchetypeModel>(propertyAlias);
            return value != null ? value.Select(x => (T)Activator.CreateInstance(typeof(T), insertArgsAtStart(args, x))) : Enumerable.Empty<T>();
        }

        public static T GetPropertyRadioAsEnum<T>(this IPublishedContent content, string propAlias, T defaultValue)
            where T : struct, IConvertible
        {
            var propValue = content.GetPropertyPreValueAsString(propAlias);
            if (string.IsNullOrEmpty(propValue))
                return defaultValue;

            T result;
            return Enum.TryParse(propValue.Replace(" ", ""), true, out result) ? result : defaultValue;
        }

        public static string GetPropertyPreValueAsString(this IPublishedContent content, string propAlias)
        {
            return umbraco.library.GetPreValueAsString(content.GetPropertyValue<int>(propAlias));
        }

        public static string GetPropertyPreValueAsString(this ArchetypeFieldsetModel content, string propAlias)
        {
            return umbraco.library.GetPreValueAsString(content.GetValue<int>(propAlias));
        }

        public static NavigationItem GetPropertyAsNavigationItem(this IPublishedContent content, string propertyAlias)
        {
            if (content.HasValue(propertyAlias)) { 
                var items = content.GetPropertyValue<MultiUrls>(propertyAlias);
                if (items == null) return null;

                return items.FirstOrDefault().ToNavigationItem();
            }

            return null;
        }

        public static NavigationItem GetValueAsNavigationItem(this ArchetypeFieldsetModel fs, string propertyAlias)
        {
            if (fs.HasValue(propertyAlias)) { 
                return fs.GetValue<MultiUrls>(propertyAlias).FirstOrDefault().ToNavigationItem();
            }

            return null;

        }

        public static IEnumerable<NavigationItem> GetValueAsNavigationItems(this ArchetypeFieldsetModel fs, string propertyAlias)
        {
            return fs.HasValue(propertyAlias) ? fs.GetValue<MultiUrls>(propertyAlias).Select(x => x.ToNavigationItem()) :
                Enumerable.Empty<NavigationItem>();
        }

        public static NavigationItem ToNavigationItem(this Link link, int[] currentPath = null)
        {
            if (link == null)
                return null;

            NavigationItem ni;

            if (link.Id.HasValue)
            {
                if (link.Type == LinkType.Content)
                {
                    var content = UmbracoContext.Current.ContentCache.GetById(link.Id.Value);
                    var isActive = currentPath != null && currentPath.Contains(content.Id);
                    ni = new NavigationItem(content, isActive);
                    ni.Title = !string.IsNullOrEmpty(link.Name) ? link.Name : ni.Title;
                }
                else
                {
                    var content = UmbracoContext.Current.MediaCache.GetById(link.Id.Value);
                    ni = new NavigationItem(content);
                    ni.Title = !string.IsNullOrEmpty(link.Name) ? link.Name : ni.Title;
                }
            }
            else
            {
                ni = new NavigationItem()
                {
                    Title = link.Name,
                    Link = link.Url,
                    Target = link.Target
                };
            }

            //parse title to get alt text inside parenthesis
            var match = Regex.Match(ni.Title, @"\(([^)]*)\)");
            if (match.Groups.Count > 1)
            {
                ni.AltText = match.Groups[1].Value;
                ni.Title = ni.Title.Replace(match.Groups[0].Value, "");
            }

            return ni;
        }

        public static T GetPropertyDropdownAsEnum<T>(this IPublishedContent content, string propAlias, T defaultValue)
            where T : struct, IConvertible
        {
            var propValue = content.GetPropertyValue<string>(propAlias);
            return propValue.ToEnum(defaultValue);
        }

        public static T GetPropertyDropdownAsEnum<T>(this ArchetypeFieldsetModel fs, string propAlias, T defaultValue)
            where T : struct, IConvertible
        {
            var propValue = fs.GetValue<string>(propAlias);
            return propValue.ToEnum(defaultValue);
        }


        public static T ToEnum<T>(this string str, T defaultValue)
            where T : struct, IConvertible
        {
            if (string.IsNullOrEmpty(str))
                return defaultValue;

            T result;
            return Enum.TryParse(str.Replace(" ", ""), true, out result) ? result : defaultValue;
        }
    }
}
