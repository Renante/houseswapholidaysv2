﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using Umbraco.Web;

namespace HouseSwapHolidays.Core.Utilities
{
    public static class SessionExtensions
    {
        public static bool IsUserFreeMembership(this HttpSessionStateBase session)
        {
            return session["__IsUserFreeMembership"] != null &&
                   (bool) session["__IsUserFreeMembership"];
        }

        public static void SetUserFreeMembership(this HttpSessionStateBase session, bool value)
        {
            session["__IsUserFreeMembership"] = value;
        }

        public static bool IsMemberInternational(this HttpSessionStateBase session)
        {
            return IsUserFreeMembership(session);
        }

        public static void SetCountryCode(this HttpSessionStateBase session, string value)
        {
            session["__CountryCode"] = value;
        }
        public static string GetCountryCode(this HttpSessionStateBase session)
        {
            return session["__CountryCode"] != null ? session["__CountryCode"].ToString().ToLower() : "";
        }
    }
}
