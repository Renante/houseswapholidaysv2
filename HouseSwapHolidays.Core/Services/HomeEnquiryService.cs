﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HouseSwapHolidays.Core.Models.DataAccess;
using HouseSwapHolidays.Core.Models.Items;
using HouseSwapHolidays.Core.Utilities;
using Umbraco.Core;

namespace HouseSwapHolidays.Core.Services
{
    public class HomeEnquiryService : Singleton<HomeEnquiryService>
    {
        RLK797_HomeExchangeEntities db = new RLK797_HomeExchangeEntities();
        public List<HomeEnquiry> GetAllForMember(int memberId)
        {
            var result = db.up_GetHomeEnquiriesByMember2(memberId);
            var member = MemberService.Instance.GetCurrentMember();

            return result.Select(s => new HomeEnquiry
            {
                Id = s.ID,
                FromName = s.FirstName, 
                ToName = member.FirstName,
                Date = s.EnquiryDate,
                MessageSummary = s.Message.Truncate(30,"..."),
                Message = s.Message,
                Reviewed = (bool)s.Reviewed,
                HomeId = (int)s.HomeID,
                RecipientMemberId = (int)s.RecipientMemberID
            }).ToList();


        }

        public void Save(HomeEnquiry h)
        {

        }
    }
}
