﻿using ChristianHomeSwap.Core.Models.Util;
using Common;
using HouseSwapHolidays.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Core.Logging;

namespace HouseSwapHolidays.Core.Services
{
    public class AccountService
    {
        public ResultObject SendAndSavePasswordResetKey(string email)
        {
            var result = new ResultObject();

            if (Validator.IsValidEmail(email))
            {
                Member member = MemberDAL.Instance.GetByEmailAddress(email);
                if (member != null)
                {
                    string resetkey = ResetPasswordDAL.Instance.GenerateResetPasswordKey();

                    // TODO @@ channelID below is hardcoded
                    // GenerateEmail.Instance.GeneratePasswordResetLink(2, member, resetkey);
                    
                    var forgotpasswordTemplate =
                        EmailTemplateService.Instance.Get(Core.Models.Items.EmailTemplate.ForgotPasswordResetKey);

                    var template = forgotpasswordTemplate.Replace("[key]", HttpUtility.UrlEncode(resetkey));
                    template = template.Replace("[firstname]", member.FirstName);
                    template = template.Replace("[lastname]", member.GivenName);

                    LogHelper.Info(GetType(), "Sending password reset to " + member.EmailAddress);
                    var isSent = EmailService.Instance.Send(member.EmailAddress, member.FirstName,
                            "Password Reset Link", template);
                    LogHelper.Info(GetType(), "Sending status: " + isSent);

                    // save reset key to database for user to verify later
                    ResetPasswordDAL.Instance.SavePasswordResetKey(email, resetkey);

                    result.Success = true;
                    result.Message = "Success, a verification was sent to your email.";
                }
                else
                {
                    result.Success = false;
                    result.Message = "Email is not existing!";
                }
            }
            else
            {
                result.Success = false;
                result.Message = "Email is invalid!";
            }

            return result;
        }
    }
}
