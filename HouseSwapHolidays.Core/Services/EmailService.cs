﻿using Common;
using HouseSwapHolidays.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using Umbraco.Core.Logging;

namespace HouseSwapHolidays.Core.Services
{
    public class EmailService : Singleton<EmailService>
    {
        
        public bool Send(string toEmail, string toName, string subject, string body)
        {

            body = body.Replace("/http://", "http://");

            // for debugging only gmail stmp doesnt work.
            if (WebConfigurationManager.AppSettings["IsTestEmail"] != null &&
                Convert.ToBoolean(WebConfigurationManager.AppSettings["IsTestEmail"]))
            {
                File.WriteAllText(@"C:\Users\Renan\Desktop\Outside Websites\houseswapholidays new\HouseSwapHolidays\email.html", body);
                return true;
            }

            // real code here
            try
            {
                GenerateEmail.Instance.SendEmail(toName, toEmail, WebConfigurationManager.AppSettings["WebsiteName"], "info@houseswapholidays.com.au", subject, body, 2);

                return true;
            }

            catch (SmtpException ex)
            {
                LogHelper.Error(GetType(), "Error sending email: " + ex.Message, ex);
                return false;
            }
        }
    }
}
