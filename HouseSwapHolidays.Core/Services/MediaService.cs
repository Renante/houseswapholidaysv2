﻿using HouseSwapHolidays.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace HouseSwapHolidays.Core.Services
{
    public class MediaService : Singleton<MediaService>
    {
        public string GetMediaUrl(int id)
        {
            var media = UmbracoContext.Current.MediaCache.GetById(id);
            if (media != null)
                return media.Url;

            return string.Empty;
        }


        public string GetMediaUrl(string id)
        {
            return GetMediaUrl(id.ToInt());
        }
    }
}
