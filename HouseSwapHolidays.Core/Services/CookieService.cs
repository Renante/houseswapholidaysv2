﻿using HouseSwapHolidays.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Services
{
    public class CookieService : Singleton<CookieService>
    {
        public int GetCookieId(string cookieName)
        {
            if (string.IsNullOrWhiteSpace(cookieName)) return 0;
            int index = cookieName.IndexOf("_");
            if (index > 0)
            {
                return int.Parse(cookieName.Substring(0, index));
            }

            return 0;
        }

        public string GetCookieRole(string cookieName)
        {
            if (!string.IsNullOrEmpty(cookieName)) { 
                int indexa = cookieName.IndexOf("_");
                int indexb = cookieName.IndexOf("_", indexa + 1);
                int length = indexb - indexa;
                return cookieName.Substring(indexa + 1, length - 1);
            }
            else
            {
                return "";
            }
        }

        public string GetCookieEmail(string cookieName)
        {
            int indexa = cookieName.IndexOf("_");
            int indexb = cookieName.IndexOf("_", indexa + 1);
            return cookieName.Substring(indexb + 1);
        }


    }
}
