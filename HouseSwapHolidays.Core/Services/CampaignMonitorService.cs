﻿using HouseSwapHolidays.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Logging;
using createsend_dotnet;
using HouseSwapHolidays.Core.Models.Utilities;
using System.Web.Configuration;

namespace HouseSwapHolidays.Core.Services
{
    
    public class CampaignMonitorService : Singleton<CampaignMonitorService>
    {
        private readonly string _listId;
        private readonly AuthenticationDetails _auth;

        public CampaignMonitorService()
        {
            //var apiKey = WebConfigurationManager.AppSettings["CMApiKey"];

            //_listId = WebConfigurationManager.AppSettings["CMListId"];
            //_auth = new ApiKeyAuthenticationDetails(apiKey);
        }

        public void Subscribe(string email)
        {
            //var member = Common.MemberDAL.Instance.GetByEmailAddress(email);
            //if (member != null)
            //    Subscribe(member.ID);
        }

        private void Subscribe(int memberId)
        {
            //var member = Common.MemberDAL.Instance.Get(memberId);
            //if (member == null) return;

            //var home = Common.HomeDAL.Instance.GetByMemberID(member.ID);
            //var homeId = home.Count > 0 ? home[0].ID.ToString() : string.Empty;

            //var s = new Subscriber(_auth, _listId);

            //var customFields =
            //    new List<SubscriberCustomField>()
            //{
            //    new SubscriberCustomField() { Key = Constants.SubscriberField.MemberId, Value = member.ID.ToString() },
            //    new SubscriberCustomField() { Key = Constants.SubscriberField.FirstName, Value = member.FirstName },
            //    new SubscriberCustomField() { Key = Constants.SubscriberField.LastName, Value = member.GivenName },
            //    new SubscriberCustomField() { Key = Constants.SubscriberField.DateCreated, Value = string.Format("{0:yyyy/MM/dd}", member.CreatedDate) },
            //    new SubscriberCustomField() { Key = Constants.SubscriberField.LastLoggedIn, Value = string.Format("{0:yyyy/MM/dd}", member.LastLoggedInDate) },
            //    new SubscriberCustomField() { Key = Constants.SubscriberField.HomeId, Value = homeId },
            //    new SubscriberCustomField() { Key = Constants.SubscriberField.MemberStatus, Value = member.Status.ToString() },
            //};

            //try
            //{
            //    s.Add(member.EmailAddress,
            //        string.Format("{0} {1}", member.FirstName, member.GivenName),
            //        customFields,
            //        true);
            //}
            //catch (Exception e)
            //{
            //    LogHelper.Info(this.GetType(),
            //        string.Format("Error subscribing user: {0}, error detail: {1}",
            //        member.EmailAddress, e.Message));
            //}

        }

        public void UnSubscribe(string email)
        {
            //var s = new Subscriber(_auth, _listId);

            //try
            //{
            //    s.Unsubscribe(email);
            //}
            //catch (Exception e)
            //{
            //    LogHelper.Info(this.GetType(),
            //        string.Format("Error Unsubscribing user: {0}, error detail: {1}",
            //        email, e.Message));
            //}
        }

    }

}

