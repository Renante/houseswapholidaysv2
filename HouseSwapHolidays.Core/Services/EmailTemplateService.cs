﻿using HouseSwapHolidays.Core.Models.Items;
using HouseSwapHolidays.Core.Utilities;
using HouseSwapHolidays.Core.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using Umbraco.Core.Logging;
using Umbraco.Web;

namespace HouseSwapHolidays.Core.Services
{
    public class EmailTemplateService : Singleton<EmailTemplateService>
    {
        public string Get(EmailTemplate emailTemplate)
        {
            var emailtemplateNode = CurrentRequest.GetUmbracoHelper()
                .TypedContentAtXPath("//EmailTemplate")
                .FirstOrDefault(x => x.GetPropertyValue<EmailTemplate>("key") == emailTemplate);

            if (emailtemplateNode == null) {
                LogHelper.Error(GetType(),
                    "Email template not found: " + emailTemplate,
                    new EmailTemplateNotFoundException());
                return "";
            }
            
            var template = emailtemplateNode.GetPropertyValue<string>("template");

            // rte editor can't accept direct site url path so we will do the fix here
            template = template.Replace("[siteurl]", WebConfigurationManager.AppSettings["WebsiteUrl"]);
            template = template.Replace("/images", WebConfigurationManager.AppSettings["WebsiteUrl"] + "/images");

            return template;

        }
    }
}
