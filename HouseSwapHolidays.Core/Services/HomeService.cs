﻿using HouseSwapHolidays.Core.Models.DataAccess;
using HouseSwapHolidays.Core.Models.Items;
using HouseSwapHolidays.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Services
{
    public class HomeService : Singleton<HomeService>
    {
        RLK797_HomeExchangeEntities db = new RLK797_HomeExchangeEntities();

        
        public Home Get(int homeId)
        {
            var result = db.up_GetHome(homeId).FirstOrDefault();
            
            if (result != null)
            {
                Home home = new Home();
                home.HomeID = result.HomeID;
                home.Location = result.Location;
                home.Summary = result.Summary;
                home.Description = result.Description;
                home.RegionDescription = result.RegionDescription;
                home.MemberID = (int)result.MemberID;
                var homeFeatures = GetHomeFeautures(homeId);
                home.HomeFeatures = FilterHomeFeaturesByFeatureType(homeFeatures, (int)HomeFeatureType.HomeFacilities);
                home.NearbyFacilities = FilterHomeFeaturesByFeatureType(homeFeatures, (int)HomeFeatureType.HomeActivities);
                home.OtherDetails = FilterHomeFeaturesByFeatureType(homeFeatures, (int)HomeFeatureType.HomeRules);
                home.OtherInformation = result.OtherInformation;
                home.BedRooms = (int)result.Bedrooms;
                home.BathRooms = (int)result.Bathrooms;
                home.City = result.City;
                home.AirportName = result.AirportName;
                home.AirportDistance = (int)result.AirportDistance;

                home.Longitude = result.Longitude;
                home.Lattitude = result.Latitude;

                home.Status = (Common.Status)result.Status;
                home.DefaultChannel = (int)result.DefaultChannel;

                //Availabilities
                home.JanuaryAvailability = (bool)result.JanuaryAvailability;
                home.FebruaryAvailability = (bool)result.FebruaryAvailability;
                home.MarchAvailability = (bool)result.MarchAvailability;
                home.AprilAvailability = (bool)result.AprilAvailability;
                home.MayAvailability = (bool)result.MayAvailability;
                home.JuneAvailability = (bool)result.JuneAvailability;
                home.JulyAvailability = (bool)result.JulyAvailability;
                home.AugustAvailability = (bool)result.AugustAvailability;
                home.SeptemberAvailability = (bool)result.SeptemberAvailability;
                home.OctoberAvailability = (bool)result.OctoberAvailability;
                home.NovemberAvailability = (bool)result.NovemberAvailability;
                home.DecemberAvailability = (bool)result.DecemberAvailability;
                
                home.AllowDestinationOffers = (bool)result.AllowDestinationOffers;

                return home;
                
            }
            else
                return null;
        }

        public Home GetByMemberId(int memberId)
        {
            var result = db.up_GetHomeByMemberID(memberId).FirstOrDefault();
            if (result != null)
            {
                Home home = new Home();
                home.HomeID = result.HomeID;
                home.Location = result.Location;
                home.Summary = result.Summary;
                home.Description = result.Description;
                home.RegionDescription = result.RegionDescription;
                home.MemberID = (int)result.MemberID;
                var homeFeatures = GetHomeFeautures(result.HomeID);
                home.HomeFeatures = FilterHomeFeaturesByFeatureType(homeFeatures, (int)HomeFeatureType.HomeFacilities);
                home.NearbyFacilities = FilterHomeFeaturesByFeatureType(homeFeatures, (int)HomeFeatureType.HomeActivities);
                home.OtherDetails = FilterHomeFeaturesByFeatureType(homeFeatures, (int)HomeFeatureType.HomeRules);
                home.OtherInformation = result.OtherInformation;
                home.BedRooms = (int)result.Bedrooms;
                home.BathRooms = (int)result.Bathrooms;

                home.AirportName = result.AirportName;
                home.AirportDistance = (int)result.AirportDistance;

                //Availabilities
                home.JanuaryAvailability = (bool)result.JanuaryAvailability;
                home.FebruaryAvailability = (bool)result.FebruaryAvailability;
                home.MarchAvailability = (bool)result.MarchAvailability;
                home.AprilAvailability = (bool)result.AprilAvailability;
                home.MayAvailability = (bool)result.MayAvailability;
                home.JuneAvailability = (bool)result.JuneAvailability;
                home.JulyAvailability = (bool)result.JulyAvailability;
                home.AugustAvailability = (bool)result.AugustAvailability;
                home.SeptemberAvailability = (bool)result.SeptemberAvailability;
                home.OctoberAvailability = (bool)result.OctoberAvailability;
                home.NovemberAvailability = (bool)result.NovemberAvailability;
                home.DecemberAvailability = (bool)result.DecemberAvailability;

                home.AllowDestinationOffers = (bool)result.AllowDestinationOffers;

                return home;
            }
            else
            {  
                return null;
            }
        }

        //public List<Home> GetAllHomes(string keyword)
        //{
        //    var results = db.up_GetCHSHomes(keyword);

        //    return results.Select(s => new Home {
        //        HomeID = s.HomeID,
        //        City = s.City,
        //        Location = s.Location,
        //        Summary = s.Summary,
        //        Description = s.Description,
        //        BedRooms = (int)s.Bedrooms,
        //        BathRooms = (int)s.Bathrooms, 
        //        //PriorityRank = (int)s.PriorityRank,
        //        ListingType = (int)s.ListingType
        //    }).ToList();
        //}

        public List<HomeImage> GetHomeImages(int homeId)
        {
            var results = db.up_GetHomeImages(homeId);

            return results.Select(s => new HomeImage {
                Id = s.HomeImageID,
                MediumImage = s.ThumbnailImage,
                MediumImagePath = string.Format(@"/images/homes/{0}/{1}", homeId, s.MediumImage),
                LargeImage = s.LargeImage,
                LargeImagePath = string.Format(@"/images/homes/{0}/{1}", homeId, s.LargeImage),
                GiantImage = s.GiantImage,
                GiantImagePath = string.Format(@"/images/homes/{0}/{1}", homeId, s.GiantImage),
                IsFeature = (bool)s.isFeature
                
            })
            .OrderByDescending(o => o.IsFeature)
            .ToList();
        }

        public HomeImage GetFeaturedThumbnailImage(int homeId)
        {
            var homeImages = GetHomeImages(homeId);
            return homeImages.Where(w => w.IsFeature).FirstOrDefault();
            
        }

        public List<HomeFeature> GetHomeFeautures(int homeId)
        {
            var features = db.up_GetHomeFeatures(homeId);

            return features.Select(s => new HomeFeature {
                Name = s.Name,
                Description = s.Description,
                Type = (int)s.FeatureTypeID
            }).ToList();
        }

        public List<HomeFeature> FilterHomeFeaturesByFeatureType(List<HomeFeature> homeFeatures, int featureType)
        {
            return homeFeatures.Where(w => w.Type == featureType).ToList();
        }

        public List<FavoriteHome> GetFavoriteHomes(int memberId)
        {
            var results = db.up_GetAllMemberShortlists(memberId);

            return results.Select(s => new FavoriteHome {
                Id = s.ID,
                Homeid = s.HomeID,
                Location = s.Location,
                HomeMemberId = MemberService.Instance.GetMemberFromHome(s.HomeID).Id,
                HomeMemberName = MemberService.Instance.GetMemberFromHome(s.HomeID).FirstName
            }).ToList();
        }

        public void GetMemberCountry(ref string countryName, int memberId)
        {
            var result = db.up_GetMemberCountryFromHome(memberId).FirstOrDefault();
            countryName = result != null ? result.ToString() : "";
        }

    }
}
