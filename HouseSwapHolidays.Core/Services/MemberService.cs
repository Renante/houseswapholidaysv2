﻿using HouseSwapHolidays.Core.Models.DataAccess;
using HouseSwapHolidays.Core.Models.Forms;
using HouseSwapHolidays.Core.Models.Items;
using HouseSwapHolidays.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;

namespace HouseSwapHolidays.Core.Services
{
    public class MemberService : Singleton<MemberService>
    {
        RLK797_HomeExchangeEntities db = new RLK797_HomeExchangeEntities();

        public Member Get(int memberId)
        {
            var result = db.up_GetMember(memberId).FirstOrDefault();
            
            if (result != null)
            {
                Member m = new Member();
                m.Id = result.MemberID;
                m.EmailAddress = result.EmailAddress;
                m.FirstName = result.FirstName;
                m.GivenName = result.GivenName;
                m.Introduction = result.Introduction;
                m.Language1 = result.Language1;
                m.Language2 = result.Language2;
                m.Language3 = result.Language3;
                m.Status = (int)result.Status;
                m.UnreadMessage = (int)result.UnreadMessageCount;
                
                return m;
            }
            else
                return null;
        }

        public void RegisterAndLogin(JoinUsForm r)
        {
            var isFreeMembershipCountry = HttpContext.Current.Session.IsUserFreeMembership();
            var umbHelper = CurrentRequest.GetUmbracoHelper();
            // process
            var salt = PasswordService.Instance.GenerateRandomSalt();
            var hash = PasswordService.Instance.GenerateHashedPassword(r.Password, salt);

            // save
            db.up_InsertMember2v2(
                r.FirstName, //firstName
                r.LastName, //givenName
                r.EmailAddress, //emalAddress
                "", // password (this is empty because we started to implement hash password)
                "", //introduction
                "", //language1
                "", //language2
                "", //language3
                DateTime.Now, //createdDate
                DateTime.Now, //updatedDate
                DateTime.Now, //lastLoggedInDate
                0, //referrerID
                0, //countryID
                0, //typeID
                0, //promotionID
                r.AcceptNewsletter == "on", //receiveNewsletter
                true, //acceptedTerms (of course user cant proceed w/out checking it)
                r.MultiChannel == "on", //isMultiChannel
                2, //defaultChannel
                true, //acceptSpecialConditions
                Guid.NewGuid().ToString(), //GUID
                isFreeMembershipCountry ? (int)Common.Status.Draft : (int)Common.Status.PrePayment, //status
                salt, //salt
                hash //hashedPassword
                );

            // subscribe to campaign monitor
            if (r.AcceptNewsletter == "on")
            {
                CampaignMonitorService.Instance.Subscribe(r.EmailAddress);
            }
            
            // login
            var member = MemberService.Instance.GetMemberByEmailAddress(r.EmailAddress);
            FormsAuthentication.SetAuthCookie(string.Format("{0}_{1}_{2}", member.Id, "user", r.EmailAddress), false);

            if (isFreeMembershipCountry)
            {
                HttpContext.Current.Response.Redirect(umbHelper.Url(Models.Utilities.Constants.NodeId.MyProfile));    
            }
            else
            {
                HttpContext.Current.Response.Redirect(umbHelper.Url(Models.Utilities.Constants.NodeId.Activate));    
            }
        }

        public Member GetCurrentMember()
        {
            var memberCookie = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrEmpty(memberCookie)) return null;

            var memberId = CookieService.Instance.GetCookieId(memberCookie);
            return Get(memberId);
        }

        public int GetCurrentMemberId()
        {
            var memberCookie = HttpContext.Current.User.Identity.Name;
            var memberId = CookieService.Instance.GetCookieId(memberCookie);
            return memberId;
        }

        public bool isAdmin()
        {
            var memberCookie = HttpContext.Current.User.Identity.Name;
            return CookieService.Instance.GetCookieRole(memberCookie) == "admin";
        }

        public Member GetMemberFromHome(int homeId)
        {
            var result = db.up_GetMemberFromHome(homeId).FirstOrDefault();
            if (result != null)
            {
                Member m = new Member();
                m.Id = result.MemberID;
                m.GivenName = result.GivenName;
                m.FirstName = result.FirstName;

                return m;
            }
            else
                return null;
        }

        public Member GetMemberByEmailAddress(string email)
        {
            var result = db.up_GetMemberByEmailAddress(email).FirstOrDefault();
            
            if (result != null)
            {
                Member m = new Member();
                m.Id = result.MemberID;
                m.EmailAddress = result.EmailAddress;
                m.FirstName = result.FirstName;
                m.GivenName = result.GivenName;
                m.Introduction = result.Introduction;
                m.Language1 = result.Language1;
                m.Language2 = result.Language2;
                m.Language3 = result.Language3;
                m.Status = (int)result.Status;
                
                return m;
            }
            else
                return null;
            
        }

        public string GetMemberSaltByEmail(string email)
        {
            var result = db.up_GetMemberSaltByEmail(email).FirstOrDefault();
            return result ?? string.Empty;
        }

        public bool IsAuthenticated(string email, string password)
        {
            var salt = GetMemberSaltByEmail(email);
            var hash = PasswordService.Instance.GenerateHashedPassword(password, salt);
            var result = db.up_AuthenticateMemberv2(email, hash).FirstOrDefault();
            
            return result != null;
        }

        public bool IsActiveMember()
        {
            var member = GetCurrentMember();
            return member.Status == (int)Common.Status.Active;
        }

        public Common.Status GetCurrentMemberStatus()
        {
            var member = GetCurrentMember();
            return (Common.Status)member.Status;
        }

        public bool IsEmailExist(string email)
        {
            var result = db.up_GetMemberEmail(email);
            return result.Any();
        }
        public IEnumerable<MemberDestination> GetMemberDestinations(int memberId)
        {
            var result = db.up_GetMemberDestinations(memberId);

            return result.Select(s => new MemberDestination {
                CountryCode = s.CountryCode,
                CountryName = s.CountryName,
                Destination = s.Destination
            });
        }


        
    }
}
