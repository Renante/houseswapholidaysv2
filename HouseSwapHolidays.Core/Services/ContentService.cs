﻿using HouseSwapHolidays.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace HouseSwapHolidays.Core.Services
{
    public class ContentService : Singleton<ContentService>
    {
        public IEnumerable<IPublishedContent> GetBydIds(string[] ids)
        {
            if (ids.Any())
                return ids.Select(x => UmbracoContext.Current.ContentCache.GetById(x.ToInt()));

            return null;
        }

        public IPublishedContent GetNodeByAlias(string alias)
        {
            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            var contentNode = umbracoHelper.TypedContentSingleAtXPath(String.Format("//{0}", alias));

            return contentNode;
        }
    }
}
