﻿using HouseSwapHolidays.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Services
{
    public class PasswordService : Singleton<PasswordService>
    {
        public bool IsPasswordValid(string password, string salt)
        {

            return true;
        }

        public string GenerateHashedPassword(string plainPassword, string salt)
        {
            var hash = new HMACSHA1();
            hash.Key = Encoding.Unicode.GetBytes(plainPassword);
            string hashedPassword = Convert.ToBase64String(hash.ComputeHash(Encoding.Unicode.GetBytes(plainPassword + salt)));
            return hashedPassword;
        }

        public string GenerateRandomSalt()
        {
            return GenerateRandomString(16);
        }

        public string GenerateRandomString(int size)
        {
            var rng = new RNGCryptoServiceProvider();
            var bytes = new Byte[16];
            rng.GetBytes(bytes);
            return Convert.ToBase64String(bytes);
        }
    }

}
