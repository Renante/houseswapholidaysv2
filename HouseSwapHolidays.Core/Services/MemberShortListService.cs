﻿using HouseSwapHolidays.Core.Models.DataAccess;
using HouseSwapHolidays.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Services
{
    public class MemberShortListService : Singleton<MemberShortListService>
    {
        RLK797_HomeExchangeEntities db = new RLK797_HomeExchangeEntities();
        public void Delete(int shortListId, int memberId)
        {
            db.up_DeleteMemberShortlist(shortListId, memberId);
        }
    }
}
