﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

public static class MiscExtensionMethods
{
    public static int ToInt(this string src)
    {
        if (string.IsNullOrEmpty(src))
            return 0;

        int tempInt;
        int.TryParse(src, out tempInt);

        return tempInt;
    }

    public static bool IsOdd(this int num)
    {
        return num % 2 == 1;
    }

    public static string[] SplitComma(this string src)
    {
        return src.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
    }

    public static bool IsEven(this int num)
    {
        return num % 2 == 0;
    }

    public static string GetStatusText(this Common.Status status)
    {
        string retVal = string.Empty;
        switch (status)
        {
            case Common.Status.NotSpecified:
                retVal = "Not specified";
                break;

            case Common.Status.Active:
                retVal = "Approved";
                break;

            case Common.Status.PendingApproval:
                retVal = "<font color=\"red\">Awaiting approval</font>";
                break;

            case Common.Status.Archived:
                retVal = "Archived";
                break;

            case Common.Status.Deleted:
                retVal = "Deleted";
                break;

            case Common.Status.Draft:
                retVal = "Draft";
                break;

            case Common.Status.PrePayment:
                retVal = "Prepayment";
                break;

        }
        return retVal;
    }

    public static bool IsValidNumber(this string str)
    {
        int n;
        bool isNumeric = int.TryParse(str, out n);
        return isNumeric;
    }
    
}

public class Paging
{
    public int ItemsPerPage { get; set; }
    public int CurrentPage { get; set; }
    public int PreviousPage { get; set; }
    public int NextPage { get; set; }
    public double TotalPages { get; set; }
    public int PageStart { get; set; }
    public int PageEnd { get; set; }
    public int Skip { get; set; }
    public int Take { get; set; }

    public static Paging GetPages(int itemCount, int itemsPerPage)
    {
        int page;

        int.TryParse(HttpContext.Current.Request.QueryString["page"], out page);

        if (page == 0) page = 1;

        var totalpages = Math.Ceiling(itemCount / (Double)itemsPerPage);

        var pages = new Paging {
            ItemsPerPage = itemsPerPage,
            CurrentPage = page,
            PreviousPage = page - 1,
            NextPage = page + 1,
            TotalPages = totalpages,
            Skip = (page * itemsPerPage) - itemsPerPage,
            Take = itemsPerPage,
            PageStart = page - 3 <= 0 ? 1 : page - 3,
            PageEnd = (page + 7) > totalpages ? (int)totalpages : (page + 7),
        };

        return pages;
    }

    
}