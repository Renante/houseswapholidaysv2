﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Helpers
{
    public class MiscHelper
    {
        public static string AvailableExchangeDates(Common.Home home)
        {
            bool isFlexible = true;
            var availabilityDates = string.Empty;

            if (home.JanuaryAvailability)
            {
                availabilityDates = "Jan, ";
            }
            else
            {
                isFlexible = false;
            }

            if (home.FebruaryAvailability)
            {
                availabilityDates = availabilityDates + "Feb, ";
            }
            else
            {
                isFlexible = false;
            }

            if (home.MarchAvailability)
            {
                availabilityDates = availabilityDates + "Mar, ";
            }
            else
            {
                isFlexible = false;
            }

            if (home.AprilAvailability)
            {
                availabilityDates = availabilityDates + "Apr, ";
            }
            else
            {
                isFlexible = false;
            }

            if (home.MayAvailability)
            {
                availabilityDates = availabilityDates + "May, ";
            }
            else
            {
                isFlexible = false;
            }

            if (home.JuneAvailability)
            {
                availabilityDates = availabilityDates + "Jun, ";
            }
            else
            {
                isFlexible = false;
            }

            if (home.JulyAvailability)
            {
                availabilityDates = availabilityDates + "Jul, ";
            }
            else
            {
                isFlexible = false;
            }

            if (home.AugustAvailability)
            {
                availabilityDates = availabilityDates + "Aug, ";
            }
            else
            {
                isFlexible = false;
            }

            if (home.SeptemberAvailability)
            {
                availabilityDates = availabilityDates + "Sep, ";
            }
            else
            {
                isFlexible = false;
            }

            if (home.OctoberAvailability)
            {
                availabilityDates = availabilityDates + "Oct, ";
            }
            else
            {
                isFlexible = false;
            }

            if (home.NovemberAvailability)
            {
                availabilityDates = availabilityDates + "Nov, ";
            }
            else
            {
                isFlexible = false;
            }

            if (home.DecemberAvailability)
            {
                availabilityDates = availabilityDates + "Dec, ";
            }
            else
            {
                isFlexible = false;
            }

            if (isFlexible)
            {
                availabilityDates = "Flexible with travel dates";
            }
            else
            {
                if (availabilityDates.Length > 2)
                {
                    availabilityDates = "<div class=\"detail-title-dates\">Preferred dates: </div> " +
                        availabilityDates.Substring(0, availabilityDates.Length - 2);
                }
                else
                {
                    availabilityDates = "<div class=\"detail-title-dates\">Dates not specified </div>";
                }
            }

            return availabilityDates;
        }
    }
}
