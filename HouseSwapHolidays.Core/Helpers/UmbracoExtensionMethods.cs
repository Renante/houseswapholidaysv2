﻿using Archetype.Models;
using HouseSwapHolidays.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web;


public static class UmbracoExtensionMethods
{
    private static UmbracoHelper Umbraco
    {
        get { return new UmbracoHelper(UmbracoContext.Current); }
    }
    public static string GetNavigationName(this IPublishedContent content)
    {
        return content.GetPropertyValue<string>("navigationName", content.Name);
    }

    public static string GetPropertyAsMediaUrl(this IPublishedContent content, string propertyAlias)
    {
        return MediaService.Instance.GetMediaUrl(content.GetPropertyValue<int>(propertyAlias));
    }

    public static string GetPropertyAsMediaUrl(this string imageId)
    {
        return !string.IsNullOrEmpty(imageId) ? MediaService.Instance.GetMediaUrl(int.Parse(imageId)) : "";
    }

    public static string GetPropertyAsMediaUrl(this IPublishedContent content, string propertyAlias, string defaultValue)
    {
        var imgUrl = MediaService.Instance.GetMediaUrl(content.GetPropertyValue<int>(propertyAlias));
        return !string.IsNullOrEmpty(imgUrl) ? imgUrl : defaultValue;
    }

    public static IEnumerable<IPublishedContent> GetPropertyAsContentList(this IPublishedContent content, string propertyAlias)
    {
        return ContentService.Instance.GetBydIds(content.GetPropertyValue<string>(propertyAlias).SplitComma());
    }

    public static IEnumerable<IPublishedContent> GetPropertyAsContentList(this string propertyAlias)
    {
        return ContentService.Instance.GetBydIds(propertyAlias.SplitComma());
    }

    //public static GoogleMap GetPropertyAsGoogleMap(this IPublishedContent content, string propertyAlias)
    //{
    //    return new GoogleMap(content.GetPropertyValue<string>(propertyAlias));
    //}

    public static IPublishedContent GetPropertyAsContent(this IPublishedContent content, string propertyAlias)
    {
        var id = content.GetPropertyValue<int>(propertyAlias);

        return UmbracoContext.Current.ContentCache.GetById(id);
    }

    public static IEnumerable<IPublishedContent> GetPropertyAsPublishedContentListFromMntp(this IPublishedContent content,
            string propAlias)
    {
        if (content.HasValue(propAlias))
        {
            var nodes = content.GetPropertyValue<string>(propAlias)
                .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            return nodes.Select(Umbraco.TypedContent).Where(x => x != null);
        }

        return Enumerable.Empty<IPublishedContent>();
    }

    public static IEnumerable<IPublishedContent> GetPropertyAsPublishedContentListFromMntp(this ArchetypeFieldsetModel fs,
            string propAlias)
    {
        if (fs.HasValue(propAlias))
        {
            var nodes = fs.GetValue<string>(propAlias)
                .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            return nodes.Select(Umbraco.TypedContent).Where(x => x != null);
        }

        return Enumerable.Empty<IPublishedContent>();
    }

    public static string GetPropertyAsContentName(this IPublishedContent content, string propertyAlias)
    {
        var publishedContent = content.GetPropertyAsContent(propertyAlias);

        if (publishedContent != null)
            return publishedContent.Name;

        return string.Empty;
    }

    public static string GetPropertyAsSafeString(this IPublishedContent content, string propertyAlias)
    {
        var str = content.GetPropertyValue<string>(propertyAlias);

        if (str == null)
            return "";

        return str;
    }

    public static string GetTitle(this IPublishedContent content)
    {
        return content.GetPropertyValue<string>("title", content.Name);

    }

    public static IHtmlString ToLineBreaksHtml(this string value)
    {
        return !string.IsNullOrEmpty(value) ? new HtmlString(Umbraco.ReplaceLineBreaksForHtml(value)) : new HtmlString("");
    }
}


