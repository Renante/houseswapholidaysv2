﻿using HouseSwapHolidays.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HouseSwapHolidays.Core.Attributes
{
    public class ActiveMemberOnlyAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var authorized = base.AuthorizeCore(httpContext);
            if (!authorized)
            {
                // The user is not authenticated
                return false;
            }

            return MemberService.Instance.IsActiveMember();
        }
    }

    public class MemberOnlyAttribute : AuthorizeAttribute
    {
        public Common.Status[] Statuses { get; set; }
        public MemberOnlyAttribute(params Common.Status[] statuses)
        {
            this.Statuses = statuses;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var authorized = base.AuthorizeCore(httpContext);
            if (!authorized)
            {
                // The user is not authenticated
                return false;
            }

            return Statuses.Any(status => status == MemberService.Instance.GetCurrentMemberStatus());
        }
    }
}
