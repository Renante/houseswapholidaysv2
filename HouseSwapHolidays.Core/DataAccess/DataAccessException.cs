﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChristianHomeSwap.Core.DataAccess
{
    /// <summary>
    /// Exception class for data access errors.
    /// </summary>
    public class DataAccessException : ApplicationException
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="message">The error message</param>
        /// <param name="sqlex">The inner exception (must be an SqlException)</param>
        /// <param name="spName">The stored procedure name.</param>
        public DataAccessException(string message, SqlException sqlex, string spName)
            : this(message, sqlex, spName, null)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="message">The error message</param>
        /// <param name="sqlex">The inner exception (must be an SqlException)</param>
        /// <param name="spName">The stored procedure name.</param>
        /// <param name="parameters">The stpred procedure parameters</param>
        public DataAccessException(string message, SqlException sqlex, string spName, object[] parameters)
            : base(message, sqlex)
        {
            _spName = spName;
            if (_parameters == null)
            {
                _parameters = new object[0];
            }
            else
            {
                _parameters = parameters;
            }
        }

        /// <summary>
        /// The stored procedure name.
        /// </summary>
        public string StoredProcedureName
        {
            get
            {
                return _spName;
            }
        }

        /// <summary>
        /// The parameters passed into the stored procedure.
        /// </summary>
        public object[] Parameters
        {
            get
            {
                return _parameters;
            }
        }

        private object[] _parameters;
        private string _spName;
    }
}
