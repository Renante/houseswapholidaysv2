//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HouseSwapHolidays.Core.Models.DataAccess
{
    using System;
    
    public partial class up_GetMember_Result
    {
        public int MemberID { get; set; }
        public Nullable<int> Status { get; set; }
        public string FirstName { get; set; }
        public string GivenName { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string Introduction { get; set; }
        public string Language1 { get; set; }
        public string Language2 { get; set; }
        public string Language3 { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<System.DateTime> LastLoggedInDate { get; set; }
        public Nullable<int> ReferrerID { get; set; }
        public Nullable<bool> AcceptedTerms { get; set; }
        public Nullable<bool> IsMultiChannel { get; set; }
        public Nullable<int> DefaultChannel { get; set; }
        public Nullable<int> PromotionID { get; set; }
        public Nullable<int> UnreadMessageCount { get; set; }
        public Nullable<bool> AllowDestinationOffers { get; set; }
        public Nullable<bool> AcceptSpecialConditions { get; set; }
        public bool ReceiveNewsletter { get; set; }
        public string guid { get; set; }
        public Nullable<int> UpdateMode { get; set; }
        public Nullable<int> UpdateTravelDetailsMode { get; set; }
        public Nullable<bool> LastMinuteAvailability { get; set; }
        public Nullable<bool> JanuaryAvailability { get; set; }
        public Nullable<bool> FebruaryAvailability { get; set; }
        public Nullable<bool> MarchAvailability { get; set; }
        public Nullable<bool> AprilAvailability { get; set; }
        public Nullable<bool> MayAvailability { get; set; }
        public Nullable<bool> JuneAvailability { get; set; }
        public Nullable<bool> JulyAvailability { get; set; }
        public Nullable<bool> AugustAvailability { get; set; }
        public Nullable<bool> SeptemberAvailability { get; set; }
        public Nullable<bool> OctoberAvailability { get; set; }
        public Nullable<bool> NovemberAvailability { get; set; }
        public Nullable<bool> DecemberAvailability { get; set; }
    }
}
