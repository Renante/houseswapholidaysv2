//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HouseSwapHolidays.Core.Models.DataAccess
{
    using System;
    
    public partial class up_GetAllReferencesForMember_Result
    {
        public int ID { get; set; }
        public Nullable<int> MemberID { get; set; }
        public string Name { get; set; }
        public string EmailAddress { get; set; }
        public Nullable<int> Rating { get; set; }
        public Nullable<bool> Type { get; set; }
        public string Comment { get; set; }
        public Nullable<bool> IsContactable { get; set; }
        public Nullable<System.DateTime> date { get; set; }
    }
}
