//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HouseSwapHolidays.Core.Models.DataAccess
{
    using System;
    
    public partial class up_GetAllChannels_Result
    {
        public Nullable<int> ChannelID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ShortURL { get; set; }
        public string LongURL { get; set; }
        public Nullable<int> OwnerID { get; set; }
        public Nullable<bool> isDefault { get; set; }
        public string SupportEmailAddress { get; set; }
        public string SalesEmailAddress { get; set; }
        public string AdvertisingEmailAddress { get; set; }
        public string ShortPageTitle { get; set; }
        public string LongPageTitle { get; set; }
        public string Stylesheet { get; set; }
        public string MetaDescriptionGeneric { get; set; }
        public string MetaDescriptionResults { get; set; }
        public string MetaKeywordsGeneric { get; set; }
        public Nullable<bool> SupportMultiChannel { get; set; }
        public Nullable<decimal> SubscriptionRate { get; set; }
        public string AnalyticsTrackingCode { get; set; }
        public string MapKey { get; set; }
        public Nullable<int> ExchangeType { get; set; }
        public Nullable<int> SubscriptionPeriod { get; set; }
    }
}
