﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Models.Items
{
    public enum HomeFeatureType
    {
        HomeFacilities = 1,
        HomeActivities = 2,
        HomePreferences = 3,
        HomeRules = 4,
        HolidayTypesToOffer = 5,
        SwapTimePreferences = 6,
        SwapLength = 7
    }
}
