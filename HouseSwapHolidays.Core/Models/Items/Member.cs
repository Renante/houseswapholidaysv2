﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Models.Items
{
    public class Member
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string GivenName { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string HashedPassword { get; set; }
        public int Status { get; set; }
        public string Introduction { get; set; }
        public string Language1 { get; set; }
        public string Language2 { get; set; }
        public string Language3 { get; set; }
        public int UnreadMessage { get; set; }
        public IEnumerable<MemberDestination> Destinations { get; set; }

    }
}
