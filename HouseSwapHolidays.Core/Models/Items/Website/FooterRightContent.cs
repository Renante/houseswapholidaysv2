﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Models.Items.Website
{
    public class FooterRightContent
    {
        public string Title { get; set; }
        public IEnumerable<SocialAccount> SocialAccounts { get; set; }
        public string Text { get; set; }
        public string Email { get; set; }
    }
}
