﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.Items.Website
{
    public class FooterMenu
    {
        public string Title { get; set; }
        public IEnumerable<IPublishedContent> Menu { get; set; }
        public bool HasMenu { get; set; }
    }
}
