﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Models.Items
{
    public class HomeImage
    {
        public int Id { get; set; }
        public string MediumImage { get; set; }
        public string MediumImagePath { get; set; }
        public string LargeImage { get; set; }
        public string LargeImagePath { get; set; }
        public string GiantImage { get; set; }
        public string GiantImagePath { get; set; }
        public bool IsFeature { get; set; }
    }
}
