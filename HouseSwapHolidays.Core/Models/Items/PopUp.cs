﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Models.Items
{
    public class PopUp
    {
        public string Title { get; set; }
        public string Summary { get; set; }
    }
}
