﻿using Archetype.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Models.Items.PageHome
{
    public class HowItWorksItem : IArchetypeObject
    {
        public HowItWorksItem()
        {
            
        }

        public HowItWorksItem(ArchetypeFieldsetModel fs)
        {
            IconUrl = fs.GetValue("icon").GetPropertyAsMediaUrl();
            Title = fs.GetValue<string>("title");
            Content = fs.GetValue<string>("content");
        }

        public string IconUrl { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
    }
}
