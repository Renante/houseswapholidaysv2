﻿using Archetype.Models;
using RJP.MultiUrlPicker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HouseSwapHolidays.Core.Utilities;

namespace HouseSwapHolidays.Core.Models.Items.PageHome
{
    public class FeaturedItem : IArchetypeObject
    {
        public FeaturedItem()
        {
            
        }

        public FeaturedItem(ArchetypeFieldsetModel fs)
        {
            Title = fs.GetValue<string>("title");
            var image = fs.GetValueAsMediaItem("image");
            ImageUrl = image != null ? image.Url : "";
            Link = fs.HasValue("link") ? fs.GetValue<MultiUrls>("link").FirstOrDefault() : null;
        }
        public string Title { get; set; }
        public string ImageUrl { get; set; }
        public Link Link { get; set; }
        public bool HasLink { get; set; }
    }
}
