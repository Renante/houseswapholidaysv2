﻿using RJP.MultiUrlPicker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Models.Items
{
    public class Banner
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public IEnumerable<Image> Images { get; set; }
        public bool HasLeftLink { get; set; }
        public Link LeftLink { get; set; }
        public bool HasRightLink { get; set; }
        public Link RightLink { get; set; }
    }
}
