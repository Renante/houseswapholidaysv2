﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using RJP.MultiUrlPicker.Models;

namespace HouseSwapHolidays.Core.Models.Items
{
    public class JoinSection
    {
        public string Title { get; set; }
        public IHtmlString Content { get; set; }
        public Link Link { get; set; }
        public bool HasLink { get; set; }
    }
}
