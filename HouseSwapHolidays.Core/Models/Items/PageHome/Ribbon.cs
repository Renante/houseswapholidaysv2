﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RJP.MultiUrlPicker.Models;
using System.Web;

namespace HouseSwapHolidays.Core.Models.Items
{
    public class Ribbon
    {
        public string Title { get; set; }
        public IHtmlString Content { get; set; }
        public Link Link { get; set; }
        public bool HasLink { get; set; }
    }
}
