﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Models.Items
{
    public class Testimonial
    {
        public string ImageUrl { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public string Text { get; set; }
        
    }
}
