﻿using Archetype.Models;
using RJP.MultiUrlPicker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Web;
using HouseSwapHolidays.Core.Utilities;

namespace HouseSwapHolidays.Core.Models.Items.PageHome
{ 
    public class HowItWorks : IArchetypeObject
    {
        public HowItWorks()
        {

        }
        public HowItWorks(ArchetypeFieldsetModel fs)
        {
            Title = fs.GetValue<string>("title");
            Content = fs.GetValue<string>("content").ToLineBreaksHtml();
            HowItWorksItems = fs.GetPropertyAsArchetypeObjectList<HowItWorksItem>("howItWorksItems").Take(3);
            Link = fs.HasValue("link") ? fs.GetValue<MultiUrls>("link").FirstOrDefault() : null;
            LinkB = fs.HasValue("linkB") ? fs.GetValue<MultiUrls>("linkB").FirstOrDefault() : null;

        }

        public string Title { get; set; }
        public IHtmlString Content { get; set; }
        public IEnumerable<HowItWorksItem> HowItWorksItems { get; set; }
        public HowItWorksItem HowItWorksItemFirst { get; set; }
        public HowItWorksItem HowItWorksItemSecond { get; set; }
        public HowItWorksItem HowItWorksItemThird { get; set; }
        public bool HasHowItWorksItemFirst { get; set; }
        public bool HasHowItWorksItemSecond { get; set; }
        public bool HasHowItWorksItemThird { get; set; }
        public Link Link { get; set; }
        public Link LinkB { get; set; }
    }
}
