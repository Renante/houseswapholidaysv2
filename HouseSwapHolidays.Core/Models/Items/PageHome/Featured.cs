﻿using Archetype.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HouseSwapHolidays.Core.Utilities;

namespace HouseSwapHolidays.Core.Models.Items.PageHome
{
    public class Featured : IArchetypeObject
    {
        public Featured()
        {
            
        }

        public Featured(ArchetypeFieldsetModel fs)
        {
            Title = fs.GetValue<string>("title");
            FeaturedItems = fs.GetPropertyAsArchetypeObjectList<FeaturedItem>("items");
        }

        public string Title { get; set; }
        public IEnumerable<FeaturedItem> FeaturedItems { get; set; }

    }
}
