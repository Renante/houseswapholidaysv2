﻿using Archetype.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using HouseSwapHolidays.Core.Utilities;

namespace HouseSwapHolidays.Core.Models.Items.PageHome {
    public class WhyJoinUs : IArchetypeObject
    {
        public WhyJoinUs()
        {
            
        }

        public WhyJoinUs(ArchetypeFieldsetModel fs)
        {
            Title = fs.GetValue<string>("title");
            Content = fs.GetValue<string>("content").ToLineBreaksHtml();
            WhyJoinUsItems = fs.GetPropertyAsArchetypeObjectList<WhyJoinUsItem>("items");
            LinkA = fs.GetValueAsNavigationItem("linkA");
            LinkA = fs.GetValueAsNavigationItem("linkB");
        }
        public string Title { get; set; }
        public IHtmlString Content { get; set; }
        public IEnumerable<WhyJoinUsItem> WhyJoinUsItems { get; set; }
        public NavigationItem LinkA { get; set; }
        public NavigationItem LinkB { get; set; }
    }
}
