﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RJP.MultiUrlPicker.Models;

namespace HouseSwapHolidays.Core.Models.Items
{
    public class Gallery
    {
        public string Name { get; set; }
        public Image Image { get; set; }
        public bool HasImage { get; set; }
        public Link Link { get; set; }
        public bool HasLink { get; set; }
    }
}
