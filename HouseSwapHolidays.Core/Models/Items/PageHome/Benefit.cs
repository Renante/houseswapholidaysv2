﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Models.Items
{
    public class Benefit
    {
        public string Title { get; set; }
        public IEnumerable<BenefitItem> BenefitItems { get; set; }
    }
}
