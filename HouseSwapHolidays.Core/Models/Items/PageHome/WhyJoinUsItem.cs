﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using HouseSwapHolidays.Core.Utilities;
using Archetype.Models;

namespace HouseSwapHolidays.Core.Models.Items.PageHome
{
    public class WhyJoinUsItem : IArchetypeObject
    {
        public WhyJoinUsItem()
        {
            
        }

        public WhyJoinUsItem(ArchetypeFieldsetModel fs)
        {
            var image = fs.GetValueAsMediaItem("image");
            ImageUrl = image != null ? image.Url : "";
            Title = fs.GetValue<string>("title");
            Content = fs.GetValue<string>("content").ToLineBreaksHtml();
        }
        public string ImageUrl { get; set; }
        public string Title { get; set; }
        public IHtmlString Content { get; set; }
    }
}
