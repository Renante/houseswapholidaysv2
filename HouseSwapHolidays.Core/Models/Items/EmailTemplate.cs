﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Models.Items
{
    public enum EmailTemplate
    {
        WelcomeEmail,
        ForgotPasswordResetKey,
        SendEnquiry,
        MembershipComplete,
        SendEnquiryCHS
    }
}
