﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RJP.MultiUrlPicker.Models;

namespace HouseSwapHolidays.Core.Models.Items
{
    public class SocialAccount
    {
        public string Title { get; set; }
        public string Icon { get; set; }
        public Link Link { get; set; }
        public bool HasLink { get; set; }
    }
}
