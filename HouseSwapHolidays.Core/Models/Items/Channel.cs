﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Models.Items
{
    public class Channel
    {
        public int Id { get; set; }
        public string ShortUrl { get; set; }
        public string LongUrl { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int OwnerId { get; set; }
        public bool IsDefault { get; set; }
        public string SupportEmailAddress { get; set; }
        public string SalesEmailAddress { get; set; }
        public string AdvertisingEmailAddress { get; set; }
        public string ShortPageTitle { get; set; }
        public string LongPageTitle { get; set; }
        public string StyleSheet { get; set; }
        public string MetaDescriptionGeneric { get; set; }
        public string MetaDescriptionResults { get; set; }
        public string MetaKeywordsGeneric { get; set; }
        public string AnalyticsTrackingCode { get; set; }
        public bool SupportMultiChannel { get; set; }
        public double SubscriptionRate { get; set; }
        public string MapKey { get; set; }
        public int ExchangeType { get; set; }
        public int SubscriptionPeriod { get; set; }



    }
}
