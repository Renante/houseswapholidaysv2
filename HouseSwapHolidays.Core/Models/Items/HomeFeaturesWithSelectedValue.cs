﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Models.Items
{
    public class HomeFeaturesWithSelectedValue
    {
        public int FeatureID { get; set; }
        public int FeatureTypeID { get; set; }
        public string Name { get; set; }
        public bool IsHomeFeature { get; set; }
    }
}
