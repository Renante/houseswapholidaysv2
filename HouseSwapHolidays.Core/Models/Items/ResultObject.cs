﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChristianHomeSwap.Core.Models.Util
{
    public class ResultObject
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
