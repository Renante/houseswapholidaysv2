﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HouseSwapHolidays.Core.Services;

namespace HouseSwapHolidays.Core.Models.Items
{
    /// <summary>
    /// To be use on Home Search Resuls
    /// </summary>
    public class SearchResult
    {
        public int Id { get; set; }
        public string ImagePath {
            get
            {
                var image = HomeService.Instance.GetFeaturedThumbnailImage(Id);
                if (image != null)
                    return image.MediumImagePath;
                else
                    return string.Empty;
            }
        }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }
        public bool IsFeatured { get; set; }
        public int BedRooms { get; set; }
        public int BathRooms { get; set; }
    }
}
