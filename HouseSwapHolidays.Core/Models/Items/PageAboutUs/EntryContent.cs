﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace HouseSwapHolidays.Core.Models.Items
{
    public class EntryContent
    {
        public string ImageUrl { get; set; }
        public bool HasImage { get; set; }
        public string Title { get; set; }
        public IHtmlString Content { get; set; }
    }
}
