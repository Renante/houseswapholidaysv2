﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace HouseSwapHolidays.Core.Models.Items
{
    public class FormContent
    {
        public string Title { get; set; }
        public IHtmlString Content { get; set; }
        public IHtmlString Quote { get; set; }
        public bool HasQuote { get; set; }
        public string Author { get; set; }
        public string Signature { get; set; }
    }
}
