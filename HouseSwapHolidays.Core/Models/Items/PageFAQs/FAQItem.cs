﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace HouseSwapHolidays.Core.Models.Items
{
    public class FAQItem
    {
        public string Title { get; set; }
        public IHtmlString Content { get; set; }
    }
}
