﻿using RJP.MultiUrlPicker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Models.Items
{
    public class FAQBottomContent
    {
        public string Title { get; set; }
        public Link Link { get; set; }
        public bool HasLink { get; set; }
    }
}
