﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.Items
{
    public class FAQ
    {
        public string Title { get; set; }
        public IEnumerable<IPublishedContent> FAQitems { get; set; }
        public bool HasFAQItem { get; set; }
    }
}
