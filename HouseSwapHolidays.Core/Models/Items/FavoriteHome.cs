﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Models.Items
{
    public class FavoriteHome
    {
        public int Id { get; set; }
        public int Homeid { get; set; }

        public string Location { get; set; }
        public int HomeMemberId { get; set; }
        public string HomeMemberName { get; set; }
    }
}
