﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Models.Items
{
    public class Home
    {
        public int HomeID { get; set; }
        public int MemberID { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string RegionCode { get; set; }
        public string CountryCode { get; set; }
        public string Postcode { get; set; }
        public string Suburb { get; set; }
        public string Location { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }
        public string RegionDescription { get; set; }
        public string OtherInformation { get; set; }
        public string AboutUs { get; set; }
        public string ThumbnailURL { get; set; }
        public int BedRooms { get; set; }
        public int BathRooms { get; set; }

        public string Longitude { get; set; }
        public string Lattitude { get; set; }

        public string AirportName { get; set; }
        public int AirportDistance { get; set; }
        public bool AllowDateOffers { get; set; }
        public bool AllowDestinationOffers { get; set; }

        public Common.Status Status { get; set; }

        public int DefaultChannel { get; set; }

        public List<HomeFeature> HomeFeatures { get; set; }
        public List<HomeFeature> NearbyFacilities { get; set; }
        public List<HomeFeature> OtherDetails { get; set; }
        public bool JanuaryAvailability { get; set; }
        public bool FebruaryAvailability { get; set; }
        public bool MarchAvailability { get; set; }
        public bool AprilAvailability { get; set; }
        public bool MayAvailability { get; set; }
        public bool JuneAvailability { get; set; }
        public bool JulyAvailability { get; set; }
        public bool AugustAvailability { get; set; }
        public bool SeptemberAvailability { get; set; }
        public bool OctoberAvailability { get; set; }
        public bool NovemberAvailability { get; set; }
        public bool DecemberAvailability { get; set; }

        public int PriorityRank { get; set; }
        public int ListingType { get; set; }
        
    }
}
