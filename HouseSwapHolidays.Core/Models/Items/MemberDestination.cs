﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Models.Items
{
    public class MemberDestination
    {
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string Destination { get; set; }
    }
}
