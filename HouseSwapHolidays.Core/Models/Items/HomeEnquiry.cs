﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using umbraco.NodeFactory;
using System.Web;

namespace HouseSwapHolidays.Core.Models.Items
{
    public class HomeEnquiry
    {
        public int Id { get; set; }
        public int HomeId { get; set; }
        public int SenderMemberId { get; set; }
        public int SenderChannelId { get; set; }
        public int RecipientMemberId { get; set; }
        public string FirstName { get; set; }
        public string GivenName { get; set; }
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string Location { get; set; }
        public DateTime EnquiryDate { get; set; }
        public bool Reviewed { get; set; }
        public int EnquiryStatus { get; set; }
        public bool HoldMessage { get; set; }


        public string FromEmail { get; set; }
        public string FromName { get; set; }
        public string ToEmail { get; set; }
        public string ToName { get; set; }
        public string MessageSummary { get; set; }
        public DateTime Date { get; set; }
    }
}
