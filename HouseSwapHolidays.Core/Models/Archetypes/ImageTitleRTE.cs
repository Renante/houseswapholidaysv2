﻿using Archetype.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HouseSwapHolidays.Core.Utilities;

namespace HouseSwapHolidays.Core.Models.Archetypes
{
    public class ImageTitleRTE : IArchetypeObject
    {
        public MediaItem Image { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        
        public ImageTitleRTE(ArchetypeFieldsetModel fs)
        {
            Image = fs.GetValueAsMediaItem("image");
            Title = fs.GetValue<string>("title");
            Content = fs.GetValue<string>("content");
        }
    }
}
