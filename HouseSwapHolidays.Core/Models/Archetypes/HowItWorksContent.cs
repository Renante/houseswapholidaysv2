﻿using Archetype.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HouseSwapHolidays.Core.Utilities;

namespace HouseSwapHolidays.Core.Models.Archetypes
{
    public class HowItWorksContent : IArchetypeObject
    {
        public IEnumerable<MediaItem> Gallery { get; set; }
        public string Content { get; set; }

        public HowItWorksContent(ArchetypeFieldsetModel fs)
        {
            Gallery = fs.GetPropertyAsMediaItems("gallery");
            Content = fs.GetValue<string>("content");
        }
    }
}
