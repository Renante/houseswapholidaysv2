﻿using Archetype.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HouseSwapHolidays.Core.Utilities;

namespace HouseSwapHolidays.Core.Models.Archetypes
{
    public class QuickSearch : IArchetypeObject
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string PlaceHolder { get; set; }
        public IEnumerable<IconTitleLink> Links { get; set; }

        public QuickSearch(ArchetypeFieldsetModel fs)
        {
            Title = fs.GetValue<string>("title");
            SubTitle = fs.GetValue<string>("subTitle");
            PlaceHolder = fs.GetValue<string>("searchPlaceholder");
            Links = fs.GetPropertyAsArchetypeObjectList<IconTitleLink>("linksss");
        }
    }
}
