﻿using Archetype.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HouseSwapHolidays.Core.Utilities;

namespace HouseSwapHolidays.Core.Models.Archetypes
{
    public class LatestListing : IArchetypeObject
    {
        public string Content { get; set; }
        public IEnumerable<ImageTitleRTELink> Homes { get; set; }
        public NavigationItem LinkA { get; set; }
        public NavigationItem LinkB { get; set; }

        public LatestListing(ArchetypeFieldsetModel fs)
        {
            Content = fs.GetValue<string>("content");
            Homes = fs.GetPropertyAsArchetypeObjectList<ImageTitleRTELink>("homes");
            LinkA = fs.GetValueAsNavigationItem("linkA");
            LinkB = fs.GetValueAsNavigationItem("linkB");
        }
    }
}
