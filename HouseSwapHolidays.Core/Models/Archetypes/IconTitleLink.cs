﻿using Archetype.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HouseSwapHolidays.Core.Utilities;

namespace HouseSwapHolidays.Core.Models.Archetypes
{
    public class IconTitleLink : IArchetypeObject
    {
        public string Icon { get; set; }
        public string Title { get; set; }
        public NavigationItem Link { get; set; }

        public IconTitleLink(ArchetypeFieldsetModel fs)
        {
            Icon = fs.GetValue<string>("icon");
            Title = fs.GetValue<string>("title");
            Link = fs.GetValueAsNavigationItem("link");
        }
    }
}
