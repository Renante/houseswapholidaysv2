﻿using Archetype.Models;
using HouseSwapHolidays.Core.Utilities;

namespace HouseSwapHolidays.Core.Models.Archetypes
{
    public class TitleContentLinkALinkB : IArchetypeObject
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public NavigationItem LinkA { get; set; }
        public NavigationItem LinkB { get; set; }

        public TitleContentLinkALinkB(ArchetypeFieldsetModel fs)
        {
            Title = fs.GetValue<string>("title");
            Content = fs.GetValue<string>("subTitle");
            LinkA = fs.GetValueAsNavigationItem("linkA");
            LinkB = fs.GetValueAsNavigationItem("linkB");
        }
    }
}
