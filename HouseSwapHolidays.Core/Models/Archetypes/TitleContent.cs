﻿using Archetype.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Models.Archetypes
{
    public class TitleContent : IArchetypeObject
    {
        public string Title { get; set; }
        public string Content { get; set; }

        public TitleContent(ArchetypeFieldsetModel fs)
        {
            Title = fs.GetValue<string>("title");
            Content = fs.GetValue<string>("content");
        }
    }
}
