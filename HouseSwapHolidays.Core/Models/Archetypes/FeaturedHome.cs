﻿using Archetype.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HouseSwapHolidays.Core.Utilities;

namespace HouseSwapHolidays.Core.Models.Archetypes
{
    public class FeaturedHome : IArchetypeObject
    {
        public string Title { get; set; }
        public IEnumerable<ImageTitleRTELink> FeaturedHomeItems { get; set; }

        public FeaturedHome(ArchetypeFieldsetModel fs)
        {
            Title = fs.GetValue<string>("title");
            FeaturedHomeItems = fs.GetPropertyAsArchetypeObjectList<ImageTitleRTELink>("featuredHomes");
        }
    }
}
