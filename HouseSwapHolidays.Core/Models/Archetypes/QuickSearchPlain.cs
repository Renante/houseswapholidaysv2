﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Archetype.Models;

namespace HouseSwapHolidays.Core.Models.Archetypes
{
    public class QuickSearchPlain : IArchetypeObject
    {
        public string Title { get; set; }
        public string SearchPlaceholder { get; set; }
        public string ButtonLabel { get; set; }

        public QuickSearchPlain(ArchetypeFieldsetModel fs)
        {
            Title = fs.GetValue<string>("title");
            SearchPlaceholder = fs.GetValue<string>("searchPlaceholder");
            ButtonLabel = fs.GetValue<string>("buttonLabel");
        }
    }
}
