﻿using Archetype.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Models.Archetypes
{
    public class PromotionalAreas : IArchetypeObject
    {
        public bool ShowPromoStripForInter { get; set; }
        public bool ShowPromoStripForLocal { get; set; }
        public bool ShowPromoCircleForInter { get; set; }
        public bool ShowPromoCircleForLocal { get; set; }

        public string PromoStripInterContent { get; set; }
        public string PromoStripLocalContent { get; set; }
        public string PromoCircleInterContent { get; set; }
        public string PromoCircleLocalContent { get; set; }

        public PromotionalAreas(ArchetypeFieldsetModel fs)
        {
            ShowPromoStripForInter = fs.GetValue<bool>("showPromoStripForInter");
            ShowPromoStripForLocal = fs.GetValue<bool>("showPromoStripForLocal");
            ShowPromoCircleForInter = fs.GetValue<bool>("showPromoCircleForInter");
            ShowPromoCircleForLocal = fs.GetValue<bool>("showPromoCircleForLocal");

            PromoStripInterContent = fs.GetValue<string>("promoStripInterContent");
            PromoStripLocalContent = fs.GetValue<string>("promoStripLocalContent");
            PromoCircleInterContent = fs.GetValue<string>("promoCircleInterContent");
            PromoCircleLocalContent = fs.GetValue<string>("promoCircleLocalContent");
        }
    }
}
