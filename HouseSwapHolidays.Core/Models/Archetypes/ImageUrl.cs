﻿using Archetype.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HouseSwapHolidays.Core.Utilities;

namespace HouseSwapHolidays.Core.Models.Archetypes
{
    public class ImageUrl : IArchetypeObject
    {
        public MediaItem Image { get; set; }
        public NavigationItem Link { get; set; }

        public ImageUrl(ArchetypeFieldsetModel fs)
        {
            Image = fs.GetValueAsMediaItem("image");
            Link = fs.GetValueAsNavigationItem("link");
        }
    }
}
