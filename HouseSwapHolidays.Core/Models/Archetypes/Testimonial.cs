﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HouseSwapHolidays.Core.Utilities;
using Archetype.Models;

namespace HouseSwapHolidays.Core.Models.Archetypes
{
    public class Testimonial : IArchetypeObject
    {
        public MediaItem Image { get; set; }
        public string Content { get; set; }
        public string Title { get; set; }
        public NavigationItem LinkA { get; set; }
        public NavigationItem LinkB { get; set; }

        public Testimonial(ArchetypeFieldsetModel fs)
        {
            Image = fs.GetValueAsMediaItem("image");
            Content = fs.GetValue<string>("content");
            Title = fs.GetValue<string>("title");
            LinkA = fs.GetValueAsNavigationItem("linkA");
            LinkB = fs.GetValueAsNavigationItem("linkB");
        }
    }
}
