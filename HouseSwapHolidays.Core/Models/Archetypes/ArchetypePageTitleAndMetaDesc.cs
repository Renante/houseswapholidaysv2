﻿using Archetype.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Models.Archetypes
{
    public class ArchetypePageTitleAndMetaDesc : IArchetypeObject
    {
        public ArchetypePageTitleAndMetaDesc(ArchetypeFieldsetModel fs)
        {
            PageTitle = fs.GetValue<string>("pageTitle");
            MetaDescription = fs.GetValue<string>("metaDescription");
        }

        public string PageTitle { get; set; }
        public string MetaDescription { get; set; }
    }
}
