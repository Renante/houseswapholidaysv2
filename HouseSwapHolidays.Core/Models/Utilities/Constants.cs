﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace HouseSwapHolidays.Core.Models.Utilities
{
    public static class Constants
    {
        public const int ChannelId = 2;
        public static class NodeId
        {
            public const int HomePage = 1227;
            public const int ViewHomes = 1230;
            public const int ViewHome = 1232;
            public const int Inbox = 1235;
            public const int AdvanceSearch = 1231;
            public const int Message = 1247;
            public const int AboutUs = 1233;
            public const int PopUps = 1180;
            public const int MyProfile = 1234;
            public const int Page404 = 1244;
            public const int Registration = 1236;
            public const int Activate = 1248;
            public const int TermsAndCondition = 1251;
            public const int EditDetails = 1242;

            public const int AdminMemberSummary = 1220;
            public const int AdminMemberEdit = 1221;
            public const int AdminMemberSearch = 1219;
            public const int AdminSendEmail = 1222;
            public const int AdminHomeEdit = 1223;
            public const int AdminHomeImages = 1225;
        }

        public static class NodeName
        {
            public const string HomePage = "PageHome";
            public const string Inbox = "PageInbox";
        }

        public static class NodeAlias
        {
            public const string ViewHome = "PageViewHome";
            public const string ViewHomes = "PageSearchResults";
            public const string MyProfile = "PageMyProfile";
            public const string AdvancedSearch = "PageAdvanceSearch";
            public const string JoinUs = "PageJoinUs";
        }

        public static class StepNodeId
        {
            public const int Step1 = 1237;
            public const int Step2 = 1238;
            public const int Step3 = 1239;
            public const int Step4 = 1240;
            public const int Step5 = 1241;
        }

        public static class SubscriberField
        {
            public const string MemberId = "Member Id";
            public const string FirstName = "First name";
            public const string LastName = "Last name";
            public const string DateCreated = "Date created";
            public const string LastLoggedIn = "Last logged in";
            public const string HomeId = "Home id";
            public const string MemberStatus = "Member status";
        }
    }
}
