﻿using HouseSwapHolidays.Core.Models.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageEditDetailsViewModel : BaseViewModel
    {
        public PageEditDetailsViewModel(IPublishedContent content)
            : base(content)
        {

        }
        public string FirstName { get; set; }
        public string GivenName { get; set; }
        public string EmailAddress { get; set; }
        public string Introduction { get; set; }

        public string Language1 { get; set; }
        public string Language2 { get; set; }
        public string Language3 { get; set; }

        public bool ReceiveNewsletter { get; set; }
        public bool AcceptTerms { get; set; }

        public List<string> Languages { get; set; }
    }
}
