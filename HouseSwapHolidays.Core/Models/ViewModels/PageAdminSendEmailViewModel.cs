﻿using HouseSwapHolidays.Core.Models.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageAdminSendEmailViewModel : BaseViewModel
    {
        public PageAdminSendEmailViewModel(IPublishedContent content)
            : base(content)
        {

        }
        public int MemberID { get; set; }
        public List<Common.EmailTemplate> EmailTemplateCollection { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }
    }
}
