﻿using HouseSwapHolidays.Core.Models.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageStepViewModel : BaseViewModel
    {
        public PageStepViewModel(IPublishedContent content)
            : base(content)
        {

        }
        public List<string> Languages { get; set; }
        public CountryCollection CountryCollection { get; set; }
        public MemberDestinationCollection MemberDestinationCollection { get; set; }
        public Common.Member Member { get; set; }
        public Home Home { get; set; }
        public bool HasHome { get; set; }
        public RegionCollection RegionCollection { get; set; }
        public bool HasRegions { get; set; }
        public HomeImageCollection HomeImageCollection { get; set; }
        public bool HomeHasImages { get; set; }

        public List<HouseSwapHolidays.Core.Models.Items.HomeFeaturesWithSelectedValue> HomeFacilities { get; set; }
        public List<HouseSwapHolidays.Core.Models.Items.HomeFeaturesWithSelectedValue> HomeActivities { get; set; }
        public List<HouseSwapHolidays.Core.Models.Items.HomeFeaturesWithSelectedValue> HomeRules { get; set; }
        public List<HouseSwapHolidays.Core.Models.Items.HomeFeaturesWithSelectedValue> HolidayToOffer { get; set; }
        public List<HouseSwapHolidays.Core.Models.Items.HomeFeaturesWithSelectedValue> SwapPref { get; set; }
        

        public List<string> HomeAvailability { get; set; }

        
        
    }

    

}
