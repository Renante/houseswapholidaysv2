﻿using HouseSwapHolidays.Core.Models.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HouseSwapHolidays.Core.Models.Items;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageAboutUsViewModel : BaseViewModel
    {
        public PageAboutUsViewModel(IPublishedContent content)
            : base(content)
        {

        }

        public bool HasEntryContent { get; set; }
        public bool HasFormContent { get; set; }

        public EntryContent EntryContent { get; set; }
        public FormContent FormContent { get; set; }
    }
}
