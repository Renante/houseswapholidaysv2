﻿using HouseSwapHolidays.Core.Models.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using Our.Umbraco.Ditto;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageStep4ViewModel : BaseViewModel
    {
        public PageStep4ViewModel(IPublishedContent content)
            : base(content)
        {

        }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Summary { get; set; }

        [DittoIgnore]
        public int HomeId { get; set; }
        [DittoIgnore]
        public HomeImageCollection HomeImageCollection { get; set; }
        [DittoIgnore]
        public HouseSwapHolidays.Core.Models.Items.StepsNavigation StepsNavigation { get; set; }
        [DittoIgnore]
        public bool HasStepsNavigation { get; set; }
    }
}
