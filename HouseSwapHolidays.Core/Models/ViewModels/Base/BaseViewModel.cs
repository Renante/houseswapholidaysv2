﻿using HouseSwapHolidays.Core.Models.Archetypes;
using HouseSwapHolidays.Core.Models.Items;
using HouseSwapHolidays.Core.Models.Items.Website;
using Our.Umbraco.Ditto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels.Base
{
    public class BaseViewModel : RenderModel
    {
        //public BaseViewModel() : base(UmbracoContext.Current.PublishedContentRequest.PublishedContent) { }
        public BaseViewModel(IPublishedContent content) : base(content) { }
        public int CurrentPageId { get; set; }
        public bool IsLoggedIn { get; set; }
        public HouseSwapHolidays.Core.Models.Items.Member Member { get; set; }
        public QuickLinks QuickLinks { get; set; }
        public bool IsPageInbox { get; set; }

        #region SEO Stuffs
        public string MetaTitle { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public string GoogleAnalytics { get; set; }
        public bool NoIndex { get; set; }
        #endregion

        #region Header

        public bool TransparentHeader { get; set; }
        public string LogoUrl { get; set; }
        public string LogoUrlTransparent { get; set; }
        public IEnumerable<IPublishedContent> TopMenu { get; set; }
        public NavigationItem JoinButton { get; set; }
        #endregion

        public bool HasBanner { get; set; }

        [DittoIgnore]
        public string PageTitle { get; set; }
        
        [DittoIgnore]
        public IPublishedContent root { get; set; }

        [DittoIgnore]
        public IPublishedContent HomePage { get; set; }

        [DittoIgnore]
        public bool IsHomePage { get; set; }

        [DittoIgnore]
        public Banner Banner { get; set; }
        [DittoIgnore]
        public bool HasRibbon { get; set; }
        [DittoIgnore]
        public Ribbon Ribbon { get; set; }

        #region Footer
        public bool HasLeftContent { get; set; }
        public bool HasFirstMenu { get; set; }
        public bool HasSecondMenu { get; set; }
        public bool HasFooterLogo { get; set; }
        public bool HasRightContent { get; set; }
        public bool HasBottomMenu { get; set; }

        public FooterLeftContent LeftContent { get; set; }
        public FooterMenu FirstMenu { get; set; }
        public FooterMenu SecondMenu { get; set; }
        public string FooterLogo { get; set; }
        public FooterRightContent RightContent { get; set; }
        public FooterMenu BottomMenu { get; set; }
        #endregion

        #region PopUps
        public PopUp PopRegistration { get; set; }
        public PopUp PopSignIn { get; set; }
        public PopUp PopForgotPassword { get; set; }
        public PopUp PopContactUs { get; set; }
        #endregion

        #region Global Settings
        public PromotionalAreas PromotionalAreas { get; set; }
        #endregion

    }
}