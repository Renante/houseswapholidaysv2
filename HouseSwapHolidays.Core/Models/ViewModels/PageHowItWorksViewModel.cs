﻿using HouseSwapHolidays.Core.Models.Archetypes;
using HouseSwapHolidays.Core.Models.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageHowItWorksViewModel : BaseViewModel
    {
        public PageHowItWorksViewModel(IPublishedContent content)
            : base(content)
        {

        }
        public ImageTitleRTE IntroBanner { get; set; }
        public HowItWorksContent Content { get; set; }
        public Testimonial Testimonial { get; set; }
    }
}
