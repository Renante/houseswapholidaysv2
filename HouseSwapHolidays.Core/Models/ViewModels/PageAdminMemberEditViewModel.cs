﻿using HouseSwapHolidays.Core.Models.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageAdminMemberEditViewModel : BaseViewModel
    {
        public PageAdminMemberEditViewModel(IPublishedContent content)
            : base(content)
        {

        }
        public int MemberID { get; set; }
        public string FirstName { get; set; }
        public string GivenName { get; set; }
        public string EmailAddress { get; set; }
        public string Introduction { get; set; }
        public string CreatedDate { get; set; }
        public string LastUpdated { get; set; }
        public string LastLoggedIn { get; set; }
        public int MemberStatus { get; set; }
        public bool IsMultiChannel { get; set; }
        public string Channel { get; set; }
        public List<Common.HomeEnquiry> HomeEnquiryCollection { get; set; }
        public List<StatusDD> StatusDropdown { get; set; }
        public List<Common.MemberDestination> MemberDestinations { get; set; }
        public List<Common.Country> Countries { get; set; }
    }
}
