﻿using HouseSwapHolidays.Core.Models.ViewModels.Base;
using Common;
using Our.Umbraco.Ditto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageStep2ViewModel : BaseViewModel
    {
        public PageStep2ViewModel(IPublishedContent content)
            : base(content)
        {

        }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Summary { get; set; }

        [DittoIgnore]
        public CountryCollection CountryCollection { get; set; }
        [DittoIgnore]
        public MemberDestinationCollection MemberDestinationCollection { get; set; }
        [DittoIgnore]
        public Common.Member Member { get; set; }
        [DittoIgnore]
        public HouseSwapHolidays.Core.Models.Items.StepsNavigation StepsNavigation { get; set; }
        [DittoIgnore]
        public bool HasStepsNavigation { get; set; }
    }
}
