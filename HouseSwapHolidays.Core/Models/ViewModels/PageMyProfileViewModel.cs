﻿using HouseSwapHolidays.Core.Models.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChristianHomeSwap.Core.Models;
using HouseSwapHolidays.Core.Models.Items;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageMyProfileViewModel : BaseViewModel
    {
        public PageMyProfileViewModel(IPublishedContent content)
            : base(content)
        {

        }
        public bool HasHome { get; set; }
        public bool MemberIsActive { get; set; }
        public int HomeId { get; set; }
        public string HomeImageUrl { get; set; }
        public string HomeDescription { get; set; }

        public bool HasUnreadMessage
        {
            get { return UnreadMessage > 0; }
        }

        public int UnreadMessage { get; set; }
        public List<FavoriteHome> FavoritesHomes { get; set; }
        public List<MemberDestination> MemberDestinations { get; set; }

        
    }
}
