﻿using HouseSwapHolidays.Core.Models.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageAdminMemberSummaryViewModel : BaseViewModel
    {
        public PageAdminMemberSummaryViewModel(IPublishedContent content)
            : base(content)
        {

        }
        public int MemberID { get; set; }
        public string FirstName { get; set; }
        public string GivenName { get; set; }
        public string EmailAddress { get; set; }
        public string CreatedDate { get; set; }
        public string LastUpdated { get; set; }
        public string LastLoggedIn { get; set; }
        public string Status { get; set; }
        public bool ReceiveNewsletter { get; set; }
        public bool IsMultiChannel { get; set; }
        public string ChannelName { get; set; }
        public List<Common.Home> HomeCollection { get; set; }
        public HomeEnquiryCollection Inbox { get; set; }
        public List<HouseSwapHolidays.Core.Controllers.SentHomeEnquiry> HomeEnquiryCollection { get; set; }
        public List<StatusDD> StatusDD { get; set; }
    }

    public class StatusDD
    {
        public int StatusId { get; set; }
        public string StatusName { get; set; }
    }
}
