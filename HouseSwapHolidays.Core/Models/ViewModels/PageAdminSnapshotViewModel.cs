﻿using HouseSwapHolidays.Core.Models.ViewModels.Base;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageAdminSnapshotViewModel : BaseViewModel
    {
        public PageAdminSnapshotViewModel(IPublishedContent content)
            : base(content)
        {

        }
        public List<Common.Member> MemberCollection { get; set; }
        public List<Home> HomeCollection { get; set; }
        public List<HomeEnquiry> HomeEnquiryCollection { get; set; }
    }
}
