﻿using HouseSwapHolidays.Core.Models.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageAdminHomeImagesViewModel : BaseViewModel
    {
        public PageAdminHomeImagesViewModel(IPublishedContent content)
            : base(content)
        {

        }
        public int HomeID { get; set; }
        public int MemberID { get; set; }
        public string HomeImageUrl { get; set; }
        public string HomeLocation { get; set; }
        public string HomeDescription { get; set; }
        public List<HomeImageViewModel> HomeImages { get; set; }
    }

    public class HomeImageViewModel
    {
        public int Id { get; set; }
        public string ImageSource { get; set; }
        public bool IsFeature { get; set; }
    }
}
