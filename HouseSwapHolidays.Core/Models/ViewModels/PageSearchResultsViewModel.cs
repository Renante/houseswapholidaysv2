﻿using HouseSwapHolidays.Core.Models.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HouseSwapHolidays.Core.Models.Items;
using Our.Umbraco.Ditto;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageSearchResultsViewModel : BaseViewModel
    {
        public PageSearchResultsViewModel(IPublishedContent content)
            : base(content)
        {

        }
        public string Title { get; set; }
        public string NoResultsMessage { get; set; }
        [DittoIgnore]
        public string HomesCountInfo { get; set; }
        [DittoIgnore]
        public List<SearchResult> SearchResults { get; set; }
        [DittoIgnore]
        public string LocationDropdownList { get; set; }
        [DittoIgnore]
        public int NewDayHomeSpan { get; set; }
        [DittoIgnore]
        public Paging Paging { get; set; }

        [DittoIgnore]
        public Common.HomeCollection HomeCollection { get; set; }
        [DittoIgnore]
        public Common.CountryCollection CountryCollection { get; set; }
        [DittoIgnore]
        public string Pagination { get; set; }

        [DittoIgnore]
        public int PageItemStart { get; set; }
        [DittoIgnore]
        public int PageItemEnd { get; set; }
        [DittoIgnore]
        public int TotalItems { get; set; }

        [DittoIgnore]
        public string ViewHomeUrl { get; set; }

    }
}
