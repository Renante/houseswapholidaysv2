﻿using HouseSwapHolidays.Core.Models.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageResetPasswordViewModel : BaseViewModel
    {
        public PageResetPasswordViewModel(IPublishedContent content)
            : base(content)
        {

        }
    }
}
