﻿using Common;
using HouseSwapHolidays.Core.Models.Archetypes;
using HouseSwapHolidays.Core.Models.Items;
using HouseSwapHolidays.Core.Models.Items.PageHome;
using HouseSwapHolidays.Core.Models.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageHomeV2ViewModel : BaseViewModel
    {
        public PageHomeV2ViewModel(IPublishedContent content)
            : base(content)
        {

        }

        public QuickSearch QuickSearch { get; set; }
        public QuickSearchPlain QuickSearchPlain { get; set; }
        public IEnumerable<BannerV2> Banners { get; set; }
        public HowItWorks HowItWorks { get; set; }
        public MediaItem SectionCImage { get; set; }
        public TitleContentLinkALinkB SectionCBlockContent { get; set; }
        public MediaItem SectionDImage { get; set; }
        public TitleContentLinkALinkB SectionDBlockContent { get; set; }
        public Featured Cases { get; set; }
        public TitleContentLinkALinkB SectionF { get; set; }
        public Archetypes.FeaturedHome FeaturedHome { get; set; }
        public WhyJoinUs SectionH { get; set; }
        //public TitleContentLinkALinkB LatestListingsLabel { get; set; }
        public LatestListing LatestListing { get; set; }
        public HomeCollection LatestListings { get; set; }
        public Archetypes.Testimonial Testimonial { get; set; }
    }
}
