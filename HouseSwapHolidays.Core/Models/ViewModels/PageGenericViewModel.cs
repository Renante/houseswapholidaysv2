﻿using HouseSwapHolidays.Core.Models.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageGenericViewModel : BaseViewModel
    {
        public PageGenericViewModel(IPublishedContent content)
            : base(content)
        {

        }
        public string Title { get; set; }
        public string Body { get; set; }
    }
}
