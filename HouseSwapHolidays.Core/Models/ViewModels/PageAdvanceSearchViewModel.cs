﻿using HouseSwapHolidays.Core.Models.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using Our.Umbraco.Ditto;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageAdvanceSearchViewModel : BaseViewModel
    {
        public PageAdvanceSearchViewModel(IPublishedContent content)
            : base(content)
        {

        }
        public CountryCollection CountryCollection { get; set; }

        public FeatureCollection Features { get; set; }
        public FeatureCollection Activities { get; set; }
        public FeatureCollection Types { get; set; }
        public FeatureCollection SwapPreferences { get; set; }
        public FeatureCollection OtherDetails { get; set; }

        [DittoIgnore]
        public string LocationDropdownList { get; set; }

    }
}
