﻿using Common;
using HouseSwapHolidays.Core.Models.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageSitemapViewModel : BaseViewModel
    {
        public PageSitemapViewModel(IPublishedContent content)
            : base(content)
        {

        }
        public CountryCollection CountryCollection { get; set; }

        public string Title { get; set; }
        public string BodyText { get; set; }
        public string BottomText { get; set; }
    }
}
