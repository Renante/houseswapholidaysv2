﻿using HouseSwapHolidays.Core.Models.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HouseSwapHolidays.Core.Models.Items;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageFAQViewModel : BaseViewModel
    {
        public PageFAQViewModel(IPublishedContent content)
            : base(content)
        {

        }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public IEnumerable<FAQ> LeftFAQs { get; set; }
        public IEnumerable<FAQ> RightFAQs { get; set; }
        public FAQBottomContent BottomContent { get; set; }
        public bool HasLFAQs { get; set; }
        public bool HasRFAQs { get; set; }
        public bool HasBottomContent { get; set; }
    }
}
