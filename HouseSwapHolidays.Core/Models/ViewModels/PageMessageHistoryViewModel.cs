﻿using HouseSwapHolidays.Core.Models.ViewModels.Base;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageMessageHistoryViewModel : BaseViewModel
    {
        public PageMessageHistoryViewModel(IPublishedContent content)
            : base(content)
        {

        }
        public HomeEnquiryCollection HomeEnquiryCollection { get; set; }
    }
}
