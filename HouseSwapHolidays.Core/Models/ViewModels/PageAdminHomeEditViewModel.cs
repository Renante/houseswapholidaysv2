﻿using HouseSwapHolidays.Core.Models.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageAdminHomeEditViewModel : BaseViewModel
    {
        public PageAdminHomeEditViewModel(IPublishedContent content)
            : base(content)
        {

        }
        public int HomeID { get; set; }
        public int MemberID { get; set; }
        public string City { get; set; }
        public string Location { get; set; }
        public string PostCode { get; set; }
        public string Suburb { get; set; }
        public int PriorityRank { get; set; }
        public Common.Status Status { get; set; }
        public string Longitude { get; set; }
        public string Lattitude { get; set; }
    }
}
