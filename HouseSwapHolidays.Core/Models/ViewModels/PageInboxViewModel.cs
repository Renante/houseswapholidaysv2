﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HouseSwapHolidays.Core.Models.Items;
using HouseSwapHolidays.Core.Models.ViewModels.Base;
using HouseSwapHolidays.Core.Utilities;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageInboxViewModel : BaseViewModel
    {
        public PageInboxViewModel(IPublishedContent content)
            : base(content)
        {

        }
        public List<Common.HomeEnquiry> HomeEnquiryCollection { get; set; }
        public Pager Pager { get; set; }

    }
}
