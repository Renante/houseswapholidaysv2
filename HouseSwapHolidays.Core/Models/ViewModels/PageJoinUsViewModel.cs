﻿using HouseSwapHolidays.Core.Models.Archetypes;
using HouseSwapHolidays.Core.Models.ViewModels.Base;
using Our.Umbraco.Ditto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageJoinUsViewModel : BaseViewModel
    {
        public PageJoinUsViewModel(IPublishedContent content)
            : base(content)
        {

        }
        [DittoIgnore]
        public string FormLabel { get; set; }
        [DittoIgnore]
        public string FormTitle { get; set; }
        public Testimonial Testimonial { get; set; }
    }
}
