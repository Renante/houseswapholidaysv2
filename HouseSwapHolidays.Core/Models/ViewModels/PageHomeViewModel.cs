﻿using HouseSwapHolidays.Core.Models.Items;
using HouseSwapHolidays.Core.Models.Items.PageHome;
using HouseSwapHolidays.Core.Models.ViewModels.Base;
using Our.Umbraco.Ditto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageHomeViewModel : BaseViewModel
    {
        public PageHomeViewModel(IPublishedContent content)
            : base(content)
        {

        }
        public string Title { get; set; }

        public bool HasHowItWork { get; set; }
        public bool HasDestination { get; set; }
        public bool HasWhyJoinUs { get; set; }
        public bool HasFeatured { get; set; }
        public bool HasBenefit { get; set; }
        public bool HasJoinSection { get; set; }
        

        [DittoIgnore]
        public HowItWorks HowItWork { get; set; }
        [DittoIgnore]
        public Destination Destination { get; set; }
        [DittoIgnore]
        public IEnumerable<Testimonial> Testimonials { get; set; }
        [DittoIgnore]
        public WhyJoinUs WhyJoinUs { get; set; }
        [DittoIgnore]
        public Featured Featured { get; set; }
        [DittoIgnore]
        public Benefit Benefit { get; set; }
        [DittoIgnore]
        public JoinSection JoinSection { get; set; }
    }

}