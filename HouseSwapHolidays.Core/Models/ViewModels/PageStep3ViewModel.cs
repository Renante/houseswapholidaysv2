﻿using HouseSwapHolidays.Core.Models.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using Our.Umbraco.Ditto;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageStep3ViewModel : BaseViewModel
    {
        public PageStep3ViewModel(IPublishedContent content)
            : base(content)
        {
            
        }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Summary { get; set; }

        [DittoIgnore]
        public Common.Member Member { get; set; }
        [DittoIgnore]
        public Home Home { get; set; }
        [DittoIgnore]
        public int HomeSummaryCharsLeft { get; set; }
        [DittoIgnore]
        public bool HasHome { get; set; }
        [DittoIgnore]
        public bool HasRegions { get; set; }

        [DittoIgnore]
        public CountryCollection CountryCollection { get; set; }
        [DittoIgnore]
        public RegionCollection RegionCollection { get; set; }
        [DittoIgnore]
        public MemberDestinationCollection MemberDestinationCollection { get; set; }
        [DittoIgnore]
        public HomeImageCollection HomeImageCollection { get; set; }
        [DittoIgnore]
        public bool HomeHasImages { get; set; }

        [DittoIgnore]
        public List<HouseSwapHolidays.Core.Models.Items.HomeFeaturesWithSelectedValue> HomeFacilities { get; set; }
        [DittoIgnore]
        public List<HouseSwapHolidays.Core.Models.Items.HomeFeaturesWithSelectedValue> HomeActivities { get; set; }
        [DittoIgnore]
        public List<HouseSwapHolidays.Core.Models.Items.HomeFeaturesWithSelectedValue> HomeRules { get; set; }
        [DittoIgnore]
        public List<HouseSwapHolidays.Core.Models.Items.HomeFeaturesWithSelectedValue> HolidayToOffer { get; set; }
        [DittoIgnore]
        public List<HouseSwapHolidays.Core.Models.Items.HomeFeaturesWithSelectedValue> SwapPref { get; set; }

        [DittoIgnore]
        public List<string> HomeAvailability { get; set; }
        [DittoIgnore]
        public HouseSwapHolidays.Core.Models.Items.StepsNavigation StepsNavigation { get; set; }
        [DittoIgnore]
        public bool HasStepsNavigation { get; set; }

    }
}
