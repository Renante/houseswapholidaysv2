﻿using HouseSwapHolidays.Core.Models.Archetypes;
using HouseSwapHolidays.Core.Models.ViewModels.Base;
using Our.Umbraco.Ditto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageActivateViewModel : BaseViewModel
    {
        public PageActivateViewModel(IPublishedContent content)
            : base(content)
        {
           
        }

        [DittoIgnore]
        public string PayPalLinkID { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string BodyB { get; set; }
        [DittoIgnore]
        public IEnumerable<ImageUrl> CreditCards { get; set; }
        public string Tip { get; set; }

        [DittoIgnore]
        public Testimonial Testimonial { get; set; }
    }
}
