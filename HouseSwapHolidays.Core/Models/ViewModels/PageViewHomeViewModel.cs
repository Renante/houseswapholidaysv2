﻿using HouseSwapHolidays.Core.Models.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HouseSwapHolidays.Core.Models.Items;
using HouseSwapHolidays.Core.Models.Archetypes;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Models.ViewModels
{
    public class PageViewHomeViewModel : BaseViewModel
    {
        public PageViewHomeViewModel(IPublishedContent content)
            : base(content)
        {

        }

        // Content
        public TitleRteLink PromoBox1 { get; set; }
        public TitleRteLink PromoBox2 { get; set; }

        // Home Data
        public int HomeID { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public IEnumerable<string> Images { get; set; }
        public string Introducing { get; set; }
        public string AboutYourHome { get; set; }
        public IEnumerable<string> Features { get; set; }
        public IEnumerable<string> NearbyFacilities { get; set; }
        public string OurLocation { get; set; }
        public string OtherComments { get; set; }
        public int BedRooms { get; set; }
        public int BathRooms { get; set; }

        public string Longitude { get; set; }
        public string Lattitude { get; set; }

        public string PreferredTravelDestinations { get; set; }
        public IEnumerable<string> PreferredDates { get; set; }
        public string ClosestAirport { get; set; }
        public string LanguageSpoken { get; set; }
        public string OtherDetails { get; set; }

        // Send Enquiry Info
        public int HomeOwnerId { get; set; }
        public string HomeOwnerName { get; set; }
        public string HomeOwerEmail { get; set; }
        public string MemberName { get; set; }
        public string MemberEmail { get; set; }

        public Common.HomeGalleryCollection HomeGalleryCollection { get; set; }
    }
}
