﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Models.Forms
{
    public class HomeFeaturedImage
    {
        public int HomeID { get; set; }
        public int FeaturedImageID { get; set; }
    }
}
