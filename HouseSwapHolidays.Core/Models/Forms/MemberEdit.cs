﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSwapHolidays.Core.Models.Forms
{
    public class MemberEdit
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string GivenName { get; set; }
        [EmailAddress]
        [Required]
        [Compare("ConfirmEmail")]
        public string EmailAddress { get; set; }
        [Required]
        public string ConfirmEmail { get; set; }

        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        [Required]
        public string Introduction { get; set; }

        public string Language1 { get; set; }
        public string Language2 { get; set; }
        public string Language3 { get; set; }

        public string ReceiveNewsletter { get; set; }
        public string AcceptTerms { get; set; }
    }
}
