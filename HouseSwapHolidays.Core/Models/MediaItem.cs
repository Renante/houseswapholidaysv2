﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace HouseSwapHolidays.Core.Models
{
    public class MediaItem
    {
        private readonly IPublishedContent _content;
        public MediaItem(IPublishedContent content)
        {
            _content = content;
        }

        public string Url
        {
            get { return _content != null ? _content.Url : null; }
        }
        public string Name
        {
            get { return _content != null ? _content.Name : null; }
        }
        public int Width
        {
            get { return _content != null ? _content.GetPropertyValue<int>("umbracoWidth") : 0; }
        }
        public int Height
        {
            get { return _content != null ? _content.GetPropertyValue<int>("umbracoHeight") : 0; }
        }

        public string GetImageUrl(string fallback)
        {
            if (string.IsNullOrEmpty(Url))
                return fallback;

            return Url;
        }

        public string GetImageUrl(string width, string height, string fallback = "")
        {
            return !string.IsNullOrEmpty(Url) ? Url + string.Format("?width={0}&height={1}", width, height) : fallback;
        }

        public bool HasUrl()
        {
            return !string.IsNullOrEmpty(Url);
        }

        public string AltText
        {
            get { return _content != null ? _content.GetPropertyValue<string>("altText", _content.Name) : ""; }
        }

        public string TitleText
        {
            get { return _content != null ? _content.GetPropertyValue<string>("titleText", _content.Name) : ""; }
        }
    }
}
