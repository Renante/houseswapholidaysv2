﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace HouseSwapHolidays.Core.Models
{
    [DataContract]
    public class NavigationItem
    {
        private readonly IPublishedContent _content;
        public NavigationItem()
        {
            
        }
        public NavigationItem(IPublishedContent content, bool isActive = false)
        {
            Link = content.Url;
            Title = content.GetNavigationName();
            IsActive = isActive;
            TrackingName = content.GetPropertyValue<string>("trackingName");

            _content = content;
        }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Link { get; set; }
        public string Target { get; set; }
        public bool IsActive { get; set; }
        public string TrackingName { get; set; }

        public bool HasUrl()
        {
            return !string.IsNullOrEmpty(Link);
        }

        private NavigationItem[] _subItems;
        public NavigationItem[] SubItems {
            get
            {
                if (_content == null)
                    return null;

                if (_subItems != null)
                    return _subItems;

                _subItems = _content.Children.Where(x => x.IsVisible()).Select(x => new NavigationItem(x)).ToArray();
                return _subItems;
            }
            set { _subItems = value; }
        }


        public string AltText { get;set; }
        
    }
}
