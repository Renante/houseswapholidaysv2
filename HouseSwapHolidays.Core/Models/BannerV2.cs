﻿using Archetype.Models;
using HouseSwapHolidays.Core.Models.Archetypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HouseSwapHolidays.Core.Utilities;

namespace HouseSwapHolidays.Core.Models
{
    public class BannerV2 : IArchetypeObject
    {
        public MediaItem Image { get; set; }
        
        public BannerV2(ArchetypeFieldsetModel fs)
        {
            Image = fs.GetValueAsMediaItem("image");
        }
    }
}
