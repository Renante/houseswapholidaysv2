﻿using ChristianHomeSwap.Core.App_Code;
using HouseSwapHolidays.Core.Attributes;
using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.ViewModels;
using HouseSwapHolidays.Core.Services;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Core.Logging;
using System.Web.Configuration;
using Umbraco.Web;

namespace HouseSwapHolidays.Core.Controllers
{
    [MemberOnly(Status.Draft, Status.Active)]
    public class PageStep5Controller : BasePageController
    {
        BasePage bp = new BasePage();
        RegistrationPage rp = new RegistrationPage();

        public ActionResult Index()
        {
            var model = GetModel<PageStep5ViewModel>(CurrentPage);
            model.Title = CurrentPage.GetPropertyValue<string>("title");
            model.SubTitle = CurrentPage.GetPropertyValue<string>("subTitle");
            model.Summary = CurrentPage.GetPropertyValue<string>("summary");

            Home home = null;
            Member member = null;
            int memberID = -1;

            memberID = HouseSwapHolidays.Core.Services.MemberService.Instance.GetCurrentMemberId();
            member = MemberDAL.Instance.Get(memberID);
            if (member.Status == Status.Active || member.Status == Status.PendingApproval)
                return Redirect(Umbraco.Url(HouseSwapHolidays.Core.Models.Utilities.Constants.NodeId.MyProfile));

                //return RedirectToUmbracoPage(HouseSwapHolidays.Core.Models.Utilities.Constants.NodeId.MyProfile);
                
            if (Request.QueryString["id"] != null)
            {
                int paymentID = Int32.Parse(Request.QueryString["id"]);

                Payment payment = PaymentDAL.Instance.Get(paymentID);

                // TODO: Remove reference to hardcoded receipt number
                payment.ReceiptNumber = "123DSA53";

                if (payment.MemberID != memberID)
                {
                    // Member session does not match paymentid - possible hack attempt?
                    LogHelper.Info(this.GetType(), "Member session does not match paymentid - possible hack attempt. Redirecting to login screen. MemberID is " + payment.MemberID.ToString());
                    // **** This is comment due to payment***//  Response.Redirect(@""+this.SiteUrl+"member/details.aspx", true);

                }

                if (payment != null)
                {
                    Channel channel = ChannelDAL.Instance.Get(bp.GetChannelID());
                    int subscriptionPeriod = channel.SubscriptionPeriod;
                    DateTime subscriptionExpiry = DateTime.Now;

                    // Check if this is part of a promotion. If it is, get the new subscription period
                    if (payment.PromotionID > 0)
                    {
                        Promotion promotion = PromotionDAL.Instance.Get(payment.PromotionID);
                        subscriptionPeriod = promotion.SubscriptionPeriod;
                    }

                    // Mark payment as successful
                    payment.Status = PaymentStatus.Successful;
                    payment.PaymentCompleteDate = DateTime.Now;
                    PaymentDAL.Instance.Save(payment);

                    // Calculate subscription expiry for current home
                    subscriptionExpiry = DateTime.Now.AddMonths(subscriptionPeriod);

                    // Set home as active
                    home = HomeDAL.Instance.Get(payment.HomeID);

                    // DU 22/06/13 : Update to automatically approve as they have paid for their membership
                    home.Status = Status.Active;
                    //home.Status = Status.PendingApproval;

                    home.SubscriptionExpiryDate = subscriptionExpiry;
                    HomeDAL.Instance.Save(home);

                    // Prepare page text
                    // TODO: Display the expiry date somewhere
                    if (payment.PaymentAmount <= 0)
                    {
                        //PaymentMessage.Text = String.Format("Your complimentary home listing is valid until {0}.", subscriptionExpiry.ToString("dd-MMM-yy"));
                    }
                    else
                    {
                        //PaymentMessage.Text = String.Format("Thank you for your payment of {0}. Your receipt number is {1}.", payment.PaymentAmount.ToString("$#,##0.00"), payment.ReceiptNumber);
                    }

                    // Added 28/12/14 - Only attempt to reload the member if it isn't already loaded
                    if (member == null)
                    {
                        member = MemberDAL.Instance.Get(home.MemberID);
                    }

                    if (member != null)
                    {
                        member.Status = Status.Active;
                        MemberDAL.Instance.Save(member);
                        // this is new comment   SetMemberSession(member);
                    }

                    rp.EndRegistrationMode();

                    channel = ChannelDAL.Instance.Get(member.DefaultChannel);

                    // Generate a welcome email
                    //string subject = String.Format("Congratuations, your home is successfully registered on {0}", channel.Name);
                    //GenerateEmail.Instance.GenerateGenericEmail(member.DefaultChannel, EmailType.ApprovalGranted, member,
                        //home.ID, home.SubscriptionExpiryDate.ToString("dd MMM yyyy"), subject);

                    var membershipCompleteTemplate =
                        EmailTemplateService.Instance.Get(Models.Items.EmailTemplate.MembershipComplete);

                    var template = membershipCompleteTemplate.Replace("[FirstName]", member.FirstName);
                    template = template.Replace("[ExpiryDate]", home.SubscriptionExpiryDate.ToString("dd MMM yyyy"));
                    template = template.Replace("[MemberURL]", WebConfigurationManager.AppSettings["WebsiteUrl"] + Umbraco.Url(Models.Utilities.Constants.NodeId.MyProfile));
                    template = template.Replace("[HomeURL]", WebConfigurationManager.AppSettings["WebsiteUrl"] + Umbraco.Url(Models.Utilities.Constants.NodeId.ViewHome) + "?id=" + home.ID);
                    template = template.Replace("[WebsiteName]", WebConfigurationManager.AppSettings["WebsiteName"]);
                    template = template.Replace("[FromEmail]", "info@houseswapholidays.com.au");

                    EmailService.Instance.Send(member.EmailAddress, member.FirstName,
                        "Your home is now registered on " + WebConfigurationManager.AppSettings["WebsiteName"] + "!", template);
                    // Send home listing email
                    //GenerateEmail.Instance.GenerateHomeSuccessfullyListedEmail(Channel.ID, home.ID, member, subscriptionExpiry.ToString("dd MMM yyyy"));

                    // TODO: Send payment confirmation email.
                }

            }
            

            return CurrentTemplate(model);
        }
    }
}
