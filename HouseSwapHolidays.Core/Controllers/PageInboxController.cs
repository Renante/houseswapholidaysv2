﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.Items;
using HouseSwapHolidays.Core.Models.ViewModels;
using HouseSwapHolidays.Core.Services;
using Umbraco.Core.Logging;
using Common;

namespace HouseSwapHolidays.Core.Controllers
{
    [Authorize]
    public class PageInboxController : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<PageInboxViewModel>(CurrentPage);
            var memberId = MemberService.Instance.GetCurrentMemberId();
            
            var homeEnquiryCollection = HomeEnquiryDAL.Instance.GetAllForMember(memberId).Cast<Common.HomeEnquiry>();
            


            // pagination
            int recordPerPage = 10;
            int totalRecords = homeEnquiryCollection.Count();
            int currentPage = Request.QueryString["page"] != null ? int.Parse(Request.QueryString["page"]) : 1;

            HouseSwapHolidays.Core.Utilities.Pager pager = new HouseSwapHolidays.Core.Utilities.Pager(totalRecords, currentPage, recordPerPage);
            model.Pager = pager;
            // endpagination

            model.HomeEnquiryCollection = homeEnquiryCollection.Skip((currentPage - 1) * 10).Take(10).ToList();

            return CurrentTemplate(model);
        }
    }
}
