﻿using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using HouseSwapHolidays.Core.Utilities;
using HouseSwapHolidays.Core.Models.Archetypes;
using Umbraco.Web;

namespace HouseSwapHolidays.Core.Controllers
{
    public class PageJoinUsController : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<PageJoinUsViewModel>(CurrentPage);
            
            model.FormLabel = CurrentPage.GetPropertyValue<string>(Session.IsUserFreeMembership() ? "formLabel" : "formLabel2");
            model.FormTitle = CurrentPage.GetPropertyValue<string>("formTitle");
            model.Testimonial = CurrentPage.GetPropertyAsArchetypeObjectList<Testimonial>("testimonial").FirstOrDefault();
            
            return CurrentTemplate(model);
        }
    }
}
