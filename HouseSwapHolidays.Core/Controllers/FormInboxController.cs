﻿using Common;
using HouseSwapHolidays.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace HouseSwapHolidays.Core.Controllers
{
    public class FormInboxController : SurfaceController
    {
        public ActionResult DeleteEnquiry(int enquiryId)
        {
            var currentMemberId = MemberService.Instance.GetCurrentMemberId();
            HomeEnquiryDAL.Instance.Delete(enquiryId, currentMemberId);

            return RedirectToCurrentUmbracoPage();
        }
    }
}
