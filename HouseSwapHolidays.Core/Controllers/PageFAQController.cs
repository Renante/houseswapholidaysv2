﻿using Archetype.Models;
using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.Items;
using HouseSwapHolidays.Core.Models.ViewModels;
using RJP.MultiUrlPicker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Web;

namespace HouseSwapHolidays.Core.Controllers
{
    public class PageFAQController : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<PageFAQViewModel>(CurrentPage);

            model.Title = CurrentPage.GetPropertyValue<string>("title");
            model.SubTitle = CurrentPage.GetPropertyValue<string>("subTitle");

            var lfaqs = CurrentPage.GetPropertyValue<ArchetypeModel>("leftFaqs");
            var rfaqs = CurrentPage.GetPropertyValue<ArchetypeModel>("rightFaqs");
            var bottomContent = CurrentPage.GetPropertyValue<ArchetypeModel>("bottomContent").FirstOrDefault();

            model.HasLFAQs = CurrentPage.HasValue("leftFaqs");
            model.HasRFAQs = CurrentPage.HasValue("rightFaqs");
            model.HasBottomContent = CurrentPage.HasValue("bottomContent");

            if (model.HasLFAQs)
            {
                model.LeftFAQs = lfaqs.Select(s => new FAQ
                {
                    Title = s.GetValue<string>("title"),
                    HasFAQItem = s.HasValue("faQitems"),
                    FAQitems = s.HasValue("faQitems") ? s.GetValue<string>("faQitems").GetPropertyAsContentList() : null
                });
            }

            if (model.HasRFAQs)
            {
                model.RightFAQs = rfaqs.Select(s => new FAQ {
                    Title = s.GetValue<string>("title"),
                    HasFAQItem = s.HasValue("faQitems"),
                    FAQitems = s.HasValue("faQitems") ? s.GetValue<string>("faQitems").GetPropertyAsContentList() : null
                });
            }

            if (model.HasBottomContent)
            {
                model.BottomContent = new FAQBottomContent
                {
                    Title = bottomContent.GetValue<string>("title"),
                    HasLink = bottomContent.HasValue("link"),
                    Link = bottomContent.HasValue("link") ? bottomContent.GetValue<MultiUrls>("link").FirstOrDefault() : null
                };
            }

            return CurrentTemplate(model);
        }
    }
}
