﻿using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.Utilities;
using HouseSwapHolidays.Core.Models.ViewModels;
using HouseSwapHolidays.Core.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using HouseSwapHolidays.Core.Utilities;
using HouseSwapHolidays.Core.Models.Archetypes;
using System.Xml.Linq;
using System.Web.Configuration;

namespace HouseSwapHolidays.Core.Controllers
{
    
    public class PageViewHomeController : BasePageController
    {
        const int maxHomesToDisplay = 3;
        public ActionResult Index()
        {

            var qid = Request.QueryString["id"];
            var model = GetModel<PageViewHomeViewModel>(CurrentPage);

            // Content
            var isFreeMembership = Session.IsUserFreeMembership();
            model.PromoBox1 = CurrentPage.GetPropertyAsArchetypeObjectList<TitleRteLink>(
                isFreeMembership ? "promoBox1" : "promoBox1_notfree"
                ).FirstOrDefault();
            model.PromoBox2 = CurrentPage.GetPropertyAsArchetypeObjectList<TitleRteLink>(
                isFreeMembership ? "promoBox2" : "promoBox2_notfree"
                ).FirstOrDefault();

            // Data

            string memberName = string.Empty;
            string memberEmail = string.Empty;

            // Get current member info if logged in
            if (model.IsLoggedIn)
            {
                int memberId = MemberService.Instance.GetCurrentMemberId();
                var currentmember = MemberService.Instance.Get(memberId);
                memberName = string.Format("{0} {1}", currentmember.FirstName, currentmember.GivenName);
                memberEmail = currentmember.EmailAddress;
            }
            

            if (qid != null) { 

                var id = int.Parse(qid.ToString());
                var home = HomeService.Instance.Get(id);
                var member = MemberService.Instance.Get(home.MemberID);

                if (home == null)
                {
                    // Redirect to 404
                }

                // Force no index for inactive and not-hsh home.
                if ((home.Status != Common.Status.Active) || (home.DefaultChannel != 2))
                {
                    model.NoIndex = true;
                }

                model.HomeID = home.HomeID;
                model.Title = home.Location;
                model.SubTitle = home.Summary;
                model.Introducing = member.Introduction;
                model.AboutYourHome = home.Description;
                model.Features = home.HomeFeatures.Select(s => s.Name);
                model.NearbyFacilities = home.NearbyFacilities.Select(s => s.Name);
                model.OurLocation = home.RegionDescription;
                model.Images = HomeService.Instance.GetHomeImages(id).Select(s => s.GiantImagePath);
                model.OtherComments = home.OtherInformation;
                model.BedRooms = home.BedRooms;
                model.BathRooms = home.BathRooms;

                model.Longitude = home.Longitude;
                model.Lattitude = home.Lattitude;

                List<string> dates = new List<string>();
                AddPreferredDates(dates, home.JanuaryAvailability, "January");
                AddPreferredDates(dates, home.FebruaryAvailability, "February");
                AddPreferredDates(dates, home.MarchAvailability, "March");
                AddPreferredDates(dates, home.AprilAvailability, "April");
                AddPreferredDates(dates, home.MayAvailability, "May");
                AddPreferredDates(dates, home.JuneAvailability, "June");
                AddPreferredDates(dates, home.JulyAvailability, "July");
                AddPreferredDates(dates, home.AugustAvailability, "August");
                AddPreferredDates(dates, home.SeptemberAvailability, "September");
                AddPreferredDates(dates, home.OctoberAvailability, "October");
                AddPreferredDates(dates, home.NovemberAvailability, "November");
                AddPreferredDates(dates, home.DecemberAvailability, "December");

                model.PreferredDates = dates;

                List<string> langSpoken = new List<string>();
                if (!string.IsNullOrEmpty(member.Language1))
                    langSpoken.Add(member.Language1);
                if (!string.IsNullOrEmpty(member.Language2))
                    langSpoken.Add(member.Language2);
                if (!string.IsNullOrEmpty(member.Language3))
                    langSpoken.Add(member.Language3);

                List<string> prefTravelDest = new List<string>();
                prefTravelDest.AddRange(MemberService.Instance.GetMemberDestinations(home.MemberID).Select(s => FormatDestination(s.Destination, s.CountryName) ));

                

                if (home.AllowDestinationOffers)
                {
                    prefTravelDest.Add("Open to Offers");
                }

                model.PreferredTravelDestinations = string.Join(", ", prefTravelDest);
                model.ClosestAirport = string.Format("{0} ({1} kms)", home.AirportName, home.AirportDistance);
                model.LanguageSpoken = string.Join(", ", langSpoken);
                model.OtherDetails = string.Join(", ", home.OtherDetails.Select(s => s.Name));
                
                
                model.MemberName = memberName;
                model.MemberEmail = memberEmail;
                model.HomeOwnerId = member.Id;
                model.HomeOwnerName = member.FirstName;
                model.HomeOwerEmail = member.EmailAddress;

                
                Common.HomeGalleryCollection homeGalleryCollection;
                int numResults = 5;

                // Don't use caching at this point in time

                //homeGalleryCollection = (HomeGalleryCollection)cacheManager.Get("homeGalleryCollection");
                //if (homeGalleryCollection == null)
                //{
                // If homeGalleryCollection is not in cache then read it from DB
                homeGalleryCollection = Common.HomeGalleryDAL.Instance.GetAll(500, int.Parse(WebConfigurationManager.AppSettings["ChannelID"]), 4);
                //}

                model.HomeGalleryCollection = GetRandomHomes(homeGalleryCollection, numResults);


                if(home.RegionDescription.Length > 0)
                {
                    model.PageTitle = String.Format("Free home exchange in {0} (ID {1}) - {2}", home.Location, home.HomeID.ToString(), "HouseSwapHolidays.com.au");
                    model.MetaDescription = String.Format("Free home exchange in {0}! {1}", home.City, home.Summary);
                }

                return CurrentTemplate(model);
                
            }

            else
            {
                return Redirect(Umbraco.Url(Constants.NodeId.Message) + "?t=Home not found.");
            }
            
            
        }

        public string FormatDestination(string destination, string countryName)
        {
            if (!string.IsNullOrEmpty(destination))
            {
                return string.Format("{0} ({1})", destination, countryName);
            }
            else {
                return countryName;
            }
        }

        public void AddPreferredDates(List<string> dates, bool isAvail, string dateName)
        {
            if (isAvail)
                dates.Add(dateName);
        }

        private Common.HomeGalleryCollection GetRandomHomes(Common.HomeGalleryCollection homeGalleryCollection, int numResults)
        {
            Common.HomeGalleryCollection randomGalleryCollection = new Common.HomeGalleryCollection();

            int recordCount;

            if (numResults >= homeGalleryCollection.Count)
            {
                numResults = homeGalleryCollection.Count;
            }

            else

                recordCount = numResults;

            recordCount = homeGalleryCollection.Count;

            int nMax = numResults;
            int MyRandomNumber = -1;
            ArrayList alInts = new ArrayList(nMax);

            System.Random RandNum = new System.Random();
            for (int i = 0; i < numResults; )
            {
                MyRandomNumber = RandNum.Next(recordCount);
                if (false == alInts.Contains(MyRandomNumber))
                {

                    alInts.Add(MyRandomNumber);
                    // Replace thumbnails with larger images
                    homeGalleryCollection[MyRandomNumber].ImageURL = homeGalleryCollection[MyRandomNumber].ImageURL.Replace("/t", "/l");

                    if (i < maxHomesToDisplay)
                        randomGalleryCollection.Add(homeGalleryCollection[MyRandomNumber]);

                    i++;
                }

            }

            return randomGalleryCollection;
        }
    }
}
