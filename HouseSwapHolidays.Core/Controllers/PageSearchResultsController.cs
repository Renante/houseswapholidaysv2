﻿using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using ChristianHomeSwap.Core.DataAccess;
using HouseSwapHolidays.Core.Models.DataAccess;
using HouseSwapHolidays.Core.Services;
using HouseSwapHolidays.Core.Models.Items;
using Common;
using System.Configuration;
using System.Web;
using System.Xml.Linq;
using Umbraco.Core.Logging;
using System.Text.RegularExpressions;
using System.Web.Configuration;
using Umbraco.Web;
using HouseSwapHolidays.Core.Models.Utilities;
using HouseSwapHolidays.Core.Utilities;
using HouseSwapHolidays.Core.Models.Archetypes;

namespace HouseSwapHolidays.Core.Controllers
{
    public class PageSearchResultsController : BasePageController
    {

        #region Constants
        const int MIN_TOTAL_PAGES_RESULT = 1;
        const int MAX_TOTAL_PAGES_TO_DISPLAY = 15;
        const int FIRST_PAGE_NUMBER_ADJUSTER = 14;
        const int LAST_PAGE_NUMBER_ADJUSTER = 7;
        const int NEW_HOME_DAY_SPAN = 30;
        #endregion

        #region Fields
        private int _sessionmemberID;
        #endregion

        public ActionResult Index()
        {

            var model = GetModel<PageSearchResultsViewModel>(CurrentPage);

            model.Title = CurrentPage.GetPropertyValue<string>("title");
            model.NoResultsMessage = CurrentPage.GetPropertyValue<string>("noResultsMessage");

            var viewHomeUrl = Umbraco.TypedContentAtXPath("//" + Constants.NodeAlias.ViewHome).FirstOrDefault();
            model.ViewHomeUrl = viewHomeUrl != null ? viewHomeUrl.Url : "";

            var currentMemberId = 0;
            if (model.IsLoggedIn) { 
                currentMemberId = MemberService.Instance.GetCurrentMemberId();
            }

            model.CountryCollection = CountryDAL.Instance.GetAllByDefault();


            // bulding location dropdown list
            

            string countrySelection = "";
            string continentSelection = "";

            if ((Request.QueryString["cn"] != null))
            {
                continentSelection = Request.QueryString["cn"].ToUpper();
            }

            if ((Request.QueryString["c"] != null))
            {
                countrySelection = Request.QueryString["c"].ToUpper();
            }

            StringBuilder locationList2 = new StringBuilder();

            locationList2.Append("<SELECT name='l' class='dropdown' id='search-country' style=\"width:135px;\" >");
            locationList2.Append(generateDropdownItem("-1", "Any", ""));
            locationList2.Append("<optgroup label='Search by continent'>");
            ContinentCollection continentCollection = ContinentDAL.Instance.GetAll();
            foreach (Continent continent in continentCollection)
            {
                //ContinentList.Items.Add(new ListItem(continent.Name, continent.ID.ToString()));
                locationList2.Append(generateDropdownItem(continent.ID.ToString(), continent.Name, continentSelection, "cn"));
            }
            locationList2.Append("</OPTGROUP>");

            locationList2.Append("<optgroup label='Search by country'>");
            CountryCollection countryCollection = CountryDAL.Instance.GetAllByDefault(1);
            foreach (Country country in countryCollection)
            {
                locationList2.Append(generateDropdownItem(country.Code, country.Name, countrySelection, "c"));
                
            }

            locationList2.Append("</OPTGROUP>");
            locationList2.Append("</SELECT>");

            model.LocationDropdownList = locationList2.ToString();

            #region New Code

            string viewMode = (Request.QueryString["vm"] ?? string.Empty).ToLowerInvariant();
            bool isMap = viewMode == "m";
            bool isMapFile = false;//Request.QueryString["mf"] != null;

            // TODO what is this?
            //if (isMapFile)
            //{
            //    var cache = System.Web.Caching.Cache[GetCacheKey()];
            //    if (cache != null)
            //    {
            //        Response.ContentType = "text/xml";
            //        Response.Write(cache.ToString());
            //        Response.End();
            //        return;
            //    }
            //}
            // wooncherk

            // Init variables
            int homeCount = 0;
            _sessionmemberID = 0;

            // Set browser page title and stylesheet
            // TODO: Reactivate below

            Session["lastsearch"] = Request.Url.AbsoluteUri;

            HomeCriteria homeCriteria = new HomeCriteria();

            PageCriteria pageCriteria = new PageCriteria();

            // Populate sort by list

            if (ConfigurationSettings.AppSettings["NumResultsPerPage"] != null)
            {
                pageCriteria.PageSize = Int32.Parse(ConfigurationSettings.AppSettings["NumResultsPerPage"]);
            }
            else
            {
                pageCriteria.PageSize = 10;
            }

            // wooncherk
            //if (isMapFile)
            //{
            //    isMap = false;
            //    pageCriteria.PageSize = int.MaxValue;
            //}
            if (isMap)
            {
                pageCriteria.PageSize = int.MaxValue;
            }
            // wooncherk

            if (Request.QueryString["rd"] != null)
                DeleteReverseCookies();

            if (Request.QueryString["pg"] != null)
            {
                pageCriteria.CurrentPage = Int32.Parse(Request.QueryString["pg"]);
            }
            else
            {
                pageCriteria.CurrentPage = 1;
            }

            // Retrieve start month search criteria if provided
            if (Request.QueryString["s"] != null)
            {
                // implement try/catch for handling backwards compatability for start month params
                try
                {
                    homeCriteria.StartMonth = Int32.Parse(Request.QueryString["s"].Substring(0, Request.QueryString["s"].IndexOf("s")));
                }
                catch
                {
                    homeCriteria.StartMonth = Int32.Parse(Request.QueryString["s"]);
                }

                
            }

            if (Request.QueryString["e"] != null)
            {
                // implement try/catch for handling backwards compatability for start month params
                try
                {
                    homeCriteria.EndMonth = Int32.Parse(Request.QueryString["e"].Substring(0, Request.QueryString["e"].IndexOf("e")));
                }
                catch
                {
                    homeCriteria.EndMonth = Int32.Parse(Request.QueryString["e"]);
                }


            }

            if (Request.QueryString["sf"] != null)
            {
                int sortField = Int32.Parse(Request.QueryString["sf"]);
                UpdateSortCriteria(ref homeCriteria, sortField);

                // @@ TODO aspnet control
                //SortOptions.SelectedValue = sortField.ToString();
            }
            else
            {
                // Update to handle last minute searches
                if ((Request.QueryString["lst"] != null) && (Request.QueryString["lst"] == "1"))
                {
                    homeCriteria.SortField = (SortField)7;
                    // @@ TODO aspnet control
                    //SortOptions.SelectedValue = "7";
                }
            }

            if (Request.QueryString["so"] != null)
            {
                homeCriteria.SortOrder = (SortBy)Int32.Parse(Request.QueryString["so"]);
            }

            if ((Request.QueryString["k"] != null) && (Request.QueryString["k"].ToUpper() != "OPTIONAL"))
            {
                switch (Request.QueryString["k"].ToUpper())
                {
                    case "ACT":
                        homeCriteria.Keyword = "Australian Capital Territory";
                        break;

                    case "NSW":
                        homeCriteria.Keyword = "New South Wales";
                        break;

                    case "NT":
                        homeCriteria.Keyword = "Northern Territory";
                        break;

                    case "QLD":
                        homeCriteria.Keyword = "Queensland";
                        break;

                    case "SA":
                        homeCriteria.Keyword = "South Australia";
                        break;

                    case "TAS":
                        homeCriteria.Keyword = "Tasmania";
                        break;

                    case "VIC":
                        homeCriteria.Keyword = "Victoria";
                        break;

                    case "WA":
                        homeCriteria.Keyword = "Western Australia";
                        break;

                    default:
                        homeCriteria.Keyword = Request.QueryString["k"];
                        break;
                }
            }
            

            homeCriteria.FeatureList = Request.QueryString["f"];
            homeCriteria.CountryCode = Request.QueryString["c"];


            // Populate reverse search country list
            CountryCollection countryCollection2 = CountryDAL.Instance.GetAllByDefault();

            StringBuilder locationList = new StringBuilder();

            locationList.Append("<SELECT id=\"country-select\" >");
            //countryDropDown.Append("<OPTION value=0 selected>Choose a destination</OPTION>");
            locationList.Append(generateDropdownItem("0", "Select your country", ""));
            locationList.Append("<OPTGROUP label=\"Local Countries\"></OPTGROUP>");
            locationList.Append("<OPTION value=AU>&nbsp;&nbsp;Australia</OPTION>");
            locationList.Append("<OPTION value=NZ>&nbsp;&nbsp;New Zealand</OPTION>");
            locationList.Append("<OPTGROUP label=\"All countries\"></OPTGROUP>");

            string reverseCountryCode = "";
            string reverseCountryName = "";
            string reverseCity = "";
            string savedReverseCountryCode = "";
            string savedReverseCountryName = "";
            string savedReverseCity = "";
            string searchCountry = "";
            string searchCountryName = "";

            searchCountry = Request.QueryString["c"];

            bool hasSavedReverseLocation = false;

            if (Request.QueryString["rc"] != null)
            {
                // User is currently doing a reverse search as its passed in via the querystring
                reverseCountryCode = Request.QueryString["rc"];
            }
            else
            {
                // Lets check the cookie to see if a location for a reverse search has been saved
                savedReverseCountryCode = GetReverseCountryFromCookie();
                if (savedReverseCountryCode == "-1")
                {
                    savedReverseCountryCode = "";
                }

                savedReverseCity = GetReverseCityFromCookie();
                if (savedReverseCity == "-1")
                {
                    savedReverseCity = "";
                }

            }

            //ReverseCountry.Items.Clear();
            //ReverseCountry.Items.Add(new ListItem("Please select...", ""));
            foreach (Country country in countryCollection2)
            {
                //ListItem countryItem = new ListItem(country.Name, country.Code);
                //if (country.Code.ToUpper() == "AU")
                //{
                //    countryItem.Selected = true;
                //}
                //ReverseCountry.Items.Add(countryItem);
                locationList.Append(generateDropdownItem(country.Code.ToString(), country.Name, reverseCountryCode));

                if (reverseCountryCode == country.Code)
                    reverseCountryName = country.Name;

                if (savedReverseCountryCode == country.Code)
                {
                    savedReverseCountryName = country.Name;
                    reverseCountryCode = country.Code;
                }

                if ((searchCountry != null) && (searchCountry.Length > 1))
                {
                    if (searchCountry.ToUpper() == country.Code.ToUpper())
                        searchCountryName = country.Name;
                }

            }

            
            // Populate SEO fields
            string seoFieldAlias = string.Format("country{0}Keyword{1}", searchCountryName.Length > 0 ? "on" : "off", Request.QueryString["k"] != null && Request.QueryString["k"] != "" ? "on" : "off");
            var seoField = CurrentPage.GetPropertyAsArchetypeObjectList<ArchetypePageTitleAndMetaDesc>(seoFieldAlias).FirstOrDefault();
            string pageTitle = "";
            string metaDescription = "";
            
            if (seoField != null)
            {
                string keyword = Request.QueryString["k"] != null && Request.QueryString["k"] != ""
                    ? char.ToUpper(Request.QueryString["k"][0]) + Request.QueryString["k"].Substring(1)
                    : "";

                pageTitle = seoField.PageTitle?.Replace("[keyword]", keyword).Replace("[country]", searchCountryName.Length > 0 ? searchCountryName : "");
                metaDescription = seoField.MetaDescription?.Replace("[keyword]", keyword)
                    .Replace("[country]", searchCountryName.Length > 0 ? searchCountryName : "");
            }

            // Put default value to SEO fields if empty
            model.PageTitle = model.MetaTitle = !string.IsNullOrEmpty(pageTitle) ? pageTitle :  CurrentPage.GetPropertyValue<string>("pageTitle");
            model.MetaDescription = !string.IsNullOrEmpty(metaDescription) ? metaDescription : CurrentPage.GetPropertyValue<string>("metaDescription");
            
            locationList.Append("</select>");

            // TODO aspnet control
            //SelectReverseCountry.Text = locationList.ToString();


            if ((Request.QueryString["lst"] != null) && (Request.QueryString["lst"] == "1") && (Request.QueryString["lst"] == null))
            {
                // We are displaying last minutes, so hide reverse search option for now
                // TODO aspnet control
                //ReverseSearchMessage.Text = "Displaying properties that are open to last minute house swap holiday offers first";
            }
            else
            {
                if (reverseCountryName != "")
                {
                    if ((Request.QueryString["sf"] != null) && (Request.QueryString["sf"] == "0"))
                    {
                        // TODO aspnet control
                        //ReverseSearchMessage.Text = String.Format("Currently displaying results by {0}. Try our <a class=\"show-modal\" href=\"#\" title=\"Click to provide your location\">Reverse search</a> to display who wants to visit your location first", SortOptions.SelectedItem);
                    }
                    else
                    {
                        if ((Request.QueryString["rk"] == null) || (Request.QueryString["rk"] == ""))
                        {
                            // TODO aspnet control
                            //ReverseSearchMessage.Text = String.Format("Displaying who wants to visit your location ({0}) first. <a href=\"/search/results.aspx?rd=1\">clear location</a>", reverseCountryName);
                        }
                        else
                        {
                            string keyword = Request.QueryString["rk"];
                            // TODO aspnet control
                            //ReverseSearchMessage.Text = String.Format("Displaying who wants to visit your location ({0}, {1}) first. <a href=\"/search/results.aspx?rd=1\">clear location</a>", char.ToUpper(keyword[0]) + keyword.Substring(1), reverseCountryName);
                        }
                    }
                }
                else
                {
                    string reverseSearchURL = "";

                    foreach (String key in Request.QueryString.AllKeys)
                    {
                        // strip out any references to a previous reverse search
                        if ((key != "rc") && (key != "rk"))
                            reverseSearchURL = reverseSearchURL + "&" + key + "=" + Request.QueryString[key];
                    }

                    if (savedReverseCountryName != "")
                    {
                        if (savedReverseCity != "")
                        {
                            if ((reverseSearchURL.Length > 0) && (reverseSearchURL[0] == '&'))
                            {
                                reverseSearchURL = reverseSearchURL.Substring(1);
                                reverseSearchURL = String.Format("/search/results.aspx?{0}&rc={1}&rk={2}", reverseSearchURL, reverseCountryCode, savedReverseCity);
                            }
                            else
                            {
                                reverseSearchURL = String.Format("/search/results.aspx?rc={0}&rk={1}", reverseCountryCode, savedReverseCity);
                            }

                            // TODO aspnet control
                            //ReverseSearchMessage.Text = String.Format("New! Try our <a href=\"{0}\" title=\"Click to show best matches first\">Reverse search</a> to display who wants to visit your location ({1}, {2}) first. <a href=\"/search/results.aspx?rd=1\">clear location</a>", reverseSearchURL, savedReverseCity, savedReverseCountryName);
                        }
                        else
                        {
                            reverseSearchURL = String.Format("/search/results.aspx?{0}&rc={1}", reverseSearchURL, reverseCountryCode);
                            // TODO aspnet control
                            //ReverseSearchMessage.Text = String.Format("New! Try our <a href=\"{0}\" title=\"Click to show best matches first\">Reverse search</a> to display who wants to visit {1} first. <a href=\"/search/results.aspx?rd=1\">clear location</a>", reverseSearchURL, savedReverseCountryName);
                        }
                    }

                    //Response.Write("The retrieved country is " + GetReverseCountryFromCookie());
                    //Response.Write("The retrieved city is " + GetReverseCityFromCookie());
                }
            }

            // Populate country dropdown				
            //ReverseCountry.DataSource = countryCollection;
            //ReverseCountry.DataValueField = "Code";
            //ReverseCountry.DataTextField = "Name";
            //ReverseCountry.DataBind();


            // check if a continent has been specified
            if (Request.QueryString["cn"] != null)
            {
                homeCriteria.ContinentID = Int32.Parse(Request.QueryString["cn"]);
            }

            homeCriteria.RegionCode = Request.QueryString["r"];

            if (Request.QueryString["b"] != null)
            {
                homeCriteria.Bedrooms = Int32.Parse(Request.QueryString["b"]);
            }

            homeCriteria.PageCriteria = pageCriteria;
            homeCriteria.HomeCount = homeCount;


            Common.Channel ch = new Common.Channel();
            homeCriteria.IsMultiChannel = ch.SupportMultiChannel;

            // TODO : HACK HERE to force display of house swap holiday homes. Could read from config instead?
            homeCriteria.DefaultChannel = GetChannelID();


            //***FIXES PUT DEFAULT VALUE
            homeCriteria.Keyword = homeCriteria.Keyword ?? string.Empty;
            homeCriteria.RegionCode = homeCriteria.RegionCode ?? string.Empty;
            homeCriteria.CountryCode = homeCriteria.CountryCode ?? string.Empty;
            homeCriteria.FeatureList = homeCriteria.FeatureList ?? string.Empty;
            //***


            // DU 4Nov2013 - Only do a reverse search if they are not showing last minute offers first
            if (((Request.QueryString["rc"] != null) || (Request.QueryString["rk"] != null)) && (Request.QueryString["lst"] == null))
            {
                // they've requested reverse search so lets use it
                if (Request.QueryString["rc"] != null)
                {
                    homeCriteria.ReverseCountryCode = Request.QueryString["rc"];
                }

                if (Request.QueryString["rk"] != null)
                {
                    homeCriteria.ReverseDestination = Request.QueryString["rk"];
                }

                homeCriteria.ReverseFilterType = 3;

                //homeCriteria = this.SearchByPreferredDestination(homeCriteria);

                int sortByField;
                if (!Int32.TryParse(Request.QueryString["sf"], out sortByField)) sortByField = 0;
                UpdateSortCriteria(ref homeCriteria, sortByField);

                /* Commented out 04/05/12 as centralising in parent class */
                
                if (model.IsLoggedIn)
                {
                    // Edit the below 04/05/13
                    Common.Member member = MemberDAL.Instance.Get(currentMemberId);

                    homeCriteria.CurrentMemberID = member.ID;
                    _sessionmemberID = member.ID;

                    LogHelper.Info(this.GetType(), "searchedbypreferredesinationbychannedwithenquiies");
                    homeCriteria = HomeDAL.Instance.SearchByPreferredDestinationByChannelWithEnquiries(homeCriteria);

                }
                else
                {
                    LogHelper.Info(this.GetType(), "searchedbypreferredesinationbychannel");
                    homeCriteria = this.SearchByPreferredDestinationByChannel(homeCriteria);
                }
            }
            else
            {
                //if (((Member)Session["membersession"]) != null)

                
                if (model.IsLoggedIn)
                {
                    // Edit the below 04/05/13
                    Common.Member member = MemberDAL.Instance.Get(currentMemberId);

                    homeCriteria.CurrentMemberID = member.ID;
                    _sessionmemberID = member.ID;
                }

                // TODO uncomment below line fixed this
                //AddSortCriteria(homeCriteria);

                // wooncherk
                if (Request.QueryString["lst"] != null &&
                    Request.QueryString["lst"].ToLowerInvariant() == "1")
                {


                    if ((Request.QueryString["sf"] != null) && (Request.QueryString["sf"] != "7"))
                    {
                        // no nothing as specifying a sort order overwrites  last minute searches
                    }
                    else
                    {
                        homeCriteria.SortField = SortField.LastMinute;
                        homeCriteria.SortOrder = SortBy.Descending;
                    }
                }
                // wooncherk

                /* Commented out 04/05/12 as centralising in parent class
                homeCriteria = HomeDAL.Instance.SearchByChannel(homeCriteria); */

                // wooncherk
                //homeCriteria = this.SearchByChannel(homeCriteria);
                //if (isMapFile)
                //    homeCriteria = HomeDAL.Instance.SearchByMap(homeCriteria);
                //else if (isMap)
                //{
                //    homeCriteria.homeCollection = new HomeCollection();
                //}
                //else {
                //    homeCriteria = this.SearchByChannel(homeCriteria);
                //}

                if (isMap) {
                    homeCriteria = HomeDAL.Instance.SearchByMap(homeCriteria);
                }
                else
                {
                    homeCriteria = this.SearchByChannel(homeCriteria);
                }
                // wooncherk

            }

            if (homeCriteria.homeCollection.Count > 0)
            {
                // wooncherk
                //SearchResults.DataSource = homeCriteria.homeCollection;
                //SearchResults.DataBind();

                if (isMapFile)
                {
                    //Response.ContentType = "text/xml";
                    //Response.Write(GenerateMapJson(homeCriteria.homeCollection));
                    //Response.End();
                }
                else
                {
                    
                    // TODO aspnet control
                    //normalSearch.Visible = true;
                    //mapSearch.Visible = false;
                    

                    // TODO aspnet control
                    //SearchResults.DataSource = homeCriteria.homeCollection;
                    //SearchResults.DataBind();


                    model.HomeCollection = homeCriteria.homeCollection;
                }
                // wooncherk
            }
            else
            {
                
                // TODO aspnet controls
                //NoResultsSection.Visible = !isMap;
                //normalSearch.Visible = !isMap;
                //mapSearch.Visible = isMap;
                
            }

            
            model.TotalItems = homeCriteria.HomeCount;


            pageCriteria.NumResults = homeCriteria.HomeCount;
            // TODO aspnet controls
            //Literal pagedetails = new Literal();
            //Literal pagedetailsFooter = new Literal();


            // TODO aspnet controls
            //ViewMode.Text = GetViewMode();
            //MapViewMode.Text = GetViewMode();

            string pagination = string.Empty;
            if (homeCriteria.HomeCount > pageCriteria.PageSize)
            {
                // TODO aspnet controls
                //topPaginationWrapper.Visible = false;
                pagination = GetPaginationLinks(homeCriteria);
                model.Pagination = pagination;
                //TopPagination.Text = pagination;
                //BottomPagination.Text = pagination;
            }
            else
            {
                // TODO aspnet controls
                //topPaginationWrapper.Visible = false;
                //TopPagination.Visible = false;
                //BottomPagination.Visible = false;
            }

            // Calculate various page positions i.e. number of houses displayed etc
            int topDisplayedHome = 1;
            if (pageCriteria.CurrentPage > 1)
            {
                topDisplayedHome = ((pageCriteria.CurrentPage - 1) * pageCriteria.PageSize) + 1;
            }

            int bottomDisplayedHome = topDisplayedHome + pageCriteria.PageSize - 1;
            if (bottomDisplayedHome > homeCriteria.HomeCount)
            {
                bottomDisplayedHome = homeCriteria.HomeCount;
            }

            // Populate page literals
            // TODO aspnet controls
            //TopDisplayHomeNum.Text = topDisplayedHome.ToString();
            //BottomDisplayHomeNum.Text = bottomDisplayedHome.ToString();
            //TopDisplayHomeNum.Text = topDisplayedHome.ToString();
            //BottomDisplayHomeNum.Text = bottomDisplayedHome.ToString();

            model.PageItemStart = topDisplayedHome;
            model.PageItemEnd = bottomDisplayedHome;

            PageManager2 pg = new PageManager2();
            if (homeCriteria.CountryCode != null)
            {
                Country country = CountryDAL.Instance.GetCountryByCode(homeCriteria.CountryCode);
                if (country != null)
                {


                    // update the page title
                    //  new Comment  PageTitle.InnerText = "Home Exchange / Swap in " + country.Name + " @ " + pg.GetShortPageTitle();
                }
            }
            else if (homeCriteria.Keyword != null)
            {
                //new Comment   PageTitle.InnerText = "Home Exchange in " + homeCriteria.Keyword + " @ " + pg.GetShortPageTitle();
            }

            // Set browser page title and stylesheet
            // TODO : Reinstate page title code here and above
            //InitialisePage(PageTitle, StyleSheetName);

            if (!string.IsNullOrWhiteSpace(Request.QueryString["rc"]))
                ReverseCountry = Request.QueryString["rc"];

            if (!string.IsNullOrWhiteSpace(Request.QueryString["rk"]))
                ReverseCity = Request.QueryString["rk"];

            #endregion

            

            if (model.Content.HasValue("newHomeDaySpan"))
            {
                model.NewDayHomeSpan = model.Content.GetPropertyValue<int>("newHomeDaySpan");
            }
            else
            {
                model.NewDayHomeSpan = NEW_HOME_DAY_SPAN;
            }

            

            return CurrentTemplate(model);
        }

        public string ReverseCountry { get; set; }
        public string ReverseCity { get; set; }

        public string GetCacheKey()
        {
            var cacheKey = string.Format("s-{0}_e-{1}_c-{2}_k-{3}_rc-{4}_rk-{5}_cn-{6}_b-{7}_",
                Request.QueryString["s"], Request.QueryString["e"], Request.QueryString["c"],
                Request.QueryString["k"], Request.QueryString["rc"], Request.QueryString["rk"],
                Request.QueryString["cn"], Request.QueryString["b"]);

            return cacheKey;
        }

        public void DeleteReverseCookies()
        {
            // Expire cookies
            HttpCookie reverseLocationCookie = new HttpCookie("ReverseCountry", "");
            reverseLocationCookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(reverseLocationCookie);

            HttpCookie reverseCityCookie = new HttpCookie("ReverseCity", "");
            reverseCityCookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(reverseCityCookie);

        }

        private void UpdateSortCriteria(ref HomeCriteria criteria, int sortField)
        {
            criteria.SortOrder = SortBy.Ascending;

            switch (sortField)
            {
                case 0:
                    criteria.SortField = SortField.NotSpecified;
                    break;
                case 1:
                    criteria.SortField = SortField.DateListed;
                    criteria.SortOrder = SortBy.Ascending;
                    break;
                case 2:
                    criteria.SortField = SortField.City;
                    break;
                case 3:
                    criteria.SortField = SortField.Region;
                    break;
                case 4:
                    criteria.SortField = SortField.Country;
                    break;
                case 5:
                    criteria.SortField = SortField.Bedrooms;
                    break;
                //case 6:
                //    criteria.SortField = SortField.LastMinute;
                //    break;
                case 7:
                    criteria.SortField = SortField.LastMinute;
                    break;
                case 8:
                    criteria.SortField = SortField.DateListed;
                    criteria.SortOrder = SortBy.Descending;
                    break;
                default:
                    criteria.SortField = SortField.NotSpecified;
                    break;
            }
        }

        
        private string generateDropdownItem(string itemValue, string itemText, string selectedItem)
        {
            if (selectedItem.ToUpper() == itemValue.ToUpper())
            {

                return String.Format("<OPTION value='{0}' SELECTED>{1}</option>", itemValue, itemText);

            }
            else
            {
                return String.Format("<OPTION value='{0}'>{1}</option>", itemValue, itemText);
            }
        }

        private string generateDropdownItem(string itemValue, string itemText, string selectedItem, string type)
        {
            if (selectedItem.ToUpper() == itemValue.ToUpper())
            {

                return String.Format("<OPTION value='{0}' data-type='{1}' SELECTED>{2}</option>", itemValue, type, itemText);

            }
            else
            {
                return String.Format("<OPTION value='{0}' data-type='{1}'>{2}</option>", itemValue, type, itemText);
            }
        }

        // Get referrerID from cookie using default referrer cookie name
        public string GetReverseCountryFromCookie()
        {
            if (Request.Cookies["ReverseCountry"] != null)
            {
                return Request.Cookies["ReverseCountry"].Value.ToString();
            }

            return "-1";
        }

        // Get referrerID from cookie using default referrer cookie name
        public string GetReverseCityFromCookie()
        {
            if (Request.Cookies["ReverseCity"] != null)
            {
                return Request.Cookies["ReverseCity"].Value.ToString();
            }

            return "-1";
        }

        public int GetChannelID()
        {
            return Int32.Parse(ConfigurationSettings.AppSettings["ChannelID"]);
            //return 1;
        }

        protected HomeCriteria SearchByPreferredDestinationByChannel(HomeCriteria homeCriteria)
        {
            homeCriteria = HomeDAL.Instance.SearchByPreferredDestinationByChannel(homeCriteria);

            return homeCriteria;
        }

        private void AddSortCriteria(HomeCriteria criteria)
        {
            criteria.SortOrder = SortBy.Ascending;

            int sortByField = -1;
            // TODO SortOptions is using aspnet control
            //if (Int32.TryParse(SortOptions.SelectedValue, out sortByField))
            //{
            //    UpdateSortCriteria(ref criteria, sortByField);
            //}

        } //end AddSortCriteria

        public HomeCriteria SearchByChannel(HomeCriteria homeCriteria)
        {
            
            //switch (this.GetChannelID())
            //{
            //    case 1:
            //        // ChristianHomeSwap.com results with enquiries
            //        homeCriteria = HomeDAL.Instance.Search2(homeCriteria);
            //        break;

            //    case 2:
            //        // HouseSwapHolidays.com.au results with enquiries
            //        homeCriteria = HomeDAL.Instance.SearchByChannel(homeCriteria);
            //        break;
            //}
            //return homeCriteria;

            // HACK: above code for case 1 (HomeDAL.Instance.Search2) does not fully work because it only return incomplete data
            // so I used directly the case 2
            return HomeDAL.Instance.SearchByChannel(homeCriteria);
            //return HomeDAL.Instance.Search2(homeCriteria);
  
        }

        private string GetPaginationLinks(HomeCriteria criteria)
        {
            // Calculate the total number of pages.
            int totalPages = (int)(Math.Ceiling((double)criteria.HomeCount / (double)criteria.PageCriteria.PageSize));
            if (totalPages <= MIN_TOTAL_PAGES_RESULT)
            {
                // Either we've got no results, or only one page - either way, no need for pagination.
                return String.Empty;
            }

            // Get the base URL (i.e. without page number).
            string baseUrl = Request.Url.PathAndQuery;
            baseUrl = Regex.Replace(baseUrl, "pg=[0-9]+(&)?", "");



            if (baseUrl.EndsWith("&"))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            else if (baseUrl.EndsWith("results.aspx"))
            {
                baseUrl = baseUrl + "?";
            }

            // Calculate the info required to display links.
            bool displayPrevious = (criteria.PageCriteria.CurrentPage > 1);
            bool displayNext = (criteria.PageCriteria.CurrentPage < totalPages);
            int firstNumber = 1;
            int lastNumber = Math.Min(totalPages, MAX_TOTAL_PAGES_TO_DISPLAY);


            if (criteria.PageCriteria.CurrentPage >= MAX_TOTAL_PAGES_TO_DISPLAY)
            {
                // Adjust the first/last page numbers, depending on where we are at in the
                // results set.
                lastNumber = Math.Min(criteria.PageCriteria.CurrentPage + LAST_PAGE_NUMBER_ADJUSTER, totalPages);
                firstNumber = lastNumber - FIRST_PAGE_NUMBER_ADJUSTER;
            }

            // Build the pagination links to display.
            StringBuilder pagination = new StringBuilder();
            
            if (displayPrevious)
            {
                pagination.Append(GetPaginationLink(baseUrl, (criteria.PageCriteria.CurrentPage - 1), "&lt;&lt; Previous"));
            }

            for (int i = firstNumber; i <= lastNumber; ++i)
            {
                if (criteria.PageCriteria.CurrentPage == i)
                {
                    pagination.AppendFormat("<li><a class=\"active\" href=\"#\">{0}</a>", i);
                }
                else
                {
                    pagination.Append(GetPaginationLink(baseUrl, i, i.ToString()));
                }
            }

            if (displayNext)
            {
                pagination.Append(GetPaginationLink(baseUrl, (criteria.PageCriteria.CurrentPage + 1), "Next &gt;&gt;"));
            }

            // Return the pagination info.
            return pagination.ToString();
        }

        private string GetPaginationLink(string baseUrl, int pageNumber, string text)
        {
            // Generate unique width for previous and next 
            string style = "";
            if(text.Contains("Previous"))
                style = "style=\"width:92px\"";
            if(text.Contains("Next"))
                style = "style=\"width:66px\"";

            

            // TODO: Loses query string here so need to reimplement
            return String.Format("<li {0}><a class=\"page-item\" data-id=\"{1}\">{2}</a>", style, pageNumber, text);
        }

        private string GenerateMapJson(HomeCollection homeCollection)
        {
            XDocument xdoc = new XDocument();
            var root = new XElement("markers");

            foreach (var item in homeCollection)
            {
                var home = item as Common.Home;

                if (!string.IsNullOrWhiteSpace(home.Latitude) &&
                    !string.IsNullOrWhiteSpace(home.Longitude))
                {
                    root.Add(
                        new XElement("marker",
                            new XAttribute("Id", home.ID),
                            new XAttribute("lat", home.Latitude),
                            new XAttribute("lng", home.Longitude),
                            new XAttribute("Location", home.Location),
                            new XAttribute("Summary", home.Summary),
                            new XAttribute("ThumbnailURL", string.IsNullOrWhiteSpace(home.ThumbnailURL) ?
                                string.Format("/images/branding/placeholders/medium_home{0}.jpg", home.ID % 4 + 1) :
                                home.ThumbnailURL),
                            new XAttribute("Bedrooms", home.Bedrooms),
                            new XAttribute("Bathrooms", home.Bathrooms)));
                }
            }

            xdoc.Add(root);

            var result = xdoc.ToString(SaveOptions.DisableFormatting);

            // TODO @@ what is this???
            //Cache.Insert(GetCacheKey(), result, null, System.Web.Caching.Cache.NoAbsoluteExpiration,
            //    TimeSpan.FromMinutes(30));

            return result;
        }

        

    }
}
