﻿using Archetype.Models;
using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.Items;
using HouseSwapHolidays.Core.Models.ViewModels;
using RJP.MultiUrlPicker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using HouseSwapHolidays.Core.Models.Items.PageHome;
using Umbraco.Web;

namespace HouseSwapHolidays.Core.Controllers
{
    
    public class PageHomeController : BasePageController
    {
        public ActionResult Index()
        {
            
            var model = GetModel<PageHomeViewModel>(CurrentPage);

            var howItWorks = CurrentPage.GetPropertyValue<ArchetypeModel>("howItWorks").FirstOrDefault();
            var destination = CurrentPage.GetPropertyValue<ArchetypeModel>("destination").FirstOrDefault();
            var testimonials = CurrentPage.GetPropertyValue<ArchetypeModel>("testimonials");
            var whyJoinUs = CurrentPage.GetPropertyValue<ArchetypeModel>("whyJoinUs").FirstOrDefault();
            var featured = CurrentPage.GetPropertyValue<ArchetypeModel>("featured").FirstOrDefault();
            var benefit = CurrentPage.GetPropertyValue<ArchetypeModel>("benefit").FirstOrDefault();
            var joinSection = CurrentPage.GetPropertyValue<ArchetypeModel>("joinSection").FirstOrDefault();
            
            model.HasHowItWork = howItWorks != null;
            model.HasDestination = destination != null;
            model.HasWhyJoinUs = whyJoinUs != null;
            model.HasFeatured = featured != null;
            model.HasBenefit = benefit != null;
            model.HasJoinSection = joinSection != null;

            if (model.HasHowItWork) { 
                model.HowItWork = new HowItWorks
                {
                    Title = howItWorks.GetValue<string>("title"),
                    Content = howItWorks.GetValue<string>("content").ToLineBreaksHtml(),
                    HowItWorksItems = howItWorks.GetValue<ArchetypeModel>("howItWorksItems").Select(s => new HowItWorksItem
                    {
                        IconUrl = s.GetValue("icon").GetPropertyAsMediaUrl(),
                        Title = s.GetValue<string>("title"),
                        Content = s.GetValue<string>("content")
                    }),

                    Link = howItWorks.GetValue<Link>("link")
                };
            }

            if (model.HasDestination) { 
                model.Destination = new Destination
                {
                    Title = destination.GetValue<string>("title"),
                    Content = destination.GetValue<string>("content").ToLineBreaksHtml(),
                    Galleries = destination.HasValue("galleries") ?
                        destination.GetValue<ArchetypeModel>("galleries").Select(s => new Gallery
                        {
                            Name = s.GetValue<string>("name"),
                            Image = s.HasValue("image") ? new Image {
                                Name = Umbraco.TypedMedia(s.GetValue<int>("image")).Name,
                                Url = s.GetValue("image").GetPropertyAsMediaUrl(),
                            } : null,
                            HasImage = s.HasValue("image"),
                            Link = s.HasValue("link") ? s.GetValue<MultiUrls>("link").FirstOrDefault() : null,
                            HasLink = s.HasValue("link")
                        }) : Enumerable.Empty<Gallery>(),
                    Link = destination.HasValue("link") ?
                        destination.GetValue<MultiUrls>("link").FirstOrDefault() : null,
                    HasLink = destination.HasValue("link")
                };
            }

            model.Testimonials = testimonials.Select(s => new Testimonial {
                ImageUrl = s.GetValue("image").GetPropertyAsMediaUrl(),
                Name = s.GetValue<string>("name"),
                Location = s.GetValue<string>("location"),
                Text = s.GetValue<string>("text")
            });

            if (model.HasWhyJoinUs)
            {
                model.WhyJoinUs = new WhyJoinUs
                {
                    Title = whyJoinUs.GetValue<string>("title"),
                    Content = whyJoinUs.GetValue<string>("content").ToLineBreaksHtml(),
                    WhyJoinUsItems = whyJoinUs.GetValue<ArchetypeModel>("items").Select(s => new WhyJoinUsItem {
                        Title = s.GetValue<string>("title"),
                        Content = s.GetValue<string>("content").ToLineBreaksHtml(),
                        ImageUrl = s.GetValue<string>("image").GetPropertyAsMediaUrl()
                    })
                };
            }

            if (model.HasFeatured)
            {
                model.Featured = new Featured
                {
                    Title = featured.GetValue<string>("title"),
                    FeaturedItems = featured.GetValue<ArchetypeModel>("items").Select(s => new FeaturedItem {
                        Title = s.GetValue<string>("title"),
                        ImageUrl = s.GetValue<string>("image").GetPropertyAsMediaUrl(),
                        HasLink = s.HasValue("link"),
                        Link = s.HasValue("link") ? s.GetValue<MultiUrls>("link").FirstOrDefault() : null
                    })
                };
            }

            if (model.HasBenefit)
            {
                model.Benefit = new Benefit
                {
                    Title = benefit.GetValue<string>("title"),
                    BenefitItems = benefit.GetValue<ArchetypeModel>("items").Select(s => new BenefitItem {
                        Title = s.GetValue<string>("title"),
                        Content = s.GetValue<string>("content").ToLineBreaksHtml(),
                        ImageUrl = s.GetValue("image").GetPropertyAsMediaUrl()
                    })
                };
            }

            if (model.HasJoinSection)
            {
                model.JoinSection = new JoinSection
                {
                    Title = joinSection.GetValue<string>("title"),
                    Content = joinSection.GetValue<string>("content").ToLineBreaksHtml(),
                    HasLink = joinSection.HasValue("link"),
                    Link = joinSection.HasValue("link") ? joinSection.GetValue<MultiUrls>("link").FirstOrDefault() : null
                };
            }

            return CurrentTemplate(model);
            
        }
    }
}