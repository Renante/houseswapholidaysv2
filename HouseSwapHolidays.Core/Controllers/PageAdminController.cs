﻿using HouseSwapHolidays.Core.Attributes;
using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.Utilities;
using HouseSwapHolidays.Core.Models.ViewModels;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;

namespace HouseSwapHolidays.Core.Controllers
{
    [Admin]
    public class PageAdminController : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<PageAdminViewModel>(CurrentPage);

            return CurrentTemplate(model);
        }

        public ActionResult ImpersonateUser()
        {
            var memberID = int.Parse(Request.Form["memberid"]);
            var member = MemberDAL.Instance.Get(memberID);
            //FormsAuthentication.SetAuthCookie(string.Format("{0}_{1}", member.ID, member.EmailAddress), false);

            return Redirect(Umbraco.Url(Constants.NodeId.MyProfile));

        }
    }

}
