﻿using ChristianHomeSwap.Core.App_Code;
using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.ViewModels;
using HouseSwapHolidays.Core.Utilities;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using HouseSwapHolidays.Core.Models.Archetypes;
using Umbraco.Web;

namespace HouseSwapHolidays.Core.Controllers
{
    [Authorize]
    public class PageActivateController : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<PageActivateViewModel>(CurrentPage);

            model.Title = CurrentPage.GetPropertyValue<string>("title");
            model.Body = CurrentPage.GetPropertyValue<string>("body");
            model.BodyB = CurrentPage.GetPropertyValue<string>("bodyB");
            model.PayPalLinkID = CurrentPage.GetPropertyValue<string>("paypalID"); 
            model.CreditCards = CurrentPage.GetPropertyAsArchetypeObjectList<ImageUrl>("creditCardss");
            model.Testimonial = CurrentPage.GetPropertyAsArchetypeObjectList<Testimonial>("testimonial").FirstOrDefault();
            
            return CurrentTemplate(model);
        }

        
    }
}
