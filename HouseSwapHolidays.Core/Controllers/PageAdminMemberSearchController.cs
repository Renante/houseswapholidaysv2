﻿using HouseSwapHolidays.Core.Attributes;
using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HouseSwapHolidays.Core.Controllers
{
    [Admin]
    public class PageAdminMemberSearchController : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<PageAdminMemberSearchViewModel>(CurrentPage);

            

            return CurrentTemplate(model);
        }
    }
}
