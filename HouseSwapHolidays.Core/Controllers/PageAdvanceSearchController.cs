﻿using ChristianHomeSwap.Core.App_Code;
using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.ViewModels;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HouseSwapHolidays.Core.Controllers
{
    public class PageAdvanceSearchController : BasePageController
    {
        BasePage bp = new BasePage();
        public ActionResult Index()
        {
            var model = GetModel<PageAdvanceSearchViewModel>(CurrentPage);

            CountryCollection countryCollection = CountryDAL.Instance.GetAllByDefault(bp.GetChannelID());
            FeatureCollection featureCollection = FeatureDAL.Instance.GetAll();

            model.CountryCollection = countryCollection;
            model.Features = FeatureDAL.Instance.GetByFeatureType((int)FeatureType.HomeFacilities, featureCollection);
            model.Activities = FeatureDAL.Instance.GetByFeatureType((int)FeatureType.HomeActivities, featureCollection);
            model.Types = FeatureDAL.Instance.GetByFeatureType((int)FeatureType.HolidayTypesToOffer, featureCollection);
            model.SwapPreferences = FeatureDAL.Instance.GetByFeatureType((int)FeatureType.SwapTimePreferences, featureCollection);
            model.SwapPreferences.AddCollection(FeatureDAL.Instance.GetByFeatureType((int)FeatureType.SwapLength, featureCollection));
            model.OtherDetails = FeatureDAL.Instance.GetByFeatureType((int)FeatureType.HomeRules, featureCollection);




            StringBuilder locationList = new StringBuilder();

            locationList.Append("<select name='l' class='dropdown' id='search-country' style=\"width:135px;\" >");
            locationList.Append(generateDropdownItem("-1", "Any", ""));
            locationList.Append("<optgroup label='Search by continent'>");
            ContinentCollection continentCollection = ContinentDAL.Instance.GetAll();
            foreach (Continent continent in continentCollection)
            {
                locationList.Append(generateDropdownItem(continent.ID.ToString(), continent.Name, "cn"));
            }
            locationList.Append("</optgroup>");
            locationList.Append("<optgroup label='Search by country'>");
            
            foreach (Country country in countryCollection)
            {
                locationList.Append(generateDropdownItem(country.Code, country.Name, "c"));
            }

            locationList.Append("</optgroup>");
            locationList.Append("</select>");

            model.LocationDropdownList = locationList.ToString();

            return CurrentTemplate(model);
        }

        private string generateDropdownItem(string itemValue, string itemText, string type)
        {
            
            return String.Format("<option value='{0}' data-type='{1}'>{2}</option>", itemValue, type, itemText);
            
        }
    }
}
