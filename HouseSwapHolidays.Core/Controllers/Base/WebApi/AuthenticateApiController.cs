﻿using HouseSwapHolidays.Core.Models.DataAccess;
using HouseSwapHolidays.Core.Models.Forms;
using HouseSwapHolidays.Core.Models.Items;
using HouseSwapHolidays.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;
using Umbraco.Web.WebApi;

namespace HouseSwapHolidays.Core.Controllers.WebApi
{
    public class AuthenticateApiController : UmbracoApiController
    {

        RLK797_HomeExchangeEntities db = new RLK797_HomeExchangeEntities();

        [HttpPost]
        public LoginStatus Login(Member m)
        {

            LoginStatus status = new LoginStatus();
            bool isAdmin = false;

            bool isAuthenticated = MemberService.Instance.IsAuthenticated(m.EmailAddress, m.Password);
            if (isAuthenticated)
            {
                var member = MemberService.Instance.GetMemberByEmailAddress(m.EmailAddress);

                if (member.Status == (int)Common.Status.Deleted || member.Status == (int)Common.Status.Suspended)
                {
                    status.Success = false;
                    status.Message = "This membership may be cancelled or currently undergoing review";
                }

                else
                {

                    var allowedlist = WebConfigurationManager.AppSettings["admin"];
                    if (allowedlist != null)
                    {
                        isAdmin = allowedlist.SplitComma().Contains(member.Id.ToString());
                    }
                
                    FormsAuthentication.SetAuthCookie(string.Format("{0}_{1}_{2}",member.Id, isAdmin ? "admin" : "user", m.EmailAddress), false);

                    if(member.Status == (int)Common.Status.PrePayment)
                        status.RedirectUrl = "/activate/";
                    else
                        status.RedirectUrl = "/my-profile/";

                    status.Success = true;

                }
            }
            else {
                status.Success = false;
                status.Message = "Incorrect username or password!";
            }

            return status;

        }
        [HttpPost]
        public void SignOut()
        {
            FormsAuthentication.SignOut();
            
        }

        public string Register(Register r)
        {
            // validations
            if (!ModelState.IsValid)
                return "All fields are required and must valid";

            if (MemberService.Instance.IsEmailExist(r.EmailAddress))
                return "Email already exist!";

            // process
            var salt = PasswordService.Instance.GenerateRandomSalt();
            var hash = PasswordService.Instance.GenerateHashedPassword(r.Password, salt);

            // save
            db.up_InsertMember2v2(
                r.FirstName, //firstName
                r.LastName, //givenName
                r.EmailAddress, //emalAddress
                "", // password (this is empty because we started to implement hash password)
                "", //introduction
                "", //language1
                "", //language2
                "", //language3
                DateTime.Now, //createdDate
                DateTime.Now, //updatedDate
                DateTime.Now, //lastLoggedInDate
                0, //referrerID
                0, //countryID
                0, //typeID
                0, //promotionID
                r.ReceivedNewsletter, //receiveNewsletter
                true, //acceptedTerms (of course user cant proceed w/out checking it)
                false, //isMultiChannel
                1, //defaultChannel
                true, //acceptSpecialConditions
                Guid.NewGuid().ToString(), //GUID
                (int)Common.Status.PrePayment, //status
                salt, //salt
                hash //hashedPassword
                );

            // subscribe to campaign monitor
            if (r.ReceivedNewsletter)
            {
                CampaignMonitorService.Instance.Subscribe(r.EmailAddress);
            }
            
            // login
            Member m = new Member();
            m.EmailAddress = r.EmailAddress;
            m.Password = r.Password;
            Login(m);

            return "success";
        }
    }

    public class LoginStatus
    {
        public bool Success { get; set; }
        public string RedirectUrl { get; set; }
        public string Message { get; set; }
    }
}
