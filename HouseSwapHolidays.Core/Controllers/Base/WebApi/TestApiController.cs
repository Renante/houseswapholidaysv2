﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using HouseSwapHolidays.Core.Services;
using Umbraco.Web.WebApi;
using Common;
using HouseSwapHolidays.Core.Models.DataAccess;
using Umbraco.Core.Models;
using Umbraco.Web;
using System.Diagnostics;
using System.Web.Security;
using Umbraco.Core.Logging;
using HouseSwapHolidays.Core.Utilities;

namespace HouseSwapHolidays.Core.Controllers.WebApi
{
    
    public class TestApiController : UmbracoApiController
    {
        [HttpGet]
        public List<MyHome> GetHomes()
        {

            
            //GenerateEmail.Instance.SendEmail("renante", "renanteabril@gmail.com", "someone", "haunter_004@yahoo.com", "test subject", "test body", 1);
            RLK797_HomeExchangeEntities r = new RLK797_HomeExchangeEntities();

            var homes = r.Database.SqlQuery<MyHome>("SELECT HomeID, DefaultChannel, ContinentID From Home WHERE HomeID = 7590").ToList();

            return homes;

            
           
            
             
        }

        public IEnumerable<Common.Home> GetHomes2()
        {
            var x = Common.HomeDAL.Instance.GetAllActive(2).Cast<Common.Home>();

            return x.Take(10);
        }

        [HttpGet]
        public int Test2()
        {
            int memberId = MemberService.Instance.GetCurrentMemberId();
            return memberId;
            
        }

        [HttpGet]
        public void SendEmailNew()
        {
            var isSent = EmailService.Instance.Send("renanteabril@gmail.com", "Renante", "Testing New Email", "Testing New Email");
            if (isSent)
            {
                LogHelper.Info(this.GetType(), "Test email sent");
            }
            else
            {
                LogHelper.Info(this.GetType(), "Test email failed");
            }
        }

        [HttpGet]
        public bool IsFreeMembership()
        {
            return UmbracoContext.HttpContext.Session.IsUserFreeMembership();
        }

        [HttpGet]
        public void SendEmailOld()
        {

            GenerateEmail.Instance.SendEmail("Renante", "renanteabril@gmail.com", "HSH", "info@houseswapholidays.com.au", "Testing Old Email", "Testing Old Email", 2);

        }

        [HttpGet]
        public void TestGenerateEmail()
        {
            
        }

        [HttpGet]
        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }

        [HttpGet]
        public string Test()
        {
            Stopwatch stopwatch = new Stopwatch();
            

            stopwatch.Start();

            var node = GetNodeByAlias("PageStep3");
            
            
            stopwatch.Stop();

            

            return string.Format("Time ellapsed: " + stopwatch.Elapsed + " , NodeID: " + node.Id);
        }

        public static IPublishedContent GetNodeByAlias(string alias)
        {
            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            var contentNode = umbracoHelper.TypedContentSingleAtXPath(String.Format("//{0}", alias));

            return contentNode;
        }
    }

    public class MyHome
    {
        public int HomeID { get; set; }
        public int DefaultChannel { get; set; }
        public int ContinentID { get; set; }
    }
}
