﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Umbraco.Web.WebApi;

namespace HouseSwapHolidays.Core.Controllers.WebApi
{
    public class UtilitiesApiController : UmbracoApiController
    {
        [HttpGet]
        public RegionCollection GetRegionsForCountry(string countryCode)
        {
            RegionCollection regionCollection = RegionDAL.Instance.GetByCountryCode(countryCode);

            return regionCollection;
        }
    }
}
