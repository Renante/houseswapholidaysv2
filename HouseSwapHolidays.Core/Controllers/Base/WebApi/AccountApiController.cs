﻿using HouseSwapHolidays.Core.Models.DataAccess;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Umbraco.Web.WebApi;
using ChristianHomeSwap.Core.Models.Util;
using HouseSwapHolidays.Core.Services;

namespace HouseSwapHolidays.Core.Controllers.Base.WebApi
{
    public class AccountApiController : UmbracoApiController
    {
        RLK797_HomeExchangeEntities db = new RLK797_HomeExchangeEntities();

        [HttpGet]
        public ResultObject SendAndSavePasswordResetKey(string email)
        {
            var accountService = new AccountService();
            return accountService.SendAndSavePasswordResetKey(email);
        }

        [HttpGet]
        public string Test()
        {
            return "Hello World!";
        }

        [HttpGet]
        public bool IsEmailNotExist(string emailaddress)
        {
            return !MemberService.Instance.IsEmailExist(emailaddress);
        }

        [HttpGet]
        public bool IsEmailExist(string emailaddress)
        {
            return MemberService.Instance.IsEmailExist(emailaddress);
        }

    }
}
