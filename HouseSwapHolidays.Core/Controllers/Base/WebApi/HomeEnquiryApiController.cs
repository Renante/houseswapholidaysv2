﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Umbraco.Web.WebApi;
using Common;
using System.Configuration;
using ChristianHomeSwap.Core.App_Code;
using ChristianHomeSwap.Core.Models.Util;
using System.ComponentModel.DataAnnotations;
using Umbraco.Core.Logging;
using HouseSwapHolidays.Core.Models.DataAccess;
using HouseSwapHolidays.Core.Services;
using System.Web.Configuration;



namespace HouseSwapHolidays.Core.Controllers.WebApi
{
    public class HomeEnquiryApiController : UmbracoApiController
    {
        Channel ch = new Channel();
        BasePage bp = new BasePage();
        RLK797_HomeExchangeEntities db = new RLK797_HomeExchangeEntities();

        [HttpPost]
        public ResultObject Send(Enquiry e)
        {
            ResultObject r = new ResultObject();

            HomeEnquiry homeEnquiry = new HomeEnquiry();

            if(string.IsNullOrEmpty(e.Message)){
                r.Success = false;
                r.Message = "Message field is required!.";
                return r;
            }

            if (!ModelState.IsValid)
            {
                r.Success = false;
                r.Message = "The current enquiry is not valid, please contact administrator";
                LogHelper.Info(this.GetType(), "Either HomeID or MemberID is not valid ");
                return r;
            }

            // Retrieve home details, so we can obtain the recipient ID

            string countryName = "";
            var currentMember = HouseSwapHolidays.Core.Services.MemberService.Instance.GetCurrentMember();

            if (currentMember == null)
            {
                r.Success = false;
                r.Message = "Session has expired. Please login to continue.";
                return r;
            }

            int currentMemberID = currentMember.Id;
            int homeID = -1;

            // Get the sender's country
            HomeDAL.Instance.GetMemberCountry(ref countryName, currentMemberID);

            // Get the recipient's memberid based on the current home
            Member recipientMember = MemberDAL.Instance.GetMemberFromHome(e.HomeId);
            homeEnquiry.RecipientMemberID = recipientMember.ID;

            Channel recipientChannel = ChannelDAL.Instance.Get(recipientMember.DefaultChannel);
            
            homeEnquiry.HomeID = e.HomeId;
            homeEnquiry.FirstName = currentMember.FirstName;
            homeEnquiry.GivenName = currentMember.GivenName;
            homeEnquiry.Email = currentMember.EmailAddress;

            homeEnquiry.SenderMemberID = currentMemberID;


            homeEnquiry.Subject = String.Format("{0} enquiry received from {1} of {2}", recipientChannel.Name, homeEnquiry.FirstName, countryName);
            homeEnquiry.Message = e.Message;

            // Commented out the below 24/03/12 as it was incorrectly always setting as YHFM (0)
            //homeEnquiry.SenderChannelID = ch.ID;
            homeEnquiry.SenderChannelID = bp.GetChannelID();
            

            // Retrieve current member details from DB
            Member member = MemberDAL.Instance.Get(currentMemberID);

            // Get recipients channel URL
            string recipientChannelURL =  ch.ShortURL;
            if (recipientChannel != null)
            {
                recipientChannelURL = recipientChannel.ShortURL;
            }

            // Check if this message needs to be approved/placed on hold before it can be sent
            if ((ConfigurationSettings.AppSettings["PreApprovalRequired"] != null) && (ConfigurationSettings.AppSettings["PreApprovalRequired"].ToUpper() == "TRUE") && (member.Status == Status.PendingApproval)
                && (homeEnquiry.SenderMemberID != homeEnquiry.RecipientMemberID))
            {
                homeEnquiry.HoldMessage = true;
            }
            else
            {
                homeEnquiry.HoldMessage = false;
            }

            homeEnquiry = HomeEnquiryDAL.Instance.Save(homeEnquiry);

            string messageLink = String.Format("http://{0}/my-profile/inbox/", recipientChannelURL, homeEnquiry.ID);

            if (homeEnquiry.HoldMessage)
            {
                // Message needs to be approved as member is not yet approved
                //_log.Info(String.Format("Holding message ID {0} from {1} to {2}", homeEnquiry.ID.ToString(), homeEnquiry.SenderMemberID.ToString(), homeEnquiry.RecipientMemberID.ToString()));
            }
            else
            {
                // Generate enquiry email
                //GenerateEmail.Instance.GenerateHomeGenericNotification(ch.ID, homeEnquiry, countryName, messageLink);
                string sendEnquiryTemplate;
                
                if (recipientMember.DefaultChannel == 1)
                    sendEnquiryTemplate = EmailTemplateService.Instance.Get(Models.Items.EmailTemplate.SendEnquiryCHS);
                else
                    sendEnquiryTemplate = EmailTemplateService.Instance.Get(Models.Items.EmailTemplate.SendEnquiry);


                var template = sendEnquiryTemplate.Replace("[SenderFirstname]", homeEnquiry.FirstName);
                template = template.Replace("[SenderCountry]", countryName);
                template = template.Replace("[SenderCountry]", countryName);
                template = template.Replace("[MessageLink]", string.Format("{0}{1}?view={2}",
                    WebConfigurationManager.AppSettings["websiteUrl"],
                    Umbraco.Url(Models.Utilities.Constants.NodeId.Inbox),
                    homeEnquiry.ID));

                Member memberRecipient = MemberDAL.Instance.Get(homeEnquiry.RecipientMemberID);

                EmailService.Instance.Send(memberRecipient.EmailAddress, memberRecipient.FirstName,
                    WebConfigurationManager.AppSettings["WebsiteName"] + " enquiry received from " + homeEnquiry.FirstName + " of " + countryName, template);
            }

            

            r.Success = true;
            r.Message = "Your enquiry has been sent!";

            return r;

        }

        [HttpGet]
        public bool SetHomeEnquiryToReviewed(int homeEnquiryId)
        {
            db.Database.ExecuteSqlCommand("UPDATE HomeEnquiry SET Reviewed = 1 WHERE HomeEnquiryID = {0} ", homeEnquiryId);
            return true;
        }

        [HttpGet]
        public string Test()
        {
            return "hello world";
        }

    }

    public class Enquiry {
        [Required]
        public int MemberId { get; set; }
        [Required]
        public int HomeId { get; set; }
        [Required]
        public string Message { get; set; }
    }
    

}
