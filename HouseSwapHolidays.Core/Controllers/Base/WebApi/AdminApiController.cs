﻿using HouseSwapHolidays.Core.Models.DataAccess;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Core.Logging;
using Umbraco.Web.WebApi;

namespace HouseSwapHolidays.Core.Controllers.Base.WebApi
{

    
    public class AdminApiController : UmbracoApiController
    {

        RLK797_HomeExchangeEntities db = new RLK797_HomeExchangeEntities();

        [HttpPost]
        public List<SearchMember> SearchMembers(SearchMemberParam s)
        {

            int statusMode;
            statusMode = s.ShowCurrentOnly ? 1 : 0;

            MemberSummaryCollection memberSummaryCollection = MemberSummaryDAL.Instance.GetAll(s.FirstName ?? "", s.GivenName ?? "", s.EmailAddress ?? "", statusMode, s.HomeID, s.MemberID);
            


            return memberSummaryCollection.Cast<MemberSummary>().Select(x => new SearchMember {
                ID = x.ID,
                Name = x.FirstName + " " + x.GivenName,
                Email = x.EmailAddress,
                DateJoined = x.CreatedDate.ToString("dd-MMM-yy"),
                LastLoggedIn = x.LastLoggedInDate.ToString("dd-MMM-yy"),
                MemberStatus = GetHomeStatus(x.HomeStatus)
            }).ToList();
           
        }

        private string GetHomeStatus(Status status)
        {
            var text = "";
            switch (status)
            {
                case Status.NotSpecified:
                    text = "No home provided";
                    break;

                case Status.Active:
                    text = "Approved";
                    break;

                case Status.PendingApproval:
                    text = "<font color=\"red\">Awaiting approval</font>";
                    break;

                case Status.Archived:
                    text = "Archived";
                    break;

                case Status.Deleted:
                    text = "Deleted";
                    break;

                case Status.Draft:
                    text = "Draft";
                    break;
            }
            return text;
        }
    }

    public class SearchMember {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string DateJoined { get; set; }
        public string LastLoggedIn { get; set; }
        public string MemberStatus { get; set; }
    }

    public class SearchMemberParam
    {
        public int MemberID { get; set; }
        public int HomeID { get; set; }
        public string FirstName { get; set; }
        public string GivenName { get; set; }
        public string EmailAddress { get; set; }
        public bool ShowCurrentOnly { get; set; }
    }
}
