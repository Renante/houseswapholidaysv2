﻿using HouseSwapHolidays.Core.Models.DataAccess;
using HouseSwapHolidays.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Umbraco.Web.WebApi;
using Common;

namespace HouseSwapHolidays.Core.Controllers.WebApi
{
    public class MemberShortListApiController : UmbracoApiController
    {
        RLK797_HomeExchangeEntities db = new RLK797_HomeExchangeEntities();

        [Authorize]
        [HttpGet]
        public bool Delete(int shortListId)
        {
            var member = MemberService.Instance.GetCurrentMember();
            db.up_DeleteMemberShortlist(shortListId, member.Id);
            return true;
        }

        [Authorize]
        [HttpGet]
        public bool Save(int homeID)
        {
            
            var memberID = MemberService.Instance.GetCurrentMemberId();
            MemberShortlist memberShortList = new Common.MemberShortlist();

            memberShortList.MemberID = memberID;
            memberShortList.HomeID = homeID;

            MemberShortlistDAL.Instance.Save(memberShortList);
            return true;             
        }

        [HttpGet]
        public string Test(){
            return "hello";
        }

    }
}
