﻿using ChristianHomeSwap.Core.Models;
using HouseSwapHolidays.Core.Models.Forms;
using HouseSwapHolidays.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Web;
using Umbraco.Web.WebApi;

namespace HouseSwapHolidays.Core.Controllers.WebApi
{
    public class ContactFormApiController : UmbracoApiController
    {
        public string ContactUs(ContactUs c)
        {

            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            var node = umbracoHelper.TypedContent(c.NodeId);


            var recipient = string.Empty;

            //if (node.HasValue("recipient"))
            //    recipient = node.GetPropertyValue<string>("recipient");
            //else
            //    recipient = "renanteabril@gmail.com";

            
            var isSent = EmailService.Instance.Send(c.Email, c.Name, c.Subject, c.Message);
            if (isSent)
                return "Email sent!";
            else
                return "Email sending fail!";
        }
    }

}
