﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web;
using Our.Umbraco.Ditto;
using HouseSwapHolidays.Core.Models.ViewModels.Base;
using HouseSwapHolidays.Core.Models.Items;
using Archetype.Models;
using HouseSwapHolidays.Core.Models.Utilities;
using RJP.MultiUrlPicker.Models;
using HouseSwapHolidays.Core.Models.Items.Website;
using HouseSwapHolidays.Core.Services;
using ChristianHomeSwap.Core.App_Code;
using HouseSwapHolidays.Core.Utilities;
using HouseSwapHolidays.Core.Models.Archetypes;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;

namespace HouseSwapHolidays.Core.Controllers.Base
{
    public class BasePageController : BaseController
    {
        
        /// <summary>
        /// Return the base model which needs to be used everywhere.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="content"></param>
        /// <returns></returns>
        protected override T GetModel<T>(IPublishedContent content)
        {
            var _model = base.GetModel<T>(content);
            var model = _model as BaseViewModel;

            if (model == null) throw new Exception("Model does not inherit from BaseViewModel");

            var member = MemberService.Instance.GetCurrentMember();
            
            if (member != null)
            {
                model.Member = member;
                QuickLinks q = new QuickLinks();
                q.UnreadMessage = member.UnreadMessage;
                model.QuickLinks = q;
            }

            // SEO
            model.MetaTitle = CurrentPage.GetPropertyValue<string>("metaTitle");
            model.MetaDescription = CurrentPage.GetPropertyValue<string>("metaDescription");
            model.MetaKeywords = CurrentPage.GetPropertyValue<string>("metaKeywords");
            model.NoIndex = CurrentPage.GetPropertyValue<bool>("noIndex");

            model.IsLoggedIn = member != null;

            model.root = CurrentPage.AncestorOrSelf(1);

            model.IsPageInbox = CurrentPage.DocumentTypeAlias == Constants.NodeName.Inbox;

            model.PageTitle = CurrentPage.HasValue("pageTitle") ? CurrentPage.GetPropertyValue<string>("pageTitle") : CurrentPage.Name;
            model.TransparentHeader = model.root.GetPropertyValue<bool>("transparentHeader");
            model.LogoUrl = model.root.GetPropertyValue<string>("logo").GetPropertyAsMediaUrl();
            model.LogoUrlTransparent = model.root.GetPropertyValue<string>("logoTransparent").GetPropertyAsMediaUrl();
            model.TopMenu = model.root.GetPropertyAsPublishedContentListFromMntp("topMenu");
            model.JoinButton = model.root.GetPropertyAsNavigationItem("joinButton");
            
            model.GoogleAnalytics = model.root.GetPropertyValue<string>("googleAnalytics");
            
            model.CurrentPageId = CurrentPage.Id;
            
            var leftContent = model.root.GetPropertyValue<ArchetypeModel>("leftContent").FirstOrDefault();
            var firstMenu = model.root.GetPropertyValue<ArchetypeModel>("firstMenu").FirstOrDefault();
            var secondMenu = model.root.GetPropertyValue<ArchetypeModel>("secondMenu").FirstOrDefault();
            var footerlogo = model.root.GetPropertyValue<string>("footerLogo").GetPropertyAsMediaUrl();
            var rightContent = model.root.GetPropertyValue<ArchetypeModel>("rightContent").FirstOrDefault();
            var bottomMenu = model.root.GetPropertyValue<ArchetypeModel>("bottomMenu").FirstOrDefault();
            
            model.HasLeftContent = leftContent != null;
            model.HasFirstMenu = firstMenu != null;
            model.HasSecondMenu = secondMenu != null;
            model.HasRightContent = rightContent != null;
            model.HasBottomMenu = bottomMenu != null;
            model.HasFooterLogo = model.root.HasProperty("footerLogo");

            if (model.HasLeftContent)
            {
                model.LeftContent = new FooterLeftContent
                {
                    Title = leftContent.GetValue<string>("title"),
                    Content = leftContent.GetValue<string>("content").ToLineBreaksHtml()
                };
            }

            if (model.HasFirstMenu)
            {
                model.FirstMenu = new FooterMenu
                {
                    Title = firstMenu.GetValue<string>("title"),
                    Menu = firstMenu.HasValue("menu") ? firstMenu.GetPropertyAsPublishedContentListFromMntp("menu") : null,
                    HasMenu = firstMenu.HasValue("menu")

                };
            }

            if (model.HasSecondMenu)
            {
                model.SecondMenu = new FooterMenu
                {
                    Title = secondMenu.GetValue<string>("title"),
                    Menu = secondMenu.HasValue("menu") ? secondMenu.GetValue<string>("menu").GetPropertyAsContentList() : null,
                    HasMenu = secondMenu.HasValue("menu")
                };
            }

            if (model.HasFooterLogo)
            {
                model.FooterLogo = footerlogo;
            }

            if (model.HasRightContent)
            {
                model.RightContent = new FooterRightContent
                {
                    Title = rightContent.GetValue<string>("title"),
                    Email = rightContent.GetValue<string>("email"),
                    Text = rightContent.GetValue<string>("text"),
                    SocialAccounts = rightContent.GetValue<ArchetypeModel>("socialAccounts").Select(s => new SocialAccount {
                        Title = s.GetValue<string>("title"),
                        Icon = s.GetValue<string>("icon"),
                        Link = s.HasValue("link") ? s.GetValue<MultiUrls>("link").FirstOrDefault() : null,
                        HasLink = s.HasValue("link")
                    })
                };
            }

            if (model.HasBottomMenu)
            {
                model.BottomMenu = new FooterMenu
                {
                    Title = bottomMenu.GetValue<string>("title"),
                    Menu = bottomMenu.HasValue("menu") ? bottomMenu.GetValue<string>("menu").GetPropertyAsContentList() : null,
                    HasMenu = bottomMenu.HasValue("menu")
                };
            }


            // ----------- Popups

            var popUpNode = Umbraco.TypedContent(Constants.NodeId.PopUps);
            var popRegistration = popUpNode.Children.FirstOrDefault(s => s.GetPropertyValue<string>("popUpAlias") == "registration");
            var popSignIn = popUpNode.Children.FirstOrDefault(s => s.GetPropertyValue<string>("popUpAlias") == "signin");
            var popForgotPassword = popUpNode.Children.FirstOrDefault(s => s.GetPropertyValue<string>("popUpAlias") == "forgotpassword");
            var popContactUs = popUpNode.Children.FirstOrDefault(s => s.GetPropertyValue<string>("popUpAlias") == "contactus");
            
            model.PopRegistration = new PopUp
            {
                Title = popRegistration != null ? popRegistration.GetPropertyValue<string>("title") : string.Empty,
                Summary = popRegistration != null ? popRegistration.GetPropertyValue<string>("summary") : string.Empty
            };

            model.PopSignIn = new PopUp
            {
                Title = popSignIn != null ? popSignIn.GetPropertyValue<string>("title") : string.Empty,
                Summary = popSignIn != null ? popSignIn.GetPropertyValue<string>("summary") : string.Empty
            };

            model.PopForgotPassword = new PopUp
            {
                Title = popForgotPassword != null ? popForgotPassword.GetPropertyValue<string>("title") : string.Empty,
                Summary = popForgotPassword != null ? popForgotPassword.GetPropertyValue<string>("summary") : string.Empty
            };

            model.PopContactUs = new PopUp
            {
                Title = popContactUs != null ? popContactUs.GetPropertyValue<string>("title") : string.Empty,
                Summary = popContactUs != null ? popContactUs.GetPropertyValue<string>("summary") : string.Empty
            };

            // ----------- End Popups

            // ---------- Global Settings
            model.PromotionalAreas = model.root.GetPropertyAsArchetypeObjectList<PromotionalAreas>("promotionalAreas").FirstOrDefault();
            // ---------- End Global Settings


            return model as T;
        }

        public StepsNavigation GetStepsNavigation()
        {
            var registrationNode = Umbraco.TypedContent(Constants.NodeId.Registration);

            if (!registrationNode.HasValue("stepsNavigation"))
                return null;

            StepsNavigation s = new StepsNavigation();

            var stepsNode = registrationNode.GetPropertyValue<ArchetypeModel>("stepsNavigation").FirstOrDefault();
            
            s.Step1 = stepsNode.GetValue<string>("step1");
            s.Step2 = stepsNode.GetValue<string>("step2");
            s.Step3 = stepsNode.GetValue<string>("step3");
            s.Step4 = stepsNode.GetValue<string>("step4");

            return s;
        }

        public List<string> GetLanguages()
        {
            return new List<string>(){
                "Chinese",
                "Dutch",
                "English",
                "French",
                "German",
                "Greek",
                "Italian",
                "Japanse",
                "Korean",
                "Polish",
                "Russian",
                "Spanish",
                "Swedish"
            };
        }

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            
            string userIp = GetIPAddress();
            //userIp = "111.125.111.158";
            if (Session.GetCountryCode() == "")
            {
                string countryCode = GeoLocation.GetCountryCode(userIp);
                Session.SetCountryCode(countryCode);
            }

            bool isFreeMembershipCountry = GeoLocation.isFreeMembershipCountry();
            
            Session.SetUserFreeMembership(isFreeMembershipCountry);

            
        }

        protected string GetIPAddress()
        {
            
            string ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }
            
            return Request.UserHostAddress;
        }

    }
}