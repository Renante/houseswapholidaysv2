﻿using HouseSwapHolidays.Core.Models.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace HouseSwapHolidays.Core.Controllers.Base
{
    public class BaseController : RenderMvcController
    {
        protected virtual T GetModel<T>(IPublishedContent content) where T : BaseViewModel
        {
            var model = Activator.CreateInstance(typeof(T), content) as BaseViewModel;
            
            return (T)model;
        }
    }
}