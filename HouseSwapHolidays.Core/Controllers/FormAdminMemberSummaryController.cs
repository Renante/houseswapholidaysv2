﻿using Common;
using HouseSwapHolidays.Core.Services;
using HouseSwapHolidays.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using HouseSwapHolidays.Core.Models.Utilities;

namespace HouseSwapHolidays.Core.Controllers
{
    public class FormAdminMemberSummaryController : SurfaceController
    {
        public ActionResult UpdateMemberDetail()
        {
            var memberID = int.Parse(Request.Form["memberid"]);
            var status = int.Parse(Request.Form["status"]);
            var ismultichannel = Request.Form["ismultichannel"];
            var receivenewsletter = Request.Form["receivenewsletter"];
            var isReceiveNewsletter = receivenewsletter == "on";

            Member member = MemberDAL.Instance.Get(memberID);
            member.Status = (Status)status;

            member.IsMultiChannel = ismultichannel == "on";
            member.ReceiveNewsletter = isReceiveNewsletter;

            MemberDAL.Instance.Save(member);

            if (isReceiveNewsletter)
                CampaignMonitorService.Instance.Subscribe(member.EmailAddress);
            else
                CampaignMonitorService.Instance.UnSubscribe(member.EmailAddress);

            return RedirectToUmbracoPage(Constants.NodeId.AdminMemberSummary, "id=" + memberID);
        }

        public ActionResult UpdateHome()
        {
            var status = int.Parse(Request.Form["status"]);
            var homeid = int.Parse(Request.Form["homeid"]);
            var memberid = int.Parse(Request.Form["memberid"]);

            Home home = HomeDAL.Instance.Get(homeid);
            if (home != null)
            {
                home.Status = (Status)status;
                HomeDAL.Instance.Save(home);
            }

            return RedirectToUmbracoPage(Constants.NodeId.AdminMemberSummary, "id=" + memberid);
        }
    }
}
