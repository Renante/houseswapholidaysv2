﻿using HouseSwapHolidays.Core.Attributes;
using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.Forms;
using HouseSwapHolidays.Core.Models.ViewModels;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HouseSwapHolidays.Core.Controllers
{
    [Admin]
    public class PageAdminHomeImagesController : BasePageController
    {
        public ActionResult Index()
        {
            var paramID = Request.QueryString["homeID"];
            if (paramID == null) return Content("You must provide parameter 'homeid' to view this content");

            int homeID = int.Parse(paramID);

            var model = GetModel<PageAdminHomeImagesViewModel>(CurrentPage);

            Home home = HomeDAL.Instance.Get(homeID);

            var homeImageUrl = home.ThumbnailURL.Replace("\\", "/");
            var homeLocation = home.Location;
            var homeDescription = home.Description;

            HomeImageCollection homeImageCollection = HomeImageDAL.Instance.GetAllForHome(homeID);
            int imageCounter = 0;

            List<HomeImageViewModel> homeImageList = new List<HomeImageViewModel>();
            foreach (HomeImage homeImage in homeImageCollection)
            {
                homeImageList.Add(new HomeImageViewModel
                {
                    Id = homeImage.ID,
                    ImageSource = string.Format("/images/homes/{0}/{1}", homeImage.HomeID, homeImage.ThumbnailImage),
                    IsFeature = homeImage.IsFeature,

                });
            }

            model.HomeID = homeID;
            model.HomeImageUrl = homeImageUrl;
            model.HomeLocation = homeLocation;
            model.HomeDescription = home.Description;
            model.HomeImages = homeImageList;
            model.MemberID = home.MemberID;

            return CurrentTemplate(model);
        }
    }
}
