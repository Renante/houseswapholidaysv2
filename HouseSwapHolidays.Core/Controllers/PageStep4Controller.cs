﻿using HouseSwapHolidays.Core.Attributes;
using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.ViewModels;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Web;

namespace HouseSwapHolidays.Core.Controllers
{
    [MemberOnly(Status.Draft, Status.Active)]
    public class PageStep4Controller : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<PageStep4ViewModel>(CurrentPage);
            model.Title = CurrentPage.GetPropertyValue<string>("title");
            model.SubTitle = CurrentPage.GetPropertyValue<string>("subTitle");
            model.Summary = CurrentPage.GetPropertyValue<string>("summary");

            var currentMemberId = HouseSwapHolidays.Core.Services.MemberService.Instance.GetCurrentMemberId();

            //var member = MemberDAL.Instance.Get(currentMemberId);
            var home = HomeDAL.Instance.GetByMemberID(currentMemberId);
            var homeId = home[0].ID;

            HomeImageCollection homeImageCollection = HomeImageDAL.Instance.GetAllForHome(homeId);

            model.HomeImageCollection = homeImageCollection;
            model.HomeId = homeId;

            model.StepsNavigation = GetStepsNavigation();
            model.HasStepsNavigation = model.StepsNavigation != null;

            return CurrentTemplate(model);
        }
    }
}
