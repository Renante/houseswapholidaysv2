﻿using Common;
using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HouseSwapHolidays.Core.Controllers
{
    public class PageSitemapController : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<PageSitemapViewModel>(CurrentPage);

            model.CountryCollection = CountryDAL.Instance.GetAllByDefault(HouseSwapHolidays.Core.Models.Utilities.Constants.ChannelId);

            return CurrentTemplate(model);
        }
    }
}
