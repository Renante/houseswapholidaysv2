﻿using HouseSwapHolidays.Core.Services;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace HouseSwapHolidays.Core.Controllers
{
    public class AccountFormController : SurfaceController
    {
        [HttpPost]
        public ActionResult SendAndSavePasswordResetKey(string email)
        {
            var accountService = new AccountService();
            accountService.SendAndSavePasswordResetKey(email);

            var qstr = new NameValueCollection();
            qstr.Add("t", "Thank you");
            qstr.Add("m", "A verification code was sent to your inbox.");
            return RedirectToCurrentUmbracoPage(qstr);
        }
    }
}
