﻿using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using HouseSwapHolidays.Core.Utilities;
using HouseSwapHolidays.Core.Models.Archetypes;
using HouseSwapHolidays.Core.Models;
using Umbraco.Web;
using HouseSwapHolidays.Core.Models.Items.PageHome;
using Common;

namespace HouseSwapHolidays.Core.Controllers
{
    public class PageHomeV2Controller : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<PageHomeV2ViewModel>(CurrentPage);

            model.QuickSearch = CurrentPage.GetPropertyAsArchetypeObjectList<QuickSearch>("quickSearch").FirstOrDefault();
            model.QuickSearchPlain = CurrentPage.GetPropertyAsArchetypeObjectList<QuickSearchPlain>("quickSearchPlain").FirstOrDefault();
            model.Banners = CurrentPage.GetPropertyAsArchetypeObjectList<BannerV2>("bannersv2");
            model.HowItWorks = CurrentPage.GetPropertyAsArchetypeObjectList<HowItWorks>("sectionB").FirstOrDefault();
            model.SectionCImage = CurrentPage.GetPropertyAsMediaItem("sectionCImage");
            model.SectionCBlockContent = CurrentPage.GetPropertyAsArchetypeObjectList<TitleContentLinkALinkB>("sectionCBlockContent").FirstOrDefault();
            model.SectionDImage = CurrentPage.GetPropertyAsMediaItem("sectionDImage");
            model.SectionDBlockContent = CurrentPage.GetPropertyAsArchetypeObjectList<TitleContentLinkALinkB>("sectionDBlockContent").FirstOrDefault();
            model.Cases = CurrentPage.GetPropertyAsArchetypeObjectList<Featured>("cases").FirstOrDefault();
            model.SectionF = CurrentPage.GetPropertyAsArchetypeObjectList<TitleContentLinkALinkB>("sectionF").FirstOrDefault();
            model.FeaturedHome = CurrentPage.GetPropertyAsArchetypeObjectList<HouseSwapHolidays.Core.Models.Archetypes.FeaturedHome>("featuredHomes").FirstOrDefault();
            model.SectionH = CurrentPage.GetPropertyAsArchetypeObjectList<WhyJoinUs>("sectionH").FirstOrDefault();
            model.LatestListing = CurrentPage.GetPropertyAsArchetypeObjectList<LatestListing>("latestListings").FirstOrDefault();
            model.Testimonial = CurrentPage.GetPropertyAsArchetypeObjectList<Testimonial>("testimonial").FirstOrDefault();
            
            return CurrentTemplate(model);
        }
    }
}
