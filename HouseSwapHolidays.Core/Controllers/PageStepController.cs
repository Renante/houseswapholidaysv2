﻿using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.ViewModels;
using HouseSwapHolidays.Core.Models.Utilities;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Core.Logging;
using HouseSwapHolidays.Core.Services;
using System.Xml.Linq;
using System.Globalization;
using HouseSwapHolidays.Core.Models.DataAccess;
using System.Collections;
using System.Web;
using System.Configuration;
using EBImageH;
using Umbraco.Web.Mvc;

namespace HouseSwapHolidays.Core.Controllers
{
    // TODO: Convert Name to FormStepController to match other surfacecontrollers
    [Authorize]
    public class PageStepController : SurfaceController
    {

        private const int FILE_SIZE_LIMIT = 10485760; // 10 MB
        Channel ch = new Channel();

        #region Old Code

        //        public ActionResult Index()
//        {
//            var model = GetModel<PageStepViewModel>();

//            model.Languages = GetLanguages();

//            var currentMemberId = HouseSwapHolidays.Core.Services.MemberService.Instance.GetCurrentMemberId();
//            model.Member = MemberDAL.Instance.Get(currentMemberId);

//            var countryCollection = CountryDAL.Instance.GetAllByDefault();
//            model.CountryCollection = countryCollection;
            
//            var memberDestinationCollection = MemberDestinationDAL.Instance.GetAllForMember(currentMemberId);
//            model.MemberDestinationCollection = memberDestinationCollection;

//            var homeId = -1;
//            Home home = null;
//            model.HasHome = false;
            
//            var requestedId = Request.QueryString["id"];
            
//            if(requestedId != null){
//                homeId = int.Parse(requestedId);
//                home = HomeDAL.Instance.Get(homeId);

//                model.Home = home;
//                model.HasHome = true;

//                HomeImageCollection homeImageCollection = HomeImageDAL.Instance.GetAllForHome(homeId);
//                if (homeImageCollection != null)
//                {
//                    model.HomeImageCollection = homeImageCollection;
//                    model.HomeHasImages = true;
//                }

//            }

//            #region HomeFeatures
//            List<HouseSwapHolidays.Core.Models.Items.HomeFeaturesWithSelectedValue> results;
//            using (var context = new RLK797_HomeExchangeEntities())
//            {
//                string sql = @"SELECT        f.FeatureID FeatureID, f.FeatureTypeID FeatureTypeID, f.Name Name, CAST((CASE WHEN hf.HomeID IS NULL THEN 0 ELSE 1 END) AS bit) AS IsHomeFeature
//                                FROM            Feature AS f LEFT OUTER JOIN
//                                                         HomeFeatures AS hf ON f.FeatureID = hf.FeatureID AND hf.HomeID = {0}
//                                ORDER BY f.FeatureTypeID";
//                results = context.Database.SqlQuery<HouseSwapHolidays.Core.Models.Items.HomeFeaturesWithSelectedValue>(sql, homeId).ToList();
//            }

//            model.HomeFacilities = results.Where(w => w.FeatureTypeID == (int)FeatureType.HomeFacilities).ToList();
//            model.HomeActivities = results.Where(w => w.FeatureTypeID == (int)FeatureType.HomeActivities).ToList();
//            model.HomeRules = results.Where(w => w.FeatureTypeID == (int)FeatureType.HomeRules).ToList();
//            model.HolidayToOffer = results.Where(w => w.FeatureTypeID == (int)FeatureType.HolidayTypesToOffer).ToList();
//            model.SwapPref = results.Where(w => w.FeatureTypeID == (int)FeatureType.SwapTimePreferences).ToList();
//            model.SwapPref.AddRange(results.Where(w => w.FeatureTypeID == (int)FeatureType.SwapLength).ToList());
            

//            #endregion


            
//            if (home != null)
//            {
//                if (!string.IsNullOrEmpty(home.CountryCode))
//                {
//                    var regionCollection = RegionDAL.Instance.GetByCountryCode(home.CountryCode);
//                    model.HasRegions = regionCollection != null;
//                    model.RegionCollection = regionCollection;
//                }
//            }

            

//            return CurrentTemplate(model);
        //        }

        #endregion

        [HttpPost]
        public ActionResult SaveStep1(Step1 s)
        {
            var currentMemberId = HouseSwapHolidays.Core.Services.MemberService.Instance.GetCurrentMemberId();

            Member member = new Member();
            member = MemberDAL.Instance.Get(currentMemberId);
            member.Introduction = string.IsNullOrEmpty(s.Introduction) ? string.Empty : s.Introduction;
            member.LastLoggedInDate = DateTime.Now;
            member.UpdatedDate = DateTime.Now;

            if (s.Language1 != "Not Specified")
                member.Language1 = s.Language1;

            if (s.Language2 != "Not Specified")
                member.Language2 = s.Language2;

            if (s.Language3 != "Not Specified")
                member.Language3 = s.Language3;

            MemberDAL.Instance.Save(member);

            return RedirectToUmbracoPage(Constants.StepNodeId.Step2);
        }

        [HttpPost]
        public ActionResult SaveStep2()
        {
            var isOpenToOffer = Request.Form["OpenToOffer"] != null;

            var currentMemberId = HouseSwapHolidays.Core.Services.MemberService.Instance.GetCurrentMemberId();
            var member = MemberDAL.Instance.Get(currentMemberId);
            member.AllowDestinationOffers = isOpenToOffer;
            MemberDAL.Instance.Save(member);

            return RedirectToUmbracoPage(Constants.StepNodeId.Step3);
        }

        [HttpPost]
        public ActionResult SaveStep3()
        {
            var countrycode = Request.Form["country"];
            var postcode = Request.Form["postcode"];
            var city = Request.Form["city"];
            var regioncode = Request.Form["region"];
            var suburb = Request.Form["suburb"];
            var bedrooms = Request.Form["bedrooms"];
            var bathrooms = Request.Form["bathrooms"];
            var sleep = Request.Form["sleep"];
            var maxpeople = Request.Form["maxpeople"];
            var distance = Request.Form["distance"];
            var airport = Request.Form["airport"];
            var isprimaryhome = Request.Form["isprimaryhome"];
            var isopentolastmin = Request.Form["isopentolastmin"];
            var homehighlights = Request.Form["homehighlights"];
            var homeabout = Request.Form["homeabout"];
            var homeloc = Request.Form["homeloc"];
            var homeinfo = Request.Form["homeinfo"];

            // @@ TODO Get rid of this Logging
            #region Logging
            StringBuilder s = new StringBuilder();

            s.AppendLine("country: " + Request.Form["country"] );
            s.AppendLine("postcode: " + Request.Form["postcode"]);
            s.AppendLine("city: " + Request.Form["city"]);
            s.AppendLine("region: " + Request.Form["region"]);
            s.AppendLine("suburb: " + Request.Form["suburb"]);
            s.AppendLine("bedrooms: " + Request.Form["bedrooms"]);
            s.AppendLine("bathrooms: " + Request.Form["bathrooms"]);
            s.AppendLine("sleep: " + Request.Form["sleep"]);
            s.AppendLine("maxpeople: " + Request.Form["maxpeople"]);
            s.AppendLine("distance: " + Request.Form["distance"]);
            s.AppendLine("airport: " + Request.Form["airport"]);
            s.AppendLine("isprimaryhome: " + Request.Form["isprimaryhome"]);
            s.AppendLine("isopentolastmin: " + Request.Form["isopentolastmin"]);
            s.AppendLine("homehighlights: " + Request.Form["homehighlights"]);
            s.AppendLine("homeabout: " + Request.Form["homeabout"]);
            s.AppendLine("homeloc: " + Request.Form["homeloc"]);
            s.AppendLine("homeinfo: " + Request.Form["homeinfo"]);

            FeatureCollection featureCollection = FeatureDAL.Instance.GetAll();

            var homeFacilitiesCollection = FeatureDAL.Instance.GetByFeatureType((int)FeatureType.HomeFacilities, featureCollection);
            var homeActivitiesCollection = FeatureDAL.Instance.GetByFeatureType((int)FeatureType.HomeActivities, featureCollection);
            var homeRulesCollection = FeatureDAL.Instance.GetByFeatureType((int)FeatureType.HomeRules, featureCollection);
            var holidayToOfferCollection = FeatureDAL.Instance.GetByFeatureType((int)FeatureType.HolidayTypesToOffer, featureCollection);
            
            FeatureCollection prefSwapCollection = new FeatureCollection();
            FeatureCollection f_a = FeatureDAL.Instance.GetByFeatureType((int)FeatureType.SwapTimePreferences, featureCollection);
            FeatureCollection f_b = FeatureDAL.Instance.GetByFeatureType((int)FeatureType.SwapLength, featureCollection);

            prefSwapCollection.AddCollection(f_a);
            prefSwapCollection.AddCollection(f_b);

            var swapPrefCollection = prefSwapCollection;

            
            #endregion


            var currentMemberId = MemberService.Instance.GetCurrentMemberId();
            var member = MemberDAL.Instance.Get(currentMemberId);

            var country = CountryDAL.Instance.GetCountryByCode(countrycode);
            var regionCollection = RegionDAL.Instance.GetByCountryCode(countrycode);
            var region = string.Empty;
            if (!string.IsNullOrEmpty(regioncode)) { 
                foreach (Common.Region item in regionCollection)
                {
                    if (item.RegionCode.ToLower() == regioncode.ToLower())
                    {
                        region = item.Name;
                        break;
                    }
                }
            }
            

            Home home = new Home();

            if (Request.QueryString["id"] != null)
            {
                //we have an existing home to use
                var homeId = int.Parse(Request.QueryString["id"]);

                HomeCollection homeCollection = HomeDAL.Instance.GetByMemberID(homeId, currentMemberId);

                if (homeCollection.Count == 0)
                {
                    throw new Exception(String.Format("Unable to retrieve details homeID {0}, memberID {1}", homeId.ToString(), currentMemberId.ToString()));
                }
                else
                {
                    home = homeCollection[0];
                }
            }

            else
            {
                //if parameter id is null then still try to look for default home
                HomeCollection homeCollection = HomeDAL.Instance.GetByMemberID(currentMemberId);

                if (homeCollection.Count == 0)
                {
                    // ok member has no home, so we are adding new one
                    home.ID = -1;
                    home.Status = Status.Draft; // TODO: Confirm this is right
                }
                else
                {
                    // use first home as default
                    home = homeCollection[0];
                }
            }

            home.MemberID = member.ID;

            // Retrieve multichannel settings for this member
            if (member != null)
            {
                home.IsMultiChannel = member.IsMultiChannel;
                home.DefaultChannel = member.DefaultChannel;
            }

            home.CountryCode = countrycode;
            home.RegionCode = regioncode;

            if (!string.IsNullOrEmpty(city)) { 
                city = char.ToUpper(city[0]) + city.Substring(1);
                home.City = city;
            }
            else
            {
                home.City = string.Empty;
            }

            if (regioncode == "00")
            {
                home.Location = String.Format("{0}, {1}", city, country.Name);
            }
            else
            {
                if (city == region)
                {
                    home.Location = String.Format("{0}, {1}", city, country.Name);
                }
                else
                {
                    home.Location = String.Format("{0}, {1}, {2}", city, region, country.Name);
                }
            }


            // Get longitude and latitude for map;
            Common.GeocodingResult mapLocation = GetMap(postcode, city, country.Name);

            if (mapLocation != null)
            {
                home.Longitude = mapLocation.Longitude.ToString();
                home.Latitude = mapLocation.Latitude.ToString();
            }

            home.Summary = homehighlights.Length > 150 ? homehighlights.Substring(0, 150) : homehighlights;
            home.Description = homeabout;
            home.RegionDescription = homeloc;
            home.OtherInformation = homeinfo;

            home.Bedrooms = !string.IsNullOrEmpty(bedrooms) && bedrooms.IsValidNumber() ? int.Parse(bedrooms) : 0;
            home.Bathrooms = !string.IsNullOrEmpty(bathrooms) && bedrooms.IsValidNumber() ? int.Parse(bathrooms) : 0;
            home.ComfortablySleeps = !string.IsNullOrEmpty(sleep) && bedrooms.IsValidNumber() ? int.Parse(sleep) : 0;
            home.MaxSleeps = !string.IsNullOrEmpty(maxpeople) && bedrooms.IsValidNumber() ? int.Parse(maxpeople) : 0;

            home.Postcode = postcode;
            home.Suburb = suburb;
            
            home.AirportName = airport;
            home.AirportDistance = !string.IsNullOrEmpty(distance) && distance.IsValidNumber() ? int.Parse(distance) : 0;

            home.LastMinuteAvailability = isopentolastmin == "1";
            home.PrimaryOrHoliday = isprimaryhome;

            home.JanuaryAvailability = !string.IsNullOrEmpty(Request.Form["availability_january"]);
            home.FebruaryAvailability = !string.IsNullOrEmpty(Request.Form["availability_february"]);
            home.MarchAvailability = !string.IsNullOrEmpty(Request.Form["availability_march"]);
            home.AprilAvailability = !string.IsNullOrEmpty(Request.Form["availability_april"]);
            home.MayAvailability = !string.IsNullOrEmpty(Request.Form["availability_may"]);
            home.JuneAvailability = !string.IsNullOrEmpty(Request.Form["availability_june"]);
            home.JulyAvailability = !string.IsNullOrEmpty(Request.Form["availability_july"]);
            home.AugustAvailability = !string.IsNullOrEmpty(Request.Form["availability_august"]);
            home.SeptemberAvailability = !string.IsNullOrEmpty(Request.Form["availability_september"]);
            home.OctoberAvailability = !string.IsNullOrEmpty(Request.Form["availability_october"]);
            home.NovemberAvailability = !string.IsNullOrEmpty(Request.Form["availability_november"]);
            home.DecemberAvailability = !string.IsNullOrEmpty(Request.Form["availability_december"]);

            // insert defaults value to prevent error as required by stored proc
            home.AboutUs = string.Empty;
            home.ThumbnailURL = string.Empty;
            
            HomeDAL.Instance.Save(home);

            HomeFeatureDAL.Instance.DeleteForHome(home.ID);

            PopulateHomeWithFeatures(home, homeFacilitiesCollection, "facilities", (int)FeatureType.HomeFacilities);
            PopulateHomeWithFeatures(home, homeActivitiesCollection, "activities", (int)FeatureType.HomeActivities);
            PopulateHomeWithFeatures(home, homeRulesCollection, "rules", (int)FeatureType.HomeRules);
            PopulateHomeWithFeatures(home, holidayToOfferCollection, "holidayType", (int)FeatureType.HolidayTypesToOffer);
            PopulateHomeWithFeatures(home, swapPrefCollection, "swapPref", (int)FeatureType.SwapTimePreferences);

            HomeFeatureDAL.Instance.SaveCollection(home.HomeFeatures);

            return RedirectToUmbracoPage(Constants.StepNodeId.Step4);
            
        }

        [HttpPost]
        public ActionResult SaveStep4()
        {

            Home home = new Home();
            Member member = new Member();

            int memberID = -1;

            memberID = MemberService.Instance.GetCurrentMemberId();

            member = MemberDAL.Instance.Get(memberID);


            // TODO: This is hardcoded to select top home only

            HomeCollection homeCollection = HomeDAL.Instance.GetByMemberID(member.ID);

            if (homeCollection == null || homeCollection.Count < 1)
            {
                throw new Exception(String.Format("Attempting to locate home on homeimages for memberID {0} but none found", member.ID.ToString()));
            }
            else
            {
                home = homeCollection[0];
            }


            // Updated 16/03/08 to support pending approval posts.
            if ((home.Status == Status.Active) || (home.Status == Status.PendingApproval))
            {
                return RedirectToUmbracoPage(Constants.NodeId.MyProfile);
            }
            else
            {
                // TODO: Temporarily pass in the homeID. Normally this would be some sort of payment reference???
                // Below code should now be redundant

                // Prepare payment information
                PaymentManager paymentManager = new PaymentManager();
                Payment payment = new Payment();

                payment.PromotionID = member.PromotionID;
                payment.MemberID = member.ID;
                payment.ChannelID = ch.ID;
                payment.HomeID = home.ID;

                MemberSession memberSession = new MemberSession();

                memberSession.FirstName = member.FirstName;
                memberSession.GivenName = member.GivenName;
                memberSession.MemberID = member.ID;
                memberSession.HomeID = member.HomeID;
                memberSession.MemberStatus = member.Status;

                Session["membersession"] = memberSession;

                // Assign default subscription rate
                // Override shortly if there is a special promotion rate
                payment.PaymentAmount = (Decimal)ch.SubscriptionRate;

                // fill in below as defaults to prevent error
                payment.ErrorNumber = string.Empty;
                payment.ReceiptNumber = string.Empty;
                payment.Comments = string.Empty;

                paymentManager.Initiate(payment);

                if ((ConfigurationSettings.AppSettings["HomeApprovalRequired"] != null) && (ConfigurationSettings.AppSettings["PreApprovalRequired"].ToUpper() == "TRUE"))
                {
                    //Response.Redirect(String.Format(this.SiteUrl + "member/specialoffer.aspx?id={0}", payment.ID), true);
                    //Response.Redirect(String.Format(this.SiteUrl + "member/membershipcomplete.aspx?id={0}", payment.ID), true);
                    return RedirectToUmbracoPage(Constants.StepNodeId.Step5, "id=" + payment.ID);

                }
                else
                {
                    // Dead link below
                    // Response.Redirect(String.Format(this.SiteUrl + "member/activatesuccess.aspx?id={0}", payment.ID), true);
                    return RedirectToUmbracoPage(Constants.NodeId.Page404);
                }
            }

            
        }

        [HttpPost]
        public ActionResult DeletePhoto()
        {
            var memberID = MemberService.Instance.GetCurrentMemberId();

            var imageinfo = Request.Form["test"];

            string[] del1 = (imageinfo.ToString()).Split(',');

            int homeid = Convert.ToInt32(del1[1]);
            int homeImageId = Convert.ToInt32(del1[0]);
            string homeimage = del1[2].ToString();

            HomeImageDAL.Instance.Delete(homeImageId, memberID);
            if (System.IO.File.Exists(Server.MapPath("~/images/homes/" + homeid + "/" + homeimage)))
                System.IO.File.Delete(Server.MapPath("~/images/homes/" + homeid + "/" + homeimage));

            string img = homeimage.TrimStart('m');

            if (System.IO.File.Exists(Server.MapPath("~/images/homes/" + homeid + "/l" + img)))
                System.IO.File.Delete(Server.MapPath("~/images/homes/" + homeid + "/l" + img));
            

            if (System.IO.File.Exists(Server.MapPath("~/images/homes/" + homeid + "/t" + img)))
                System.IO.File.Delete(Server.MapPath("~/images/homes/" + homeid + "/t" + img));

            if (System.IO.File.Exists(Server.MapPath("~/images/homes/" + homeid + "/g" + img)))
                System.IO.File.Delete(Server.MapPath("~/images/homes/" + homeid + "/g" + img));

            //HomeDAL.Instance

            return RedirectToCurrentUmbracoPage();
        }

        [HttpPost]
        public ActionResult AddPhoto(HttpPostedFileBase photo)
        {
            ImageFixerH imageFixer = new ImageFixerH();

            
            var memberID = MemberService.Instance.GetCurrentMemberId();

            bool hasUploadedPhoto = photo != null;
            bool isNewImage = true;
            int homeImageID = -1;
            Common.ImageProcessor imageProcessor = new Common.ImageProcessor();
            HomeImage homeImage;

            // File & path variables
            string destinationPath;
            string sourceFilename, outputFileName;
            string thumbnailImageName = "";
            string mediumImageName = "";
            string largeImageName = "";
            string giantImageName = "";

            // Retrieve first home based on member
            HomeCollection homeCollection = HomeDAL.Instance.GetByMemberID(memberID);
            // TODO: Currently we only support one home listing. Update this to support multiple listings
            Home home;
            home = (Home)homeCollection[0];

            // Check if we're in editing an existing image, or uploading a new one
            if (Request.QueryString["imgID"] != null)
            {
                // set edit mode, and retrieve passed in homeimageID
                isNewImage = false;
                homeImageID = Int32.Parse(Request.QueryString["imgID"]);
            }

            

            if (isNewImage)
            {
                if (hasUploadedPhoto)
                {
                    if (photo.ContentLength > FILE_SIZE_LIMIT)
                    {
                        // @@ TODO Tell user photo limit exceeded
                        return RedirectToCurrentUmbracoPage();
                    }

                    // extract file extension
                    int fileextensionPos = photo.FileName.LastIndexOf(".");
                    string fileextension = photo.FileName.Substring(fileextensionPos);
                    // First upload the current file to the server
                    outputFileName = System.Guid.NewGuid().ToString() + fileextension;

                    string outputpath = Server.MapPath("~/images/temp/" + outputFileName);

                    photo.SaveAs(outputpath);
                    photo.InputStream.Dispose();
                    
                    // Fix rotated image
                    imageFixer.FixRotatedImage(Server.MapPath("~/images/temp/"), outputFileName);
                    
                }

                else
                {
                    // @@ TODO tell user to upload photo first
                    return RedirectToCurrentUmbracoPage();
                }

                destinationPath = Server.MapPath(string.Format("~/images/homes/{0}/", home.ID.ToString()));
                sourceFilename = outputFileName;

                // Generate random filename based on a Guid
                string fileName = System.Guid.NewGuid().ToString() + ".jpg";

                // Create filenames
                thumbnailImageName = "t" + fileName;
                mediumImageName = "m" + fileName;
                largeImageName = "l" + fileName;
                giantImageName = "g" + fileName;

                // Save and resize images
                // - this size is primarily used for thumbnails
                Resize(sourceFilename, thumbnailImageName, destinationPath, memberID, 115); // was 115,115
                // - this size is primarily used for featured homes (ie homepage promo etc)
                Resize(sourceFilename, mediumImageName, destinationPath, memberID, 200); // was 200,200
                // - this size is used for the main 'view home' image
                Resize(sourceFilename, largeImageName, destinationPath, memberID, 380); // was 300,300
                // - this size is used for the zoomed in image
                Resize(sourceFilename, giantImageName, destinationPath, memberID, 600); // was 300,300

                // Delete original image as we've finished processing it
                System.IO.File.Delete(Server.MapPath("~/images/temp/" + outputFileName));

            }

            if (!isNewImage)
            {
                // Populate home image with previously saved data
                homeImage = HomeImageDAL.Instance.Get(homeImageID);
            }
            else
            {
                homeImage = new HomeImage();
            }

            

            if (!string.IsNullOrEmpty(Request.Form["featuredImage"]))
            {
                // TODO: Confirm that the thumbnail code/generation below can be overwritten

                home.ThumbnailURL = "/images/homes/" + home.ID.ToString() + "/" + thumbnailImageName;

                // TODO: Handle subscription expire date
                if (home != null)
                {
                    HomeDAL.Instance.Save(home);
                }
                else
                {
                    LogHelper.Info(this.GetType(), "attempting to save data from registration step 4 when no home is found. MemberID is " + memberID);
                }

                homeImage.IsFeature = true;
            }
            else
            {
                // the image currently being uploaded is not a featured image
                homeImage.IsFeature = false;
            }

            if (isNewImage)
            {
                // TODO: Is below field now redundant?

                homeImage.ThumbnailImage = thumbnailImageName;
                homeImage.MediumImage = mediumImageName;
                homeImage.LargeImage = largeImageName;
                homeImage.GiantImage = giantImageName;
                homeImage.HomeID = home.ID;
            }

            // Save home image information to database

            homeImage.ID = homeImageID;
            homeImage.ImageDescription = Request.Form["photoDescription"];

            // just put default value as stored proc is expecting this parameter
            homeImage.ImageURL = string.Empty;

            HomeImageDAL.Instance.Save(homeImage);


            return RedirectToCurrentUmbracoPage();
        }

        

        #region Helper Methods
        
        private void PopulateHomeWithFeatures(Home home, FeatureCollection featureCollection, string featureName, int featureTypeId)
        {
            foreach (Feature feature in featureCollection)
            {
                if (!string.IsNullOrEmpty(Request.Form[string.Format("{0}_{1}", featureName, feature.ID)]))
                {
                    HomeFeature homeFeature = new HomeFeature();
                    homeFeature.HomeID = home.ID;
                    homeFeature.FeatureID = feature.ID;
                    homeFeature.FeatureTypeID = featureTypeId;
                    home.HomeFeatures.Add(homeFeature);
                }
            }
        }

        /// <summary>
        /// Retrieves the longitude and latitude based on the form home details
        /// </summary>
        /// <returns>GeocodingResult containing map co-ordinates (when known)</returns>
        private Common.GeocodingResult GetMap(string postal, string city, string country)
        {
            string location = "";

            // DU commented out 27/07/12
            //Common.GeocodingResult mapResult;

            if (postal.Length > 0)
            {
                if (city.Length > 0)
                {
                    location = String.Format("{0} ,{1}, {2}", postal, city, country);
                }
                else
                {
                    location = String.Format("{0} ,{1}", postal, country);
                }
            }
            else
            {
                if (city.Length > 0)
                {
                    location = String.Format("{0} ,{1}", city, country);
                }
            }

            // disabled below 27/07/12
            //mapResult = Common.Geocoding.GetLatLong(location);

            var url = String.Format("http://maps.google.com/maps/api/geocode/xml?address={0}&sensor=false", Server.UrlEncode(location));

            // Load the XML into an XElement object (whee, LINQ to XML!)
            var results = XElement.Load(url);

            // Determine how many elements exist
            //var resultCount = results.Elements("result").Count();

            //Response.Write(Server.UrlEncode(results.Element("result").Element("geometry").Element("location").Element("lat").Value));
            //Response.Write(Server.UrlEncode(results.Element("result").Element("geometry").Element("location").Element("lng").Value));

            Common.GeocodingResult mapResult = new Common.GeocodingResult();

            mapResult.Latitude = -1;
            mapResult.Longitude = -1;

            if (results.Element("status").Value == "OK")
            {
                mapResult.Latitude = Convert.ToDecimal((Server.UrlEncode(results.Element("result").Element("geometry").Element("location").Element("lat").Value)), CultureInfo.InvariantCulture);
                mapResult.Longitude = Convert.ToDecimal((Server.UrlEncode(results.Element("result").Element("geometry").Element("location").Element("lng").Value)), CultureInfo.InvariantCulture);
                //Response.Write(Server.UrlEncode(results.Element("result").Element("geometry").Element("location").Element("lng").Value));
            }

            return mapResult;
        }

        private void Resize(string sourceFilename, string destinationFilename, string destinationPath, int memberID, int size)
        {
            string ThumbnailPath = destinationPath;
            if (!System.IO.Directory.Exists(ThumbnailPath))
            {
                System.IO.Directory.CreateDirectory(ThumbnailPath);
            }
            ThumbnailPath = ThumbnailPath + destinationFilename;
            thumbQualtiy(sourceFilename, ThumbnailPath, size);
        }

        private void thumbQualtiy(String St, string ThumbnailPath, int newSize)
        {
            using (System.Drawing.Image Img = System.Drawing.Image.FromFile(Server.MapPath("~/Images/temp/") + St))
            {
                System.Drawing.Size ThumbNailSize = NewImageSize(Img.Height, Img.Width, newSize);

                using (System.Drawing.Image ImgThumbnail = new System.Drawing.Bitmap(Img, ThumbNailSize.Width, ThumbNailSize.Height))
                {
                    System.Drawing.Graphics gr = System.Drawing.Graphics.FromImage(ImgThumbnail);
                    gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    gr.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                    gr.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    System.Drawing.Rectangle rectDestination = new System.Drawing.Rectangle(0, 0, ThumbNailSize.Width, ThumbNailSize.Height);
                    gr.DrawImage(Img, rectDestination, 0, 0, Img.Width, Img.Height, System.Drawing.GraphicsUnit.Pixel);
                    ImgThumbnail.Save(ThumbnailPath, Img.RawFormat);
                }
                Img.Dispose();
            }
        }

        public System.Drawing.Size NewImageSize(int OriginalHeight, int OriginalWidth, double FormatSize)
        {
            System.Drawing.Size NewSize;
            double tempval;
            if (OriginalHeight > FormatSize && OriginalWidth > FormatSize)
            {
                if (OriginalHeight > OriginalWidth)
                    tempval = FormatSize / Convert.ToDouble(OriginalHeight);
                else
                    tempval = FormatSize / Convert.ToDouble(OriginalWidth);
                NewSize = new System.Drawing.Size(Convert.ToInt32(tempval * OriginalWidth), Convert.ToInt32(tempval * OriginalHeight));
            }
            else
                NewSize = new System.Drawing.Size(OriginalWidth, OriginalHeight); return NewSize;
        }

        #endregion

    }

    public class Step1
    {
        public string Introduction { get; set; }
        public string Language1 { get; set; }
        public string Language2 { get; set; }
        public string Language3 { get; set; }
    }

    
    
}


