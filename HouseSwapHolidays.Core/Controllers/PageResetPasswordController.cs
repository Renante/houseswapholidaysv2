﻿using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.Utilities;
using HouseSwapHolidays.Core.Models.ViewModels;
using Common;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HouseSwapHolidays.Core.Controllers
{
    public class PageResetPasswordController : BasePageController
    {
        const int resetKeyExpiry = 30; //30mins
        public ActionResult Index()
        {
            var model = GetModel<PageResetPasswordViewModel>(CurrentPage);

            var resetkey = Request.QueryString["key"];
            

            if (resetkey != null) { 
                ResetPassword rp = ResetPasswordDAL.Instance.GetResetPassword(resetkey);

                if (rp == null || isLinkExpired(rp.DateRequest))
                {
                    TempData["message"] = "Verification key is invalid or has been expired, please request password again.";
                }
            }

            else
            {
                TempData["message"] = "Reset key not found!";
            }

            return CurrentTemplate(model);
        }
        
        bool isLinkExpired(DateTime linkDateRequest) {
            return linkDateRequest.AddMinutes(resetKeyExpiry) < DateTime.Now;
        }
    }

    public class ResetPasswordForm
    {
        [Required]
        public string NewPassword { get; set; }

        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "Confirm password doesn't match, Type again !")]
        public string RepeatPassword { get; set; }

    }
}
