﻿using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Web;

namespace HouseSwapHolidays.Core.Controllers
{
    public class PageGenericController : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<PageGenericViewModel>(CurrentPage);
            model.Body = CurrentPage.GetPropertyValue<string>("body");

            return CurrentTemplate(model);
        }
    }
}
