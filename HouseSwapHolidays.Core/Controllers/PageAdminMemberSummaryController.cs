﻿using HouseSwapHolidays.Core.Attributes;
using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.Utilities;
using HouseSwapHolidays.Core.Models.ViewModels;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using HouseSwapHolidays.Core.Services;
using Umbraco.Core.Logging;

namespace HouseSwapHolidays.Core.Controllers
{
    [Admin]
    public class PageAdminMemberSummaryController : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<PageAdminMemberSummaryViewModel>(CurrentPage);

            int memberID = 0;

            if (Request.QueryString["id"] != null) { 
                
                memberID = int.Parse(Request.QueryString["id"]);

                Member member = new Member();

                // Retrieve member details
                if (memberID > 0)
                {
                    member = MemberDAL.Instance.Get(memberID);
                }

                if (member != null)
                {
                    model.MemberID = member.ID;
                    model.FirstName = member.FirstName;
                    model.GivenName = member.GivenName;
                    model.EmailAddress = member.EmailAddress;
                    model.CreatedDate = member.CreatedDate.ToString("dd-MMM-yy");
                    model.LastUpdated = member.UpdatedDate.ToString("dd-MMM-yy");
                    model.LastLoggedIn = member.LastLoggedInDate.ToString("dd-MMM-yy");

                    // Set drop down value of member status
                    Status memberStatus = (Status)member.Status;
                    model.Status = ((int)memberStatus).ToString();

                    model.ReceiveNewsletter = member.ReceiveNewsletter;
                    model.IsMultiChannel = member.IsMultiChannel;
                    
                    Channel channel = ChannelDAL.Instance.Get(member.DefaultChannel);
                    var isChsReferred = ChannelDAL.Instance.IsChsReferred(member.ID);
                    model.ChannelName = channel != null ? channel.Name + (isChsReferred ? " (CHS referred)" : "") : "<font color=\"red\">Unknown</font>";

                    // Retrieve member's homes
                    var homeCollection = HomeDAL.Instance.GetByMemberID(memberID).Cast<Home>().ToList();
                    model.HomeCollection = homeCollection;

                    var temphomeEnquiryCollection = HomeEnquiryDAL.Instance.GetAllHomeEnquiries(memberID, -1, false, 3000).Cast<HomeEnquiry>().ToList();
                    var inbox = HomeEnquiryDAL.Instance.GetAllForMember(memberID);

                    model.Inbox = inbox;
                    model.HomeEnquiryCollection = temphomeEnquiryCollection.Select(s => new SentHomeEnquiry
                    {
                        Status = GetText("status", s.Reviewed, s.HoldMessage),
                        ReferenceNo = s.ID.ToString(),
                        Date = s.EnquiryDate.ToString("dd MMM yyyy"),
                        Message = s.Message,
                        ReviewedByRecipient = GetText("reviewed", s.Reviewed, s.HoldMessage),
                        OnHold = GetText("onhold", s.Reviewed, s.HoldMessage)
                    }).ToList();
                    
                }

            }
            else
            {
                return Content("Please provide parameter id to view this page");
            }

            model.StatusDD = GetStatusDropdown();

            return CurrentTemplate(model);
        }
        
        public List<StatusDD> GetStatusDropdown()
        {
            List<StatusDD> s = new List<StatusDD>();
            s.Add(new StatusDD { StatusId = (int)Status.Active, StatusName = Status.Active.ToString() });
            s.Add(new StatusDD { StatusId = (int)Status.PendingApproval, StatusName = Status.PendingApproval.ToString() });
            s.Add(new StatusDD { StatusId = (int)Status.Archived, StatusName = Status.Archived.ToString() });
            s.Add(new StatusDD { StatusId = (int)Status.Draft, StatusName = Status.Draft.ToString() });
            s.Add(new StatusDD { StatusId = (int)Status.Deleted, StatusName = Status.Deleted.ToString() });
            s.Add(new StatusDD { StatusId = (int)Status.PrePayment, StatusName = Status.PrePayment.ToString() });
            s.Add(new StatusDD { StatusId = (int)Status.Expired, StatusName = Status.Expired.ToString() });
            var x = 1;
            return s;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">status, onhold, reviewed</param>
        /// <param name="hasReviewed"></param>
        /// <param name="holdMessage"></param>
        /// <returns></returns>
        private string GetText(string type, bool hasReviewed, bool holdMessage)
        {
            string status = string.Empty;
            string onhold = string.Empty;
            string reviewed = string.Empty;

            if (hasReviewed)
            {
                reviewed = "Yes";
                onhold = "No";
                status = "Message sent";
            }
            else
            {
                reviewed = "No";
                status = "Awaiting review";

                if (holdMessage)
                {
                    onhold = "Yes";
                    status = "<font color=\"red\"><strong>Release message</strong></font>";
                    //linkAction.NavigateUrl = String.Format(siteurl + "admin/members/generateemail.aspx?mid={0}&emid={1}&eid={2}", ((HomeEnquiry)e.Item.DataItem).SenderMemberID, ((HomeEnquiry)e.Item.DataItem).ID, ((int)EmailType.MessageReleased).ToString());
                }
            }

            if (type == "status")
                return status;
            else if (type == "onhold")
                return onhold;
            else if (type == "reviewed")
                return reviewed;
            else
                return string.Empty;

        }
    }

    public class SentHomeEnquiry
    {
        public string Status { get; set; }
        public string ReferenceNo { get; set; }
        public string Date { get; set; }
        public string Message { get; set; }
        public string ReviewedByRecipient { get; set; }
        public string OnHold { get; set; }
    }
}
