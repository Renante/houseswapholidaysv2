﻿using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HouseSwapHolidays.Core.Controllers
{
    public class Page404Controller : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<Page404ViewModel>(CurrentPage);


            return CurrentTemplate(model);
        }
    }
}
