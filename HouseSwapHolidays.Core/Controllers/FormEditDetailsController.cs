﻿using HouseSwapHolidays.Core.Models.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Common;
using HouseSwapHolidays.Core.Services;
using Umbraco.Web.Mvc;
using HouseSwapHolidays.Core.Models.Utilities;

namespace HouseSwapHolidays.Core.Controllers
{
    public class FormEditDetailsController : SurfaceController
    {
        public ActionResult Save(MemberEdit me)
        {

            // Validations here
            if (!ModelState.IsValid)
            {
                StringBuilder str = new StringBuilder();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                    foreach (ModelError error in modelState.Errors)
                        str.Append(error.ErrorMessage + ", ");

                return Content(str.ToString());
            }
            // End Validation

            var currentMemberID = MemberService.Instance.GetCurrentMemberId();


            Member member = MemberDAL.Instance.Get(currentMemberID);

            member.FirstName = me.FirstName;
            member.GivenName = me.GivenName;

            var oldEmailAddress = member.EmailAddress;
            member.EmailAddress = me.EmailAddress;
            var newEmailAddress = member.EmailAddress;

            var changePassword = !string.IsNullOrEmpty(me.Password);
            if (changePassword)
            {
                var salt = PasswordService.Instance.GenerateRandomSalt();
                var hashedpw = PasswordManager.Instance.GenerateHashedPassword(me.Password, salt);

                member.Salt = salt;
                member.HashedPassword = hashedpw;
            }

            member.Introduction = me.Introduction;
            member.Language1 = me.Language1;
            member.Language2 = me.Language2;
            member.Language3 = me.Language3;

            member.ReceiveNewsletter = me.ReceiveNewsletter == "on";
            member.AcceptedTerms = me.AcceptTerms == "on";

            MemberDAL.Instance.Save(member);

            if (me.ReceiveNewsletter == "on")
            {
                CampaignMonitorService.Instance.Subscribe(member.EmailAddress);
            }
            else
            {
                CampaignMonitorService.Instance.UnSubscribe(member.EmailAddress);
            }

            // unsubscribe old email if member changed email
            if (oldEmailAddress != newEmailAddress)
            {
                CampaignMonitorService.Instance.UnSubscribe(oldEmailAddress);
            }

            return RedirectToUmbracoPage(Constants.NodeId.MyProfile);
        }

    }
}
