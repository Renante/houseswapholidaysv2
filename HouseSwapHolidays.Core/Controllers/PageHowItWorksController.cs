﻿using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using HouseSwapHolidays.Core.Utilities;
using HouseSwapHolidays.Core.Models.Archetypes;

namespace HouseSwapHolidays.Core.Controllers
{
    public class PageHowItWorksController : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<PageHowItWorksViewModel>(CurrentPage);

            model.IntroBanner = CurrentPage.GetPropertyAsArchetypeObjectList<ImageTitleRTE>("introBanner").FirstOrDefault();
            model.Content = CurrentPage.GetPropertyAsArchetypeObjectList<HowItWorksContent>("content").FirstOrDefault();
            model.Testimonial = CurrentPage.GetPropertyAsArchetypeObjectList<Testimonial>("testimonial").FirstOrDefault();

            return CurrentTemplate(model);
        }
    }
}
