﻿using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.ViewModels;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HouseSwapHolidays.Core.Controllers
{
    [Authorize]
    public class PageMessageHistoryController : BasePageController
    {
        public ActionResult Index()
        {
            var reqId = Request.QueryString["id"];
            if (reqId == null) return Content("A parameter id is required");

            var model = GetModel<PageMessageHistoryViewModel>(CurrentPage);

            var memberId = HouseSwapHolidays.Core.Services.MemberService.Instance.GetCurrentMemberId();
            var senderId = int.Parse(reqId);

            model.HomeEnquiryCollection = HomeEnquiryDAL.Instance.GetAllForMemberSender(memberId, senderId);

            return CurrentTemplate(model);
        }
    }
}
