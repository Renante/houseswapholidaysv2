﻿using HouseSwapHolidays.Core.Attributes;
using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.ViewModels;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace HouseSwapHolidays.Core.Controllers
{
    [Admin]
    public class PageAdminHomeEditController : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<PageAdminHomeEditViewModel>(CurrentPage);

            if(Request.QueryString["mid"] == null || Request.QueryString["hid"] == null )
                return Content("Please provide parameter mid and hid to view this page");

            var memberID = Request.QueryString["mid"] != null ? int.Parse(Request.QueryString["mid"]) : 0;
            var homeID = Request.QueryString["hid"] != null ? int.Parse(Request.QueryString["hid"]) : 0;

            if(memberID > 0 && homeID > 0){
                HomeAdmin homeAdmin = HomeDAL.Instance.GetAdminDetails(homeID);
                Channel channel = ChannelDAL.Instance.Get(1);
                Channel ch2 = ChannelDAL.Instance.Get(1); // chs ID
                
                if (homeAdmin != null)
				{

					model.HomeID = homeAdmin.ID;
					model.MemberID = homeAdmin.MemberID;
					model.City = homeAdmin.City;
					model.Location = homeAdmin.Location;
					model.PostCode = homeAdmin.Postcode;
					model.Suburb = homeAdmin.Suburb;
					model.PriorityRank = homeAdmin.PriorityRank;
					model.Status = (Status)homeAdmin.Status;
					model.Longitude = homeAdmin.Longitude;
					model.Lattitude = homeAdmin.Latitude;
	
				}

            }

            return CurrentTemplate(model);
        }
    }

    public class FormAdminHomeEditController : SurfaceController
    {
        public ActionResult SaveHomeDetails()
        {
            var homeid = int.Parse(Request.Form["homeid"]);
            var memberid = int.Parse(Request.Form["memberid"]);
            var priorityrank = Request.Form["priorityrank"];
            var city = Request.Form["city"];
            var location = Request.Form["location"];
            var suburb = Request.Form["suburb"];
            var postcode = Request.Form["postcode"];

            HomeAdmin homeAdmin = HomeDAL.Instance.GetAdminDetails(homeid);
            homeAdmin.MemberID = memberid;
            homeAdmin.City = city;
            homeAdmin.Location = location;
            homeAdmin.PriorityRank = int.Parse(priorityrank);
            homeAdmin.Suburb = suburb;
            homeAdmin.Postcode = postcode;

            HomeDAL.Instance.SaveAdminDetails(homeAdmin);

            return RedirectToCurrentUmbracoUrl();
        }
    }
}
