﻿using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.Forms;
using HouseSwapHolidays.Core.Models.Utilities;
using HouseSwapHolidays.Core.Models.ViewModels;
using HouseSwapHolidays.Core.Services;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Core.Logging;

namespace HouseSwapHolidays.Core.Controllers
{
    [Authorize]
    public class PageEditDetailsController : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<PageEditDetailsViewModel>(CurrentPage);

            var user = MemberDAL.Instance.Get(MemberService.Instance.GetCurrentMemberId());

            model.FirstName = user.FirstName;
            model.GivenName = user.GivenName;
            model.EmailAddress = user.EmailAddress;

            model.Introduction = user.Introduction;
            model.Language1 = user.Language1;
            model.Language2 = user.Language2;
            model.Language3 = user.Language3;

            model.ReceiveNewsletter = user.ReceiveNewsletter;
            model.AcceptTerms = user.AcceptedTerms;
            model.Languages = GetLanguages();

            return CurrentTemplate(model);
        }

    }
}
