﻿using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Common;
using HouseSwapHolidays.Core.Models.DataAccess;
using HouseSwapHolidays.Core.Attributes;
using Umbraco.Web;

namespace HouseSwapHolidays.Core.Controllers
{
    [MemberOnly(Status.Draft, Status.Active)]
    public class PageStep3Controller : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<PageStep3ViewModel>(CurrentPage);
            model.Title = CurrentPage.GetPropertyValue<string>("title");
            model.SubTitle = CurrentPage.GetPropertyValue<string>("subTitle");
            model.Summary = CurrentPage.GetPropertyValue<string>("summary");

            var currentMemberId = HouseSwapHolidays.Core.Services.MemberService.Instance.GetCurrentMemberId();
            model.Member = MemberDAL.Instance.Get(currentMemberId);

            var countryCollection = CountryDAL.Instance.GetAllByDefault();
            model.CountryCollection = countryCollection;

            var memberDestinationCollection = MemberDestinationDAL.Instance.GetAllForMember(currentMemberId);
            model.MemberDestinationCollection = memberDestinationCollection;

            var homeId = -1;
            var homeSumCharsLeft = 150;
            Home home = null;
            model.HasHome = false;
            

            var requestedId = Request.QueryString["id"];

            if (requestedId != null)
            {
                homeId = int.Parse(requestedId);
                home = HomeDAL.Instance.Get(homeId);

                model.Home = home;
                model.HasHome = true;

                HomeImageCollection homeImageCollection = HomeImageDAL.Instance.GetAllForHome(homeId);
                if (homeImageCollection != null)
                {
                    model.HomeImageCollection = homeImageCollection;
                    model.HomeHasImages = true;
                }

            }
            else
            {
                HomeCollection homeCollection = HomeDAL.Instance.GetByMemberID(currentMemberId);
                if (homeCollection.Count > 0)
                {
                    home = homeCollection[0];
                    homeId = home.ID;
                    model.Home = home;
                    model.HasHome = true;

                    HomeImageCollection homeImageCollection = HomeImageDAL.Instance.GetAllForHome(homeId);
                    if (homeImageCollection != null)
                    {
                        model.HomeImageCollection = homeImageCollection;
                        model.HomeHasImages = true;
                    }
                }
            }

            if (model.HasHome)
            {
                homeSumCharsLeft = homeSumCharsLeft - home.Summary.Count();
            }
            
            model.HomeSummaryCharsLeft = homeSumCharsLeft;

            // homefeatures
            List<HouseSwapHolidays.Core.Models.Items.HomeFeaturesWithSelectedValue> results;
            using (var context = new RLK797_HomeExchangeEntities())
            {
                string sql = @"SELECT        f.FeatureID FeatureID, f.FeatureTypeID FeatureTypeID, f.Name Name, CAST((CASE WHEN hf.HomeID IS NULL THEN 0 ELSE 1 END) AS bit) AS IsHomeFeature
                                FROM            Feature AS f LEFT OUTER JOIN
                                                         HomeFeatures AS hf ON f.FeatureID = hf.FeatureID AND hf.HomeID = {0}
                                ORDER BY f.FeatureTypeID";
                results = context.Database.SqlQuery<HouseSwapHolidays.Core.Models.Items.HomeFeaturesWithSelectedValue>(sql, homeId).ToList();
            }

            model.HomeFacilities = results.Where(w => w.FeatureTypeID == (int)FeatureType.HomeFacilities).ToList();
            model.HomeActivities = results.Where(w => w.FeatureTypeID == (int)FeatureType.HomeActivities).ToList();
            model.HomeRules = results.Where(w => w.FeatureTypeID == (int)FeatureType.HomeRules).ToList();
            model.HolidayToOffer = results.Where(w => w.FeatureTypeID == (int)FeatureType.HolidayTypesToOffer).ToList();
            model.SwapPref = results.Where(w => w.FeatureTypeID == (int)FeatureType.SwapTimePreferences).ToList();
            model.SwapPref.AddRange(results.Where(w => w.FeatureTypeID == (int)FeatureType.SwapLength).ToList());

            if (home != null)
            {
                if (!string.IsNullOrEmpty(home.CountryCode))
                {
                    var regionCollection = RegionDAL.Instance.GetByCountryCode(home.CountryCode);
                    model.HasRegions = regionCollection != null;
                    model.RegionCollection = regionCollection;
                }
            }

            model.StepsNavigation = GetStepsNavigation();
            model.HasStepsNavigation = model.StepsNavigation != null;

            return CurrentTemplate(model);
        }
    }
}
