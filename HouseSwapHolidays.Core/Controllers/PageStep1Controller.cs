﻿using HouseSwapHolidays.Core.Attributes;
using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.ViewModels;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Web;

namespace HouseSwapHolidays.Core.Controllers
{
    [MemberOnly(Status.Draft, Status.Active)]
    public class PageStep1Controller : BasePageController
    {
        public ActionResult Index()
        {

            var model = GetModel<PageStep1ViewModel>(CurrentPage);
            model.Title = CurrentPage.GetPropertyValue<string>("title");
            model.SubTitle = CurrentPage.GetPropertyValue<string>("subTitle");
            model.Summary = CurrentPage.GetPropertyValue<string>("summary");

            var currentMemberId = HouseSwapHolidays.Core.Services.MemberService.Instance.GetCurrentMemberId();
            var member = MemberDAL.Instance.Get(currentMemberId);
            model.Member = member;
            model.Languages = GetLanguages();
            model.StepsNavigation = GetStepsNavigation();
            model.HasStepsNavigation = model.StepsNavigation != null;

            return CurrentTemplate(model);
        }

    }
}
