﻿using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Common;
using HouseSwapHolidays.Core.Attributes;
using Umbraco.Web;

namespace HouseSwapHolidays.Core.Controllers
{
    [MemberOnly(Status.Draft, Status.Active)]
    public class PageStep2Controller : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<PageStep2ViewModel>(CurrentPage);
            model.Title = CurrentPage.GetPropertyValue<string>("title");
            model.SubTitle = CurrentPage.GetPropertyValue<string>("subTitle");
            model.Summary = CurrentPage.GetPropertyValue<string>("summary");

            var currentMemberId = HouseSwapHolidays.Core.Services.MemberService.Instance.GetCurrentMemberId();
            model.Member = MemberDAL.Instance.Get(currentMemberId);

            var countryCollection = CountryDAL.Instance.GetAllByDefault();
            model.CountryCollection = countryCollection;

            var memberDestinationCollection = MemberDestinationDAL.Instance.GetAllForMember(currentMemberId);
            model.MemberDestinationCollection = memberDestinationCollection;

            model.StepsNavigation = GetStepsNavigation();
            model.HasStepsNavigation = model.StepsNavigation != null;

            return CurrentTemplate(model);
        }
    }
}
