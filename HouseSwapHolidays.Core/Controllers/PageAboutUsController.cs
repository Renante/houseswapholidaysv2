﻿using Archetype.Models;
using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.Items;
using HouseSwapHolidays.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Web;

namespace HouseSwapHolidays.Core.Controllers
{
    
    public class PageAboutUsController : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<PageAboutUsViewModel>(CurrentPage);

            var entryContent = CurrentPage.GetPropertyValue<ArchetypeModel>("entryContent").FirstOrDefault();

            model.HasEntryContent = entryContent != null;

            if (model.HasEntryContent)
            {
                model.EntryContent = new EntryContent
                {
                    ImageUrl = entryContent.GetValue("image").GetPropertyAsMediaUrl(),
                    HasImage = entryContent.HasValue("image"),
                    Title = entryContent.GetValue<string>("title"),
                    Content = entryContent.GetValue<string>("content").ToLineBreaksHtml()
                };
            }


            var formContent = CurrentPage.GetPropertyValue<ArchetypeModel>("formContent").FirstOrDefault();

            model.HasFormContent = formContent != null;

            if (model.HasFormContent)
            {
                model.FormContent = new FormContent
                {
                    Title = formContent.GetValue<string>("title"),
                    Content = formContent.GetValue<string>("content").ToLineBreaksHtml(),
                    Quote = formContent.GetValue<string>("quote").ToLineBreaksHtml(),
                    HasQuote = formContent.HasValue("quote"),
                    Author = formContent.GetValue<string>("author"),
                    Signature = formContent.GetValue<string>("signature")
                };
            }

            return CurrentTemplate(model);
        }
    }
}
