﻿using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HouseSwapHolidays.Core.Controllers
{
    public class PageMessageController : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<PageMessageViewModel>(CurrentPage);

            model.Title = Request.QueryString["t"];
            model.Message = Request.QueryString["m"];


            return CurrentTemplate(model);
        }
    }
}
