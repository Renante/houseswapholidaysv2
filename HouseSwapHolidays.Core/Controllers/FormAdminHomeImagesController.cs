﻿using Common;
using HouseSwapHolidays.Core.Models.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace HouseSwapHolidays.Core.Controllers
{
    public class FormAdminHomeImagesController : SurfaceController
    {
        [HttpPost]
        public ActionResult UpdateFeaturedHome(HomeFeaturedImage h)
        {
            HomeImageDAL.Instance.SetDefaultImage(h.HomeID, h.FeaturedImageID);
            return RedirectToCurrentUmbracoUrl();
        }
    }
}
