﻿using HouseSwapHolidays.Core.Attributes;
using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.ViewModels;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HouseSwapHolidays.Core.Controllers
{
    [Admin]
    public class PageAdminSnapshotController : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<PageAdminSnapshotViewModel>(CurrentPage);

            int hoursBack;
            if (Request.QueryString["h"] != null)
            {
                hoursBack = Int32.Parse(Request.QueryString["h"]);
            }
            else
            {
                hoursBack = 12;
            }

            // Display recent members
            MemberCollection memberCollection = MemberDAL.Instance.GetNew(hoursBack);
            
            model.MemberCollection = memberCollection.Cast<Member>().ToList();
            
            
            // Display recent homes		
            HomeCollection homeCollection = HomeDAL.Instance.GetNew(hoursBack);
            
            model.HomeCollection = homeCollection.Cast<Home>().ToList();
            
            

            // Display recent home enquiries
            HomeEnquiryCollection homeEnquiryCollection = HomeEnquiryDAL.Instance.GetNew(hoursBack);
            
            model.HomeEnquiryCollection = homeEnquiryCollection.Cast<HomeEnquiry>().ToList();
            
            

            return CurrentTemplate(model);
        }

        public string Test()
        {
            return "test";
        }
    }

    
}
