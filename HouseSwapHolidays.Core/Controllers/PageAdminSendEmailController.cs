﻿using HouseSwapHolidays.Core.Attributes;
using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.ViewModels;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HouseSwapHolidays.Core.Controllers
{
    [Admin]
    public class PageAdminSendEmailController : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<PageAdminSendEmailViewModel>(CurrentPage);

            if(Request.QueryString["id"] == null)
                return Content("Please provide parameter id to view this page");

            var memberid = Request.QueryString["id"] != null ? int.Parse(Request.QueryString["id"]) : 0;
            var templateid = Request.QueryString["tid"] != null ? int.Parse(Request.QueryString["tid"]) : 0;

            model.MemberID = memberid;

            var emailTemplateCollection = EmailTemplateDAL.Instance.GetAll(-1).Cast<EmailTemplate>().ToList();
            model.EmailTemplateCollection = emailTemplateCollection;

            if (memberid > 0 && templateid > 0)
            {
                Member member = MemberDAL.Instance.Get(memberid);
                Channel channel = ChannelDAL.Instance.Get(member.DefaultChannel);
                EmailTemplate emailTemplate = EmailTemplateDAL.Instance.Get(templateid, -1);

                string emailBody = "";

                model.EmailSubject = emailTemplate.Subject.Replace("%%FirstName%%", member.FirstName);

                emailBody = emailTemplate.Body.Replace("%%FirstName%%", member.FirstName);
                emailBody = emailBody.Replace("%%GivenName%%", member.GivenName);
                emailBody = emailBody.Replace("%%FromEmail%%", String.Format("info@{0}", channel.ShortURL.ToLower()));
                emailBody = emailBody.Replace("%%WebsiteName%%", channel.Name);
                emailBody = emailBody.Replace("%%websiteurl%%", channel.Name);
                
                model.EmailBody = emailBody;
            }
            else
            {
                if (memberid > 0)
                {
                    Member member = MemberDAL.Instance.Get(memberid);
                    Channel channel = ChannelDAL.Instance.Get(member.DefaultChannel);
                    model.EmailSubject = String.Format("Important message to {0} from {1}", member.FirstName, channel.Name);

                    model.EmailBody = String.Format("<p>Hi {0},</p><br/><p>Warm regards,</p>Customer Service<br/>{1}", member.FirstName, channel.Name);
                }
            }


            return CurrentTemplate(model);
        }
    }
}
