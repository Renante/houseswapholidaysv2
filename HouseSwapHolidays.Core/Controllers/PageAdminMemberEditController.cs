﻿using HouseSwapHolidays.Core.Attributes;
using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.Utilities;
using HouseSwapHolidays.Core.Models.ViewModels;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using HouseSwapHolidays.Core.Services;
using Umbraco.Web.Mvc;

namespace HouseSwapHolidays.Core.Controllers
{
    [Admin]
    public class PageAdminMemberEditController : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<PageAdminMemberEditViewModel>(CurrentPage);

            if(Request.QueryString["id"] == null)
                return Content("Please provide parameter id to view this page");

            var memberID = int.Parse(Request.QueryString["id"]);

            Member member = new Member();

            member = MemberDAL.Instance.Get(memberID);

            if (member != null)
            {
                model.MemberID = member.ID;
                model.FirstName = member.FirstName;
                model.GivenName = member.GivenName;
                model.EmailAddress = member.EmailAddress;
                model.Introduction = member.Introduction;
                model.CreatedDate = member.CreatedDate.ToString("dd-MMM-yy");
                model.LastUpdated = member.UpdatedDate.ToString("dd-MMM-yy");
                model.LastLoggedIn = member.LastLoggedInDate.ToString("dd-MMM-yy");
                model.MemberStatus = (int)member.Status;
                model.IsMultiChannel = member.IsMultiChannel;

                Channel channel = ChannelDAL.Instance.Get(member.DefaultChannel);

                model.Channel = channel != null ? channel.Name : "<font color=\"red\">Unknown</font>";
                
            }

            HomeEnquiryCollection homeEnquiryCollection = HomeEnquiryDAL.Instance.GetAllHomeEnquiries(memberID, -1, false, 365);
            model.HomeEnquiryCollection = homeEnquiryCollection.Cast<HomeEnquiry>().ToList();
            
            model.StatusDropdown = GetMemberStatus();

            var memberDestinations = MemberDestinationDAL.Instance.GetAllForMember(member.ID).Cast<MemberDestination>().ToList();
            model.MemberDestinations = memberDestinations;

            var countries = CountryDAL.Instance.GetAll().Cast<Country>().ToList();
            model.Countries = countries;


            return CurrentTemplate(model);
        }
        
        public List<StatusDD> GetMemberStatus()
        {
            List<StatusDD> s = new List<StatusDD>();
            s.Add(new StatusDD { StatusId = (int)Status.Active, StatusName = Status.Active.ToString() });
            s.Add(new StatusDD { StatusId = (int)Status.PendingApproval, StatusName = Status.PendingApproval.ToString() });
            s.Add(new StatusDD { StatusId = (int)Status.Archived, StatusName = Status.Archived.ToString() });
            s.Add(new StatusDD { StatusId = (int)Status.Draft, StatusName = Status.Draft.ToString() });
            s.Add(new StatusDD { StatusId = (int)Status.Deleted, StatusName = Status.Deleted.ToString() });

            return s;
        }
    }

    public class FormAdminMemberEditController : SurfaceController
    {
        [HttpPost]
        public ActionResult SaveMemberDetail()
        {
            var memberID = int.Parse(Request.Form["id"]);
            var firstname = Request.Form["firstname"];
            var givenname = Request.Form["givenname"];
            var emailaddress = Request.Form["emailaddress"];
            var status = int.Parse(Request.Form["status"]);
            var introduction = Request.Form["introduction"];
            var ismultichannel = Request.Form["ismultichannel"] == "on";

            Member member = MemberDAL.Instance.Get(memberID);
            member.Status = (Status)status;
            member.IsMultiChannel = ismultichannel;
            member.Introduction = introduction;
            member.FirstName = firstname;
            member.GivenName = givenname;
            var oldEmailAddress = member.EmailAddress;
            member.EmailAddress = emailaddress;
            var newEmailAddress = member.EmailAddress;

            MemberDAL.Instance.Save(member);

            // update info
            if (member.ReceiveNewsletter)
                CampaignMonitorService.Instance.Subscribe(member.EmailAddress);

            // unsubscribe old email address if user changed email address
            if (oldEmailAddress != newEmailAddress)
                CampaignMonitorService.Instance.UnSubscribe(oldEmailAddress);

            return RedirectToCurrentUmbracoUrl();
        }

        [HttpPost]
        public ActionResult SaveMemberDestination()
        {
            var memberID = int.Parse(Request.QueryString["id"]);
            var destinationid = int.Parse(Request.Form["destinationid"]);
            var countrycode = Request.Form["countrycode"];
            var destination = Request.Form["destination"];

            if (destinationid > 0) // you are editing existing record
            {
                if (!string.IsNullOrEmpty(countrycode)) // you are just editing
                {
                    MemberDestination dest = MemberDestinationDAL.Instance.Get(memberID, destinationid);
                    if (dest != null)
                    {
                        dest.CountryCode = countrycode;
                        dest.Destination = destination;
                        MemberDestinationDAL.Instance.Save(dest);
                    }
                }
                else // you are deleting
                {
                    MemberDestinationDAL.Instance.Delete(memberID, destinationid);
                }
            }
            else // you are trying to add new record
            {
                if (!string.IsNullOrEmpty(countrycode)) // check if you really adding new record
                {
                    MemberDestination m = new MemberDestination();
                    m.ID = 0;
                    m.CountryCode = countrycode;
                    m.RegionCode = "";
                    m.Destination = destination;
                    m.MemberID = memberID;

                    MemberDestinationDAL.Instance.Save(m);
                }
            }

            return RedirectToCurrentUmbracoUrl();
        }
    }
}
