﻿using HouseSwapHolidays.Core.Controllers.Base;
using HouseSwapHolidays.Core.Models.Utilities;
using HouseSwapHolidays.Core.Models.ViewModels;
using HouseSwapHolidays.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Core.Logging;

namespace HouseSwapHolidays.Core.Controllers
{
    [Authorize]
    public class PageMyProfileController : BasePageController
    {
        public ActionResult Index()
        {
            
            var model = GetModel<PageMyProfileViewModel>(CurrentPage);
            var member = MemberService.Instance.GetCurrentMember();
            var home = HomeService.Instance.GetByMemberId(member.Id);

            if (member.Status == (int) Common.Status.PrePayment)
                return Redirect(Umbraco.Url(Constants.NodeId.Activate));
                
            //return RedirectToUmbracoPage(Constants.NodeId.Activate);

            model.MemberIsActive = member.Status == (int)Common.Status.Active;
            model.HasHome = home != null;
            if (model.HasHome) { 
                model.HomeId = home.HomeID;
                var featuredThumbImg = HomeService.Instance.GetFeaturedThumbnailImage(home.HomeID);
                model.HomeImageUrl = featuredThumbImg != null ? featuredThumbImg.LargeImagePath : string.Empty;
                model.HomeDescription = home.Description;
            }
            model.UnreadMessage = member.UnreadMessage;
            model.FavoritesHomes = HomeService.Instance.GetFavoriteHomes(member.Id);
            model.MemberDestinations = MemberService.Instance.GetMemberDestinations(member.Id).ToList();

            return CurrentTemplate(model);
        }
    }
}
