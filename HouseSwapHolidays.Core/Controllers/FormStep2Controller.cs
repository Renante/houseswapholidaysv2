﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace HouseSwapHolidays.Core.Controllers
{
    public class FormStep2Controller : SurfaceController
    {
        public ActionResult AddDestination()
        {
            var currentMemberId = HouseSwapHolidays.Core.Services.MemberService.Instance.GetCurrentMemberId();

            var country = Request.Form["country"];
            var city = Request.Form["city"];

            if (country != "0")
            {
                MemberDestination m = new MemberDestination();
                m.MemberID = currentMemberId;
                m.CountryCode = country;
                m.Destination = !string.IsNullOrEmpty(city) ? city : string.Empty;
                m.RegionCode = string.Empty;
                MemberDestinationDAL.Instance.Save(m);
            }

            return RedirectToCurrentUmbracoPage();
        }

        public ActionResult DeleteDestination()
        {
            var currentMemberId = HouseSwapHolidays.Core.Services.MemberService.Instance.GetCurrentMemberId();
            var id = int.Parse(Request.Form["destinationId"]);
            MemberDestinationDAL.Instance.Delete(currentMemberId, id);

            return RedirectToCurrentUmbracoPage();
        }
    }
}
