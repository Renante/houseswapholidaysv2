﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using Common;

namespace HouseSwapHolidays.Core.Controllers
{
    public class FormAdminSendEmailController : SurfaceController
    {
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SendEmail()
        {
            var memberID = int.Parse(Request.Form["memberid"]);
            var emailbody = Request.Form["emailbody"];
            var emailsubject = Request.Form["emailsubject"];

            if (memberID > 0)
            {
                Member member = MemberDAL.Instance.Get(memberID);
                Channel channel = ChannelDAL.Instance.Get(member.DefaultChannel);

                string bodyText = "";

                bodyText = emailbody;

                Common.GenerateEmail.Instance.SendEmail(String.Format("{0} {1}", member.FirstName, member.GivenName), member.EmailAddress, String.Format("{0} Support", channel.Name), String.Format("info@{0}", channel.ShortURL), emailsubject, bodyText, channel.ID);

            }
            else
            {
                TempData["msg"] = "Missing information to send";
            }

            return RedirectToCurrentUmbracoUrl();

        }
    }
}
