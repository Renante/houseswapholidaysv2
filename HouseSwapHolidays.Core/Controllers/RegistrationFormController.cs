﻿using HouseSwapHolidays.Core.Models.Forms;
using HouseSwapHolidays.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace HouseSwapHolidays.Core.Controllers
{
    public class RegistrationFormController : SurfaceController
    {
        [HttpPost]
        public ActionResult Register(JoinUsForm form)
        {
            if (!ModelState.IsValid)
                return CurrentUmbracoPage();

            var acceptTerms = form.AcceptTermsAndCondition == "on";
            var emailExistAlready = MemberService.Instance.IsEmailExist(form.EmailAddress);

            if (!acceptTerms || emailExistAlready)
                return CurrentUmbracoPage();

            // building template
            //var body = new StringBuilder();
            //body.AppendFormat("<strong>First name</strong> :");
            //body.AppendFormat("<strong>Last name</strong> :");
            //body.AppendFormat("<strong>Email Address</strong> :");
            //body.AppendFormat("<strong>Password</strong> :");
            //body.AppendFormat("<strong>Address Line 1</strong> :");
            //body.AppendFormat("<strong>Address Line 2</strong> :");
            //body.AppendFormat("<strong>City</strong> :");
            //body.AppendFormat("<strong>Zip</strong> :");
            //body.AppendFormat("<strong>State</strong> :");
            //body.AppendFormat("<strong></strong> :");
            //body.AppendFormat("<strong></strong> :");

            MemberService.Instance.RegisterAndLogin(form);

            return RedirectToCurrentUmbracoPage();
        }
    }
}
